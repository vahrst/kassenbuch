package de.diakon.persistence;

import java.sql.Connection;
import java.sql.SQLException;
/**
 * Zentrale Steuerung von Persistence-Diensten. Hier wird
 * z.B. gepr�ft, ob alle Tabellen existieren und diese
 * im Bedarfsfall angelegt. Au�erdem dient diese Klasse zur
 * einfachen Bereitstellung einer JDBC-Connection. 
 * @author Thomas
 * @version 
 */
public interface IPersistenceManager {


	/**
	 * startet eine neue Transaction. Wenn bereits eine aktiv ist, wird
	 * eine PersistenceException geworfen
	 */
	public void startTransaction() throws PersistenceException;
		
	/** 
	 * beendet eine laufende Transaktion mit commit. Wirft PersistenceException,
	 * wenn keine Transaktion aktiv ist.
	 */
	public void commitTransaction() throws PersistenceException;
					
	/** 
	 * beendet eine laufende Transaktion mit rollback. Wirft PersistenceException,
	 * wenn keine Transaktion aktiv ist.
	 */
	public void rollbackTransaction() throws PersistenceException;
	
	
	
	/**
	 * pr�ft, ob alle Tabellen angelegt sind. Wenn das nicht
	 * der Fall ist, werden die Tabellen erzeugt
	 */
	public void checkPersistenceLayer()throws PersistenceException;
	
	
	/**
	 * liefert eine JDBC-Connection f�r den Zugriff
	 * auf das DB-System.
	 */
	public Connection getConnection() throws SQLException;
	
	
	/**
	 * schlie�t eine evlt. noch offene Connection
	 */
	public void close();
	
}
