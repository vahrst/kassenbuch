package de.diakon.persistence;

/**
 * Factory-Klasse zur Beschaffung des richten Persistence-Managers.
 *
 */
public class PersistenceManager {
	private static IPersistenceManager pm = null;
	
	public static IPersistenceManager getPersistenceManager(){
		if(pm == null){
			createPersistenceManager();
		}
		return pm;
	}
	
	private static void createPersistenceManager(){
		pm = new PersistenceManagerIDB();
	}
}
