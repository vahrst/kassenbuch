package de.diakon.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import de.diakon.entities.BuchungsperiodeHome;
import de.diakon.entities.BuchungssatzHome;
import de.diakon.entities.KasseHome;
import de.diakon.entities.KontoHome;
import de.diakon.entities.KostenstelleHome;
import de.diakon.global.Konstanten;
import de.diakon.main.PropertiesManager;
/**
 * Zentrale Steuerung von Persistence-Diensten. Hier wird
 * z.B. gepr�ft, ob alle Tabellen existieren und diese
 * im Bedarfsfall angelegt. Au�erdem dient diese Klasse zur
 * einfachen Bereitstellung einer JDBC-Connection. 
 * @author Thomas
 * @version 
 */
public class PersistenceManagerIDB extends AbstractPersistenceManager implements IPersistenceManager{
	static Logger logger = Logger.getLogger(PersistenceManagerIDB.class);

	static int CONNECTION_ACCESS_MAXIMUM = 20;
	
	private static String dbUrl;
	private static Connection connection = null;
	private static int connectionAccessCounter = 0;

	private static Connection transactionConnection = null;

	
	static{
		try{
			Class.forName("com.lutris.instantdb.jdbc.idbDriver");
		}catch(ClassNotFoundException e){
			logger.fatal("JDBC-Driver (f�r Datenbank-Zugriff) nicht gefunden", e);
			System.exit(1);
		}
	}			

	/**
	 * Constructor for PersistenceManager.
	 */
	public PersistenceManagerIDB() {
		super();
	}


	/**
	 * startet eine neue Transaction. Wenn bereits eine aktiv ist, wird
	 * eine PersistenceException geworfen
	 */
	public  void startTransaction() throws PersistenceException{
		if(transactionConnection != null){
			throw new PersistenceException("Es ist bereits eine Transaktion aktiv");
		}
		try{
			transactionConnection = DriverManager.getConnection(getDBUrl(), "sa", "");		
			transactionConnection.setAutoCommit(false);
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}
		
	/** 
	 * beendet eine laufende Transaktion mit commit. Wirft PersistenceException,
	 * wenn keine Transaktion aktiv ist.
	 */
	public  void commitTransaction() throws PersistenceException{
		if(transactionConnection == null){
			throw new PersistenceException("Commit nicht m�glich, keine Transaktion aktiv");
		}
		try{
			transactionConnection.commit();
			transactionConnection.close();
			transactionConnection = null;
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}
					
	/** 
	 * beendet eine laufende Transaktion mit rollback. Wirft PersistenceException,
	 * wenn keine Transaktion aktiv ist.
	 */
	public  void rollbackTransaction() throws PersistenceException{
		if(transactionConnection == null){
			throw new PersistenceException("Rollback nicht m�glich, keine Transaktion aktiv");
		}
		try{
			transactionConnection.rollback();
			transactionConnection.close();
			transactionConnection = null;
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}
					
	
	
	
	/**
	 * pr�ft, ob alle Tabellen angelegt sind. Wenn das nicht
	 * der Fall ist, werden die Tabellen erzeugt
	 */
	public  void checkPersistenceLayer()throws PersistenceException{
		logger.debug("check PersistenceLayer");
		try{
			Connection conn = getConnection();
		
			Set<String> tablenames = new HashSet<String>();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select tablename from diakon$db$tables");
			while(rs.next()){
				tablenames.add(rs.getString(1));
			}					
			stmt.close();
	
			if(!tablenames.contains(KontoHome.TABLENAME)){
				createKontenTabelle(conn);
			}else{
				updateKontenTabelle(conn);
			}
				
			if(!tablenames.contains(KostenstelleHome.TABLENAME))
				createKostenstellenTabelle(conn);
		
			if(!tablenames.contains(KasseHome.TABLENAME)){
				createKassenTabelle(conn);
			}else{
				new KasseHome().updateKassenTabelle(conn);
			}

			if(!tablenames.contains(BuchungsperiodeHome.TABLENAME))
				createBuchungsperiodenTabelle(conn);

			if(!tablenames.contains(BuchungssatzHome.TABLENAME))
				createBuchungsTabelle(conn);

		}catch(SQLException e){
			throw new PersistenceException(e);
		}
		
		logger.debug("checked.");
	}
	
	
	/**
	 * liefert eine JDBC-Connection f�r den Zugriff
	 * auf das DB-System.
	 */
	public  Connection getJDBCConnection() throws SQLException{
		logger.debug("getConnection");

		// wenn ein Transactionskontext aktiv ist, liefern wir
		// die zugeh�rige Connection heraus...
		if(transactionConnection != null){
			if(!transactionConnection.isClosed()){
				return transactionConnection;
			}else{
				transactionConnection = null;
			}
		}

		// ... ansonsten die 'normale' Connection
		// wenn eine existiert, jedoch der Counter den Max-Wert erreicht hat:
		if(connection != null && connectionAccessCounter > CONNECTION_ACCESS_MAXIMUM){
			logger.info("Max. Anzahl Zugriffe f�r Connection erreicht. Connection wird geschlossen");
			try{
				connection.close();
			}catch(Exception e){
				logger.error("Fehler beim Schie�en einer Connection", e);
			}
			connection = null;
		}
		
		if(connection == null || connection.isClosed()){
			connection = DriverManager.getConnection(getDBUrl(), "sa", "");
			connection.setAutoCommit(true);
			connectionAccessCounter = 0;
		}		
			
		connectionAccessCounter++;
		return connection;
	
	}
	
	/**
	 * liefert die URL f�r den Datenbank-Zugriff
	 */
	private  String getDBUrl(){
		if(dbUrl == null){
			String dbFile = PropertiesManager.getProperty(Konstanten.PROPS_DB_FILE);
			dbUrl = "jdbc:idb:" + dbFile;
		}
		return dbUrl;
	}
	
	/**
	 * schlie�t eine evlt. noch offene Connection
	 */
	public  void close(){
		logger.debug("try to close Connection");
		try{
			if(connection != null && !connection.isClosed()){
				connection.close();
			}
		}catch(SQLException e){
			logger.warn("Fehler beim Schlie�en der Connection", e);
		}
		connection = null;
	}

	public void finalize(){
		logger.debug("in Finalizer");
		this.close();
	}
	

}
