package de.diakon.persistence;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import de.diakon.entities.BuchungsperiodeHome;
import de.diakon.entities.BuchungssatzHome;
import de.diakon.entities.KasseHome;
import de.diakon.entities.KontoHome;
import de.diakon.entities.KostenstelleHome;
import de.vahrst.application.db.WrappedConnection;
import de.vahrst.application.logging.TraceKonstanten;

/**
 * Superklasse f�r alle PersistenceManager-Implementierungen.
 */
public abstract class AbstractPersistenceManager {
	private static Logger logger = Logger.getLogger(AbstractPersistenceManager.class);

	private static Logger sqlStatementLogger = Logger.getLogger(TraceKonstanten.TRACE_SQL_STATEMENTS); 
	private static Logger sqlPerformanceLogger = Logger.getLogger(TraceKonstanten.TRACE_SQL_PERFORMANCE);


	// die Create-Methoden
	protected  void createKontenTabelle(Connection conn)throws SQLException{
		logger.info("Erzeuge Konten-Tabelle");
		String ddl = new KontoHome().getDDL();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(ddl);
		stmt.close();
	}
	
	protected void updateKontenTabelle(Connection conn) throws SQLException{
		
	}
	

	protected  void createKostenstellenTabelle(Connection conn)throws SQLException{
		logger.info("Erzeuge Kostenstellen-Tabelle");
		String ddl = new KostenstelleHome().getDDL();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(ddl);
		stmt.close();
	}

	protected  void createKassenTabelle(Connection conn)throws SQLException{
		logger.info("Erzeuge Kassen-Tabelle");
		String ddl = new KasseHome().getDDL();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(ddl);
		stmt.close();
	}
	

	protected  void createBuchungsperiodenTabelle(Connection conn)throws SQLException{
		logger.info("Erzeuge Buchungsperioden-Tabelle");
		String ddl = new BuchungsperiodeHome().getDDL();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(ddl);
		stmt.close();
	}

	protected  void createBuchungsTabelle(Connection conn)throws SQLException{
		logger.info("Erzeuge Buchungs-Tabelle");
		String ddl = new BuchungssatzHome().getDDL();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(ddl);
		stmt.close();
	}

	/**
	 * liefert die konkrete JDBC-Connection.
	 * @return
	 * @throws SQLException
	 */
	protected abstract Connection getJDBCConnection() throws SQLException;
	
	public final Connection getConnection() throws SQLException{
		boolean sqlStatementTrace = sqlStatementLogger.isInfoEnabled();
		boolean sqlPerformanceTrace = sqlPerformanceLogger.isInfoEnabled();
		
		Connection conn =  getJDBCConnection();
		if(sqlStatementTrace || sqlPerformanceTrace){
			WrappedConnection wConn = new WrappedConnection(conn, sqlStatementTrace, sqlPerformanceTrace, sqlPerformanceTrace);
			return wConn;
		}else{
			return conn;
		}
	}
}
