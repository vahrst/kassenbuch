package de.diakon.persistence;
/**
 * 
 * @author Thomas
 * @version 
 * @file PersistenceException.java
 */
public class PersistenceException extends Exception {

	/**
	 * Constructor for PersistenceException.
	 */
	public PersistenceException() {
		super();
	}

	/**
	 * Constructor for PersistenceException.
	 * @param message
	 */
	public PersistenceException(String message) {
		super(message);
	}

	/**
	 * Constructor for PersistenceException.
	 * @param message
	 * @param cause
	 */
	public PersistenceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor for PersistenceException.
	 * @param cause
	 */
	public PersistenceException(Throwable cause) {
		super(cause);
	}

}
