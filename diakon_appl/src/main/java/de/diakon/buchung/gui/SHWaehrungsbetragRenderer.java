package de.diakon.buchung.gui;

import java.awt.*;

import javax.swing.JTable;

import de.vahrst.application.gui.WaehrungsbetragRenderer;

/**
 * 
 * @author Thomas
 * @version 
 * @file SHWaehrungsbetragRenderer.java
 */
public class SHWaehrungsbetragRenderer extends WaehrungsbetragRenderer {

	/**
	 * Constructor for SHWaehrungsbetragRenderer.
	 */
	public SHWaehrungsbetragRenderer() {
		super();
	}

	public Component getTableCellRendererComponent(
		JTable table,
		Object o,
		boolean isSelected,
		boolean hasFocus,
		int row,
		int column) {

		Component c =
			super.getTableCellRendererComponent(
				table,
				o,
				isSelected,
				hasFocus,
				row,
				column);

		Boolean soll = (Boolean) table.getModel().getValueAt(row, 7);
		if (soll != null && soll.booleanValue()) {
			c.setForeground(Color.RED);
		}
		return c;
	}
}
