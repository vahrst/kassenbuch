package de.diakon.buchung.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;

import de.diakon.gui.StammdatenPflegeMainPanel;
import de.vahrst.application.gui.BaseTextField;
import de.vahrst.application.gui.ColorKonstanten;
import de.vahrst.application.gui.KombiTextField;
import de.vahrst.application.gui.Table;
import de.vahrst.application.gui.TextFieldNumericInteger;
import de.vahrst.application.gui.TextFieldWaehrung;
import de.vahrst.application.gui.VaComboBox;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.types.SollHabenHome;
import de.vahrst.application.types.Waehrungsbetrag;

public class BuchungspanelMain extends StammdatenPflegeMainPanel {
	private static Logger logger = Logger.getLogger(BuchungspanelMain.class);
	
	private AbstractController controller;
	public JTextField tfNummer;
	public JTextField tfKontoBezeichnung;
	public JTextField tfKostenstellenBezeichnung;
	public BaseTextField tfZusatz;
	public TextFieldNumericInteger tfTag;
	public KombiTextField tfKontoAlphaNum;
	public TextFieldNumericInteger tfKostenstelle;
	public TextFieldWaehrung tfBetrag;
	public VaComboBox cbSH;
	private JPanel pnUebersicht;

	private Table uebersichtTable = null;
	private TableModelListener tmListener = null;

	
	/**
	 * Constructor for BuchungspanelMain.
	 */
	public BuchungspanelMain(AbstractController controller, boolean neuanlage, TableModel uebersichtTableModel) {
		super(neuanlage);
		this.controller = controller;
		
		JLabel lblNummer = new JLabel("Nummer");
		
		JLabel lblTag = new JLabel("Tag");
		
		JLabel lblKonto = new JLabel("Konto");
		
		JLabel lblZusatztext = new JLabel("Zusatztext");
		
		JLabel lblKostenstelle = new JLabel("Kostenstelle");
		
		JLabel lblBetrag = new JLabel("Betrag");
		
		JLabel lblSollhaben = new JLabel("Soll/Haben");
		
		tfKostenstelle = new TextFieldNumericInteger();
		tfKostenstelle.setName("tfKst");
		tfKostenstelle.setColumns(6);
		tfKostenstelle.addFocusListener(controller);
		
		tfTag = new TextFieldNumericInteger();
		
		tfNummer = new JTextField();
		tfNummer.setFocusable(false);
		tfNummer.setEditable(false);
		tfNummer.setColumns(10);
		
		tfKontoAlphaNum = new KombiTextField();
		tfKontoAlphaNum.setColumns(6);
		tfKontoAlphaNum.setName("tfKonto"); // wird f�r FocusEventMethode im Controller ben�tigt.
		tfKontoAlphaNum.setMaxLength(26);
		tfKontoAlphaNum.addFocusListener(controller);
		tfKontoAlphaNum.setToolTipText("F11 f�r Kontenliste");
		
		tfZusatz = new BaseTextField();
		tfZusatz.setMaxLength(30);
		
		
		
		tfBetrag = new TextFieldWaehrung();
		
		cbSH = new VaComboBox();
		cbSH.addItem(SollHabenHome.SOLL);
		cbSH.addItem(SollHabenHome.HABEN);
		cbSH.setSelectedIndex(0);
		
		
		tfKontoBezeichnung = new JTextField();
		tfKontoBezeichnung.setFocusable(false);
		tfKontoBezeichnung.setEditable(false);
		tfKontoBezeichnung.setColumns(10);
		
		tfKostenstellenBezeichnung = new JTextField();
		tfKostenstellenBezeichnung.setFocusable(false);
		tfKostenstellenBezeichnung.setEditable(false);
		tfKostenstellenBezeichnung.setColumns(10);
		
		pnUebersicht = new JPanel();
		pnUebersicht.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "\u00DCbersicht", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(pnUebersicht, GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblKostenstelle)
								.addComponent(lblNummer)
								.addComponent(lblTag)
								.addComponent(lblKonto)
								.addComponent(lblZusatztext)
								.addComponent(lblBetrag)
								.addComponent(lblSollhaben))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(tfZusatz, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(tfKontoAlphaNum, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
										.addComponent(tfNummer, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(tfKontoBezeichnung, GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(tfKostenstelle, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
										.addComponent(tfBetrag, Alignment.LEADING, 0, 0, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(tfKostenstellenBezeichnung, GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE))
								.addComponent(cbSH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tfTag, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNummer)
						.addComponent(tfNummer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTag)
						.addComponent(tfTag, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblKonto)
						.addComponent(tfKontoAlphaNum, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfKontoBezeichnung, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblZusatztext)
						.addComponent(tfZusatz, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblKostenstelle)
						.addComponent(tfKostenstelle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfKostenstellenBezeichnung, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblBetrag)
						.addComponent(tfBetrag, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSollhaben)
						.addComponent(cbSH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pnUebersicht, GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
					.addContainerGap())
		);
		pnUebersicht.setLayout(new BorderLayout(0, 0));
		groupLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {tfKostenstelle, tfTag, tfNummer, tfKontoAlphaNum});
		setLayout(groupLayout);
		
		initUebersichtsPanel(uebersichtTableModel);
		
		
		addKeyListeners();
		
		
	}

	/**
	 * initialisiert die �bersichtstabelle, falls das TableModel != null ist.
	 * @param tm
	 */
	private void initUebersichtsPanel(TableModel tm){
		if(tm == null){
			pnUebersicht.setVisible(false);
			return;
		}
		
		uebersichtTable = new Table(tm, getTableColumnModel());
		uebersichtTable.setDefaultRenderer(Waehrungsbetrag.class, new SHWaehrungsbetragRenderer());
		uebersichtTable.setDefaultRenderer(Kontobezeichnung.class, new KontobezeichnungRenderer());
		uebersichtTable.setPreferredScrollableViewportSize(new Dimension(150,100));
		uebersichtTable.setFocusable(false);
		
		JScrollPane scp = new JScrollPane(uebersichtTable);
		scp.setOpaque(false);
		scp.getVerticalScrollBar().setFocusable(false);
		scp.getViewport().setBackground(ColorKonstanten.COLOR_FORMPANEL_BG);

		tmListener = new TableModelListener(){
			public void tableChanged(TableModelEvent e){
				nachUntenScrollen();
			}
		};
		tm.addTableModelListener(tmListener);
		
		pnUebersicht.add(scp, BorderLayout.CENTER);
		nachUntenScrollen();
		
		
	}

	/** 
	 * sorgt daf�r, dass die uebersichtstabelle immer die letzte Zeile
	 * anzeigt
	 */
	private void nachUntenScrollen(){
		logger.debug("Tabelle nach unten scrollen");
		uebersichtTable.invalidate();
		uebersichtTable.revalidate();
		int lastRowIndex = uebersichtTable.getRowCount() - 1;
		Rectangle rect = uebersichtTable.getCellRect(lastRowIndex,0,false);
		uebersichtTable.scrollRectToVisible(rect);
	}
	
	
	/**
	 * liefert das TableColumnModel f�r die �bersichtstabelle.
	 * @return
	 */
	private TableColumnModel getTableColumnModel() {

		TableColumnModel tcm = new DefaultTableColumnModel();

		TableColumn nrColumn = new TableColumn(0, 35);
		nrColumn.setHeaderValue("lfd Nr.");
		nrColumn.setMaxWidth(60);
		tcm.addColumn(nrColumn);

		TableColumn tagColumn = new TableColumn(1, 35);
		tagColumn.setHeaderValue("Tag");
		tagColumn.setMaxWidth(60);
		tcm.addColumn(tagColumn);

		TableColumn ktoColumn = new TableColumn(2, 35);
		ktoColumn.setHeaderValue("Konto");
		tcm.addColumn(ktoColumn);

		TableColumn ktobezColumn = new TableColumn(3, 150);
		ktobezColumn.setHeaderValue("Kontobezeichnung");
		tcm.addColumn(ktobezColumn);
		
		TableColumn zusatzColumn = new TableColumn(4, 100);
		zusatzColumn.setHeaderValue("Zusatztext");
		tcm.addColumn(zusatzColumn);
		
		TableColumn kstColumn = new TableColumn(5, 35);
		kstColumn.setHeaderValue("Kostenstelle");
		tcm.addColumn(kstColumn);
		
		TableColumn betragColumn = new TableColumn(6,60);
		betragColumn.setHeaderValue("Betrag");
		tcm.addColumn(betragColumn);

		return tcm;

	}
	
	
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setFocus(){
		if(tfTag != null){
			tfTag.requestFocusInWindow();
		}
	}
	
	
	/**
	 * F�gt KeyListener f�r F11 (f�r Konten�bersicht) hinzu
	 */
	private void addKeyListeners(){
		tfKontoAlphaNum.addKeyListener(new KeyAdapter(){
			public void keyReleased(KeyEvent e){
				if(e.getKeyCode() == KeyEvent.VK_F11){
					actionPerformed(new ActionEvent(tfKontoAlphaNum, ActionEvent.ACTION_PERFORMED, "kontoSuche"));		
				}
			}
		});	
	}

	/** Hilfsmethode f�r addKeyListener */
	private void actionPerformed(ActionEvent ae){
		controller.actionPerformed(ae);
	}
	
	public void resetListener(TableModel tm){
		tm.removeTableModelListener(tmListener);
		tm.removeTableModelListener(uebersichtTable);
	}
	
}
