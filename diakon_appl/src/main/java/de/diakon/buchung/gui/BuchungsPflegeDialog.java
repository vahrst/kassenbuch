package de.diakon.buchung.gui;

import java.awt.*;

import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file KontenAnlageDialog.java
 */
public class BuchungsPflegeDialog extends AbstractDialog {
	private Buchungspanel buchungspanel = null;

		/**
	 * Constructor for KontenAnlageDialog.
	 * @param owner
	 * @param modal
	 * @throws HeadlessException
	 */
	public BuchungsPflegeDialog(
		Dialog owner, 
		boolean modal, 
		AbstractController controller,
		String title)
		throws HeadlessException {
		super(owner, modal, controller);

		setTitle(title);
		initialize(controller);
	}

	
	/**
	 * Constructor for BuchungsPflegeDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 * @wbp.parser.constructor
	 */
	public BuchungsPflegeDialog(
		Frame owner,
		boolean modal,
		AbstractController controller,
		String title)
		throws HeadlessException {

		super(owner, modal, controller);
		
		this.setTitle(title);
		this.initialize(controller);
	}


	private void initialize(AbstractController controller){
		this.setSize(560,300);
		this.center();
		this.setResizable(false);
		buchungspanel = new Buchungspanel(controller, null, false, null);
		this.getContentPane().add(buchungspanel, BorderLayout.CENTER);
		
	}
	
	public BuchungspanelMain getMainPanel(){
		return buchungspanel.getBuchungspanelMain();
	}

}
