package de.diakon.buchung.gui;

import java.awt.event.ActionListener;

import javax.swing.table.*;

import de.diakon.gui.StammdatenUebersichtTablePanel;

/**
 * 
 * @author Thomas
 * @version 
 */
public class KontoListeUebersichtTablePanel
	extends StammdatenUebersichtTablePanel {

	/**
	 * Constructor for KassenAuswahlUebersichtTablePanel.
	 * @param tm
	 * @param controller
	 */
	public KontoListeUebersichtTablePanel(
		TableModel tm,
		ActionListener controller) {
		super(tm, controller);
		
		// Bei Doppelklick auf die Tabelle muss ein anderer
		// als der Standard-Event (GeheZu) gefeuert werden.
		table.setSelectionEventName("KontoGeheZu");
	}

	/**
	 * @see de.diakon.gui.StammdatenUebersichtTablePanel#getTableColumnModel()
	 */
	protected TableColumnModel getTableColumnModel() {
		TableColumnModel tcm = new DefaultTableColumnModel();

		TableColumn ktonrColumn = new TableColumn(0, 50);
		ktonrColumn.setHeaderValue("Nummer");
		tcm.addColumn(ktonrColumn);
		
		TableColumn nameColumn = new TableColumn(1, 200);
		nameColumn.setHeaderValue("Bezeichnung");
		tcm.addColumn(nameColumn);


		return tcm;
	}

}
