package de.diakon.buchung.gui;

import javax.swing.table.TableModel;

import de.diakon.gui.*;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 */
public class Buchungspanel extends StammdatenPflegePanel {
	private BuchungspanelMain mainpanel;

	/**
	 * Constructor for Buchungspanel.
	 */
	public Buchungspanel(
		AbstractController controller,
		Object model,
		boolean neuanlage,
		TableModel tm) {
		super(controller, "Buchen", tm, neuanlage);
		setPaintHeaderArea(true);

		// wenn wir in einem Dialog existieren, f�hrt der folgende
		// Befehl dazu, dass der Dialog zuerst den Focus hat. Wenn wir
		// im Formpanel leben, ist die folgende Zeile notwendig. Also
		// machen wir das ganze abh�ngig vom Neuanlage-Flag - nicht sch�n, 
		// aber selten.
		if(neuanlage)
			this.setFocusCycleRoot(true);

		/*
		btOk.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				System.out.println(e);
			}
		});
		*/
	}

	/**
	 * erzeugt die Instanz des PflegeMainPanels und liefert diese Zur�ck. Diese Instanz
	 * wird von der Superklasse verwendet, um das Panel-Layout zusammenzubauen.
	 */
	protected StammdatenPflegeMainPanel createPflegeMainPanel(
		Object model,
		boolean neuanlage) {
		mainpanel = new BuchungspanelMain(controller, neuanlage, (TableModel) model);
		return mainpanel;
	}

	public void setFocus() {
		mainpanel.setFocus();
	}

	public BuchungspanelMain getBuchungspanelMain() {
		return mainpanel;
	}

	/**
	 * entfernt die Listener auf dem TableModel
	 * @param tm
	 */
	public void resetListener(TableModel tm) {
		mainpanel.resetListener(tm);
	}
	
	
}
