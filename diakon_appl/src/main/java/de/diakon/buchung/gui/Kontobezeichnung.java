package de.diakon.buchung.gui;

/**
 * Im Feld Kontobezeichnung wird eine Fehlermeldung angezeigt, wenn der
 * Buchungssatz nicht existiert. Daher dieser komplexe Type, der zus�tzlich
 * 'wei�', dass dies eine Fehlermeldung ist. siehe auch KontobezeichnungRenderer
 */
public class Kontobezeichnung {
	String content;
	boolean errorFlag;
	
	public Kontobezeichnung(String value, boolean error){
		super();
		this.content = value;
		this.errorFlag = error;
	}
	
	public String getContent(){
		return content;
	}
	
	public boolean isError(){
		return errorFlag;
	}
}
