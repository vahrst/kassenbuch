package de.diakon.buchung.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * TableCellRenderer f�r ErrorStrings.
 */
public class KontobezeichnungRenderer extends DefaultTableCellRenderer {

	public KontobezeichnungRenderer() {
		super();
	}

	public Component getTableCellRendererComponent(
		JTable table,
		Object o,
		boolean isSelected,
		boolean hasFocus,
		int row,
		int column) {

		Kontobezeichnung bez = (Kontobezeichnung) o;
		String content = bez.getContent();
		
		Component c =
			super.getTableCellRendererComponent(
				table,
				content,
				isSelected,
				hasFocus,
				row,
				column);

		// Die Superklasse 'cached' Background und Foreground Colors. 
		// Daher hier nochmal setzen.
		if(!bez.isError()){
			if (isSelected) {
			   setForeground(table.getSelectionForeground());
			   setBackground(table.getSelectionBackground());
			}
			else {
				setForeground( table.getForeground());
				setBackground( table.getBackground());
			}
		}else{
			setBackground(Color.RED);
			setForeground(Color.BLACK);
			Font f = c.getFont();
			Font newFont = new Font(f.getName(), Font.BOLD, f.getSize());
			setFont(newFont);
		}		
		return c;
	}
}
