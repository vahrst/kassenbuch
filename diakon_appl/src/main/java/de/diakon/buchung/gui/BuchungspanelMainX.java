package de.diakon.buchung.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;

import de.diakon.gui.StammdatenPflegeMainPanel;
import de.vahrst.application.gui.BaseTextField;
import de.vahrst.application.gui.ColorKonstanten;
import de.vahrst.application.gui.KombiTextField;
import de.vahrst.application.gui.Strut;
import de.vahrst.application.gui.Table;
import de.vahrst.application.gui.TextFieldNumericInteger;
import de.vahrst.application.gui.TextFieldWaehrung;
import de.vahrst.application.gui.VaComboBox;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.types.SollHabenHome;
import de.vahrst.application.types.Waehrungsbetrag;
/**
 * Das eigentliche Buchungspanel. Im oberen Teil die Erfassungsmaske
 * f�r eine Buchung, im unteren Teil die Liste der bisherigen Buchungen (nur
 * im Neuanlage-Modus)
 * @author Thomas
 * @version 
 * @file BuchungspanelMain.java
 */
public class BuchungspanelMainX extends StammdatenPflegeMainPanel {
	private static Logger logger = Logger.getLogger(BuchungspanelMain.class);
		

	public JTextField tfNummer = null;
	public TextFieldNumericInteger tfTag = null;
	
	// in das Eingabefeld f�r die Kontonummer kann auch alphanumerischer 
	// Text eingegeben werden. In diesem Fall wird nach einem passenden Konto
	// gesucht.
	// public TextFieldNumericInteger tfKonto = null;
	public KombiTextField tfKontoAlphaNum = null;
	
	public BaseTextField tfZusatz = null;
	public TextFieldNumericInteger tfKostenstelle = null;
	public TextFieldWaehrung tfBetrag = null;

	public JTextField tfKontoBezeichnung = null;
	public JTextField tfKostenstellenBezeichnung = null;
	public VaComboBox cbSH = null;

	private Table uebersichtTable = null;
	private AbstractController controller = null;

	private TableModelListener tmListener = null;
	private GridBagConstraints gbc_1;
	private GridBagConstraints gbc_2;
	private GridBagConstraints gbc_3;
	private GridBagConstraints gbc_4;
	private GridBagConstraints gbc_5;
	private GridBagConstraints gbc_6;
	private GridBagConstraints gbc_7;
	private GridBagConstraints gbc_8;
		
	/**
	 * Constructor for BuchungspanelMain.
	 */
	public BuchungspanelMainX(AbstractController controller, boolean neuanlage, TableModel uebersichtTableModel) {
		super(neuanlage);
		this.controller = controller;
		initialize(uebersichtTableModel);
	}

	public void setFocus(){
		if(tfTag != null){
			tfTag.requestFocusInWindow();
		}
	}
	

	
	private void initialize(TableModel tm){
		this.setLayout(new BorderLayout());
		
		JPanel centerPanel = createCenterPanel();
		this.add(centerPanel, BorderLayout.CENTER);

		// im �nderungsmodus haben wir keine �bersichtstabelle.
		if(tm != null){
			JPanel uebersichtsPanel = createUebersichtsPanel(tm);
			this.add(uebersichtsPanel, BorderLayout.SOUTH);
			nachUntenScrollen();	
		}

		// Auf F11 im Kontofeld reagieren.
		addKeyListeners();
		
	}

	/**
	 * Erzeugt das Panel mit der Eingabemaske f�r einen
	 * Buchungssatz.
	 * @return JPanel
	 */
	private JPanel createCenterPanel(){
//		RepaintManager rp = RepaintManager.currentManager(this);
//		rp.setDoubleBufferingEnabled(false);
//		this.setDebugGraphicsOptions(DebugGraphics.FLASH_OPTION);

		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setMinimumSize(new Dimension(150,150));

		panel.setLayout(new GridBagLayout());

		GridBagConstraints gbc = null;
		
		JLabel lb1 = new JLabel("Nummer");
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		gbc.gridx = 0;
		gbc.gridy = 0;			
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		panel.add(lb1, gbc);
		
		JLabel lb2 = new JLabel("Tag");
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		panel.add(lb2,gbc);
		
		JLabel lb3 = new JLabel("Konto");
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		gbc.gridx = 0;
		gbc.gridy = 2;			
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		panel.add(lb3, gbc);
		
		JLabel lb4 = new JLabel("Zusatztext");
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		gbc.gridx = 0;
		gbc.gridy = 3;			
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		panel.add(lb4, gbc);
		
		JLabel lb5 = new JLabel("Kostenstelle");
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		gbc.gridx = 0;
		gbc.gridy = 4;			
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		panel.add(lb5, gbc);
		
		JLabel lb6 = new JLabel("Betrag");
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		gbc.gridx = 0;
		gbc.gridy = 5;			
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		panel.add(lb6, gbc);
		
		JLabel lb7 = new JLabel("Soll/Haben");
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		gbc.gridx = 0;
		gbc.gridy = 6;			
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		panel.add(lb7, gbc);



		tfNummer = new JTextField(3);
		tfNummer.setEditable(false);
		tfNummer.setFocusable(false);
		tfNummer.setHorizontalAlignment(JTextField.RIGHT);		

		gbc_1 = new GridBagConstraints();
		gbc_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_1.anchor = GridBagConstraints.WEST;
		gbc_1.insets = new Insets(1,4,1,4);
		gbc_1.gridx = 1;
		gbc_1.gridy = 0;			
		gbc_1.gridwidth = 1;
		gbc_1.gridheight = 1;
		gbc_1.weightx = 0;
		gbc_1.weighty = 0;
		panel.add(tfNummer,gbc_1);
		
		tfTag = new TextFieldNumericInteger(3);
		tfTag.setIntegerPart(2);
		tfTag.setHorizontalAlignment(TextFieldNumericInteger.RIGHT);
		
/*
  	HashSet fwdSet = new HashSet();
		fwdSet.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER,0, false));
		fwdSet.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB,0, false));
		fwdSet.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_DOWN,0, false));
		tfTag.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,fwdSet);		
*/

		gbc_2 = new GridBagConstraints();
		gbc_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_2.anchor = GridBagConstraints.WEST;
		gbc_2.insets = new Insets(1,4,1,4);
		gbc_2.gridx = 1;
		gbc_2.gridy = 1;			
		gbc_2.gridwidth = 1;
		gbc_2.gridheight = 1;
		gbc_2.weightx = 0;
		gbc_2.weighty = 0;
		panel.add(tfTag,gbc_2);
		
		// tfKonto = new TextFieldNumericInteger(6);
		// tfKonto.setName("tfKonto"); // wird f�r FocusEventMethoden in Controller ben�tigt.
		// tfKonto.addFocusListener(controller);
		// tfKonto.setToolTipText("F11 f�r Kontenliste");
		tfKontoAlphaNum = new KombiTextField(6);
		tfKontoAlphaNum.setName("tfKonto"); // wird f�r FocusEventMethode im Controller ben�tigt.
		tfKontoAlphaNum.setMaxLength(20);
		tfKontoAlphaNum.addFocusListener(controller);
		tfKontoAlphaNum.setToolTipText("F11 f�r Kontenliste");
		
		gbc_3 = new GridBagConstraints();
		gbc_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_3.anchor = GridBagConstraints.WEST;
		gbc_3.insets = new Insets(1,4,1,4);
		gbc_3.gridx = 1;
		gbc_3.gridy = 2;			
		gbc_3.gridwidth = 1;
		gbc_3.gridheight = 1;
		gbc_3.weightx = 0;
		gbc_3.weighty = 0;
		panel.add(tfKontoAlphaNum,gbc_3);
		
		
		tfZusatz = new BaseTextField(30);
		tfZusatz.setMaxLength(30);
		tfZusatz.setMinimumSize(new Dimension(100,20));
		gbc_8 = new GridBagConstraints();
		gbc_8.fill = GridBagConstraints.HORIZONTAL;
		gbc_8.gridwidth = 3;
		gbc_8.anchor = GridBagConstraints.WEST;
		gbc_8.insets = new Insets(1,4,1,4);
		gbc_8.gridx = 1;
		gbc_8.gridy = 3;
		gbc_8.gridheight = 1;
		gbc_8.weightx = 0;
		gbc_8.weighty = 0;
		panel.add(tfZusatz, gbc_8);
		
		tfKostenstelle  = new TextFieldNumericInteger(6);
		tfKostenstelle.setName("tfKst");
		tfKostenstelle.addFocusListener(controller);
		gbc_4 = new GridBagConstraints();
		gbc_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_4.anchor = GridBagConstraints.WEST;
		gbc_4.insets = new Insets(1,4,1,4);
		gbc_4.gridx = 1;
		gbc_4.gridy = 4;			
		gbc_4.gridwidth = 1;
		gbc_4.gridheight = 1;
		gbc_4.weightx = 0;
		gbc_4.weighty = 0;
		panel.add(tfKostenstelle, gbc_4);
		
		tfBetrag  = new TextFieldWaehrung(new Waehrungsbetrag(0));
		tfBetrag.setColumns(10);
		gbc_5 = new GridBagConstraints();
		gbc_5.fill = GridBagConstraints.HORIZONTAL;
		gbc_5.anchor = GridBagConstraints.WEST;
		gbc_5.insets = new Insets(1,4,1,4);
		gbc_5.gridx = 1;
		gbc_5.gridy = 5;			
		gbc_5.gridwidth = 1;
		gbc_5.gridheight = 1;
		gbc_5.weightx = 0;
		gbc_5.weighty = 0;
		panel.add(tfBetrag, gbc_5);
		
		cbSH = new VaComboBox();
		cbSH.addItem(SollHabenHome.SOLL);
		cbSH.addItem(SollHabenHome.HABEN);
		cbSH.setSelectedIndex(0);
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		gbc.gridx = 1;
		gbc.gridy = 6;			
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(cbSH, gbc);
		
		

		tfKontoBezeichnung = new JTextField(30);
//		tfKtoBez.setEditable(false);
		tfKontoBezeichnung.setFocusable(false);
		tfKontoBezeichnung.setOpaque(false);
		
		gbc_6 = new GridBagConstraints();
		gbc_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_6.anchor = GridBagConstraints.WEST;
		gbc_6.insets = new Insets(1,4,1,4);
		gbc_6.gridx = 2;
		gbc_6.gridy = 2;			
		gbc_6.gridwidth = 3;
		gbc_6.gridheight = 1;
		gbc_6.weightx = 0;
		gbc_6.weighty = 0;
		panel.add(tfKontoBezeichnung, gbc_6);

		tfKostenstellenBezeichnung = new JTextField(30);
//		tfKstBez.setEditable(false);
		tfKostenstellenBezeichnung.setFocusable(false);
		tfKostenstellenBezeichnung.setOpaque(false);
		
		gbc_7 = new GridBagConstraints();
		gbc_7.fill = GridBagConstraints.HORIZONTAL;
		gbc_7.anchor = GridBagConstraints.WEST;
		gbc_7.insets = new Insets(1,4,1,4);
		gbc_7.gridx = 2;
		gbc_7.gridy = 4;			
		gbc_7.gridwidth = 3;
		gbc_7.gridheight = 1;
		gbc_7.weightx = 0;
		gbc_7.weighty = 0;
		panel.add(tfKostenstellenBezeichnung, gbc_7);


		JPanel miniUebersicht = new JPanel();
		miniUebersicht.setOpaque(false);
		miniUebersicht.setBorder( BorderFactory.createTitledBorder(new LineBorder(Color.BLACK), "�bersicht"));
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		gbc.gridx = 4;
		gbc.gridy = 7;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		panel.add(new Strut(),gbc);


		return panel;
	}

		
	
	
	
	/** 
	 * erzeugt das �bersichtspanel mit der Tabelle
	 */
	private JPanel createUebersichtsPanel(TableModel tm){
		uebersichtTable = new Table(tm, getTableColumnModel());
		uebersichtTable.setDefaultRenderer(Waehrungsbetrag.class, new SHWaehrungsbetragRenderer());
		uebersichtTable.setDefaultRenderer(Kontobezeichnung.class, new KontobezeichnungRenderer());
		uebersichtTable.setPreferredScrollableViewportSize(new Dimension(150,100));
		uebersichtTable.setFocusable(false);
		
		JScrollPane scp = new JScrollPane(uebersichtTable);
		scp.getVerticalScrollBar().setFocusable(false);
		scp.getViewport().setBackground(ColorKonstanten.COLOR_FORMPANEL_BG);

		tmListener = new TableModelListener(){
			public void tableChanged(TableModelEvent e){
				nachUntenScrollen();
			}
		};
		tm.addTableModelListener(tmListener);
		


		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(	 BorderFactory.createTitledBorder(new LineBorder(Color.BLACK), "�bersicht"));
		panel.add(scp, BorderLayout.CENTER);
		panel.setOpaque(false);
		
//		panel.setMinimumSize(new Dimension(150,50));
//		panel.setMaximumSize(new Dimension(150,150));
//		panel.setPreferredSize(new Dimension(150,150));
		return panel;
		
	}


	/** 
	 * sorgt daf�r, dass die uebersichtstabelle immer die letzte Zeile
	 * anzeigt
	 */
	private void nachUntenScrollen(){
		logger.debug("Tabelle nach unten scrollen");
		uebersichtTable.invalidate();	
		int lastRowIndex = uebersichtTable.getRowCount() - 1;
		Rectangle rect = uebersichtTable.getCellRect(lastRowIndex,0,false);
		uebersichtTable.scrollRectToVisible(rect);
	}

	protected TableColumnModel getTableColumnModel() {

		TableColumnModel tcm = new DefaultTableColumnModel();

		TableColumn nrColumn = new TableColumn(0, 35);
		nrColumn.setHeaderValue("lfd Nr.");
		nrColumn.setMaxWidth(60);
		tcm.addColumn(nrColumn);

		TableColumn tagColumn = new TableColumn(1, 35);
		tagColumn.setHeaderValue("Tag");
		tagColumn.setMaxWidth(60);
		tcm.addColumn(tagColumn);

		TableColumn ktoColumn = new TableColumn(2, 35);
		ktoColumn.setHeaderValue("Konto");
		tcm.addColumn(ktoColumn);

		TableColumn ktobezColumn = new TableColumn(3, 150);
		ktobezColumn.setHeaderValue("Kontobezeichnung");
		tcm.addColumn(ktobezColumn);
		
		TableColumn zusatzColumn = new TableColumn(4, 100);
		zusatzColumn.setHeaderValue("Zusatztext");
		tcm.addColumn(zusatzColumn);
		
		TableColumn kstColumn = new TableColumn(5, 35);
		kstColumn.setHeaderValue("Kostenstelle");
		tcm.addColumn(kstColumn);
		
		TableColumn betragColumn = new TableColumn(6,60);
		betragColumn.setHeaderValue("Betrag");
		tcm.addColumn(betragColumn);

		return tcm;

	}
	
	
	/**
	 * @see de.diakon.gui.StammdatenPflegeMainPanel#getModel()
	 */
	public Object getModel() {
		return null;
	}


	/**
	 * F�gt KeyListener f�r F11 (f�r Konten�bersicht) hinzu
	 */
	private void addKeyListeners(){
		tfKontoAlphaNum.addKeyListener(new KeyAdapter(){
			public void keyReleased(KeyEvent e){
				if(e.getKeyCode() == KeyEvent.VK_F11){
					actionPerformed(new ActionEvent(tfKontoAlphaNum, ActionEvent.ACTION_PERFORMED, "kontoSuche"));		
				}
			}
		});	
	}

	/** Hilfsmethode f�r addKeyListener */
	private void actionPerformed(ActionEvent ae){
		controller.actionPerformed(ae);
	}
	
	public void resetListener(TableModel tm){
		tm.removeTableModelListener(tmListener);
		tm.removeTableModelListener(uebersichtTable);
	}
}
