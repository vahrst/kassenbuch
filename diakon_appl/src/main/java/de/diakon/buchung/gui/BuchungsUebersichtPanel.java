package de.diakon.buchung.gui;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.TableModel;

import de.diakon.global.Konstanten;
import de.diakon.gui.*;
import de.vahrst.application.gui.VaButton;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file BuchungsUebersichtPanel.java
 */
public class BuchungsUebersichtPanel extends StammdatenUebersichtPanel {

	/**
	 * Constructor for BuchungsUebersichtPanel.
	 * @param tm
	 * @param controller
	 */
	public BuchungsUebersichtPanel(TableModel tm, AbstractController controller) {
		super(tm, controller);
	}

	/**
	 * @see de.diakon.gui.StammdatenUebersichtPanel#getTablePanel(TableModel, ActionListener)
	 */
	protected StammdatenUebersichtTablePanel getTablePanel(
		TableModel tm,
		ActionListener controller) {
		return new BuchungsUebersichtTablePanel(tm, controller);
	}

	/**
	 * @see de.diakon.gui.StammdatenUebersichtPanel#getHeaderTitle()
	 */
	protected String getHeaderTitle() {
		return "Buchungsübersicht / Korrektur";
	}


	/**
	 * Überschreibt die Default-Implementierung von StammdatenUebersichtPanel,
	 * weil hier nur 'gehezu' und 'buchen' erlaubt sind.
	 */
	protected JPanel createButtonPanel(AbstractController controller){
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		buttonPanel.setMinimumSize(new Dimension(10, 24));
		buttonPanel.setPreferredSize(new Dimension(10, 24));
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		btGeheZu = new VaButton("GeheZu");
		btGeheZu.setActionCommand("GeheZu");
		btGeheZu.addActionListener(controller);
		buttonPanel.add(btGeheZu);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btNeu = new VaButton(controller.getAction(Konstanten.ACTION_BUCHEN));
		buttonPanel.add(btNeu);


		return buttonPanel;		
	}

}
