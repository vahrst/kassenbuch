package de.diakon.buchung.gui;

import java.awt.*;

import javax.swing.JPanel;
import javax.swing.table.TableModel;

import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * Dialog zur Anzeige und Auswahl eines Kontos.
 * @author Thomas
 * @version 
 * @file KontoListe.java
 */
public class KontoListeDialog extends AbstractDialog {
	private KontoListeUebersichtPanel uebersichtPanel;


	/**
	 * Constructor for KontoListe.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public KontoListeDialog(Frame owner, boolean modal, AbstractController controller, TableModel tm)
		throws HeadlessException {
		super(owner, modal, controller);
		init(tm, controller);
	}

	/**
	 * Constructor for KontoListe.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public KontoListeDialog(Dialog owner, boolean modal, AbstractController controller, TableModel tm)
		throws HeadlessException {
		super(owner, modal, controller);
		init(tm, controller);
	}

	/**
	 * Constructor for KontoListe.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public KontoListeDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller,
		TableModel tm)
		throws HeadlessException {
		super(owner, title, modal, controller);
		init(tm, controller);
	}


	private void init(TableModel tm, AbstractController controller){
		this.setSize(300,500);
		uebersichtPanel = new KontoListeUebersichtPanel(tm, controller);
		JPanel cp = new JPanel(new BorderLayout());
		this.setContentPane(cp);
		
		cp.add(uebersichtPanel, BorderLayout.CENTER);
		
		//this.pack();
		this.center();	

	}
	
	public int getSelectedRow(){
		return uebersichtPanel.getSelectedRow();
	}
	
}
