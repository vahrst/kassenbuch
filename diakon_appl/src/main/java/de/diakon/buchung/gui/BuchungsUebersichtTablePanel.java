package de.diakon.buchung.gui;

import java.awt.Rectangle;
import java.awt.event.ActionListener;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;

import de.diakon.gui.StammdatenUebersichtTablePanel;
import de.vahrst.application.types.Waehrungsbetrag;
/**
 * Dieses Panel enth�lt die reine Konten�bersicht-Tabelle.
 * @author Thomas
 * @version 
  */
public class BuchungsUebersichtTablePanel extends StammdatenUebersichtTablePanel {
	private static Logger logger = Logger.getLogger(BuchungsUebersichtTablePanel.class);	

	private TableModelListener tmListener = null;
	/**
	 * Constructor for TestTablePanel.
	 */
	public BuchungsUebersichtTablePanel(TableModel tm, ActionListener controller) {
		super(tm, controller);
		table.setDefaultRenderer(Waehrungsbetrag.class, new SHWaehrungsbetragRenderer());
		table.setDefaultRenderer(Kontobezeichnung.class, new KontobezeichnungRenderer());
	
		nachUntenScrollen();
		
		// tmListener muss als Instanzvariable gehalten werden, weil der
		// Listener sonst direkt wieder entfernt wird (WeakReferences in EntityListe)
		tmListener = new TableModelListener(){
			public void tableChanged(TableModelEvent e){
				logger.debug("tableChangedEvent in tmListener BuchungsUebersicht");
				nachUntenScrollen();
			}
		};
		tm.addTableModelListener(tmListener);		
	}
	
	
	/** 
	 * sorgt daf�r, dass die uebersichtstabelle immer die letzte Zeile
	 * anzeigt
	 */
	private void nachUntenScrollen(){
		logger.debug("nach unten scrollen");
		table.invalidate();	
		int lastRowIndex = table.getRowCount() - 1;
		Rectangle rect = table.getCellRect(lastRowIndex,0,false);
		table.scrollRectToVisible(rect);
	}




	protected TableColumnModel getTableColumnModel() {

		TableColumnModel tcm = new DefaultTableColumnModel();

		TableColumn nrColumn = new TableColumn(0, 15);
		nrColumn.setHeaderValue("lfd Nr.");
		tcm.addColumn(nrColumn);

		TableColumn tagColumn = new TableColumn(1, 15);
		tagColumn.setHeaderValue("Tag");
		tcm.addColumn(tagColumn);


		TableColumn ktoColumn = new TableColumn(2, 30);
		ktoColumn.setHeaderValue("Konto");
		tcm.addColumn(ktoColumn);

		TableColumn ktobezColumn = new TableColumn(3, 150);
		ktobezColumn.setHeaderValue("Kontobezeichnung");
		tcm.addColumn(ktobezColumn);
		
		TableColumn zusatzColumn = new TableColumn(4, 100);
		zusatzColumn.setHeaderValue("Zusatztext");
		tcm.addColumn(zusatzColumn);
		
		TableColumn kstColumn = new TableColumn(5, 40);
		kstColumn.setHeaderValue("Kostenstelle");
		tcm.addColumn(kstColumn);
		
		TableColumn betragColumn = new TableColumn(6,40);
		betragColumn.setHeaderValue("Betrag");
		tcm.addColumn(betragColumn);

		return tcm;
	}

}
