package de.diakon.buchung.prozesse;

import javax.swing.table.TableModel;

import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.*;
import de.vahrst.application.types.*;

/**
 * Prozesskomponente f�r die Erfassung von Buchungen.
 * @author Thomas
 * @version 
 * @file BuchungsProcess.java
 */
public class BuchungsProcess extends AbstractBuchungsProcess {
	private BuchungsController controller;


	/**
	 * Constructor for BuchungsProcess.
	 * @param parent
	 * @param eth
	 */
	public BuchungsProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		KassenKontext kontext) {
		super(parent, eth, kontext);

		openController();
	}

	/**
	 * startet den Controller
	 */
	private void openController() {
		controller = new BuchungsController(this);
		controller.start();
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	/**
	 * Liefert die Nummer f�r den n�chsten Buchungssatz
	 */
	public int getNaechsteBuchungsNummer() {
		return kassenKontext.getNaechsteBuchungsNummer();
	}

	/**
	 * erzeugt einen neuen Buchungssatz
	 */
	public void createBuchungssatz(
		int tag,
		int ktonummer,
		String zusatztext,
		int kostenstelle,
		Waehrungsbetrag betrag,
		SollHaben sh)
		throws PersistenceException, PlausibilitaetsException {

		Konto kto = kontoHome.getKontoByKontonummer(ktonummer);
		Kostenstelle kst = kstHome.getKostenstelleByKostenstellenummer(kostenstelle);
		
		checkPlausis(tag, kto, kst, zusatztext);

		Buchungssatz satz = new Buchungssatz( 
			kassenKontext.getNaechsteBuchungsNummer(),
			tag,
			kto,
			zusatztext,
			kst,
			betrag,
			sh);
		
		kassenKontext.addBuchungssatz(satz);
		log.info("Buchungssatz angelegt: " + satz.toString());
	}
	
	/**
	 * liefert das Tabellen-modell f�r die Buchungs�bersicht
	 */
	public TableModel getBuchungsUebersichtTableModel(){
		return kassenKontext.getBuchungslisteTableModel();
	}
}
