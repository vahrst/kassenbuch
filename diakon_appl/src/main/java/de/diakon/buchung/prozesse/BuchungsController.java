package de.diakon.buchung.prozesse;

import java.util.*;

import javax.swing.JOptionPane;

import de.diakon.buchung.gui.*;
import de.diakon.entities.Buchungssatz;
import de.diakon.entities.PlausibilitaetsException;
import de.diakon.global.Konstanten;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.gui.ClientAreaPanel;
import de.vahrst.application.prozesse.*;
import de.vahrst.application.types.*;

/**
 * Controller f�r BuchungsProcess. �ffnet ein Panel im Formularbereich, 
 * �ber das einzelne Buchungen erfa�t werden k�nnen. Die Eingabemaske
 * enth�lt auch eine �bersicht der bisherigen Buchungen.
 * @author Thomas
 * @version 
 * @file BuchungsController.java
 */
public class BuchungsController extends AbstractBuchungsController {
	private BuchungsProcess process = null;

	private Buchungspanel buchungspanel = null;
	private ClientAreaPanel clpanel = null;

	
	
	/** 
	 * der Tag, f�r den die letzte Buchung erfasst wurde. Wird bei
	 * Folge-Erfassungen wiederverwendet
	 */
	private int letzterBuchungsTag;

	/**
	 * Constructor for BuchungsController.
	 */
	public BuchungsController(BuchungsProcess p) {
		super();
		this.process = p;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		logger.debug("start Controller");
		initializeBuchungsTag();		

//		try{


			buchungspanel = new Buchungspanel(this, null, true, process.getBuchungsUebersichtTableModel());
			mainPanel = buchungspanel.getBuchungspanelMain();
			
			initializeAspekts();
			initializePanel();
							
			buchungspanel.setVisible(true);

			clpanel = ClientAreaProvider.getClientArea(Konstanten.CLIENT_AREA_FORM);
			clpanel.add(buchungspanel, "Buchen");
			buchungspanel.setFocus();
//		}catch(PersistenceException e){
//			logger.error("Fehler beim Lesen der Buchungen", e);
//		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
		logger.debug("reactivate");

		clpanel.activate(buchungspanel);
		buchungspanel.setFocus();
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		logger.debug("destroy()");
		super.destroy();
		clpanel.remove(buchungspanel);
		buchungspanel.setVisible(false);
		buchungspanel.resetListener(process.getBuchungsUebersichtTableModel());
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}


	/**
	 * initialisiert das Buchungspanel
	 */
	private void initializePanel(){
		mainPanel.tfTag.setText(Integer.toString(letzterBuchungsTag));
		mainPanel.tfNummer.setText(Integer.toString(process.getNaechsteBuchungsNummer()));
		mainPanel.tfKontoAlphaNum.setText("");
		mainPanel.tfKontoBezeichnung.setText("");
		mainPanel.tfZusatz.setText("");
		mainPanel.tfKostenstelle.setText("");
		mainPanel.tfKostenstellenBezeichnung.setText("");
		mainPanel.tfBetrag.setBetrag(new Waehrungsbetrag(0));
	}

	/**
	 * initialisiert den letztenBuchungstag mit dem aktuellen Tagesdatum
	 */
	private void initializeBuchungsTag(){
		Calendar cal = GregorianCalendar.getInstance();
		letzterBuchungsTag = cal.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * behandelt den AbbrechenEvent
	 */
	public void handleAbbrechenEvent(){
		process.rollback();
	}


	/**
	 * behandelt den OK-Event
	 */
	public void handleOkEvent(){
		try{
			if(!mainPanel.tfKontoAlphaNum.isNumeric()){
				Fehlermeldung m = new Fehlermeldung(0, "Bitte eine g�ltige Kontonummer eingeben!", Buchungssatz.ASPEKT_KONTO);
				throw new PlausibilitaetsException(m);
			}
			
			process.createBuchungssatz(
				mainPanel.tfTag.getIntegerValue(),
				mainPanel.tfKontoAlphaNum.getIntegerValue(),
				mainPanel.tfZusatz.getText(),
				mainPanel.tfKostenstelle.getIntegerValue(),
				mainPanel.tfBetrag.getBetrag(),
				(SollHaben) mainPanel.cbSH.getSelectedItem()
				);

			
			
			letzterBuchungsTag = mainPanel.tfTag.getIntegerValue();
			playSound();			
			initializePanel();
			mainPanel.setFocus();
		}catch(PlausibilitaetsException e){
			Fehlermeldung m = e.getMeldungsListe().get(0);
			if(m != null){
				// Object[] options = new Object[]{ new VaButton("Ok Ok")};
				
				// JOptionPane.showOptionDialog(buchungspanel, m.getMessage(), "Fehlerhafte Eingabe", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, null);
				
				JOptionPane.showMessageDialog(buchungspanel, m.getMessage(), "Fehlerhafte Eingabe", JOptionPane.INFORMATION_MESSAGE);
				
				focusOnAspekt(m.getAspekt());
			}
			
		}catch(PersistenceException e){
			logger.error("Fehler", e);
			showErrorMessage("Fehler bei der Speicherung des Buchungssatzes aufgetreten!");
			process.rollback();
		}
	}

	
	public void handleViewClosedEvent(Object source){
		closeKontoListe();
		mainPanel.tfKontoAlphaNum.requestFocusInWindow();

	}
	
	/**
	 * @see de.diakon.buchung.prozesse.AbstractBuchungsController#automatischeKontenListePossible()
	 */
	protected boolean automatischeKontenListePossible() {
		return true;
	}


	protected void createKontoListeDialog()throws PersistenceException{
		if(kontoListeDialog == null){
			kontoListeDialog = new KontoListeDialog(ApplicationContext.getMainApplicationWindow(), false,this, getCurrentKontenListe());
		}
	}
}
