package de.diakon.buchung.prozesse;

import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.*;
import de.vahrst.application.types.Fehlermeldung;

/**
 * Superklasse f�r die Buchungsprozesse zur Neuanlage
 * und Pflege. 
 * @author Thomas
 * @version 
 */
public abstract class AbstractBuchungsProcess extends AbstractProcess {
	protected KassenKontext kassenKontext;
	protected KontoHome kontoHome;
	protected KostenstelleHome kstHome;

	/**
	 * Constructor for AbstractBuchungsProcess.
	 * @param parent
	 * @param eth
	 */
	public AbstractBuchungsProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		KassenKontext kontext) {
		super(parent, eth);
		this.kassenKontext = kontext;
		this.kontoHome = new KontoHome();
		this.kstHome = new KostenstelleHome();
	}

	/**
	 * �berpr�ft die Plausi-Regeln
	 */
	protected void checkPlausis(int tag, Konto kto, Kostenstelle kst, String zusatztext) throws PlausibilitaetsException, PersistenceException{
		Buchungsperiode p = kassenKontext.getAktuelleBuchungsperiode();
		p.checkBuchungstag(tag);
		
		if(kto == null){
			Fehlermeldung m = new Fehlermeldung(0, "Bitte eine g�ltige Kontonummer eingeben!", Buchungssatz.ASPEKT_KONTO);
			throw new PlausibilitaetsException(m);
		}
		
		if(kto.isKostenstelleErforderlich()){
			if(kst == null){
				Fehlermeldung m = new Fehlermeldung(0, "F�r dieses Konto muss eine Kostenstelle vorgegeben werden!", Buchungssatz.ASPEKT_KST);
				throw new PlausibilitaetsException(m);
			}
		}
		
		if(zusatztext.length() > 30){
			Fehlermeldung m = new Fehlermeldung(0, "Zusatztext darf max. 30 Zeichen lang sein.", Buchungssatz.ASPEKT_ZUSATZTEXT);
			throw new PlausibilitaetsException(m);
		}
		
	}
	
	
	public KontenListe findKontenByName(String name)throws PersistenceException{
		return kontoHome.findKontenByName(name);		
	}


}
