package de.diakon.buchung.prozesse;


import javax.swing.JOptionPane;

import de.diakon.buchung.gui.*;
import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.types.*;

/**
 * Controller f�r BuchungsPflegeProzess. �ffnet ein Dialogfenster zur
 * Pflege einer einzelnen Buchung.
 * @author Thomas
 * @version 
 * @file BuchungsPflegeController.java
 */
public class BuchungsPflegeController extends AbstractBuchungsController {
	private BuchungsPflegeProcess process;
	private BuchungsPflegeDialog dialog;
	
	/**
	 * Constructor for BuchungsPflegeController.
	 */
	public BuchungsPflegeController(BuchungsPflegeProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		dialog = new BuchungsPflegeDialog(ApplicationContext.getMainApplicationWindow(), true, this, "Buchung korrigieren");
		
		mainPanel = dialog.getMainPanel();
			
		initializeAspekts();
		initializePanel();		
		
		mainPanel.setFocus();
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		super.destroy();
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * initialisiert das Buchungspanel
	 */
	private void initializePanel(){
		Buchungssatz satz =  process.getBuchungssatz();
		mainPanel.tfTag.setIntegerValue(satz.getTag());
		mainPanel.tfNummer.setText(Integer.toString(satz.getLfdNummer()));
		mainPanel.tfKontoAlphaNum.setIntegerValue(satz.getKontonummer());
		mainPanel.tfKontoBezeichnung.setText(satz.getKontoBezeichnung());
		mainPanel.tfZusatz.setText(satz.getZusatztext());
		if(satz.getKostenstelle() != null){
			mainPanel.tfKostenstelle.setIntegerValue(satz.getKostenstellenNummer());
			mainPanel.tfKostenstellenBezeichnung.setText(satz.getKostenstelle().getBezeichnung());
		}
		mainPanel.tfBetrag.setBetrag(satz.getBetrag());
		mainPanel.cbSH.setSelectedItem(satz.isSoll()? SollHabenHome.SOLL: SollHabenHome.HABEN);
		
		if(!satz.getKonto().isKostenstelleErforderlich()){
			mainPanel.tfKostenstelle.setEnabled(false);
		}
	
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

	/**
	 * @see de.diakon.buchung.prozesse.AbstractBuchungsController#automatischeKontenListePossible()
	 */
	protected boolean automatischeKontenListePossible() {
		return false;
	}



	// handler
	public void handleAbbrechenEvent(){
		process.rollback();
	}

	public void handleOkEvent(){
		try{
			process.updateBuchungssatz(
				mainPanel.tfTag.getIntegerValue(),
				mainPanel.tfKontoAlphaNum.getIntegerValue(),
				mainPanel.tfZusatz.getText(),
				mainPanel.tfKostenstelle.getIntegerValue(),
				mainPanel.tfBetrag.getBetrag(),
				(SollHaben) mainPanel.cbSH.getSelectedItem()
				);
			
			playSound();
			process.commit();
			
		}catch(PlausibilitaetsException e){
			Fehlermeldung m =  e.getMeldungsListe().get(0);
			if(m != null){
				JOptionPane.showMessageDialog(dialog, m.getMessage(), "Fehlerhafte Eingabe", JOptionPane.INFORMATION_MESSAGE);
				focusOnAspekt(m.getAspekt());
			}
			
		}catch(PersistenceException e){
			logger.error("Fehler", e);
			showErrorMessage("Fehler bei der Speicherung des Buchungssatzes aufgetreten!");
			process.rollback();
		}
			
	}
	
	public void handleViewClosedEvent(Object source){
		logger.debug("view closed: " + source);
		if(source == dialog){
			process.rollback();
		}
		
		if(source == kontoListeDialog){
			// nichts tun
		}
	}

	protected void createKontoListeDialog()throws PersistenceException{
		if(kontoListeDialog == null){
			kontoListeDialog = new KontoListeDialog(dialog, false,this, getCurrentKontenListe());
		}
	}

}
