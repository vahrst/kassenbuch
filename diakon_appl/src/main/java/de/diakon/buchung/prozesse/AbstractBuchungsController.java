package de.diakon.buchung.prozesse;


import de.diakon.buchung.gui.*;
import de.diakon.entities.*;
import de.diakon.global.Konstanten;
import de.diakon.main.PropertiesManager;
import de.diakon.persistence.PersistenceException;
import de.diakon.sound.SoundManager;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.AbstractController;

/**
 * Abstrakte Oberklasse f�r Buchungscontroller
 * @author Thomas
 * @version 
 * @file AbstractBuchungsController.java
 */
public abstract class AbstractBuchungsController extends AbstractController {
	protected KontoHome kontoHome = null;
	protected KostenstelleHome kstHome = null;

	protected BuchungspanelMain mainPanel = null;
	protected KontoListeDialog kontoListeDialog = null;

	protected KontenListe selektierteKontenListe = null;

	/**
	 * wenn wir im Konto-Feld sind und die Kontoliste
	 * automatisch aufpoppt, und diese dann wieder geschlossen
	 * wird, darf der neue FocusGained Event im Kontoeingabefeld
	 * nicht erneut zu einem Aufpoppen der Kontoliste f�hren. Daf�r
	 * gibt's dieses Flag
	 */
	protected boolean kontolisteAllreadyOpened = false;


	/**
	 * Constructor for AbstractBuchungsController.
	 */
	public AbstractBuchungsController() {
		super();
		this.kontoHome = new KontoHome();
		this.kstHome = new KostenstelleHome();
	
	}
	
	protected void playSound(){
		if("true".equals(PropertiesManager.getProperty(Konstanten.PROPS_KLAENGE))){
			SoundManager.getInstance().playClip("/sounds/kasse.wav");
		}
	}
	


	protected void closeKontoListe(){
		if(kontoListeDialog != null){
			kontoListeDialog.setVisible(false);
			kontoListeDialog.dispose();
			kontoListeDialog = null;
		}
		selektierteKontenListe = null;	
	}

	

	/**
	 * liefert die aktuelle Kontenliste f�r den Auswahldialog. Dies ist entweder
	 * die Gesamtliste aller Konten oder die eingeschr�nkte Liste nach Bezeichnung.
	 * @return
	 * @throws PersistenceException
	 */
	protected KontenListe getCurrentKontenListe()throws PersistenceException{
		if(selektierteKontenListe != null){
			return selektierteKontenListe;
		}else{
			return kontoHome.getAllKonten(); 
		}
	}



	/**
	 * Im Kontofeld wurde eine Nummer eingegeben, entweder direkt, oder �ber
	 * die Kontoliste. Hier wird die Eingabe validiert und die abh�ngigen
	 * Felder vorbereitet.
	 * Falls das Eingabefeld alphanumerisch ist, wird versucht, ein passendes
	 * Konto zu �ber den Text zu suchen. Folgende M�glichkeiten:
	 * <ol>
	 * <li> kein Treffer<br>
	 *      In diesem Fall wir das KontonummernFeld so gelassen, wie es ist und
	 *      im Kontobezeichungsfeld 'Konto unbekannt' ausgegeben.
	 * <li> ein Treffer<br>
	 *      die entsprechende Kontonummer wird im Eingabefeld eingetragen
	 * <li> mehrere Treffer<br>
	 *      Eine Auswahlliste mit den passenden Treffern wird angezeigt
	 * </ol> 
	 * @exception PersistenceException
	 * @return boolean true, wenn eine Kontoliste ge�ffnet wurde.
	 */
	protected boolean checkKontonummerEingabe() throws PersistenceException{
		Konto kto = null;
		boolean kontolisteGeoeffnet = false;
		if(mainPanel.tfKontoAlphaNum.isNumeric()){
		
			int ktoNummer = mainPanel.tfKontoAlphaNum.getIntegerValue();
			kto = kontoHome.getKontoByKontonummerNotNull(ktoNummer);

			// und jetzt anhand der Kontodaten die Eingabefelder vorbelegen oder
			// sperren
			// erstmal die Kontobezeichnung ausgeben:
		}else{
			// die Eingabe ist alpahnumerisch
			String suchstring = mainPanel.tfKontoAlphaNum.getText();
			KontenListe liste = ((AbstractBuchungsProcess) getProcess()).findKontenByName(suchstring);
			
			kto = kontoHome.getKontoByKontonummerNotNull(-1);
			if(liste.getRowCount() == 1){
				kto = liste.getKontoAt(0);
				mainPanel.tfKontoAlphaNum.setIntegerValue(kto.getKontonummer());
			}
			if(liste.getRowCount() > 1){
				selektierteKontenListe = liste;
				handleKontoSucheEvent();
				kontolisteGeoeffnet = true;				
		
			}
		}
		mainPanel.tfKontoBezeichnung.setText(kto.getBezeichnung());
					
		// dann das Kostenstellenfeld en/disablen:
		if(kto.isKostenstelleErforderlich()){
			mainPanel.tfKostenstelle.setEnabled(true);
		}else{
			mainPanel.tfKostenstelle.setEnabled(false);
			mainPanel.tfKostenstelle.setText("");
			mainPanel.tfKostenstellenBezeichnung.setText("");
		}
			
		// und die S/H Vorgabe richtig setzen:
		mainPanel.cbSH.setSelectedItem(kto.getSollHabenVorgabe());

		// wenn wir nicht explizit wieder eine Kontoliste ge�ffnet haben
		// (aufgrund von alphanum. Eingaben)
		// wird jetzt das Flag f�r die Kontoliste auf false gesetzt.
		if(!kontolisteGeoeffnet)
			kontolisteAllreadyOpened = false;
			
		return kontolisteGeoeffnet;
	}

	/**
	 * behandelt Auswahl-Event in der KontoListe
	 */
	public void handleKontoGeheZuEvent(){
		int selRow = kontoListeDialog.getSelectedRow();
		
		try{
			Konto kto = null;
			if(selektierteKontenListe == null){
				kto = kontoHome.getAllKonten().getKontoAt(selRow);
			}else{
				kto = selektierteKontenListe.getKontoAt(selRow);
			}
			mainPanel.tfKontoAlphaNum.setIntegerValue(kto.getKontonummer());
			checkKontonummerEingabe();
			mainPanel.tfZusatz.requestFocusInWindow();
		}catch(PersistenceException e){
			showErrorMessage("Fehler beim Lesen des ausgew�hlten Kontos");
		}finally{
			closeKontoListe();
		}
	}

	/**
	 * f�r die Erzeugung des KontoListe-Dialogs verantwortlich
	 * @throws PersistenceException
	 */
	protected abstract void createKontoListeDialog()throws PersistenceException;

	public void handleKontoSucheEvent(){
		logger.debug("Event: kontoSuche");	
		try{
			createKontoListeDialog();
			kontoListeDialog.setVisible(true);
		}catch(PersistenceException e){
			showErrorMessage("Fehler beim Lesen der Kontodaten");
		}
	}


	/**
	 * initialisiert die Zuordnungstabelle von Aspekten zu 
	 * Eingabefeldern
	 */
	protected void initializeAspekts(){
		aspektTabelle.put(Buchungssatz.ASPEKT_TAG, mainPanel.tfTag);
		aspektTabelle.put(Buchungssatz.ASPEKT_KONTO, mainPanel.tfKontoAlphaNum);
		aspektTabelle.put(Buchungssatz.ASPEKT_KST, mainPanel.tfKostenstelle);
		aspektTabelle.put(Buchungssatz.ASPEKT_ZUSATZTEXT, mainPanel.tfZusatz);
	}



	/**
	 * behandelt FocusGained im KontoFeld
	 */
	public void handleTfKontoFocusGainedEvent(){
		ApplicationContext.statusmeldung("F11 f�r Konto-Liste");
		if("true".equalsIgnoreCase(PropertiesManager.getProperty(Konstanten.PROPS_KONTOLISTE))){
			if(automatischeKontenListePossible() && !kontolisteAllreadyOpened){
				handleKontoSucheEvent();
				kontolisteAllreadyOpened = true;
			}
				
		}	
	}



	/**
	 * behandelt FocusLost im KontoFeld
	 */
	public void handleTfKontoFocusLostEvent(){
		ApplicationContext.statusmeldung("");
		boolean kontolisteGeoeffnet = false;
		try{
			kontolisteGeoeffnet = checkKontonummerEingabe();
		}catch(PersistenceException e){
			logger.error("Fehler", e);
		}

		// Bei Eingabe eines alphanumerischen Textes in das Kontonummern-
		// Feld wird evtl. eine Liste ge�ffnet. Wenn das der Fall ist darf diese
		// nicht sofort wieder geschlossen werden. Diese Zeile ist nur dann erlaubt,
		// wenn �ber die F11 Taste eine KontoListe angefordert wurde.
		if(!kontolisteGeoeffnet)
			closeKontoListe();
	}



	/**
	 * behandelt FocusLost im Kostenstellen-Feld
	 */
	public void handleTfKstFocusLostEvent(){
		int kstNummer = mainPanel.tfKostenstelle.getIntegerValue();
		try{
			Kostenstelle kst = kstHome.getKostenstelleByKostenstellenummer(kstNummer);
			if(kst != null)
				mainPanel.tfKostenstellenBezeichnung.setText(kst.getBezeichnung());
			else{
				//Toolkit.getDefaultToolkit().beep();
				mainPanel.tfKostenstellenBezeichnung.setText("");
			}
		}catch(PersistenceException e){
			logger.error("Fehler!", e);
		}
		
	}
	

	/**
	 * ist eine automatische Anzeige der Kontoliste �berhaupt gew�nscht
	 * (unterschied zwischen Neuanlage und Pflege !)
	 */
	protected abstract boolean automatischeKontenListePossible();


	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(kontoListeDialog != null){
			kontoListeDialog.setVisible(false);
			kontoListeDialog.dispose();
			kontoListeDialog = null;
		}
		
	}

}
