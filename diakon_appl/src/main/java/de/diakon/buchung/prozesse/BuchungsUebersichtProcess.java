package de.diakon.buchung.prozesse;

import java.util.List;

import javax.swing.table.TableModel;

import de.diakon.entities.Buchungssatz;
import de.diakon.entities.KassenKontext;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.DefaultEndTransactionHandler;
import de.vahrst.application.prozesse.EndTransactionHandler;

/**
 * Prozess-Komponente f�r die Anzeige der Liste aller Buchungen im
 * aktuellen Monat 
 * @author Thomas
 * @version 
 * @file BuchungsUebersichtProcess.java
 */
public class BuchungsUebersichtProcess extends AbstractProcess {
	private BuchungsUebersichtController controller;
	private KassenKontext kassenKontext;
	
	
	/**
	 * Constructor for BuchungsUebersichtProcess.
	 * @param parent
	 * @param eth
	 */
	public BuchungsUebersichtProcess(
			AbstractProcess parent,
			EndTransactionHandler eth,
			KassenKontext kontext) {

		super(parent, eth);
		this.kassenKontext = kontext;
		openController();
		
	}

	private void openController(){
		controller = new BuchungsUebersichtController(this);
		controller.start();
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	/**
	 * liefert die aktuelle Buchungsliste
	 */
	public TableModel getBuchungsListe()throws PersistenceException{
		return kassenKontext.getBuchungslisteTableModel();
	}

	/**
	 * starten den Prozess zur Pflege eines Buchungssatzes
	 */
	public void startBuchungssatzPflege(int selectedRow) throws PersistenceException{
		
		// IndexOutOfBounds Exception vermeiden. Diese war zweimal in den Errorlogfiles 
		// enthalten, obwohl mir nicht ganz klar ist, wie der User das hinbekommen hat.
		List<Buchungssatz> bsList = kassenKontext.getAktuelleBuchungsperiode().getBuchungssaetze();
		if(selectedRow >=0 && selectedRow < bsList.size()){
			Buchungssatz bs = bsList.get(selectedRow);
			logger.debug("starte BuchungssatzPflege f�r: " + bs);
		
			new BuchungsPflegeProcess(this, new DefaultEndTransactionHandler(), kassenKontext, bs);
		}
	}

	public boolean hasFehlermeldungen(){
		return kassenKontext.getAktuelleBuchungsperiode().hasFehlermeldungen();
	}
	
	public String[] getFehlermeldungen(){
		return kassenKontext.getAktuelleBuchungsperiode().getFehlermeldungen();
	}

}
