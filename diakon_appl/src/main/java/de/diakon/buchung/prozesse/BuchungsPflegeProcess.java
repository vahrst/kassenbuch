package de.diakon.buchung.prozesse;

import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.*;
import de.vahrst.application.types.*;

/**
 * Prozess-Komponente zur Pflege einer einzelnen Buchung.
 * @author Thomas
 * @version 
 * @file BuchungsPflegeProcess.java
 */
public class BuchungsPflegeProcess extends AbstractBuchungsProcess {
	private BuchungsPflegeController controller;
	
	private Buchungssatz buchungssatz;
	/**
	 * Constructor for BuchungsPflegeProcess.
	 * @param parent
	 * @param eth
	 */
	public BuchungsPflegeProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		KassenKontext kontext,
		Buchungssatz satz) {

		super(parent, eth, kontext);
		this.buchungssatz = satz;
		
		openController();
	}
	
	private void openController(){
		controller = new BuchungsPflegeController(this);
		controller.start();
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	/**
	 * liefert den zu korriegenden Buchungssatz
	 */
	public Buchungssatz getBuchungssatz(){
		return buchungssatz;
	}

	/**
	 * aktualisiert den Buchungssatz.
	 */
	public void updateBuchungssatz(
		int tag,
		int ktonummer,
		String zusatztext,
		int kostenstelle,
		Waehrungsbetrag betrag,
		SollHaben sh)
		throws PersistenceException, PlausibilitaetsException {

		// wir erzeugen erstmal eine neue Buchungssatz-Instanz, weil die
		// die interne Buchungssatz-Variable noch von den Listen refernziert wird
		// und bei einer Plausi-Verletzung bereits die Daten ver�ndert wurden.

		Konto kto = kontoHome.getKontoByKontonummer(ktonummer);
		Kostenstelle kst = kstHome.getKostenstelleByKostenstellenummer(kostenstelle);
		
		checkPlausis(tag, kto, kst, zusatztext);

		Buchungssatz satz = new Buchungssatz( 
			buchungssatz.getLfdNummer(),
			tag,
			kto,
			zusatztext,
			kst,
			betrag,
			sh);
		
		kassenKontext.updateBuchungssatz(satz);
		log.info("Buchung ge�ndert: " + satz.toString());

	}

}
