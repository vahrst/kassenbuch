package de.diakon.buchung.prozesse;

import javax.swing.table.TableModel;

import de.diakon.buchung.gui.BuchungsUebersichtPanel;
import de.diakon.global.Konstanten;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.*;

/**
 *
 * @author Thomas
 * @version 
 * @file BuchungsUebersichtController.java
 */
public class BuchungsUebersichtController extends AbstractController {
	private BuchungsUebersichtProcess process;
	private BuchungsUebersichtPanel uebersichtPanel;

	/**
	 * Constructor for BuchungsUebersichtController.
	 */
	public BuchungsUebersichtController(BuchungsUebersichtProcess p) {
		super();
		this.process = p;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		logger.debug("start Buchungs-Übersicht");
		try{
			TableModel tm = process.getBuchungsListe();
			uebersichtPanel = new BuchungsUebersichtPanel(tm, this);
			
			ClientAreaProvider.getClientArea(Konstanten.CLIENT_AREA_FORM).add(uebersichtPanel, "Buchungsübersicht");
			
		}catch(PersistenceException e){
			showErrorMessage("Fehler beim Lesen der Buchungssätze aufgetreten");
		}
		
		if(process.hasFehlermeldungen()){
			String[] fehlermeldungen = process.getFehlermeldungen();
			for(int i=0;i<fehlermeldungen.length;i++){
				showErrorMessage(fehlermeldungen[i]);
			}
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
		logger.debug("reactivate Buchungs-Übersicht");	
		ClientAreaProvider.getClientArea(Konstanten.CLIENT_AREA_FORM).activate(uebersichtPanel);
		
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		ClientAreaProvider.getClientArea(Konstanten.CLIENT_AREA_FORM).remove(uebersichtPanel);
		uebersichtPanel.setVisible(false);
		uebersichtPanel = null;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}


	// die Event-Handler
	public void handleGeheZuEvent(){
		int row = uebersichtPanel.getSelectedRow();
		
		try{
			process.startBuchungssatzPflege(row);
		}catch(PersistenceException e){
			showErrorMessage("Fehler beim Lesen der ausgewählten Buchung");
		}
	}
	

}
