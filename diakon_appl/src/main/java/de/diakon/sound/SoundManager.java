package de.diakon.sound;

import java.io.InputStream;

import javax.sound.sampled.*;

import org.apache.log4j.Logger;
/**
 * 
 * @author Thomas
 * @version 
 */
public class SoundManager {
	private static Logger logger = Logger.getLogger(SoundManager.class);
	private static SoundManager singleton;

	DataLine.Info kasseLineInfo;

	/**
	 * Constructor for SoundManager.
	 */
	public SoundManager() {
		super();
		loadClips();
	}

	public static void initialize(){
		singleton = new SoundManager();
	}
	
	/**
	 * l�dt die Audioclips in einem eigenen Thread
	 */
	private void loadClips(){

		
	}

	public static SoundManager getInstance(){
		if(singleton == null)
			initialize();
			
		return singleton;
	}


	public void playClip(String resource){
		logger.debug("start playing Clip");
		long start = System.currentTimeMillis();
		try{
			InputStream in = this.getClass().getResourceAsStream(resource);
			if(in == null){
				System.out.println("Resource nicht gefunden");
			}else{
				AudioInputStream audioIn = AudioSystem.getAudioInputStream(in);
				if (audioIn != null){
					DataLine.Info info = new DataLine.Info(Clip.class, audioIn.getFormat());
					Clip clip = (Clip) AudioSystem.getLine(info);
					clip.open(audioIn);
					clip.loop(0);
					audioIn.close();
				}
			}
		}catch(Exception e){
			logger.error(e);
		}
		logger.debug("ende playing, " + (System.currentTimeMillis()-start));
	}		
		
		
	
	public static void main(String[] args){
		getInstance().playClip("/sounds/kasse.wav");
		try{Thread.sleep(1000);}catch(Exception  e){}
		getInstance().playClip("/sounds/kasse.wav");
	}
}
