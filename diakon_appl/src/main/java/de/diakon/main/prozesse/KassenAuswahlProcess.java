package de.diakon.main.prozesse;

import java.util.List;

import de.diakon.entities.Kasse;
import de.diakon.entities.KasseHome;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.EndTransactionHandler;

/**
 * Prozess f�r Kasse-Wechsel / _Auswahl
 * @author Thomas
 * @version 
 * @file KasseAuswahlProcess.java
 */
public class KassenAuswahlProcess extends AbstractProcess implements KassenAuswahl{

	private KassenAuswahlController controller = null;


	/** die Id der neu angelegten Kasse */
	private long kassenId = 0;

	/**
	 * Constructor for KasseAuswahlProcess.
	 * @param parent
	 * @param eth
	 */
	public KassenAuswahlProcess(
		AbstractProcess parent,
		EndTransactionHandler eth) {
		super(parent, eth);
		
		openController();
	}
	
	/**
	 * startet den Controller 
	 */
	private void openController(){
		controller = new KassenAuswahlController(this);
		controller.start();
	}
	

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	/**
	 * liefert - im commit-Fall - die Id der neu angelegten Kasse
	 */
	public long getKassenId(){
		return kassenId;
	}

	/**
	 * Liefert die Liste mit den verf�gbaren Kassen
	 */
	public List<Kasse> getKassenListe() throws PersistenceException{
		return new KasseHome().getAllKassen();		
	}
	
	/**
	 * w�hlt die vorgegebene Kasse aus und committed diese Prozess
	 */
	public void selectKasse(Kasse k){
		kassenId = k.getObjectId();
		commit();
	}
		
		
}
