package de.diakon.main.prozesse;

import java.util.List;

import de.diakon.entities.EntityListe;
import de.diakon.entities.Kasse;

/**
 * 
 * @author Thomas
 * @version 
 * @file KassenTableModel.java
 */
public class KassenAuswahlTableModel extends EntityListe<Kasse> {

	/**
	 * Constructor for KassenTableModel.
	 */
	public KassenAuswahlTableModel(List<Kasse> kassenListe) {
		super();
		this.entities = kassenListe;
		
	}

	/**
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return 1;
	}

	/**
	 * @see javax.swing.table.TableModel#getColumnClass(int)
	 */
	public Class<?> getColumnClass(int columnIndex) {
		switch(columnIndex){
			case 0: return String.class;
		}
		return null;
	}

	/**
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		Kasse k = entities.get(rowIndex);
		switch(columnIndex){
			case 0: return k.getName();
		}
		
		return null;
	}

}
