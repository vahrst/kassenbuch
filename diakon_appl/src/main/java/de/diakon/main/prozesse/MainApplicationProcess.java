package de.diakon.main.prozesse;

import java.awt.Component;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Properties;

import javax.swing.SwingUtilities;

import de.diakon.abschluss.prozesse.*;
import de.diakon.buchung.prozesse.BuchungsProcess;
import de.diakon.buchung.prozesse.BuchungsUebersichtProcess;
import de.diakon.diagnose.prozesse.DBDiagnoseProcess;
import de.diakon.entities.KassenKontext;
import de.diakon.global.Konstanten;
import de.diakon.main.PropertiesManager;
import de.diakon.persistence.PersistenceException;
import de.diakon.persistence.PersistenceManager;
import de.diakon.stammdaten.prozesse.ExportProcess;
import de.diakon.stammdaten.prozesse.KassenAnlageProcess;
import de.diakon.stammdaten.prozesse.KassenPflegeProcess;
import de.diakon.stammdaten.prozesse.KontenPflegeProcess;
import de.diakon.stammdaten.prozesse.KostenstellenPflegeProcess;
import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.DefaultEndTransactionHandler;
import de.vahrst.application.prozesse.EndTransactionHandler;
/**
 * Die Zentrale Anwendungsklasse.
 * @author Thomas
 * @version 
 */
public class MainApplicationProcess extends AbstractProcess{

	private MainApplicationController controller = null;

	private KassenKontext kassenKontext = new KassenKontext();  // init: leerer Kassenkontext, um NPE zu vermeiden.
	
	// Referenzen auf die m�glichen Sub-Processe, die von
	// diesem Process gestartet werden k�nnen.
	private KontenPflegeProcess kontenPflegeProcess = null;
	private KostenstellenPflegeProcess kostenstellenPflegeProcess = null;
	private BuchungsUebersichtProcess buchungsUebersichtProcess = null;
	private BuchungsProcess buchungsProcess = null;



    // die EndTransactionHandler-Klassen
	
	class KassenAuswahlETH extends DefaultEndTransactionHandler{
		public void commit(AbstractProcess source){
			source.destroy();
			commitKassenAuswahl((KassenAuswahl) source);		
		}	
	}

	class AutoBackupETH extends DefaultEndTransactionHandler{
		public void commit(AbstractProcess source){
			source.destroy();
			System.exit(0);
		}
	}
	
	
	class BuchenETH extends DefaultEndTransactionHandler{
		public void commit(AbstractProcess source){
			source.destroy();
			buchungsProcess = null;
		}
		public void rollback(AbstractProcess source){
			source.destroy();
			buchungsProcess = null;
			startBuchungsUebersicht();
		}
	}

	class KassenPflegeETH extends DefaultEndTransactionHandler{
		@Override
		public void commit(AbstractProcess source) {
			source.destroy();
			kassenKontext.kasseAktualisiert();
		}
	}
	

	class MonatswechselETH extends DefaultEndTransactionHandler{
		public void commit(AbstractProcess source){
			source.destroy();
			// im Commit-Fall m�ssen die Actions im Controller (insb. 
			// 'Korrektur Vormonat' aktualisiert werden
			controller.updateActions();	
		}
	}
	
	/**
	 * Constructor for MainApplicationProcess.
	 */
	public MainApplicationProcess() {
		super(null, new DefaultEndTransactionHandler());

/*		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(Exception e){
		}
*/		
		log.info("Kassenprogramm gestartet");
		openController();
		// einige Initialisierungen vornehmen. Wir beginnen
		// mit Step 1. 
		init00(); // checkProperties. 
		
		showMainView();
		
		// Buchungs�bersicht erst nach mainview �ffnen, weil diese evtl.
		// Dialoge hochpoppt. Und ohne die MainView w�rden diese Meldungsdialoge
		// schlecht wirken.
		post_init00();
		
	}



	public static void main(String[] args) {

		
		new MainApplicationProcess();
		
	}

	/**
	 * erzeugt das korrespondieren Controller-Objekt
	 */
	private void openController(){

		if(controller != null) return;

		logger.debug("openController");
		
		controller = new MainApplicationController(this);
		controller.start();
	}

	/**
	 * hier wird das Hauptanwendungsfenster sichtbar gemacht
	 */
	private void showMainView(){
		controller.reactivate();
	}

	/**
	 * Vorbereitungen f�r KeyboardFocusManager
	 */
	private void init00(){
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(
			new KeyEventDispatcher(){
				public boolean dispatchKeyEvent(KeyEvent e){
					if(e.getKeyCode() == KeyEvent.VK_ESCAPE && e.getID() == KeyEvent.KEY_RELEASED){
						Component root = SwingUtilities.getRoot(e.getComponent());
						if(root != null && root instanceof AbstractDialog){
							((AbstractDialog)root).closeView();
							return true;
						}
					}
					return false;
				}
			}
		);
		init01();
	}

	
	/**
	 * pr�ft auf runOnce Eintrag in den Properties
	 */
	private void init01(){
		RunOnce runOnce = new RunOnce();
		runOnce.run();
		
		init02();
	}
	

	/**
	 * pr�ft, ob die Tabellen angelegt sind. Wenn nicht
	 * wird das jetzt nachgeholt.
	 */
	private void init02(){
		try{
			PersistenceManager.getPersistenceManager().checkPersistenceLayer();
			init03();
		}catch(PersistenceException e){
			showMainView();
			String message = "Fehler beim Zugriff auf das Datenbank-System";
			logger.error(message, e);
			controller.showErrorMessage(message);
			System.exit(1);
		}
	}

	/**
	 * �ffnet die Kasse, die beim letzten Mal verwendet wurde
	 */
	private void init03(){
		long kassenId = PropertiesManager.getPropertyAsLong(Konstanten.PROPS_LAST_KASSE_ID);
		if(kassenId != -1){
			try{
				kassenKontext.setKasse(kassenId);
				controller.updateActions();
				log.info("Kasse geladen: " + kassenKontext.getKasse().toString());
			}catch(Exception e){
				logger.warn(e);
			}
		}	
	}




	/**
	 * Zum Schluss starten wir noch den Buchungs�bersichts-prozess
	 */
	private void post_init00(){
		if(kassenKontext.getKasse() != null)		
			startBuchungsUebersicht();
		
	}


	public AbstractController getController(){
		return controller;
	}
	
	/** 
	 * liefert den aktuellen KassenKontext
	 */
	public KassenKontext getKassenKontext(){
		return kassenKontext;		
	}
	

	/**
	 * startet den Prozess zur Neuanlage einer Kasse
	 */
	public void startKassenAnlage(){
		logger.info("Erzeuge KassenAnlageProcess");
		
		new KassenAnlageProcess (this, new KassenAuswahlETH()); 			
		
	}
	
	
	/**
	 * startet oder reaktiviert die Kontenpflege
	 */
	public void startKontenpflege(){
		// bereits gestartet?
		if(kontenPflegeProcess != null){
			kontenPflegeProcess.reactivate();
		}else{
			logger.info("Erzeuge Kontenpflege Prozess");
			kontenPflegeProcess = new KontenPflegeProcess(this, new DefaultEndTransactionHandler());	
			
		}
	}

	/**
	 * startet oder reaktiviert die Kontenpflege
	 */
	public void startKostenstellenpflege(){
		// bereits gestartet?
		if(kostenstellenPflegeProcess != null){
			kostenstellenPflegeProcess.reactivate();
		}else{
			logger.info("Erzeuge Kostenstellenpflege Prozess");
			kostenstellenPflegeProcess = new KostenstellenPflegeProcess(this, new DefaultEndTransactionHandler());	
			
		}
	}

	/**
	 * startet den Prozess zur Pflege der Properties
	 * bzw. der Einstellungen
	 */
	public void startPropertiespflege(boolean neuanlage){
		if(neuanlage){
			EndTransactionHandler eth = new DefaultEndTransactionHandler(){
				public void commit(AbstractProcess source){
					source.destroy();
					init02();	
				}
			};
			new PropertiesProcess(this, eth, neuanlage);

		}else{
			new PropertiesProcess(this, new DefaultEndTransactionHandler(), neuanlage);
		}
	}
	
	/**
	 * startet die Datensicherung
	 */
	public void startDatensicherung(){
		new DatensicherungProcess(this, new DefaultEndTransactionHandler(), false);
	}



	
	/**
	 * startet oder reaktiviert die Buchungs�bersicht
	 */
	public void startBuchungsUebersicht(){
		if(buchungsUebersichtProcess != null){
			buchungsUebersichtProcess.reactivate();
		}else{
			logger.info("Erzeuge Buchungs�bersicht Prozess");
			buchungsUebersichtProcess = new BuchungsUebersichtProcess(this, new DefaultEndTransactionHandler(), kassenKontext);
		}		
	}

	/**
	 * startet oder reaktiviert den Buchunsprozess
	 */
	public void startBuchen(){
		if(buchungsProcess != null){
			buchungsProcess.reactivate();
		}else{
			logger.debug("Erzeuge Buchungsprozess");
			buchungsProcess = new BuchungsProcess(this, new BuchenETH(), kassenKontext);
		}
	}
			
	/**
	 * startet den Druck des Kassenbuchs
	 */
	public void startKassenbuchdruck(){
		new DruckKassenbuchProcess(this, new DefaultEndTransactionHandler(), kassenKontext);
			
	}

	/**
	 * startet den Druck der Debitorenlisten
	 */
	public void startDebitorendruck(){
		new DruckDebitorenlisteProcess(this, new DefaultEndTransactionHandler(), kassenKontext);	
	}

	/**
	 * starten den Druck der Summenliste
	 */
	public void startSummenlistendruck(){
		new DruckSummenlisteProcess(this, new DefaultEndTransactionHandler(), kassenKontext);
	}

	/**
	 * startet den Prozess zur Auswahl einer Kasse
	 */
	public void startKassenWechsel(){
		logger.info("Erzeuge KassenAuswahlProcess");
		new KassenAuswahlProcess(this, new KassenAuswahlETH());
	}
		
	/**
	 * startet den Monatsabschluss f�r die aktuelle Kasse
	 */
	public void startMonatsabschluss(){
		logger.info("Starte Monatsabschluss");	
		new MonatsabschlussProcess(this, new MonatswechselETH(), kassenKontext);
	}

	public void startKorrekturVormonat(){
		logger.info("Starte Korrektur Vormonat");
		new KorrekturVormonatProcess(this, new MonatswechselETH(), kassenKontext);
	}

	public void startRepairVormonat() {
		logger.info("Fehlende Vormonatssalden reparieren");
		new ReparaturVormonatSaldenProcess(this, new MonatswechselETH(), kassenKontext);
	}


	public void startDBDiagnose(){
		logger.info("Starte Diagnose der Datenbank");
		new DBDiagnoseProcess(this, new DefaultEndTransactionHandler());
	}

	public void startExportStammdaten(){
		logger.info("Start Export der Stammdaten");
		new ExportProcess(this, new DefaultEndTransactionHandler());
	}

	public void startExportBuchungen(){
		logger.info("Start Export Buchungen");
		new ExportBuchungenProcess(this, new DefaultEndTransactionHandler(), kassenKontext);
	}
	
	/**
	 * beendet das Programm. Schlie�t vorher die Connection
	 */
	public void beenden(){
		PersistenceManager.getPersistenceManager().close();
		logger.debug("check Datensicherung");
		
		if(checkBackupOnExit()){
			return;
		}

		saveCurrentScreenPosition();
		
		log.info("Kassenprogramm beendet");
		System.exit(0);
	}

	
	/**
	 * sichert die aktuelle Position des Fensters auf der Oberfl�che.
	 */
	private void saveCurrentScreenPosition(){
		Properties props = PropertiesManager.getProperties();
		Point p = controller.getCurrentScreenPosition();
		if(p == null){
			props.put(Konstanten.PROPS_SCREEN_X, "-1");
			props.put(Konstanten.PROPS_SCREEN_Y, "-1");
		}else{
			props.put(Konstanten.PROPS_SCREEN_X, Integer.toString(p.x));
			props.put(Konstanten.PROPS_SCREEN_Y, Integer.toString(p.y));
		}
		try {
			PropertiesManager.storeProperties(props);
		} catch (IOException e) {
			logger.error("Fehler beim Schreiben der Properties", e);
		}
	}
	
	/**
	 * pr�ft, ob automatische Backup eingestellt ist. Wenn ja: Backup ausf�hren.
	 * Wenn nein: Frage auf Sicherung.
	 */
	private boolean checkBackupOnExit(){
		if(PropertiesManager.getPropertyAsBoolean(Konstanten.PROPS_AUTO_BACKUP)){
			new DatensicherungProcess(this, new AutoBackupETH(), true);
			return true;
		}
		return false;
	}
	
	
	public void test1(){
		logger.debug("Garbage Collection");
		System.gc();
		logger.debug("total Mem  : " + Runtime.getRuntime().totalMemory());
		logger.debug("free Memory: " + Runtime.getRuntime().freeMemory());
	}
	
	public void test2() {
//		new ReplayProcess(this, new DefaultEndTransactionHandler(), kassenKontext, filename);
	}


	// ---------------------------------------------------------------------
	// die lokalen commit-Methoden
	// ---------------------------------------------------------------------
	/**
	 * eine neue Kasse wurde angelegt oder ausgew�hlt. Jetzt die Main-Application aktualisieren.
	 */
	protected void commitKassenAuswahl(KassenAuswahl process){
		long id = process.getKassenId();
		
		if(id == kassenKontext.getKassenId())
			return;
		
		// und jetzt Kassenwechsel veranlassen:
		try{
			kassenKontext.setKasse(id);	
			log.info("Kasse ge�ffnet: " + kassenKontext.getKasse().toString());
			
			if(buchungsProcess != null){
				buchungsProcess.destroy();
				buchungsProcess = null;
			}

			// Falls die Buchungs�bersicht noch nicht gestartet war, holen
			// wir dies jetzt nach. Wenn sie bereits gestartet ist, wird sie
			// nach vorn geholt.
			startBuchungsUebersicht();

			controller.updateActions();

		}catch(PersistenceException e){
			controller.showErrorMessage("Fehler beim �ffnen der Kasse");
			logger.error("Fehler", e);
		}
		
	}



	public void startExportDatenbank() {
		logger.info("Start Export der Datenbank");
		new ExportDBProcess(this, new DefaultEndTransactionHandler());
		
	}


	/**
	 * startet die Kassenpflege
	 */
	public void startKassenPflege() {
		logger.info("Erzeuge KassenPflegeProcess");
		new KassenPflegeProcess(this, new KassenPflegeETH(), kassenKontext.getKasse()); 			
		
	}



}
