package de.diakon.main.prozesse;

/**
 * Gemeinsames Interface f�r Kassen Neuanlage und Kassenwechsel.
 * @author Thomas
 * @version 
 * @file KassenAuswahl.java
 */
public interface KassenAuswahl {
	
	/**
	 * liefert - im commit-Fall - die Id der neu angelegten Kasse
	 */
	public long getKassenId();
}
