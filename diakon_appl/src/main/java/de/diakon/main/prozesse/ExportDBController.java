package de.diakon.main.prozesse;

import java.io.File;

import javax.swing.JOptionPane;

import de.diakon.main.gui.ExportDBDialog;
import de.diakon.main.gui.ExportDBPanel;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;

/**
 * 
 * @author Thomas
 * @version 
 * @file ExportController.java
 */
public class ExportDBController extends AbstractController {
	private ExportDBProcess process;
	private ExportDBDialog dialog;
	/**
	 * Constructor for ExportController.
	 */
	public ExportDBController(ExportDBProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		dialog = new ExportDBDialog(ApplicationContext.getMainApplicationWindow(), true, this);

		// DAteiname vorbelegen.
		String filename = System.getProperty("user.home")+	System.getProperty("file.separator") + "database_export.xml";
		dialog.getMainPanel().getTfFilename().setText(filename);
		
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

	public void handleStartEvent(){
		ExportDBPanel panel = dialog.getMainPanel();
		String filename = panel.getTfFilename().getText();
		
		File f = new File(filename);
		if(f.isDirectory()){
			JOptionPane.showMessageDialog(dialog, "Die angegebene Datei ist ein Verzeichnis!");
			return;
		}
		
		if(f.exists()){
			int result = JOptionPane.showConfirmDialog(dialog, "Datei existiert bereits. Soll die Datei überschrieben werden?", "Datei bereits vorhanden", JOptionPane.YES_NO_OPTION);	
			if(result != 0)
				return;
		}

		try{
			process.exportDatabase(filename);
		}catch(Exception e){
			logger.error("Fehler beim Exportieren der Datenbank", e);
			showErrorMessage("Fehler beim Exportieren der Datenbank aufgetreten");
			process.rollback();
		}
		
		ApplicationContext.statusmeldung("Datenbank wurden exportiert");
		process.commit();
		
	}
	
	public void handleAbbrechenEvent(){
		process.rollback();	
	}


}
