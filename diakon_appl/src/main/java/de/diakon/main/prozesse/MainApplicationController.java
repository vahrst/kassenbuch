package de.diakon.main.prozesse;


import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import de.diakon.global.Konstanten;
import de.diakon.main.PropertiesManager;
import de.diakon.main.gui.AboutDialog;
import de.diakon.main.gui.MainApplicationWindow;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.gui.ClientAreaPanel;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.ClientAreaProvider;

/**
 * Controller für MainApplicationProcess. Steuert die
 * Präsentationsschicht.
 * @author Thomas
 * @version 
 */
public class MainApplicationController extends AbstractController{
	private MainApplicationProcess process = null;

	private ActionListener helpListener;
	
	/** Das von diesem Controller verwaltete Frame */
	private MainApplicationWindow window = null;
	
	private AboutDialog aboutDialog = null; 

	/**
	 * Constructor for MainApplicationController.
	 */
	public MainApplicationController(MainApplicationProcess p) {
		super();
		this.process = p;
		initializeActions();
	}


	/**
	 * initialisiert die Action-Objekte
	 */
	private void initializeActions(){
		logger.debug("initializeActions");
		try{
			createAction("Beenden", Konstanten.ACTION_BEENDEN);
			createAction("Datensicherung", Konstanten.ACTION_DATENSICHERUNG);
			createAction("Buchen", Konstanten.ACTION_BUCHEN);
			createAction("Uebersicht", Konstanten.ACTION_UEBERSICHT);
			createAction("Kassewechseln", Konstanten.ACTION_KASSEWECHSEL);
			createAction("Kasseanlegen", Konstanten.ACTION_KASSEANLEGEN);
			createAction("Kassepflegen", Konstanten.ACTION_KASSENPFLEGE);
			createAction("Kostenstellen", Konstanten.ACTION_KOSTENSTELLEN);
			createAction("Konten", Konstanten.ACTION_KONTEN);
			createAction("ExportStammdaten", Konstanten.ACTION_EXPORT_STAMMDATEN);
			createAction("About", Konstanten.ACTION_ABOUT);
			createAction("Hilfe", Konstanten.ACTION_HILFE);
			createAction("DBDiagnose", Konstanten.ACTION_DIAGNOSE);
			createAction("Properties", Konstanten.ACTION_PROPERTIES);
			createAction("Test1", Konstanten.ACTION_TEST1);
			createAction("Test2", Konstanten.ACTION_TEST2);
			createAction("Monatsabschluss", Konstanten.ACTION_MONATSABSCHLUSS);
			createAction("KorrekturVormonat", Konstanten.ACTION_VORMONAT);
			createAction("RepairVormonat", Konstanten.ACTION_REPAIR_VORMONAT);
			createAction("DruckKassenbuch", Konstanten.ACTION_DRUCK_KASSENBUCH);
			createAction("DruckDebitoren", Konstanten.ACTION_DRUCK_DEBITOREN);
			createAction("DruckSummenliste", Konstanten.ACTION_DRUCK_SUMMENLISTE);
			createAction("ExportBuchungen", Konstanten.ACTION_EXPORT_BUCHUNGEN);
			createAction("ExportDatenbank", Konstanten.ACTION_EXPORT_DB);
			
			updateActions();			
		}catch(NoSuchMethodException e){
			logger.fatal("Fehler bei Erzeugung der Action-Objekte", e);
			System.exit(9);
		}			
		
	}

	/**
	 * aktualisiert die Actions. Hier werden insbesondere die Actions, je
	 * nach Zustand des Kassenkontextes, dis oder enabled
	 */
	public void updateActions(){
		boolean kasseOpen = process.getKassenKontext().getKasse() != null;
		
		getAction(Konstanten.ACTION_BUCHEN).setEnabled(kasseOpen);		
		getAction(Konstanten.ACTION_UEBERSICHT).setEnabled(kasseOpen);		
		getAction(Konstanten.ACTION_MONATSABSCHLUSS).setEnabled(kasseOpen);		
		getAction(Konstanten.ACTION_VORMONAT).setEnabled(kasseOpen);		
		getAction(Konstanten.ACTION_DRUCK_DEBITOREN).setEnabled(kasseOpen);		
		getAction(Konstanten.ACTION_DRUCK_KASSENBUCH).setEnabled(kasseOpen);		
		getAction(Konstanten.ACTION_DRUCK_SUMMENLISTE).setEnabled(kasseOpen);		
		getAction(Konstanten.ACTION_EXPORT_BUCHUNGEN).setEnabled(kasseOpen);		
		getAction(Konstanten.ACTION_KASSENPFLEGE).setEnabled(kasseOpen);

		getAction(Konstanten.ACTION_VORMONAT).setEnabled(process.getKassenKontext().hasVormonat());
		getAction(Konstanten.ACTION_REPAIR_VORMONAT).setEnabled(process.getKassenKontext().getVormonatReparaturKandidat() != null);
	}

	/** 
	 * startet diesen Controller. Initialisiert die Oberflächen
	 * und erzeugt die Action-Objekte.
	 */
	public void start(){

		window = new MainApplicationWindow(this);
		ApplicationContext.setMainApplicationWindow(window);
		
		window.setKassenKontext(process.getKassenKontext());
		
		ClientAreaPanel clientArea = window.getClientArea();
		ClientAreaProvider.registerClientArea(Konstanten.CLIENT_AREA_FORM, clientArea);

		
		// Position des Windows setzen:
		int x = PropertiesManager.getPropertyAsInt(Konstanten.PROPS_SCREEN_X);
		int y = PropertiesManager.getPropertyAsInt(Konstanten.PROPS_SCREEN_Y);
		
		if(x == -1 || y == -1){
			window.centerToScreen();
		}else{
			window.setLocation(x, y);
		}
		
	}
	
	
	public void reactivate(){
		window.setVisible(true);
//		HelpBroker hb = ApplicationContext.getHelpBroker();
//		if(hb != null){
//			hb.enableHelpKey(window.getContentPane(), "welcome", hb.getHelpSet());
//		}		
	}
	
	/**
	 * Löscht diesen Controller. Schließt das MainApplicationWindow
	 */
	public void destroy(){
		window.setVisible(false);
		window.dispose();
	}
			
	public AbstractProcess getProcess(){
		return process;
	}

//	private ActionListener getHelpListener(){
//		if(helpListener == null){
//			helpListener = new CSH.DisplayHelpFromSource(ApplicationContext.getHelpBroker());
//		}
//		return helpListener;
//	}



	public void handleBeendenEvent(){
		process.beenden();
	}

	public void handleDatensicherungEvent(){
		logger.debug("Action = Datensicherung");
		process.startDatensicherung();
	}
	
	
	public void handleBuchenEvent(){
		logger.debug("Action = Buchen");
		process.startBuchen();
	}
	
	public void handleUebersichtEvent(){
		logger.debug("Action = Uebersicht");
		process.startBuchungsUebersicht();
	}
	
	public void handleKassewechselnEvent(){
		logger.debug("Action = Kassewechseln");
		process.startKassenWechsel();
	}

	public void handleHilfeEvent(){
		logger.debug("Action = Hilfe");	
		
		ActionEvent event = new ActionEvent(window, ActionEvent.ACTION_PERFORMED, "help");
//		getHelpListener().actionPerformed(event);
		
	}
	
	public void handleAboutEvent(){
		logger.debug("Action = About");
		aboutDialog = new AboutDialog(window,true, this);
		aboutDialog.setVisible(true);
	}	

	public void handleKasseanlegenEvent(){
		logger.debug("Action = Kasseanlegen");
		process.startKassenAnlage();
	}	

	public void handleKassepflegenEvent(){
		logger.debug("Action = Kassenpflege");
		process.startKassenPflege();
		
	}
	
	public void handleKostenstellenEvent(){
		logger.debug("Action = Kostenstellen");
		process.startKostenstellenpflege();
	}
	
	public void handleKontenEvent(){
		logger.debug("Action = Konten");
		process.startKontenpflege();
	}

	public void handlePropertiesEvent(){
		logger.debug("Action = Properties");
		process.startPropertiespflege(false);
	}

	public void handleDruckKassenbuchEvent(){
		process.startKassenbuchdruck();
	}
	
	public void handleDruckDebitorenEvent(){
		process.startDebitorendruck();
	}
	
	public void handleDruckSummenlisteEvent(){
		process.startSummenlistendruck();
	}
	
	public void handleExportBuchungenEvent(){
		process.startExportBuchungen();
	}
	
	public void handleExportDatenbankEvent(){
		process.startExportDatenbank();
	}
	
	public void handleDBDiagnoseEvent(){
		process.startDBDiagnose();
	}
	
	public void handleExportStammdatenEvent(){
		process.startExportStammdaten();	
	}
	
	public void handleViewClosedEvent(Object source){
		if(source == window)
			process.beenden();

		if(source == aboutDialog){
			aboutDialog.setVisible(false);
			aboutDialog.dispose();
			aboutDialog = null;
		}
			
	}
	
	public void handleTest1Event(){
		process.test1();
	}

	public void handleTest2Event(){
		process.test2();
	}
	
	public void handleMonatsabschlussEvent(){
		process.startMonatsabschluss();
	}
	
	public void handleKorrekturVormonatEvent(){
		process.startKorrekturVormonat();
	}

	public void handleRepairVormonatEvent() {process.startRepairVormonat();}

	/**
	 * liefert die aktuelle Position des Kassenprogramms auf dem Bildschirm.
	 */
	public Point getCurrentScreenPosition() {
		try{
			return window.getLocationOnScreen();
		}catch(Exception e){
			// Wenn Window aus irgendeinem Grund nicht sichtbar ist.
			return null;
		}
		
	}
}
