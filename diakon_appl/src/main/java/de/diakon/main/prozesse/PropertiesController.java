package de.diakon.main.prozesse;

import java.awt.Toolkit;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JOptionPane;

import de.diakon.main.gui.PropertiesDialog;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file PropertiesController.java
 */
public class PropertiesController extends AbstractController {
	private PropertiesProcess process;
	private PropertiesDialog dialog;

	private boolean neuanlage;

	/**
	 * Constructor for PropertiesController.
	 */
	public PropertiesController(PropertiesProcess process, boolean neuanlage) {
		super();
		this.neuanlage = neuanlage;
		this.process = process;
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#start()
	 */
	public void start() {
		dialog = new PropertiesDialog(ApplicationContext.getMainApplicationWindow(),"Einstellungen",true,this, neuanlage);
		dialog.setDiakonProperties(process.getProperties());
		dialog.setVisible(true);
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}
	public AbstractProcess getProcess(){
		return process;
	}


	// Event-Handler
	public void handleOkEvent(){
		Properties props = dialog.getDiakonProperties();
		try{
			process.storeProperties(props);
		}catch(IOException e){
			logger.error("Fehler beim Speichen der Properties", e);
			JOptionPane.showConfirmDialog(dialog, "Fehler beim Speichern der Einstellungen", "Fehler!", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE);
			process.rollback();
		}
	}
	
	public void handleAbbrechenEvent(){
		if(neuanlage){
			Toolkit.getDefaultToolkit().beep();
		}else{
			process.rollback();
		}
		
	}
}
