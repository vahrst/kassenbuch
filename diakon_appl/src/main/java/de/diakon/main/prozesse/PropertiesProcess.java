package de.diakon.main.prozesse;

import java.io.IOException;
import java.util.Properties;

import de.diakon.main.PropertiesManager;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file PropertiesProcess.java
 */
public class PropertiesProcess extends AbstractProcess {
	private PropertiesController controller ;
	private boolean neuanlage;

	/**
	 * Constructor for PropertiesProcess.
	 */
	public PropertiesProcess(AbstractProcess parent, EndTransactionHandler eth, boolean neuanlage) {
		super(parent, eth);
		this.neuanlage = neuanlage;
		openController();
	}


	/**
	 * @see de.diakon.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	private void openController(){
		controller = new PropertiesController(this, neuanlage);
		controller.start();
	}

	public Properties getProperties(){
		return PropertiesManager.getProperties();
	}
	
	public void storeProperties(Properties props)throws IOException{
		PropertiesManager.storeProperties(props);
		commit();
	}
}
