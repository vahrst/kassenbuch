package de.diakon.main.prozesse;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import de.diakon.main.PropertiesManager;

/**
 * Klasse, die einmalig ausgef�hrt werden soll.
 * @author thomas
 *
 */
public class RunOnce {
	private static Logger logger = Logger.getLogger(RunOnce.class);
	private static Logger log = Logger.getLogger("application");
	
	private static final String PROPS_KEY_RUNONCE = "RunOnce";
	private static final int runOnceNumber = 0x01; // Bei folgenden Versionen dann 0x02, 0x04 ... 
	
	
	
	/**
	 * f�hrt diese Klasse aus. In diesem Fall werden die �berfl�ssigen
	 * Dateien gel�scht.
	 */
	public void run(){
		if(alreadyExecuted()){
			return;
		}
		
		removeOldFiles();
		
		setRunProperty();
		
	}
	
	/**
	 * 
	 * @return
	 */
	private boolean alreadyExecuted(){
		int value = PropertiesManager.getPropertyAsInt(PROPS_KEY_RUNONCE);
		if(value == -1 || (value &= runOnceNumber) == 0){
			return false;
		}
		return true;
	}
	
	
	/**
	 * 
	 */
	private void removeOldFiles(){
		log.info("RunOnce: L�schen Alter Dateien");
		
		List<String> loeschListe = Arrays.asList(
				"help", "basis.jar", "Historie.txt", "idb.jar", "jhall.jar", "LabelsBundle.properties", 
				"log4j.properties", "log4j-1.2.4.jar", "rreport.jar"
				);
		
		
		String currentDir = System.getProperty("user.dir");
		File dir =new File(currentDir);  
		File[] files = dir.listFiles();
		
		for(File f : files){
			if(loeschListe.contains(f.getName())){
				log.info("L�schen: " + f.getName());
				deleteFileOrDir(f);
			}
		}
		
		
	}
	
	/**
	 * l�scht das vorgegebene File oder Verzeichnis rekursiv.
	 * @param file
	 */
	private void deleteFileOrDir(File file) {
   if (file.exists() && file.isDirectory()) {
       File[] children = file.listFiles();
       for (File child : children) {
           deleteFileOrDir(child);
       }
   }
   // The directory is now empty so delete it
   file.delete();
}	
	
	/**
	 * setzt den Merker, der steuert, das diese Klasse einmalig gelaufen ist.
	 */
	private void setRunProperty(){
		Properties props = PropertiesManager.getProperties();
		
		String valueString = props.getProperty(PROPS_KEY_RUNONCE, "0");
		int value = 0;
		try{
			value = Integer.parseInt(valueString);
		}catch(NumberFormatException nfe){
			value = 0;
		}
		
		value |= runOnceNumber;
		
		props.setProperty(PROPS_KEY_RUNONCE, Integer.toString(value));
		
		try{
			PropertiesManager.storeProperties(props);
		}catch(IOException ioe){
			logger.error("Fehler!!", ioe);
		}
		
	}
}
