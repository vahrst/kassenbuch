package de.diakon.main.prozesse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import de.diakon.global.Konstanten;
import de.diakon.main.PropertiesManager;
import de.diakon.persistence.PersistenceManager;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.EndTransactionHandler;

/**
 * 
 * @author Thomas
 * @version 
 * @file DatensicherungProcess.java
 */
public class DatensicherungProcess extends AbstractProcess {
	private static int BLOCKSIZE = 512;
	private DatensicherungController controller;

	private String backupdir;
	private boolean autobackup;
	
	private static final int DEFAULT_GENERATION_COUNT = 6;

	private byte[] buffer = new byte[BLOCKSIZE];
	
	/**
	 * Constructor for DatensicherungProcess.
	 * @param parent
	 * @param eth
	 */
	public DatensicherungProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		boolean autobackup) {
		super(parent, eth);
		this.autobackup = autobackup;
		controller = new DatensicherungController(this);
		controller.start();		
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	/**
	 * liefert das Verzeichnis, in dem die Sicherungskopie angelegt werden
	 * soll
	 */
	public String getBackupDir(){
		if(backupdir == null){
			backupdir = PropertiesManager.getProperty(Konstanten.PROPS_BACKUP_DIR);
		}
		if(backupdir == null){
			backupdir = "." + System.getProperty("file.separator") + "backup";
		}
		
		return backupdir;
	}
	
	/**
	 * true, wenn dieser Process automatisch beim Beenden des Programms
	 * aufgerufen wurde.
	 * @return
	 */
	public boolean isAutoBackup(){
		return autobackup;
	}
		
	/**
	 * Liefert die Anzahl Generationen f�r die Sicherung
	 * @return
	 */
	private int getGenerationCount(){
		int anzahl = PropertiesManager.getPropertyAsInt(Konstanten.PROPS_BACKUP_GENCOUNT);
		if(anzahl != -1){
			return anzahl;
		}else{
			logger.warn("Anzahl Sicherungsgenerationen nicht gesetzt!");
			return DEFAULT_GENERATION_COUNT;
		}
	}
		
		
	/**
	 * startet die Datensicherung
	 */
	public void startSicherung() throws IOException{
		PersistenceManager.getPersistenceManager().close();
		File dir = createBackupDirectory();

		File savingDir = getSavingDirectory();

		File f = File.createTempFile("sich",".zip", dir);
		ZipOutputStream zo = new ZipOutputStream(new FileOutputStream(f));

		writeToZip(zo, savingDir, "");
		zo.close();
		
		shiftGenerations(f);
		log.info("Datensicherung durchgef�hrt");
	}
	
	
	/**
	 * schreibt den Inhalt des vorgegebenen Verzeichnisses auf den
	 * ZipOutputstream
	 */
	private void writeToZip(ZipOutputStream out, File savingDir, String dirname) throws IOException{
		File[] files = savingDir.listFiles();
		for(int i=0;i<files.length;i++){
			File f = files[i];
			if(f.isDirectory()){
				writeToZip(out, f, dirname + f.getName() + File.separator);
			}else{
				// jetzt das File schreiben
				ZipEntry ze = new ZipEntry(dirname + f.getName());	
				out.putNextEntry(ze);
				FileInputStream in = new FileInputStream(f);
				int count = 0;
				while((count = in.read(buffer,0,BLOCKSIZE)) != -1){
					out.write(buffer,0,count);
				}
				
				out.flush();
			}
		}
		
	}

	
	/**
	 * liefert das Verzeichnis, das gesichert werden soll
	 */
	private File getSavingDirectory() throws IOException{
		String dirname = "." + File.separator + "db";
		File dir = new File(dirname);
		if(!dir.exists() || !dir.isDirectory())
			throw new IOException("db Verzeichnis nicht gefunden: " + dirname);
			
		return dir;	
	}

	
	/**
	 * legt, falls notwendig, das Sicherungsverzeichnis an
	 */
	private File createBackupDirectory()throws IOException{
		File dir = new File(getBackupDir());
		if(dir.exists()){
			if(dir.isDirectory()){
				return dir;
			}else{
				throw new IOException("Anstelle des Verzeichnisses existiert eine Datei mit gleichem Namen!");
			}
		}else{
			dir.mkdirs();
		}
		
		return dir;
	}
	
	/**
	 * l�scht die �lteste Generation und benennt alle anderen um
	 */
	private void shiftGenerations(File newTempFile){
		// erstmal das �lteste l�schen:
		int generationCount = getGenerationCount();
		File f = getSicherungsFile(generationCount);
		if(f.exists()){
			f.delete();
		}
		
		for(int i=generationCount -1; i>0; i--){
			f = getSicherungsFile(i);
			if(f.exists()){
				f.renameTo(getSicherungsFile(i+1));
			}
		}
		
		newTempFile.renameTo(getSicherungsFile(1));
	}		
	
	/**
	 * liefert das File mit der vorgegebenen Generation
	 */
	private File getSicherungsFile(int generation){
		return new File( getBackupDir() + File.separator + "sicherung_" + generation + ".zip");
	}
}
