package de.diakon.main.prozesse;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import org.dom4j.DocumentFactory;

import de.diakon.entities.Buchungsperiode;
import de.diakon.entities.BuchungsperiodeHome;
import de.diakon.entities.Buchungssatz;
import de.diakon.entities.BuchungssatzHome;
import de.diakon.entities.Kasse;
import de.diakon.entities.KasseHome;
import de.diakon.entities.Konto;
import de.diakon.entities.KontoHome;
import de.diakon.entities.Kostenstelle;
import de.diakon.entities.KostenstelleHome;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.EndTransactionHandler;
import de.vahrst.application.xml.Document;
import de.vahrst.application.xml.DocumentImpl;
import de.vahrst.application.xml.Element;
import de.vahrst.application.xml.XMLWriter;

/**
 * 
 * @author Thomas
 * @version 
 * @file ExportProcess.java
 */
public class ExportDBProcess extends AbstractProcess {
	private ExportDBController controller;
	
	/**
	 * Constructor for ExportProcess.
	 * @param parent
	 * @param eth
	 */
	public ExportDBProcess(AbstractProcess parent, EndTransactionHandler eth) {
		super(parent, eth);
		controller = new ExportDBController(this);
		controller.start();
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}


	public void exportDatabase(String filename) throws PersistenceException, IOException{
		log.info("Export Datenbank nach " + filename);

		
		
		
		Document doc = new DocumentImpl();
		Element root = doc.createElement("Kassenbuch");
		doc.appendChild(root);
		
		exportKonten(root, doc);
		exportKostenstellen(root, doc);

		exportKassen(root, doc);
		
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(filename), "UTF8");
		
		XMLWriter.print(doc, writer);
		writer.close();
		
	}

	/**
	 * f�gt alle Konten in das vorgegebene XML-Root-Element ein.
	 * @param root
	 * @param factory
	 * @throws PersistenceException
	 */
	private void exportKonten(Element root, Document factory) throws PersistenceException{
		KontoHome home = new KontoHome();
		List<Konto> liste = home.getAllKonten().getktoListe();
		
		Element kontoRoot = factory.createElement("Konten");
		root.appendChild(kontoRoot);
			
		for(int i=0;i<liste.size();i++){
			Konto kto = liste.get(i);
			kontoRoot.appendChild(kto.toXML(factory));
				
		}
	}	
		
	/**
	 * f�gt alle Kostenstellen in das vorgegebene XML-Root-Element ein.
	 * @param root
	 * @param factory
	 * @throws PersistenceException
	 */
	private void exportKostenstellen(Element root, Document factory) throws PersistenceException{
		KostenstelleHome home = new KostenstelleHome();
		List<Kostenstelle> liste = home.getAllKostenstellen().getKstListe();
		
		Element kstRoot = factory.createElement("Kostenstellen");
		root.appendChild(kstRoot);
			
		for(int i=0;i<liste.size();i++){
			Kostenstelle kst = liste.get(i);
			kstRoot.appendChild(kst.toXML(factory));
				
		}
	}	

	
	/**
	 * f�gt f�r alle vorhandenen Kassen die Buchungen in das vorgegebene XML-Root-Element ein.
	 * @param root
	 * @param factory
	 * @throws PersistenceException
	 */
	private void exportKassen(Element root, Document factory) throws PersistenceException{

		Element kassenRoot = factory.createElement("Kassen");
		root.appendChild(kassenRoot);
		
		KasseHome kasseHome = new KasseHome();
		BuchungsperiodeHome periodeHome = new BuchungsperiodeHome();
		BuchungssatzHome bsHome = new BuchungssatzHome();

		List<Kasse>kassen = kasseHome.getAllKassen();
		
		for(Kasse kasse : kassen){
			Element kasseRoot = kasse.toXML(factory);
			kassenRoot.appendChild(kasseRoot);
			
			List<Buchungsperiode> periodeListe = periodeHome.getAllBuchungsperiodenForKasse(kasse);
			for(Buchungsperiode periode : periodeListe){
				Element periodeRoot = periode.toXML(factory);
				kasseRoot.appendChild(periodeRoot);
				
				List<Buchungssatz> allBuchungssaetze = bsHome.getAllBuchungssaetze(periode);
				for(Buchungssatz bs: allBuchungssaetze){
					Element bsElement = bs.toXML(factory);
					periodeRoot.appendChild(bsElement);
				}
				
			}
			
		}
		
		
		
		
		
	}
	
}
