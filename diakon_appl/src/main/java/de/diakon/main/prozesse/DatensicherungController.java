package de.diakon.main.prozesse;

import java.io.IOException;

import javax.swing.JOptionPane;

import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;

/**
 * 
 * @author Thomas
 * @version 
 * @file DatensicherungController.java
 */
public class DatensicherungController extends AbstractController {
	private DatensicherungProcess process;
	/**
	 * Constructor for DatensicherungController.
	 */
	public DatensicherungController(DatensicherungProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		if(process.isAutoBackup()){
			startAutoBackup();
		}else{
			startManualBackup();
		}
	}

	/**
	 * startet Backup beim Beenden des Programms.
	 */
	private void startAutoBackup(){
		int option = JOptionPane.showConfirmDialog(
			ApplicationContext.getMainApplicationWindow(),
			"Soll eine Datensicherung durchgeführt werden?",
			"Automatische Datensicherung",
			JOptionPane.YES_NO_OPTION, 
			JOptionPane.INFORMATION_MESSAGE);

		if(option == JOptionPane.YES_OPTION){
			try{
				
				process.startSicherung();
				process.commit();
				
			}catch(IOException e){
				logger.error("Fehler bei Datensicherung", e);
				showErrorMessage("Bei der Sicherung ist ein Fehler aufgetreten. Die Daten wurden NICHT gesichert!");
				process.rollback();
			}
		}else{
			process.commit();
		}
			
	}
	
	
	/**
	 * Startet Backup, das explizit angewählt wurde.
	 */
	private void startManualBackup(){
	
		String backupdir = process.getBackupDir();
		
		int option = JOptionPane.showConfirmDialog(
			ApplicationContext.getMainApplicationWindow(),
			"Daten werden gesichert im Verzeichnis: " + backupdir,
			"Datensicherung", 
			JOptionPane.OK_CANCEL_OPTION, 
			JOptionPane.INFORMATION_MESSAGE);
			
		if(option == JOptionPane.OK_OPTION){
			try{
				
				process.startSicherung();
				
				JOptionPane.showMessageDialog(ApplicationContext.getMainApplicationWindow(), "Daten wurden gesichert!");
				process.commit();
				
			}catch(IOException e){
				logger.error("Fehler bei Datensicherung", e);
				showErrorMessage("Bei der Sicherung ist ein Fehler aufgetreten. Die Daten wurden NICHT gesichert!");
				process.rollback();
			}
		}else{
			process.rollback();
		}
		
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

}
