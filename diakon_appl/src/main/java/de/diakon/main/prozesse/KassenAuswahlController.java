package de.diakon.main.prozesse;

import java.util.List;

import de.diakon.entities.Kasse;
import de.diakon.main.gui.KassenAuswahlDialog;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;

/**
 * 
 * @author Thomas
 * @version 
 * @file KasseAuswahlController.java
 */
public class KassenAuswahlController extends AbstractController {
	private KassenAuswahlProcess process = null;
	private KassenAuswahlDialog dialog = null;

	private List<Kasse> kassenListe = null;
	
	/**
	 * Constructor for KasseAuswahlController.
	 */
	public KassenAuswahlController(KassenAuswahlProcess p) {
		super();
		this.process = p;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		try{
			kassenListe = process.getKassenListe();
		}catch(PersistenceException e){
			logger.error("Fehler", e);
			showErrorMessage("Fehler beim Lesen der angelegten Kassen");
			process.rollback();
			return;
		}		

		KassenAuswahlTableModel tm = new KassenAuswahlTableModel(kassenListe);
		
		dialog = new KassenAuswahlDialog(ApplicationContext.getMainApplicationWindow(), "Kasse ausw�hlen", true, this, tm);
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}


	public void handleGeheZuEvent(){
		int i = dialog.getSelectedRow();
		Kasse kasse = kassenListe.get(i);
		
		process.selectKasse(kasse);
		
	}
}
