package de.diakon.main;

import java.io.*;
import java.util.Properties;

import org.apache.log4j.Logger;

import de.diakon.global.Konstanten;
/**
 * Zentrale Manager-Klasse f�r die Programm-Einstellungen
 * @author Thomas
 * @version 
 * @file PropertiesManager.java
 */
public class PropertiesManager {
	private static Logger logger = Logger.getLogger(PropertiesManager.class);
	private static final String PROPS_FILENAME = "diakon.ini";

	private static String propsFilePath;
	private static Properties cachedProps = null;	

	static{
		initProperties();
	}

	
	private static void initProperties(){
		if(cachedProps == null){
			Properties props = new Properties();
			try{
				loadProperties(props);
			}catch(PropertiesNotFoundException e){
			}
			
			completeWithDefaultProps(props);
			
			cachedProps = props;
		}
	}
	
	/**
	 * l�dt die Programm-Properties. Wirft eine PropertiesNotFound
	 * Exception, wenn noch keine Properties Datei existiert.
	 */
	private static void loadProperties(Properties props) throws PropertiesNotFoundException{
		String iniFileName = getPropsFilePath();
		logger.debug("load Properties: " + iniFileName);
		
		BufferedInputStream in = null;
		try{
			in = new BufferedInputStream(new FileInputStream(iniFileName));
			props.load(in);
			in.close();
			
		}catch(IOException e){
			logger.info("Propertiesdatei nicht gefunden: " + iniFileName, e);
			throw new PropertiesNotFoundException();
			
		}
	}

	public static Properties getProperties(){
		return (Properties) cachedProps.clone();
	}

	private static void completeWithDefaultProps(Properties props){
		Properties defaultprops = new Properties();
		defaultprops.put(Konstanten.PROPS_DB_FILE, System.getProperty("user.dir") + System.getProperty("file.separator") + "diakon.prp");
		defaultprops.put(Konstanten.PROPS_BACKUP_DIR, System.getProperty("user.dir") + System.getProperty("file.separator") + "backup");
		defaultprops.put(Konstanten.PROPS_KLAENGE, "false");
		defaultprops.put(Konstanten.PROPS_CSV_EXPORT_CHARSET, "ISO-8859-15");
		defaultprops.put(Konstanten.PROPS_EXPORT_DIR,  System.getProperty("user.home"));
		
		for(String key : defaultprops.stringPropertyNames()){
			if(!props.containsKey(key)){
				props.setProperty(key, defaultprops.getProperty(key));
			}
		}
	}
	
	private static String getPropsFilePath(){
		if(propsFilePath == null){
			String currentDir = System.getProperty("user.dir");		
			String filesep = System.getProperty("file.separator");
		
			propsFilePath = currentDir + filesep + PROPS_FILENAME;
		}
		return propsFilePath;
	}
		

	public static void storeProperties(Properties p) throws IOException{
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(getPropsFilePath()));
		p.store(out, "Einstellungen Kassenbuch Diakon");
		cachedProps = (Properties) p.clone();					
	}
	
	/**
	 * liefert die Property f�r den vorgegebenen Key, oder einen Leerstring,
	 * falls dieser Key nicht existiert.
	 */
	public static String getProperty(String key){
		String result = cachedProps.getProperty(key);
		if(result == null)
			result = "";
			
		return result;
	}

	/**
	 * liefert true, wenn der Wert zum vorgegebenen Key 'true' ist.
	 * @param key
	 * @return
	 */
	public static boolean getPropertyAsBoolean(String key){
		return "true".equalsIgnoreCase(cachedProps.getProperty(key));
	}
	
	
	/** 
	 * liefert die Propertiy f�r den vorgegebenen Key als long-Wert. -1, wenn
	 * der Key nicht existiert oder nicht numerisch ist.
	 */
	public static long getPropertyAsLong(String key){
		long value = -1;
		String result = cachedProps.getProperty(key);
		if(result != null){
			try{
				value = Long.parseLong(result);
			}catch(NumberFormatException e){
			}
		}
		return value;
	}
	
	/** 
	* liefert die Propertiy f�r den vorgegebenen Key als int-Wert. -1, wenn
	* der Key nicht existiert oder nicht numerisch ist.
	*/
	public static int getPropertyAsInt(String key){
		int value = -1;
		String result = cachedProps.getProperty(key);
		if(result != null){
			try{
				value = Integer.parseInt(result);
			}catch(NumberFormatException e){
			}
		}
		return value;
	}
	
	
}
