package de.diakon.main.gui;

import java.text.SimpleDateFormat;

import javax.swing.JLabel;

import org.apache.log4j.Logger;
/**
 * 
 * @author Thomas
 * @version 
 * @file StatusLeisteThread.java
 */
public class StatusLeisteThread extends Thread {
	private Logger logger = Logger.getLogger(StatusLeisteThread.class);

	private StatusLeistePanel statusPanel = null;

	SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
	java.util.Date date = new java.util.Date();

	/**
	 * Constructor for StatusLeisteThread.
	 * @param name
	 */
	public StatusLeisteThread(String name, StatusLeistePanel panel) {
		super(name);
		this.statusPanel = panel;
	}


	public void run(){
		logger.info("Start Statuszeile Timerthread");
		
		while(true){
			updateTimeLabel();
			
			try{
				Thread.sleep(5000);
			}catch(Exception e){
			}
		}
		
	}
	

	private void updateTimeLabel(){
		JLabel timeLabel = statusPanel.lbTime;
			
		date.setTime(System.currentTimeMillis());
		String zeit = df.format(date);
		timeLabel.setText(zeit);			
			
	}
}
