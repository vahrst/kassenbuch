package de.diakon.main.gui;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Properties;

import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 */
public class PropertiesDialog extends AbstractDialog {
	private PropertiesPanel propertiesPanel;
	/**
	 * Constructor for PropertiesDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public PropertiesDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller,
		boolean neuanlage)
		throws HeadlessException {
		super(owner, title, modal, controller);

		initialize(controller, neuanlage);
	}



	private void initialize(ActionListener controller, boolean neuanlage){
		this.setSize(600,450);
		if(neuanlage)
			setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			
		propertiesPanel	= new PropertiesPanel(false, controller, neuanlage);
		getContentPane().add(propertiesPanel, BorderLayout.CENTER);

		center();		
	}
	
	public void setDiakonProperties(Properties props){
		propertiesPanel.setDiakonProperties(props);
	}
	
	public Properties getDiakonProperties(){
		return propertiesPanel.getDiakonProperties();
	}
}
