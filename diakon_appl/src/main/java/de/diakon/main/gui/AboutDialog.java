package de.diakon.main.gui;

import java.awt.*;
import java.io.IOException;
import java.net.URL;

import javax.swing.*;

import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file AboutDialog.java
 */
public class AboutDialog extends AbstractDialog {

	/**
	 * Constructor for AboutDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public AboutDialog(
		Frame owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		init();
	}

	/**
	 * Constructor for AboutDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public AboutDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, title, modal, controller);
		init();
	}

	/**
	 * Constructor for AboutDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public AboutDialog(
		Dialog owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		init();
	}


	private void init(){
		this.setSize(650,450);
		this.center();
		
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setEditable(false);
		try{
			URL aboutURL = this.getClass().getResource("/help/about.html");
			editorPane.setPage(aboutURL);
		}catch(IOException e){
			editorPane.setText(e.toString());
		}

		JScrollPane scp = new JScrollPane(editorPane);
		this.getContentPane().add(scp, BorderLayout.CENTER);
	}
}
