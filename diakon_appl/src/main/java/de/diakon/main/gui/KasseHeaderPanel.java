package de.diakon.main.gui;

import java.awt.*;
import java.beans.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import de.diakon.entities.*;
import de.diakon.global.Konstanten;
import de.diakon.main.prozesse.MainApplicationController;
import de.vahrst.application.gui.*;

/**
 * Das Kopfpanel der Anwendung. Enth�lt die Kassendaten.
 * @author Thomas
 * @version 
 */
public class KasseHeaderPanel extends JPanel implements PropertyChangeListener{

	/**
	 * Der KassenKontext, enth�lt die Daten f�r den Kopf
	 * bereich der Maske
	 */
	private KassenKontext kassenKontext = null;

	/** Der Controller von MainApplicationWindow ist auch mein Controller */
	MainApplicationController controller = null;

	private JLabel lbAktKassenbuch = null;
	private JLabel lbAktBuchungsMonat = null;
	private InfoPanel infoPanel = null;

	public KasseHeaderPanel(MainApplicationController controller){
		super();
		this.controller = controller;
		initialize();
	}
	
	private void initialize(){
		this.setMinimumSize(new Dimension(300,100));
		this.setPreferredSize(new Dimension(700,100));
		this.setBackground(ColorKonstanten.COLOR_KASSEHEADERPANEL);

		this.setLayout(new BorderLayout());
		
		JPanel leftPanel = new JPanel(new GridBagLayout());
		leftPanel.setOpaque(false);
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		
		JLabel lb1 = new JLabel("KASSENBUCH");
		lb1.setOpaque(true);
		lb1.setBackground(new Color(255,255,100));
		lb1.setFont(new Font(null, Font.BOLD, 20));
		lb1.setBorder(new EmptyBorder(2,10,2,10));
		gbc.insets = new Insets(0,0,10,4);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		leftPanel.add(lb1, gbc);

		VaButton button = new VaButton(controller.getAction(Konstanten.ACTION_KASSEWECHSEL));
		button.setBackground(ColorKonstanten.COLOR_KASSEHEADERPANEL);
		button.setFocusPainted(false);
		button.setBorderPainted(true);
		gbc.gridx = 2;
		gbc.anchor = GridBagConstraints.EAST;
		// leftPanel.add(button, gbc);
	
		JLabel lb2 = new JLabel("aktuelles Kassenbuch");
		gbc.anchor = GridBagConstraints.WEST;
		gbc.gridy = 1;
		gbc.gridx = 0;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(1,4,1,4);
		leftPanel.add(lb2, gbc);

		lbAktKassenbuch = new JLabel();
		lbAktKassenbuch.setOpaque(true);
		lbAktKassenbuch.setBackground(Color.WHITE);
		gbc.gridy = 1;
		gbc.gridx = 1;
		gbc.fill=GridBagConstraints.HORIZONTAL;
		leftPanel.add(lbAktKassenbuch, gbc);

		JLabel lb3 = new JLabel("aktueller Monat");
		gbc.gridy = 2;
		gbc.gridx = 0;
		leftPanel.add(lb3, gbc);

		lbAktBuchungsMonat = new JLabel();
		lbAktBuchungsMonat.setOpaque(true);
		lbAktBuchungsMonat.setBackground(Color.WHITE);
		gbc.gridy = 2;
		gbc.gridx = 1;
		gbc.fill=GridBagConstraints.HORIZONTAL;
		leftPanel.add(lbAktBuchungsMonat, gbc);

		Strut strut1 = new Strut();
		gbc.gridy = 3;
		gbc.gridx = 2;
		gbc.gridheight = 1;
		gbc.weighty = 1;
		gbc.weightx = 1;
		leftPanel.add(strut1, gbc);
		
		
		infoPanel = new InfoPanel();
		this.add(infoPanel, BorderLayout.EAST);		
		this.add(leftPanel, BorderLayout.CENTER);

		
	}


	public void setKassenKontext(KassenKontext kk){
		this.kassenKontext = kk;
		refreshKassendaten();
		kk.addPropertyChangeListener(this);
	}
	
	public void propertyChange(PropertyChangeEvent e){
		if(e.getSource() == kassenKontext){
			refreshKassendaten();
		}
	}

	private void refreshKassendaten(){
		if(kassenKontext != null){
			Kasse kasse = kassenKontext.getKasse();
			if(kasse != null){
				lbAktKassenbuch.setText(kasse.getName());
			}
			lbAktBuchungsMonat.setText(kassenKontext.getAktuelleBuchungsperiodeFormatiert());
			infoPanel.setValues(kassenKontext.getAktuelleBuchungsperiode());
		}
	}
}
