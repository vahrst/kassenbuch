package de.diakon.main.gui;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.net.URL;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

import de.diakon.entities.KassenKontext;
import de.diakon.global.Konstanten;
import de.diakon.main.prozesse.MainApplicationController;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.gui.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file MainApplicationWindow.java
 */
public class MainApplicationWindow extends AbstractFrame {

	/** 
	 * mein Controller. �ber diesen erhalte ich u.a.
	 * alle m�glichen Actions
	 */
	private MainApplicationController controller = null;


	
	// ein paar Panels, die hier referenziert werden
	private KasseHeaderPanel kasseHeaderPanel = null;
	private StatusLeistePanel statusLeistePanel = null;
	private ClientAreaPanel clientArea = null;


	/**
	 * Constructor for MainApplicationWindow.
	 * @throws HeadlessException
	 */
	public MainApplicationWindow(MainApplicationController controller) throws HeadlessException {
		super(controller);
		this.controller = controller;
		initialize();
		
	}


	
	/**
	 * initialisiert dieses ApplicationWindow
	 * erzeugt die komplette GUI-Struktur
	 */
	private void initialize(){
		//this.setFocusTraversalPolicy(new MyFocusTraversalPolicy());
		//this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setSize(760, 550);
		this.setTitle("Kassenbuch Diakonisches Werk");
		
		ImageIcon icon = ImageIconFactory.getImageIcon("/images/kasse.gif");
		if(icon != null)
			setIconImage(icon.getImage());
		
		createMenue();
		createToolbar();
		createContentPane();

	}
	
	/**
	 * erzeugt das Men�
	 */
	private void createMenue(){
		JMenuBar menubar = new JMenuBar();
		//menubar.setBackground(Color.YELLOW);

		JMenu menueDatei = new JMenu("Datei");
		menueDatei.setMnemonic(KeyEvent.VK_D);
		//menueDatei.setBackground(Color.CYAN);
		
		menueDatei.add(new JMenuItem(controller.getAction(Konstanten.ACTION_KASSEANLEGEN)));
		menueDatei.add(new JMenuItem(controller.getAction(Konstanten.ACTION_KASSEWECHSEL)));
		menueDatei.add(new JSeparator());

		menueDatei.add(new JMenuItem(controller.getAction(Konstanten.ACTION_KONTEN)));
		menueDatei.add(new JMenuItem(controller.getAction(Konstanten.ACTION_KOSTENSTELLEN)));
		menueDatei.add(new JMenuItem(controller.getAction(Konstanten.ACTION_EXPORT_STAMMDATEN)));
		menueDatei.add(new JMenuItem(controller.getAction(Konstanten.ACTION_EXPORT_DB)));
		menueDatei.add(new JSeparator());

		menueDatei.add(new JMenuItem(controller.getAction(Konstanten.ACTION_PROPERTIES)));
		menueDatei.add(new JSeparator());
		
		menueDatei.add(new JMenuItem(controller.getAction(Konstanten.ACTION_DATENSICHERUNG)));
		menueDatei.add(new JSeparator());
		
		menueDatei.add(new JMenuItem(controller.getAction(Konstanten.ACTION_BEENDEN)));
		menubar.add(menueDatei);

		JMenu menueKasse = new JMenu("Kasse");
		menueKasse.setMnemonic(KeyEvent.VK_K);
		menueKasse.add(new JMenuItem(controller.getAction(Konstanten.ACTION_BUCHEN)));
		menueKasse.add(new JMenuItem(controller.getAction(Konstanten.ACTION_UEBERSICHT)));
		menueKasse.add(new JSeparator());
		menueKasse.add(new JMenuItem(controller.getAction(Konstanten.ACTION_KASSENPFLEGE)));
		menueKasse.add(new JSeparator());
		menueKasse.add(new JMenuItem(controller.getAction(Konstanten.ACTION_DRUCK_KASSENBUCH)));
		menueKasse.add(new JMenuItem(controller.getAction(Konstanten.ACTION_DRUCK_DEBITOREN)));
		menueKasse.add(new JMenuItem(controller.getAction(Konstanten.ACTION_DRUCK_SUMMENLISTE)));
		menueKasse.add(new JMenuItem(controller.getAction(Konstanten.ACTION_EXPORT_BUCHUNGEN)));
		menueKasse.add(new JMenuItem(controller.getAction(Konstanten.ACTION_MONATSABSCHLUSS)));
		menueKasse.add(new JSeparator());
		menueKasse.add(new JMenuItem(controller.getAction(Konstanten.ACTION_VORMONAT)));
		menueKasse.add(new JMenuItem(controller.getAction(Konstanten.ACTION_REPAIR_VORMONAT)));
		menubar.add(menueKasse);

		JMenu menueHilfe = new JMenu("Hilfe");
		menueHilfe.setMnemonic(KeyEvent.VK_H);
		// menubar.add(Box.createHorizontalGlue());
		// menueHilfe.add(new JMenuItem(controller.getAction(Konstanten.ACTION_HILFE)));
		menueHilfe.add(new JMenuItem(controller.getAction(Konstanten.ACTION_ABOUT)));
		menueHilfe.add(new JSeparator());
		menueHilfe.add(new JMenuItem(controller.getAction(Konstanten.ACTION_DIAGNOSE)));
		menubar.add(menueHilfe);
		
		/*
		JMenu menueTest = new JMenu("Test");
		menueTest.add(new JMenuItem(controller.getAction(Konstanten.ACTION_TEST1)));
		menueTest.add(new JMenuItem(controller.getAction(Konstanten.ACTION_TEST2)));
		menubar.add(menueTest);
		*/		
		this.setJMenuBar(menubar);
	}

	/**
	 * erzeugt und setzt das ContentPane
	 */
	private void createContentPane(){
		JPanel applpanel = new JPanel();
		applpanel.setLayout(new BorderLayout());

		kasseHeaderPanel = new KasseHeaderPanel(controller);
		statusLeistePanel = new StatusLeistePanel();
		ApplicationContext.setStatusAnzeige( statusLeistePanel);

		applpanel.add(kasseHeaderPanel, BorderLayout.NORTH);
		applpanel.add(statusLeistePanel, BorderLayout.SOUTH);

		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		
		clientArea = new ClientAreaPanel();
		
//		clientArea.add(new KontenUebersichtPanel(), "BuchungsPanel");
		
		centerPanel.add(clientArea, BorderLayout.CENTER);
		JPanel navPanel = new JPanel();
		navPanel.setBackground(ColorKonstanten.COLOR_KASSEHEADERPANEL);
		navPanel.setMinimumSize(new Dimension(100, 10));
		navPanel.setPreferredSize(new Dimension(100,10));
		
		centerPanel.add(navPanel, BorderLayout.WEST);		
		
		applpanel.add(centerPanel, BorderLayout.CENTER);
		
		setContentPane(applpanel);	
	}

	/**
	 * erzeugt und setzt die Toolbar
	 */
	private void createToolbar(){
		JToolBar toolBar = new JToolBar();
    toolBar.setFloatable(true);
		JButton button = null;

		//first button
		String imgLocation = "/toolbarButtonGraphics/general/SaveAll24.gif";
		URL imageURL = getClass().getResource(imgLocation);

		if (imageURL != null) {
    	button = new JButton(new ImageIcon(imageURL));
    	
			button.setToolTipText("This is the left button");
  	  toolBar.add(button);
		}
    
    toolBar.addSeparator();
    
    toolBar.setRollover(true);
    toolBar.setBorder(new EtchedBorder());
 //   localContentPane.add(toolBar, BorderLayout.NORTH);
	}

	/** 
	 * setzt den Kassenkontext
	 */
	public void setKassenKontext(KassenKontext kk){
		kasseHeaderPanel.setKassenKontext(kk);
	}
	
	/**
	 * Liefert die ClientArea
	 */
	public ClientAreaPanel getClientArea(){
		return clientArea;
	}
}
