package de.diakon.main.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.Properties;

import javax.swing.*;

import org.apache.log4j.Logger;

import de.diakon.global.Konstanten;
import de.vahrst.application.gui.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
/**
 * 
 * @author Thomas
 * @version 
 * @file PropertiesPanel.java
 */
public class PropertiesPanel extends FormPanel {
	private static Logger logger = Logger.getLogger(PropertiesPanel.class);

	private boolean erstAnlage = false;
	
	private JPanel mainPanel;

	private VaButton btOk;
	private VaButton btAbbrechen;

	private BaseTextField tfDBFile;
	private BaseTextField tfBackupDir;
	private TextFieldNumericInteger tfAnzahlSicherungen;
	private VaCheckBox cbAutoBackup;
	private VaCheckBox cbAutomatischeKontoListe;
	private VaCheckBox cbKlaenge;

	
	private Properties properties;
	private BaseTextField tfCSVExportCharset;



	// ActionListeners
	class SearchActionListener implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			logger.debug("Action = FileSearch");			
			JFileChooser fc = new JFileChooser(tfDBFile.getText());
			fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			
			int retval = fc.showOpenDialog(PropertiesPanel.this);
			if(retval == JFileChooser.APPROVE_OPTION){
				tfDBFile.setText(fc.getSelectedFile().getAbsolutePath());
			}
		}
	}


	/**
	 * Constructor for PropertiesPanel.
	 */
	public PropertiesPanel(boolean paintHeader, ActionListener controller, boolean erstAnlage) {
		super();
		this.setPaintHeaderArea(paintHeader);
		this.erstAnlage = erstAnlage;
		initialize(controller);
		
	}

	public void setDiakonProperties(Properties props){
		properties = props;
		tfDBFile.setText(properties.getProperty(Konstanten.PROPS_DB_FILE));
		tfBackupDir.setText(properties.getProperty(Konstanten.PROPS_BACKUP_DIR));

		
		int genCount = 6;
		try{
			genCount = Integer.parseInt(properties.getProperty(Konstanten.PROPS_BACKUP_GENCOUNT)); // Default 6
		}catch(Exception e){
		}
		tfAnzahlSicherungen.setIntegerValue(genCount);
		

		String property = properties.getProperty(Konstanten.PROPS_KONTOLISTE);
		cbAutomatischeKontoListe.setSelected(Boolean.valueOf(property).booleanValue());

		
		property = properties.getProperty(Konstanten.PROPS_KLAENGE);
		cbKlaenge.setSelected(Boolean.valueOf(property).booleanValue());
		
		property = properties.getProperty(Konstanten.PROPS_AUTO_BACKUP);
		cbAutoBackup.setSelected(Boolean.valueOf(property).booleanValue());
		
		property = properties.getProperty(Konstanten.PROPS_CSV_EXPORT_CHARSET);
		tfCSVExportCharset.setText(property);
	}
	
	public Properties getDiakonProperties(){
		properties.put(Konstanten.PROPS_DB_FILE, tfDBFile.getText());
		properties.put(Konstanten.PROPS_BACKUP_DIR, tfBackupDir.getText());
		properties.put(Konstanten.PROPS_BACKUP_GENCOUNT, tfAnzahlSicherungen.getText());
		
		properties.put(Konstanten.PROPS_KONTOLISTE, Boolean.toString(cbAutomatischeKontoListe.isSelected()));
		properties.put(Konstanten.PROPS_KLAENGE, Boolean.toString(cbKlaenge.isSelected()));
		properties.put(Konstanten.PROPS_AUTO_BACKUP, Boolean.toString(cbAutoBackup.isSelected()));
		
		return properties;
	}
	

	private void initialize(ActionListener controller) {
		this.setLayout(new BorderLayout());

		this.setTitle("Einstellungen");

		JPanel buttonPanel = createButtonPanel(controller);
		this.add(buttonPanel, BorderLayout.SOUTH);
			

		JPanel mainPanel = createMainPanel();
		this.add(mainPanel, BorderLayout.CENTER);

	}

	private JPanel createButtonPanel(ActionListener controller) {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		//		buttonPanel.setBackground(ColorKonstanten.COLOR_BUTTONPANEL);
		buttonPanel.setMinimumSize(new Dimension(10, 24));
		buttonPanel.setPreferredSize(new Dimension(10, 24));
		//		buttonPanel.setBorder(new EmptyBorder(3,0,0,0));			
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		btOk = new VaButton("Ok");
		btOk.setActionCommand("Ok");
		btOk.addActionListener(controller);
		buttonPanel.add(btOk);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btAbbrechen = new VaButton("Abbrechen");
		btAbbrechen.setActionCommand("Abbrechen");
		if(erstAnlage)
			btAbbrechen.setEnabled(false);
			
		btAbbrechen.addActionListener(controller);
		buttonPanel.add(btAbbrechen);

		return buttonPanel;
	}


	private JPanel createMainPanel() {
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setOpaque(false);
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.add("Buchen", getBuchenPanel());
		tabbedPane.add("Speichern", getSpeichernPanel());

		mainPanel.add(tabbedPane, BorderLayout.CENTER);
		
		return mainPanel;
	}

	/**
	 * liefert das Einstellungs-Panel f�r Buchen
	 * @return JPanel
	 */
	private JPanel getBuchenPanel(){
		JPanel buchenPanel = new JPanel();
		buchenPanel.setBackground(ColorKonstanten.COLOR_FORMPANEL_BG);
		
		JLabel lb1 = new JLabel("Automatische Kontoliste beim Buchen");
		JLabel lb2 = new JLabel("Kl�nge");
		
		
		cbAutomatischeKontoListe = new VaCheckBox();
		cbKlaenge = new VaCheckBox();
		
		
		

		Strut st1 = new Strut();
		GroupLayout gl_buchenPanel = new GroupLayout(buchenPanel);
		gl_buchenPanel.setHorizontalGroup(
			gl_buchenPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(st1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGroup(gl_buchenPanel.createSequentialGroup()
					.addGap(4)
					.addComponent(lb1)
					.addGap(8)
					.addComponent(cbAutomatischeKontoListe, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_buchenPanel.createSequentialGroup()
					.addGap(4)
					.addComponent(lb2)
					.addGap(227)
					.addComponent(cbKlaenge, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
		);
		gl_buchenPanel.setVerticalGroup(
			gl_buchenPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buchenPanel.createSequentialGroup()
					.addComponent(st1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addGroup(gl_buchenPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_buchenPanel.createSequentialGroup()
							.addGap(2)
							.addComponent(lb1))
						.addComponent(cbAutomatischeKontoListe, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(8)
					.addGroup(gl_buchenPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_buchenPanel.createSequentialGroup()
							.addGap(2)
							.addComponent(lb2))
						.addComponent(cbKlaenge, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		buchenPanel.setLayout(gl_buchenPanel);
		
		return buchenPanel;
		
	}



	/**
	 * liefert das SpeichernPanel
	 * @return JPanel
	 */
	private JPanel getSpeichernPanel(){
		JPanel speichernPanel = new JPanel();
		speichernPanel.setBackground(ColorKonstanten.COLOR_FORMPANEL_BG);
		
		JLabel lb1 = new JLabel("Datenbank-Verzeichnis");
		JLabel lb2 = new JLabel("Sicherungs-Verzeichnis");
		JLabel lb3 = new JLabel("Anzahl Sicherungskopieren");
		JLabel lb4 = new JLabel("Autom. Sicherung");
		lb4.setToolTipText("Autom. Sicherung bei Programmende");
		
		
		
		tfDBFile = new BaseTextField();
		tfDBFile.setEditable(false);
		tfDBFile.setBackground(Color.WHITE);

		
		VaButton searchButton = new VaButton("Durchsuchen...");
		searchButton.setText("Auswahl");
		searchButton.setActionCommand("SearchDBDir");
		searchButton.addActionListener(new SearchActionListener());

		tfBackupDir = new BaseTextField();

		tfAnzahlSicherungen = new TextFieldNumericInteger();
		tfAnzahlSicherungen.setPreferredSize(new Dimension(100,0));
		cbAutoBackup = new VaCheckBox();
		cbAutoBackup.setToolTipText("Autom. Sicherung bei Programmende");
		
		Strut st1 = new Strut();
		
		JLabel lblCharsetFrCsvexport = new JLabel("Charset f\u00FCr CSV-Export");
		
		tfCSVExportCharset = new BaseTextField();
		GroupLayout gl_speichernPanel = new GroupLayout(speichernPanel);
		gl_speichernPanel.setHorizontalGroup(
			gl_speichernPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_speichernPanel.createSequentialGroup()
					.addGroup(gl_speichernPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(st1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_speichernPanel.createSequentialGroup()
							.addGap(4)
							.addGroup(gl_speichernPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lb3)
								.addComponent(lb1)
								.addComponent(lb2)
								.addComponent(lb4)
								.addComponent(lblCharsetFrCsvexport))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_speichernPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(cbAutoBackup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(Alignment.TRAILING, gl_speichernPanel.createSequentialGroup()
									.addGroup(gl_speichernPanel.createParallelGroup(Alignment.TRAILING)
										.addComponent(tfCSVExportCharset, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
										.addComponent(tfBackupDir, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
										.addComponent(tfAnzahlSicherungen, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
										.addComponent(tfDBFile, GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(searchButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
					.addContainerGap())
		);
		gl_speichernPanel.setVerticalGroup(
			gl_speichernPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_speichernPanel.createSequentialGroup()
					.addGroup(gl_speichernPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_speichernPanel.createSequentialGroup()
							.addComponent(st1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addGroup(gl_speichernPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_speichernPanel.createSequentialGroup()
									.addGap(5)
									.addComponent(lb1))
								.addGroup(gl_speichernPanel.createSequentialGroup()
									.addGap(3)
									.addGroup(gl_speichernPanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(tfDBFile, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(searchButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
							.addGap(13)
							.addComponent(lb2)
							.addGroup(gl_speichernPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_speichernPanel.createSequentialGroup()
									.addGap(10)
									.addComponent(lb3)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(lb4))
								.addGroup(gl_speichernPanel.createSequentialGroup()
									.addGap(8)
									.addComponent(tfAnzahlSicherungen, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(cbAutoBackup, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))))
						.addGroup(gl_speichernPanel.createSequentialGroup()
							.addGap(44)
							.addComponent(tfBackupDir, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_speichernPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCharsetFrCsvexport)
						.addComponent(tfCSVExportCharset, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(110, Short.MAX_VALUE))
		);
		speichernPanel.setLayout(gl_speichernPanel);
		
		return speichernPanel;
		
	}
	public BaseTextField getTfCSVExportCharset() {
		return tfCSVExportCharset;
	}
}
