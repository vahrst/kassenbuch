package de.diakon.main.gui;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.LineBorder;

import de.diakon.entities.Buchungsperiode;
import de.vahrst.application.gui.Strut;

/**
 * 
 * @author Thomas
 * @version 
 * @file InfoPanel.java
 */
public class InfoPanel extends JPanel {

	private JLabel lbAnfangsSaldo = null;
	private JLabel lbEingang = null;
	private JLabel lbAusgaben = null;
	private JLabel lbAktSaldo = null;
	
	/**
	 * Constructor for InfoPanel.
	 */
	public InfoPanel() {
		super();
		initialize();
	}

	private void initialize(){
		setPreferredSize(new Dimension(300,100));
		setMinimumSize(new Dimension(100,100));
		setOpaque(false);;
		
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createTitledBorder(new LineBorder(Color.BLACK),"Salden"));
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(1,4,1,4);

		
		JLabel lb1 = new JLabel("Kassenbuch Anfangssaldo");
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(lb1, gbc);
		
		JLabel lb2 = new JLabel("Kassenbuch Eingang");
		gbc.gridy = 1;
		this.add(lb2, gbc);

		JLabel lb3 = new JLabel("Kassenbuch Ausgaben");
		gbc.gridy = 2;
		this.add(lb3, gbc);

		JLabel lb4 = new JLabel("Kassenbuch aktueller Saldo");
		gbc.gridy = 3;
		this.add(lb4, gbc);
		
		lbAnfangsSaldo = new JLabel();
		lbAnfangsSaldo.setOpaque(true);
		lbAnfangsSaldo.setBackground(Color.WHITE);
		lbAnfangsSaldo.setHorizontalAlignment(JLabel.RIGHT);
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.weightx = 0.5;
		this.add(lbAnfangsSaldo, gbc);
		
		lbEingang = new JLabel();
		lbEingang.setOpaque(true);
		lbEingang.setBackground(Color.WHITE);
		lbEingang.setHorizontalAlignment(JLabel.RIGHT);
		gbc.gridy = 1;
		this.add(lbEingang, gbc);

		lbAusgaben = new JLabel();
		lbAusgaben.setOpaque(true);
		lbAusgaben.setBackground(Color.WHITE);
		lbAusgaben.setHorizontalAlignment(JLabel.RIGHT);
		gbc.gridy = 2;
		this.add(lbAusgaben, gbc);

		lbAktSaldo = new JLabel();
		lbAktSaldo.setOpaque(true);
		lbAktSaldo.setBackground(Color.WHITE);
		lbAktSaldo.setHorizontalAlignment(JLabel.RIGHT);
		gbc.gridy = 3;
		this.add(lbAktSaldo, gbc);

		Strut strut1 = new Strut();
		gbc.gridy= 5;
		gbc.weighty = 1.0;
		this.add(strut1, gbc);
	}


	public void setValues(Buchungsperiode periode){
		if(periode != null){
			lbAnfangsSaldo.setText(periode.getAnfangsSaldo().format());
			lbEingang.setText(periode.getHabenSaldo().format());
			lbAusgaben.setText(periode.getSollSaldo().format());
			lbAktSaldo.setText(periode.getSaldo().format());
		}
	}
	

	public static void main(String[] args) {
		try {
//			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			JFrame testFrame = new JFrame("Test InfoPanel");
			testFrame.getContentPane().add(
				new InfoPanel(),
				BorderLayout.CENTER);
			testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			testFrame.pack();
			testFrame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
