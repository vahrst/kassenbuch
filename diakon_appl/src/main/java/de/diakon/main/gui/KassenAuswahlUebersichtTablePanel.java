package de.diakon.main.gui;

import java.awt.event.ActionListener;

import javax.swing.table.*;

import de.diakon.gui.StammdatenUebersichtTablePanel;

/**
 * 
 * @author Thomas
 * @version 
 * @file KassenAuswahlUebersichtTablePanel.java
 */
public class KassenAuswahlUebersichtTablePanel
	extends StammdatenUebersichtTablePanel {

	/**
	 * Constructor for KassenAuswahlUebersichtTablePanel.
	 * @param tm
	 * @param controller
	 */
	public KassenAuswahlUebersichtTablePanel(
		TableModel tm,
		ActionListener controller) {
		super(tm, controller);
	}

	/**
	 * @see de.diakon.gui.StammdatenUebersichtTablePanel#getTableColumnModel()
	 */
	protected TableColumnModel getTableColumnModel() {
		TableColumnModel tcm = new DefaultTableColumnModel();

		TableColumn ktoColumn = new TableColumn(0);
		ktoColumn.setHeaderValue("Bezeichnung");
		tcm.addColumn(ktoColumn);


		return tcm;
	}

}
