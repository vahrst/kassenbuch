package de.diakon.main.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;

import de.vahrst.application.gui.BaseTextField;
import de.vahrst.application.gui.FormPanel;
import de.vahrst.application.gui.VaButton;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file DruckDebitorenPanel.java
 */
public class ExportDBPanel extends FormPanel {
	private VaButton btStarten;
	private VaButton btAbbrechen;

	private BaseTextField tfFilename;

	/**
	 * Constructor for DruckDebitorenPanel.
	 */
	public ExportDBPanel(AbstractController controller) {
		super();
		this.setPaintHeaderArea(false);
		
		initialize(controller);
	}

	/**
	 * initialisiert dieses Panel
	 */
	private void initialize(AbstractController controller){
		this.setLayout(new BorderLayout());
		
		this.add(createDatenPanel(), BorderLayout.CENTER);
		this.add(createButtonPanel(controller), BorderLayout.SOUTH);	
		
	}

	/**
	 * erzeugt das Eingabepanel
	 */
	private JPanel createDatenPanel(){
		JPanel datenPanel = new JPanel();
		datenPanel.setOpaque(false);
		
		JTextArea txtrDieGesamteDatenbasis = new JTextArea();
		txtrDieGesamteDatenbasis.setOpaque(false);
		txtrDieGesamteDatenbasis.setEditable(false);
		txtrDieGesamteDatenbasis.setWrapStyleWord(true);
		txtrDieGesamteDatenbasis.setLineWrap(true);
		txtrDieGesamteDatenbasis.setText("Die gesamte Datenbasis mit allen Kassen und den darin enthaltenen Buchungen, Konten und Kostenstellen wird in die vorgegebene Datei exportiert.");
		
		JLabel lblDateiname = new JLabel("Dateiname");
		
		tfFilename = new BaseTextField();
		GroupLayout gl_datenPanel = new GroupLayout(datenPanel);
		gl_datenPanel.setHorizontalGroup(
			gl_datenPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_datenPanel.createSequentialGroup()
					.addGroup(gl_datenPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_datenPanel.createSequentialGroup()
							.addGap(13)
							.addComponent(txtrDieGesamteDatenbasis, GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE))
						.addGroup(gl_datenPanel.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblDateiname)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(tfFilename, GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_datenPanel.setVerticalGroup(
			gl_datenPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_datenPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(txtrDieGesamteDatenbasis, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_datenPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDateiname)
						.addComponent(tfFilename, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(134, Short.MAX_VALUE))
		);
		datenPanel.setLayout(gl_datenPanel);
		

		return datenPanel;

	}


	public BaseTextField getTfFilename() {
		return tfFilename;
	}
	

	/**
	 * Erzeugt das ButtonPanel
	 */
	protected JPanel createButtonPanel(AbstractController controller){
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		buttonPanel.setMinimumSize(new Dimension(10, 24));
		buttonPanel.setPreferredSize(new Dimension(10, 24));
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		btStarten = new VaButton("Start");
		btStarten.setActionCommand("Start");
		btStarten.addActionListener(controller);
		buttonPanel.add(btStarten);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btAbbrechen = new VaButton("Abbrechen");
		btAbbrechen.setActionCommand("Abbrechen");
		btAbbrechen.addActionListener(controller);
		buttonPanel.add(btAbbrechen);


		return buttonPanel;		
	}
}
