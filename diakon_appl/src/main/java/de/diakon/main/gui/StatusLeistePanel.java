package de.diakon.main.gui;

import java.awt.Dimension;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

import de.vahrst.application.global.StatusAnzeige;

/**
 * 
 * @author Thomas
 * @version 
 */
public class StatusLeistePanel extends JPanel implements StatusAnzeige{

	JLabel lbTime = null;
	JLabel lbStatus = null;
	
	/**
	 * Constructor for StatusLeiste.
	 */
	public StatusLeistePanel() {
		super();
		initialize();
		StatusLeisteThread t = new StatusLeisteThread("TimerThread", this);
		t.setDaemon(true);
		t.start();
	}


	public void setStatusMeldung(String meldung){
		lbStatus.setText(meldung);
	}


	/**
	 * initialisiert dieses Panel
	 */
	private void initialize(){
		setMinimumSize(new Dimension(10, 25));
		setMaximumSize(new Dimension(10, 25));
		setPreferredSize(new Dimension(10,25));
		
		setBorder(new EtchedBorder(EtchedBorder.RAISED));
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		JLabel lb1 = new JLabel("");
		lb1.setBorder(BorderFactory.createLoweredBevelBorder());
		lb1.setMaximumSize(new Dimension(100,25));

		
		lbStatus = new JLabel("");
		lbStatus.setBorder(BorderFactory.createLoweredBevelBorder());
		lbStatus.setMaximumSize(new Dimension(1000,25));

		lbTime = new JLabel("");
		lbTime.setMinimumSize(new Dimension(100,25));
		lbTime.setBorder(BorderFactory.createLoweredBevelBorder());
		
		
		this.add(lb1);
		this.add(lbStatus);
		this.add(lbTime);
	}
}
