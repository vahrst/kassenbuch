package de.diakon.main.gui;

import java.awt.*;

import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file ExportDialog.java
 */
public class ExportDBDialog extends AbstractDialog {
	private ExportDBPanel panel;

	/**
	 * Constructor for ExportDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public ExportDBDialog(
		Frame owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for ExportDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public ExportDBDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, title, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for ExportDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public ExportDBDialog(
		Dialog owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}


	private void init(AbstractController controller){
		this.setTitle("Export Datenbank");
		this.setSize(550,300);
		
		panel = new ExportDBPanel(controller);
		getContentPane().add(panel, BorderLayout.CENTER);

		
		this.center();	
	}


	public ExportDBPanel getMainPanel(){
		return panel;	
	}

}
