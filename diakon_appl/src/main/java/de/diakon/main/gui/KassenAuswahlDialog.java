package de.diakon.main.gui;

import java.awt.*;

import javax.swing.JPanel;
import javax.swing.table.TableModel;

import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file KassenAuswahlDialog.java
 */
public class KassenAuswahlDialog extends AbstractDialog {
	private KassenAuswahlUebersichtPanel uebersichtPanel = null;

	/**
	 * Constructor for KassenAuswahlDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public KassenAuswahlDialog(
		Frame owner,
		boolean modal,
		AbstractController controller,
		TableModel model)
		throws HeadlessException {
		super(owner, modal, controller);
		this.initialize(model, controller);
	}

	/**
	 * Constructor for KassenAuswahlDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public KassenAuswahlDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller,
		TableModel model)
		throws HeadlessException {
		super(owner, title, modal, controller);
		this.initialize(model, controller);
	}

	private void initialize(TableModel model, AbstractController controller){
		this.setSize(400,250);
		uebersichtPanel = new KassenAuswahlUebersichtPanel(model, controller);
		JPanel cp = new JPanel(new BorderLayout());
		this.setContentPane(cp);
		
		cp.add(uebersichtPanel, BorderLayout.CENTER);
		
		//this.pack();
		this.center();	
	}

	public int getSelectedRow(){
		return uebersichtPanel.getSelectedRow();
	}
}
