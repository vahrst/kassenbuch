package de.diakon.main.gui;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.TableModel;

import de.diakon.gui.*;
import de.vahrst.application.gui.VaButton;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file KassenAuswahlUebersichtPanel.java
 */
public class KassenAuswahlUebersichtPanel extends StammdatenUebersichtPanel {


	/**
	 * Constructor for KassenAuswahlUebersichtPanel.
	 * @param tm
	 * @param controller
	 */
	public KassenAuswahlUebersichtPanel(
		TableModel tm,
		AbstractController controller) {
		super(tm, controller);
		this.setPaintHeaderArea(false);
	}

	/**
	 * @see de.diakon.gui.StammdatenUebersichtPanel#getTablePanel(TableModel, ActionListener)
	 */
	protected StammdatenUebersichtTablePanel getTablePanel(
		TableModel tm,
		ActionListener controller) {
		return new KassenAuswahlUebersichtTablePanel(tm, controller);
	}

	/**
	 * @see de.diakon.gui.StammdatenUebersichtPanel#getHeaderTitle()
	 */
	protected String getHeaderTitle() {
		return "Kasse ausw�hlen";
	}


	protected JPanel createButtonPanel(AbstractController controller){
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
//		buttonPanel.setBackground(ColorKonstanten.COLOR_BUTTONPANEL);
		buttonPanel.setMinimumSize(new Dimension(10, 24));
		buttonPanel.setPreferredSize(new Dimension(10, 24));
//		buttonPanel.setBorder(new EmptyBorder(3,0,0,0));			
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		btGeheZu = new VaButton("GeheZu");
		btGeheZu.setActionCommand("GeheZu");
		btGeheZu.addActionListener(controller);
		buttonPanel.add(btGeheZu);


		return buttonPanel;		
	}

}
