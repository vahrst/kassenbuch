package de.diakon.stammdaten.prozesse;

import javax.swing.JOptionPane;

import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.diakon.stammdaten.gui.*;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.*;
import de.vahrst.application.types.Fehlermeldung;

/**
 * 
 * @author Thomas
 * @version 
 * @file KontenAnlageController.java
 */
public class KontenAnlageController extends AbstractController {
	private KontenAnlageProcess process;
	private KontenPflegeDialog dialog;

		
	/**
	 * Constructor for KontenAnlageController.
	 */
	public KontenAnlageController(KontenAnlageProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#start()
	 */
	public void start() {
		logger.debug("start Controller");
		dialog = new KontenPflegeDialog(
			ApplicationContext.getMainApplicationWindow(),
			true,
			this,
			"Konto anlegen",
			process.getKonto(),
			true);

		initializeAspekts();
			
		dialog.setVisible(true);
			
			
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * L�scht diesen Controller.
	 */
	public void destroy(){
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog=null;
		}
	}
			
	public AbstractProcess getProcess(){
		return process;
	}
			
	/**
	 * initialisiert die Zuordnungstabelle von Aspekten zu 
	 * Eingabefeldern
	 */
	protected void initializeAspekts(){
		KontenPflegeMainPanel mainPanel = dialog.getMainPanel();		
		aspektTabelle.put(Konto.ASPEKT_KONTONUMMER, mainPanel.tfKonto);
	}



	// die Handler Methoden
	public void handleOkEvent(){
		logger.debug("Action: Ok");
		Konto kto = (Konto) dialog.getModel();
		try{
			process.createKonto(kto);
		}catch(PlausibilitaetsException e){
			Fehlermeldung m = e.getMeldungsListe().get(0);
			if(m != null){
				JOptionPane.showMessageDialog(dialog, m.getMessage(), "Fehlerhafte Eingabe", JOptionPane.INFORMATION_MESSAGE);
				focusOnAspekt(m.getAspekt());
			}
		}catch(PersistenceException e){
			String message = "Fehler bei der Anlage eines Kontos";
			logger.error(message, e);
			showErrorMessage(message);
			process.rollback();
		}
			
	}
	
	public void handleAbbrechenEvent(){
		logger.debug("Action: Abbrechen");
		process.rollback();
	}
}
