package de.diakon.stammdaten.prozesse;

import java.awt.event.ActionListener;
import java.io.*;

import javax.swing.*;

import de.diakon.entities.*;
import de.diakon.global.Konstanten;
import de.diakon.persistence.PersistenceException;
import de.diakon.stammdaten.gui.KostenstellenUebersichtPanel;
import de.vahrst.application.gui.ClientAreaPanel;
import de.vahrst.application.prozesse.*;
import de.vahrst.application.xml.Element;
/**
 * 
 * @author Thomas
 * @version 
 * @file KostenstellenPflegeController.java
 */
public class KostenstellenPflegeController
	extends AbstractController
	implements ActionListener {

	private KostenstellenPflegeProcess process = null;

	private KostenstellenListe kostenstellenListe = null;
	private KostenstellenUebersichtPanel uebersichtPanel = null;
	private ClientAreaPanel clpanel = null;

	/**
	 * Constructor for KostenstellenPflegeController.
	 */
	public KostenstellenPflegeController(KostenstellenPflegeProcess p) {
		super();
		this.process = p;
	}

	public void reactivate() {
		logger.debug("reactivate");

		clpanel.activate(uebersichtPanel);

	}

	/**
	 * Löscht diesen Controller. Schließt das 
	 * KostenstellenÜbersichtPanel.
	 */
	public void destroy() {
		clpanel.remove(uebersichtPanel);
		uebersichtPanel.setVisible(false);
	}

	/**
	 * allgemeine Start-Methode dieses Controllers
	 */
	public void start() {
		logger.debug("start Controller");

		try{
			kostenstellenListe = process.getKostenstellen(true);

			uebersichtPanel = new KostenstellenUebersichtPanel(kostenstellenListe, this);
			uebersichtPanel.setVisible(true);

			clpanel = ClientAreaProvider.getClientArea(Konstanten.CLIENT_AREA_FORM);
			clpanel.add(uebersichtPanel, "KostenstellenUebersicht");
		}catch(PersistenceException e){
			logger.error("Fehler beim Lesen der Kostenstellen", e);
		}
	}

	public void refreshKostenstellenListe(){
		logger.debug("refreshKostenstellenListe");
		try{
			kostenstellenListe = process.getKostenstellen(true);
			uebersichtPanel.setEntityListe(kostenstellenListe);
		}catch(PersistenceException e){
			logger.error("Fehler beim Aktualisiern der Kostenstellenliste", e);
		}
	
	}

	public AbstractProcess getProcess(){
		return process;
	}


	// die Handler-Methoden
	public void handleNeuEvent() {
		logger.debug("Action: Neu");
		process.startKostenstellenAnlageProcess();
	}

	public void handleDeleteEvent() {
		logger.debug("Action: Delete");
		int selectedRow = uebersichtPanel.getSelectedRow();
		if (selectedRow != -1) {

			int result =
				JOptionPane.showConfirmDialog(
					uebersichtPanel,
					"Soll die ausgewählte Kostenstelle wirklich gelöscht werden?",
					"Kostenstelle löschen",
					JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE);
			if (result == JOptionPane.YES_OPTION) {

				Kostenstelle kst = kostenstellenListe.getKostenstelleAt(selectedRow);
				try{
					process.deleteKostenstelle(kst);
				}catch(PersistenceException e){
					String message = "Fehler beim Löschen des Kostenstelles aufgetreten";
					logger.error(message, e);
					showErrorMessage(message);
					
				}

			}
		}
	}

	public void handleGeheZuEvent() {
		logger.debug("Action: Gehezu");
		int selectedRow = uebersichtPanel.getSelectedRow();
		if(selectedRow != -1){
			Kostenstelle kst = kostenstellenListe.getKostenstelleAt(selectedRow);
			process.startKostenstellenAendernProcess(kst);
		}
	}

	public void handleImportEvent() {
		logger.debug("Action: Import");
		JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
		fc.setDialogTitle("XML-Datei auswählen");
		int result = fc.showOpenDialog(uebersichtPanel);
		if(result == JFileChooser.APPROVE_OPTION){
			File file = fc.getSelectedFile();
			
			try{
				Element kostenstellen = process.loadImportFile(file.getAbsolutePath());
				int count = process.getKostenstellenAnzahl(kostenstellen);
				if(count == 0){
					JOptionPane.showMessageDialog(uebersichtPanel, "Import-Datei enthält keine Kostenstellen-Daten");
				}else{
					// jetzt beginnt der Import. Das ganze in einem eigenen
					// Thread. Dazu erzeugen wir uns einen kleinen Fortschrittsdialog
					// ...
					ProgressMonitor pm = new ProgressMonitor(uebersichtPanel, "Kostenstellen importieren","",0,count);
					pm.setProgress(0);
					
					process.startImport(kostenstellen, pm);
					
					refreshKostenstellenListe();
				}
				
			}catch(IOException e){
				logger.error("Fehler!", e);
				showErrorMessage("Fehler beim Einlesen der Kostenstellendaten");
			}
		}
		
		
	}

	// private Service-Methoden

}
