package de.diakon.stammdaten.prozesse;

import java.util.*;

import javax.swing.JOptionPane;

import de.diakon.persistence.PersistenceException;
import de.diakon.stammdaten.gui.KassenNeuanlageDialog;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file KassenAnlageController.java
 */
public class KassenAnlageController extends AbstractController {
	private KassenAnlageProcess process;
	private KassenNeuanlageDialog dialog;
	/**
	 * Constructor for KassenAnlageController.
	 */
	public KassenAnlageController(KassenAnlageProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		logger.debug("start");
		Kassendaten vorgabe = new Kassendaten();

		Calendar c = GregorianCalendar.getInstance();
		vorgabe.setMonat(c.get(Calendar.MONTH)+1);
		vorgabe.setJahr(c.get(Calendar.YEAR));

		dialog = new KassenNeuanlageDialog(
			ApplicationContext.getMainApplicationWindow(),
			true, 
			this, 
			"Neuanlage Kasse", 
			vorgabe);
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

	
	public void handleAbbrechenEvent(){
		process.rollback();
	}


	public void handleOkEvent(){
		Kassendaten daten = (Kassendaten) dialog.getModel();
		if(daten.getBezeichnung() == null || daten.getBezeichnung().trim().length() == 0){
			JOptionPane.showMessageDialog(dialog, "Bitte Bezeichnung eingeben");
			return;
		}
		if(daten.getMonat() < 1 || daten.getMonat() > 12){
			JOptionPane.showMessageDialog(dialog, "Was soll das f�r ein Monat sein?");
			return;
		}
		if(daten.getJahr() < 2000){
			JOptionPane.showMessageDialog(dialog, "Eine Reise in die Vergangenheit? - Zur�ck in die Zukunft!");
			return;
		}
		
		try{
			process.createKasse(daten);
			process.commit();
		}catch(PersistenceException e){
			String message = "Fehler bei der Neuanlage einer Kasse";
			logger.error(message, e);
			JOptionPane.showMessageDialog(dialog, message);
			process.rollback();
		}
			
	}
}
