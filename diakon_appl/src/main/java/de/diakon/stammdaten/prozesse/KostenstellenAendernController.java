package de.diakon.stammdaten.prozesse;

import de.diakon.entities.Kostenstelle;
import de.diakon.stammdaten.gui.KostenstellenPflegeDialog;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 */
public class KostenstellenAendernController extends AbstractController {
	private KostenstellenAendernProcess process;
	private KostenstellenPflegeDialog dialog;

	/**
	 * Constructor for KontenAendernController.
	 */
	public KostenstellenAendernController(KostenstellenAendernProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#start()
	 */
	public void start() {
		dialog = new KostenstellenPflegeDialog(ApplicationContext.getMainApplicationWindow(), true, this, "Kostenstelle �ndern", process.getKostenstelle(), false);
		dialog.setVisible(true);
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog=null;
		}
	}

	public AbstractProcess getProcess(){
		return process;
	}
	
	// die Handler Methoden
	public void handleOkEvent(){
		logger.debug("Action: Ok");
		Kostenstelle kst = (Kostenstelle) dialog.getModel();
		process.updateKostenstelle(kst);
	}
	
	public void handleAbbrechenEvent(){
		logger.debug("Action: Abbrechen");
		process.rollback();
	}
}
