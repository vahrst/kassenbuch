package de.diakon.stammdaten.prozesse;

import de.diakon.entities.*;
import de.diakon.main.prozesse.KassenAuswahl;
import de.diakon.persistence.*;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file KassenAnlageProcess.java
 */
public class KassenAnlageProcess extends AbstractProcess implements KassenAuswahl{
	private KassenAnlageController controller ;

	/** die Id der neu angelegten Kasse */
	private long kassenId = 0;
	
	/**
	 * Constructor for KassenAnlageProcess.
	 * @param parent
	 * @param eth
	 */
	public KassenAnlageProcess(
		AbstractProcess parent,
		EndTransactionHandler eth) {
		super(parent, eth);
		openController();
	}

	public AbstractController getController(){
		return controller;
	}

	/**
	 * startet den Controller
	 */
	private void openController(){
		controller = new KassenAnlageController(this);
		controller.start();	
		
	}
	
	/**
	 * liefert - im commit-Fall - die Id der neu angelegten Kasse
	 */
	public long getKassenId(){
		return kassenId;
	}

	
	/**
	 * erzeugt eine Kasse und die erste Buchungsperiode
	 */
	public void createKasse(Kassendaten daten) throws PersistenceException {
		logger.debug("erzeuge eine neue Kasse");
		Kasse kasse = new Kasse(daten.getBezeichnung(), daten.getMonat(), daten.getJahr(), daten.getKassenkonto(), daten.getBuchungskreis(), daten.getMandant());
		try{
			PersistenceManager.getPersistenceManager().startTransaction();
			new KasseHome().addKasse(kasse);
			Buchungsperiode p = new Buchungsperiode(daten.getMonat(), daten.getJahr(), daten.getAnfangssaldo());
			new BuchungsperiodeHome().addBuchungsperiode(p, kasse);
			
		}catch(PersistenceException e){
			PersistenceManager.getPersistenceManager().rollbackTransaction();
			throw e;
		}finally{
			PersistenceManager.getPersistenceManager().commitTransaction();
			kassenId = kasse.getObjectId();
			
		}
	}
}
