package de.diakon.stammdaten.prozesse;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import de.diakon.entities.Konto;
import de.diakon.entities.KontoHome;
import de.diakon.entities.Kostenstelle;
import de.diakon.entities.KostenstelleHome;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.EndTransactionHandler;
import de.vahrst.application.xml.Document;
import de.vahrst.application.xml.DocumentImpl;
import de.vahrst.application.xml.Element;
import de.vahrst.application.xml.XMLWriter;

/**
 * 
 * @author Thomas
 * @version 
 * @file ExportProcess.java
 */
public class ExportProcess extends AbstractProcess {
	private ExportController controller;
	
	/**
	 * Constructor for ExportProcess.
	 * @param parent
	 * @param eth
	 */
	public ExportProcess(AbstractProcess parent, EndTransactionHandler eth) {
		super(parent, eth);
		controller = new ExportController(this);
		controller.start();
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}


	public void exportStammdaten(boolean exportKonten, boolean exportKst, String filename) throws PersistenceException, IOException{
		log.info("Export Stammdaten nach " + filename);
		log.info("Konten = " + exportKonten + "  Kostenstellen = " + exportKst);
		
		Document doc = new DocumentImpl();
		Element root = doc.createElement("Stammdaten");
		doc.appendChild(root);
		
		if(exportKonten){
			exportKonten(root, doc);
		}
		
		if(exportKst){
			exportKostenstellen(root, doc);
		}

		
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(filename), "UTF8");
		
		XMLWriter.print(doc, writer);
		writer.close();
		
	}

	private void exportKonten(Element root, Document factory) throws PersistenceException{
		KontoHome home = new KontoHome();
		List<Konto> liste = home.getAllKonten().getktoListe();
		
		Element kontoRoot = factory.createElement("Konten");
		root.appendChild(kontoRoot);
			
		for(int i=0;i<liste.size();i++){
			Konto kto = liste.get(i);
			kontoRoot.appendChild(kto.toXML(factory));
				
		}
	}	
		
	private void exportKostenstellen(Element root, Document factory) throws PersistenceException{
		KostenstelleHome home = new KostenstelleHome();
		List<Kostenstelle> liste = home.getAllKostenstellen().getKstListe();
		
		Element kstRoot = factory.createElement("Kostenstellen");
		root.appendChild(kstRoot);
			
		for(int i=0;i<liste.size();i++){
			Kostenstelle kst = liste.get(i);
			kstRoot.appendChild(kst.toXML(factory));
				
		}
	}	

}
