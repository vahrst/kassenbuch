package de.diakon.stammdaten.prozesse;

import java.io.File;

import javax.swing.JOptionPane;

import de.diakon.stammdaten.gui.*;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file ExportController.java
 */
public class ExportController extends AbstractController {
	private ExportProcess process;
	private ExportDialog dialog;
	/**
	 * Constructor for ExportController.
	 */
	public ExportController(ExportProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		dialog = new ExportDialog(ApplicationContext.getMainApplicationWindow(), true, this);
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

	public void handleStartEvent(){
		ExportPanel panel = dialog.getMainPanel();
		boolean exportKonten = panel.cbExportKonten.isSelected();
		boolean exportKst = panel.cbExportKostenstellen.isSelected();
		String filename = panel.tfExportfile.getText();
		
		if(!exportKonten && !exportKst){
			JOptionPane.showMessageDialog(dialog, "Was genau wollen Sie denn exportieren ??");
			return;
		}
		
		File f = new File(filename);
		if(f.isDirectory()){
			JOptionPane.showMessageDialog(dialog, "Die angegebene Datei ist ein Verzeichnis!");
			return;
		}
		
		if(f.exists()){
			int result = JOptionPane.showConfirmDialog(dialog, "Datei existiert bereits. Soll die Datei überschrieben werden?", "Datei bereits vorhanden", JOptionPane.YES_NO_OPTION);	
			if(result != 0)
				return;
		}

		try{
			process.exportStammdaten(exportKonten, exportKst, filename);
		}catch(Exception e){
			logger.error("Fehler beim Exportieren der Stammdaten", e);
			showErrorMessage("Fehler beim Exportieren der Stammdaten aufgetreten");
			process.rollback();
		}
		
		ApplicationContext.statusmeldung("Stammdaten wurden exportiert");
		process.commit();
		
	}
	
	public void handleAbbrechenEvent(){
		process.rollback();	
	}


}
