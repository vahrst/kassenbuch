package de.diakon.stammdaten.prozesse;

import java.io.*;

import org.apache.log4j.Logger;

import de.vahrst.application.xml.*;

/**
 * Diese Klasse dient zum Laden einer Export-Datei.
 * @author Thomas
 * @version 
 * @file ImportExportFileLoader.java
 */
public class ImportExportFileLoader {
	private static Logger logger = Logger.getLogger(ImportExportFileLoader.class);

	/**
	 * Constructor for ImportExportFileLoader.
	 */
	public ImportExportFileLoader() {
		super();
	}


	public static Element getRootElement(String filename, String name) throws IOException{
		Document doc = loadFile(filename);

		Element root = doc.getDocumentElement();
		Element element = root.getFirstElementByName(name);
		
		return element;		
	
		
	}
	
	
	private static Document loadFile(String filename) throws IOException{
		byte[] daten = loadBytes(filename);
	
		// erstmal pr�fen, ob dies eine Exportdatei aus dem alten Kassenprogramm
		// ist. Dazu erzeugen wir aus dem byte[] einen String mit codepage cp850
		// Dann suchen wir nach dem fehlerhaften Eintrag 'Kostenstllen' im Exportfile
		// Wenn wir dieses Wort finden, handelt es sich um eine alte Exportdatei.
		String xmlString = new String(daten, "Cp850");
		if(xmlString.indexOf("Kostenstllen") != -1){
			logger.info("Export-Datei aus dem alten Kassenprogramm erkannt");
			xmlString = xmlString.replaceAll("Kostenstllen", "Kostenstellen");
			return Parser.parse(xmlString);
		}
		
		logger.info("Export-Datei im neuen Format (utf8) erkannt");
		xmlString = new String(daten, "UTF8");
		return Parser.parse(xmlString);
		
	}
	
	
	private static byte[] loadBytes(String filename) throws IOException{
		File file = new File(filename);
		
		int laenge = (int)file.length();
		byte[] daten = new byte[laenge];

		BufferedInputStream in = new BufferedInputStream( new FileInputStream(file));
		
		int count = 0;
		int sum = 0;
		while(count != -1 && sum < laenge){
			count = in.read(daten, count, laenge-sum);
			if(count != -1){
				sum += count;
			}
		}
		
		if(count == -1){
			throw new IOException("konnte Datei nicht vollst�ndig lesen");
		}
		
		return daten;					
			
	}
}
