package de.diakon.stammdaten.prozesse;

import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 */
public class KostenstellenAnlageProcess extends AbstractProcess {
	private KostenstellenAnlageController controller;

	private Kostenstelle neueKostenstelle = null;

	/**
	 * Constructor for KontenAnlageProcess.
	 */
	public KostenstellenAnlageProcess(AbstractProcess parent, EndTransactionHandler eth) {
		super(parent, eth);
		openController();
	}

	/**
	 * erzeugt das korrespondieren Controller-Objekt
	 */
	private void openController(){
		logger.debug("openController");
		
		controller = new KostenstellenAnlageController(this);
		controller.start();
	}

	public AbstractController getController(){
		return controller;
	}

	/**
	 * liefert eine leer intialisiertes Kostenstelle
	 */
	public Kostenstelle getKostenstelle(){
		return new Kostenstelle();
	}
	
	/**
	 * legt die neue Kosteenstelle an.
	 */
	public void createKostenstelle(Kostenstelle kst){
		logger.debug("Anlage Kostenstelle: " + kst);
		// hier noch Plausis pr�fen...
		try{
			new KostenstelleHome().addKostenstelle(kst);
			neueKostenstelle = kst;
			commit();
		}catch(PersistenceException e){
			String message = "Fehler bei der Anlage einer Kostenstelle";
			logger.error(message, e);
			controller.showErrorMessage(message);
			rollback();
		}
	}
	
	/**
	 * liefert die neu angelegte Kostenstelle.
	 */
	public Kostenstelle getAddedKostenstelle(){
		return neueKostenstelle;
	}
}
