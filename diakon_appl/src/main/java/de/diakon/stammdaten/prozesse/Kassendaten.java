package de.diakon.stammdaten.prozesse;

import de.vahrst.application.types.Waehrungsbetrag;

/**
 * Kapselt die Eingabedaten f�r KassenNeuanlage 
 * @author Thomas
 * @version 
 * @file Kassendaten.java
 */
public class Kassendaten {

	private String bezeichnung;
	private int monat;
	private int jahr;
	private Waehrungsbetrag anfangssaldo;
	
	private int kassenkonto;
	private int buchungskreis;
	private String mandant;

	/**
	 * Constructor for Kassendaten.
	 */
	public Kassendaten() {
		super();
	}


	/**
	 * Returns the anfangssaldo.
	 * @return Waehrungsbetrag
	 */
	public Waehrungsbetrag getAnfangssaldo() {
		return anfangssaldo;
	}

	/**
	 * Returns the bezeichnung.
	 * @return String
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * Returns the jahr.
	 * @return int
	 */
	public int getJahr() {
		return jahr;
	}

	/**
	 * Returns the monat.
	 * @return int
	 */
	public int getMonat() {
		return monat;
	}

	/**
	 * Sets the anfangssaldo.
	 * @param anfangssaldo The anfangssaldo to set
	 */
	public void setAnfangssaldo(Waehrungsbetrag anfangssaldo) {
		this.anfangssaldo = anfangssaldo;
	}

	/**
	 * Sets the bezeichnung.
	 * @param bezeichnung The bezeichnung to set
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * Sets the jahr.
	 * @param jahr The jahr to set
	 */
	public void setJahr(int jahr) {
		this.jahr = jahr;
	}

	/**
	 * Sets the monat.
	 * @param monat The monat to set
	 */
	public void setMonat(int monat) {
		this.monat = monat;
	}
	
	public void setBuchungskreis(int buchungskreis) {
		this.buchungskreis = buchungskreis;
	}
	public int getBuchungskreis() {
		return buchungskreis;
	}
	public void setMandant(String mandant) {
		this.mandant = mandant;
	}
	public String getMandant() {
		return mandant;
	}
	public int getKassenkonto() {
		return kassenkonto;
	}
	public void setKassenkonto(int kassenkonto) {
		this.kassenkonto = kassenkonto;
	}

}
