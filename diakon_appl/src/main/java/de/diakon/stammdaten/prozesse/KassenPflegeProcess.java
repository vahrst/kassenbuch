package de.diakon.stammdaten.prozesse;

import de.diakon.entities.*;
import de.diakon.main.prozesse.KassenAuswahl;
import de.diakon.persistence.*;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file KassenAnlageProcess.java
 */
public class KassenPflegeProcess extends AbstractProcess{
	private KassenPflegeController controller ;

	// Referenz auf die zu pflegende Kasse.
	private Kasse kasse;
	
	/**
	 * Constructor for KassenAnlageProcess.
	 * @param parent
	 * @param eth
	 */
	public KassenPflegeProcess(	AbstractProcess parent,	EndTransactionHandler eth,	Kasse kasse) {
		super(parent, eth);

		this.kasse = kasse;
		openController();
		
	}

	public AbstractController getController(){
		return controller;
	}

	/**
	 * startet den Controller
	 */
	private void openController(){
		controller = new KassenPflegeController(this);
		controller.start();	
		
	}
	
	/**
	 * liefert die Kasse
	 * @return
	 */
	public Kasse getKasse() {
		return kasse;
	}
	
	public Kassendaten getKassendaten(){
		Kassendaten kassendaten = new Kassendaten();

		kassendaten.setBezeichnung(kasse.getName());
		kassendaten.setBuchungskreis(kasse.getBuchungskreis());
		kassendaten.setMandant(kasse.getMandant());
		kassendaten.setKassenkonto(kasse.getKassenkonto());
		
		return kassendaten;
	}

	/**
	 * 
	 * @param daten
	 */
	public void updateKasse(Kassendaten daten)  throws PersistenceException{
		kasse.setName(daten.getBezeichnung());
		kasse.setBuchungskreis(daten.getBuchungskreis());
		kasse.setMandant(daten.getMandant());
		kasse.setKassenkonto(daten.getKassenkonto());

		try{
			PersistenceManager.getPersistenceManager().startTransaction();
			new KasseHome().changeKasse(kasse);
			
		}catch(PersistenceException e){
			PersistenceManager.getPersistenceManager().rollbackTransaction();
			throw e;
		}finally{
			PersistenceManager.getPersistenceManager().commitTransaction();
		}
		
		
	}
	
	
//	/**
//	 * erzeugt eine Kasse und die erste Buchungsperiode
//	 */
//	public void createKasse(Kassendaten daten) throws PersistenceException {
//		logger.debug("erzeuge eine neue Kasse");
//		Kasse kasse = new Kasse(daten.getBezeichnung(), daten.getMonat(), daten.getJahr(), daten.getKassenkonto(), daten.getBuchungskreis(), daten.getMandant());
//		try{
//			PersistenceManager.getPersistenceManager().startTransaction();
//			new KasseHome().addKasse(kasse);
//			Buchungsperiode p = new Buchungsperiode(daten.getMonat(), daten.getJahr(), daten.getAnfangssaldo());
//			new BuchungsperiodeHome().addBuchungsperiode(p, kasse);
//			
//		}catch(PersistenceException e){
//			PersistenceManager.getPersistenceManager().rollbackTransaction();
//			throw e;
//		}finally{
//			PersistenceManager.getPersistenceManager().commitTransaction();
//			kassenId = kasse.getObjectId();
//			
//		}
//	}
}
