package de.diakon.stammdaten.prozesse;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;

import de.diakon.entities.Kasse;
import de.diakon.entities.KassenKontext;
import de.diakon.persistence.PersistenceException;
import de.diakon.stammdaten.gui.KassenNeuanlageDialog;
import de.diakon.stammdaten.gui.KassenPflegeDialog;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;

/**
 * 
 * @author Thomas
 * @version 
 * @file KassenAnlageController.java
 */
public class KassenPflegeController extends AbstractController {
	private KassenPflegeProcess process;
	private KassenPflegeDialog dialog;
	/**
	 * Constructor for KassenAnlageController.
	 */
	public KassenPflegeController(KassenPflegeProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		logger.debug("start");

		Kassendaten vorgabe = process.getKassendaten();
		
		dialog = new KassenPflegeDialog(
			ApplicationContext.getMainApplicationWindow(),
			true, 
			this, 
			"Pflege Kassendaten", 
			vorgabe);
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

	
	public void handleAbbrechenEvent(){
		process.rollback();
	}


	public void handleOkEvent(){
		Kassendaten daten = (Kassendaten) dialog.getModel();
		if(daten.getBezeichnung() == null || daten.getBezeichnung().trim().length() == 0){
			JOptionPane.showMessageDialog(dialog, "Bitte Bezeichnung eingeben");
			return;
		}
		
		try{
			process.updateKasse(daten);
			process.commit();
		}catch(PersistenceException e){
			String message = "Fehler bei der Neuanlage einer Kasse";
			logger.error(message, e);
			JOptionPane.showMessageDialog(dialog, message);
			process.rollback();
		}
			
	}
}
