package de.diakon.stammdaten.prozesse;

import java.io.IOException;

import javax.swing.ProgressMonitor;

import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.*;
import de.vahrst.application.xml.*;


/**
 * Process-Objekt f�r Pflege der Konten.
 * @author Thomas
 * @version 
 */
public class KontenPflegeProcess extends AbstractProcess{

	private KontenPflegeController controller = null;
	private KontenListe kontenListe = null;
	
	// EndTransactionHandler f�r Anlage
	class AnlageETH extends DefaultEndTransactionHandler{
		public void commit(AbstractProcess source){
			commitAnlage((KontenAnlageProcess)source);
		}
	}
	
	class AendernETH extends DefaultEndTransactionHandler{
		public void commit(AbstractProcess source){
			commitAendern((KontenAendernProcess)source);
		}
	}
	
	// Thread-Klasse f�r Import von Konten
	class ImportThread implements Runnable{
		private Element konten;
		private ProgressMonitor monitor;
		
		ImportThread(Element kontenElement, ProgressMonitor pm){
			super();
			konten = kontenElement;
			monitor = pm;
		} 
		
		public void run(){

			ElementEnumeration enum1 = konten.getElementsByTagName("Konto");
			int zaehler = 0;
			KontoHome kontoHome = new KontoHome();
			while(enum1.hasMoreElements() && !monitor.isCanceled()){
				Element kontoElement = enum1.nextElement();
				try{
					Konto kto = new Konto(kontoElement);
					kontoHome.addKonto(kto);						

				}catch(Exception e){
				}
				
				zaehler++;
				
				monitor.setProgress(zaehler);
				monitor.setNote("Eingelesen: " + zaehler + " von " + monitor.getMaximum());
			}
 			controller.refreshKontenListe();
			monitor.close();
		}
	}
	
	/**
	 * Constructor for KontenPflegeProcess.
	 */
	public KontenPflegeProcess(AbstractProcess parent, EndTransactionHandler eth) {
		super(parent, eth);
		openController();
	}

	/**
	 * erzeugt das korrespondieren Controller-Objekt
	 */
	private void openController(){
		logger.debug("openController");
		
		controller = new KontenPflegeController(this);
		controller.start();
	}

	public AbstractController getController(){
		return controller;
	}
	
	/**
	 * Liefert die Liste aller Konten, als ArrayList
	 */
	public KontenListe getKonten(boolean reload)throws PersistenceException{
		if(kontenListe == null || reload){
			kontenListe = new KontoHome().getAllKonten();
		}
		return kontenListe;
	}
	
	/**
	 * l�scht das vorgegebene Konto
	 */
	public void deleteKonto(Konto kto)throws PersistenceException{
		new KontoHome().deleteKonto(kto);	
		kontenListe.remove(kto);
	}
	
	/** 
	 * Neuanlage eines Kontos. Das �bernimmt ein weiterer
	 * Prozess
	 */
	public void startKontoAnlageProcess(){
		new KontenAnlageProcess(this, new AnlageETH());
	}

	/**
	 * l�dt das vorgegebene XML-File und liefert das 
	 * Root-Element f�r Konten zur�ck
	 */
	public Element loadImportFile(String filename)throws IOException{
		return ImportExportFileLoader.getRootElement(filename, "Konten");
		
		
	}
	
	/**
	 * liefert zum vorgegebenen Root-Element f�r Konten die
	 * Anzahl der enthaltenen Konto-Definitionen
	 */
	public int getKontoAnzahl(Element kontenElement){
		ElementEnumeration enum1 = kontenElement.getElementsByTagName("Konto");
		
		// einmal vorab zaehlen, wieviele Konten wir haben:
		int zaehler = 0;
		while(enum1.hasMoreElements()){
			enum1.nextElement();
			zaehler++;
		}
		return zaehler;		
		
	}
	
	
	/**
	 * Importiert Daten aus dem vorgegebene XML-Element. Das
	 * ganze l�uft in einem eigenen Thread ab.
	 */
	public void startImport(Element kontenElement, ProgressMonitor monitor) {
		log.info("Import Konten");
		
		ImportThread it = new ImportThread(kontenElement, monitor);
		Thread t = new Thread(it);
		t.start();
	}

	/**
	 * �ndern eines bestehenden Kontos. Das �bernimmt ein weiterer
	 * Prozess
	 */
	public void startKontoAendernProcess(Konto kto){
		new KontenAendernProcess(this, new AendernETH(), (Konto) kto.clone());
	}
	




	private void commitAnlage(KontenAnlageProcess anlProcess){
		Konto kto = anlProcess.getAddedKonto();
		if(kto != null){
			kontenListe.add(kto);
		}else{
			logger.warn("Konto Anlage Commit, aber kein neues Konto im Process gefunden");
		}
		anlProcess.destroy();			
	}


	private void commitAendern(KontenAendernProcess aendernProcess){
		// Kontenliste neu beschaffen
	
		controller.refreshKontenListe();
		aendernProcess.destroy();			
	}
	
}
