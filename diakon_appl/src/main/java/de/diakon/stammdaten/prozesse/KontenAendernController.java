package de.diakon.stammdaten.prozesse;

import de.diakon.entities.Konto;
import de.diakon.stammdaten.gui.KontenPflegeDialog;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file KontenAendernController.java
 */
public class KontenAendernController extends AbstractController {
	private KontenAendernProcess process;
	private KontenPflegeDialog dialog;

	/**
	 * Constructor for KontenAendernController.
	 */
	public KontenAendernController(KontenAendernProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#start()
	 */
	public void start() {
		dialog = new KontenPflegeDialog(ApplicationContext.getMainApplicationWindow(), true, this, "Konto �ndern", process.getKonto(), false);
		dialog.setVisible(true);
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog=null;
		}
	}

	public AbstractProcess getProcess(){
		return process;
	}
	
	// die Handler Methoden
	public void handleOkEvent(){
		logger.debug("Action: Ok");
		Konto kto = (Konto) dialog.getModel();
		process.updateKonto(kto);
	}
	
	public void handleAbbrechenEvent(){
		logger.debug("Action: Abbrechen");
		process.rollback();
	}
}
