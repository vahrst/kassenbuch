package de.diakon.stammdaten.prozesse;

import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.*;
import de.vahrst.application.types.Fehlermeldung;

/**
 * 
 * @author Thomas
 * @version 
 * @file KontenAnlageProcess.java
 */
public class KontenAnlageProcess extends AbstractProcess {
	private KontenAnlageController controller;

	private Konto neuesKonto = null;

	/**
	 * Constructor for KontenAnlageProcess.
	 */
	public KontenAnlageProcess(AbstractProcess parent, EndTransactionHandler eth) {
		super(parent, eth);
		openController();
	}

	/**
	 * erzeugt das korrespondieren Controller-Objekt
	 */
	private void openController(){
		logger.debug("openController");
		
		controller = new KontenAnlageController(this);
		controller.start();
	}

	public AbstractController getController(){
		return controller;
	}

	/**
	 * liefert ein leer intialisiertes Konto
	 */
	public Konto getKonto(){
		return new Konto();
	}
	
	/**
	 * legt das neue Konto an.
	 */
	public void createKonto(Konto kto)throws PlausibilitaetsException, PersistenceException{
		logger.debug("Anlage Konto: " + kto);
		// hier noch Plausis pr�fen...
		KontoHome kontoHome = new KontoHome();
		if(kontoHome.getKontoByKontonummer(kto.getKontonummer()) != null){
			Fehlermeldung fm = new Fehlermeldung(0, "Ein Konto mit dieser Nummer existiert bereits", Konto.ASPEKT_KONTONUMMER);
			throw new PlausibilitaetsException(fm);
		}

		kontoHome.addKonto(kto);
		neuesKonto = kto;
		commit();
	}
	
	/**
	 * liefert das neu angelegte Konto.
	 */
	public Konto getAddedKonto(){
		return neuesKonto;
	}
}
