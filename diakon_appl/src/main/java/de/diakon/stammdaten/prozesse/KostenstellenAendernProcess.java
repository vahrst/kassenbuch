package de.diakon.stammdaten.prozesse;

import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.*;

/**
 * Prozess f�rs �ndern einer Kostenstelle
 * @author Thomas
 * @version 
 */
public class KostenstellenAendernProcess extends AbstractProcess {
	private KostenstellenAendernController controller;
	private Kostenstelle kostenstelle;
	/**
	 * Constructor for KontenAendernProcess.
	 * @param parent
	 * @param eth
	 */
	public KostenstellenAendernProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		Kostenstelle kostenstelle) {
		super(parent, eth);
		this.kostenstelle = kostenstelle;
		openController();
	}

	/**
	 * erzeugt das korrespondieren Controller-Objekt
	 */
	private void openController(){
		logger.debug("openController");
		
		controller = new KostenstellenAendernController(this);
		controller.start();
		
	}
	/**
	 * @see de.diakon.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}
	
	/**
	 * liefert die zu ver�ndernde Kostenstelle
	 */
	public Kostenstelle getKostenstelle(){
		return kostenstelle;
	}

	/**
	 * aktualisiert die vorgegebene Kostenstelle und
	 * beendet diesen Process
	 */
	public void updateKostenstelle(Kostenstelle kst){
		try{
			new KostenstelleHome().changeKostenstelle(kst);
			commit();
		}catch(PersistenceException e){
			String message = "Fehler beim Aktualisieren der Kostenstelle aufgetreten";
			logger.error(message, e);
			controller.showErrorMessage(message);
			rollback();
		}			
	}
}
