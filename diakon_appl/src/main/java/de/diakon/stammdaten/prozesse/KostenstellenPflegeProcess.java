package de.diakon.stammdaten.prozesse;

import java.io.IOException;

import javax.swing.ProgressMonitor;

import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.*;
import de.vahrst.application.xml.*;


/**
 * Process-Objekt f�r Pflege der Kostenstellen.
 * @author Thomas
 * @version 
 */
public class KostenstellenPflegeProcess extends AbstractProcess{

	private KostenstellenPflegeController controller = null;
	private KostenstellenListe kostenstellenListe = null;
	
	// EndTransactionHandler f�r Anlage
	class AnlageETH extends DefaultEndTransactionHandler{
		public void commit(AbstractProcess source){
			commitAnlage((KostenstellenAnlageProcess)source);
		}
	}
	
	class AendernETH extends DefaultEndTransactionHandler{
		public void commit(AbstractProcess source){
			commitAendern((KostenstellenAendernProcess)source);
		}
	}
	
	// Thread-Klasse f�r Import von Kostenstellen
	class ImportThread implements Runnable{
		private Element kst;
		private ProgressMonitor monitor;
		
		ImportThread(Element kstElement, ProgressMonitor pm){
			super();
			kst = kstElement;
			monitor = pm;
		} 
		
		public void run(){
			
			ElementEnumeration enum1 = kst.getElementsByTagName("Kostenstelle");
			int zaehler = 0;
			KostenstelleHome kstHome = new KostenstelleHome();
			while(enum1.hasMoreElements() && !monitor.isCanceled()){
				Element kstElement = enum1.nextElement();
				try{
					Kostenstelle kst = new Kostenstelle(kstElement);
					kstHome.addKostenstelle(kst);						

				}catch(Exception e){
					logger.error("", e);
				}
				
				zaehler++;
				
				monitor.setProgress(zaehler);
				monitor.setNote("Eingelesen: " + zaehler + " von " + monitor.getMaximum());
			}
			controller.refreshKostenstellenListe();
			monitor.close();
			logger.debug("Kostenstellen wurden importiert");			
		}
	}
	
	/**
	 * Constructor for KontenPflegeProcess.
	 */
	public KostenstellenPflegeProcess(AbstractProcess parent, EndTransactionHandler eth) {
		super(parent, eth);
		openController();
	}

	/**
	 * erzeugt das korrespondieren Controller-Objekt
	 */
	private void openController(){
		logger.debug("openController");
		
		controller = new KostenstellenPflegeController(this);
		controller.start();
	}

	public AbstractController getController(){
		return controller;
	}
	
	/**
	 * Liefert die Liste aller Kostenstellen, als ArrayList
	 */
	public KostenstellenListe getKostenstellen(boolean reload)throws PersistenceException{
		if(kostenstellenListe == null || reload){
			kostenstellenListe = new KostenstelleHome().getAllKostenstellen();
		}
		return kostenstellenListe;
	}
	
	/**
	 * l�scht die vorgegebene Kostenstelle
	 */
	public void deleteKostenstelle(Kostenstelle kst)throws PersistenceException{
		new KostenstelleHome().deleteKostenstelle(kst);	
		kostenstellenListe.remove(kst);
	}
	
	/** 
	 * Neuanlage einer Kostenstelle. Das �bernimmt ein weiterer
	 * Prozess
	 */
	public void startKostenstellenAnlageProcess(){
		new KostenstellenAnlageProcess(this, new AnlageETH());
	}

	/**
	 * �ndern eines bestehenden Kontos. Das �bernimmt ein weiterer
	 * Prozess
	 */
	public void startKostenstellenAendernProcess(Kostenstelle kst){
		new KostenstellenAendernProcess(this, new AendernETH(), (Kostenstelle) kst.clone());
	}


	/**
	 * l�dt das vorgegebene XML-File und liefert das 
	 * Root-Element f�r Kostenstellen zur�ck
	 */
	public Element loadImportFile(String filename)throws IOException{
		return ImportExportFileLoader.getRootElement(filename, "Kostenstellen");
		
	}
	
	/**
	 * liefert zum vorgegebenen Root-Element f�r Kostenstellen die
	 * Anzahl der enthaltenen Kostenstellen-Definitionen
	 */
	public int getKostenstellenAnzahl(Element kstElement){
		ElementEnumeration enum1 = kstElement.getElementsByTagName("Kostenstelle");
		
		// einmal vorab zaehlen, wieviele Konten wir haben:
		int zaehler = 0;
		while(enum1.hasMoreElements()){
			enum1.nextElement();
			zaehler++;
		}
		return zaehler;		
		
	}
	
	
	/**
	 * Importiert Daten aus dem vorgegebene XML-Element. Das
	 * ganze l�uft in einem eigenen Thread ab.
	 */
	public void startImport(Element kstElement, ProgressMonitor monitor) {
		log.info("Import Kostenstellen");
		
		ImportThread it = new ImportThread(kstElement, monitor);
		Thread t = new Thread(it);
		t.start();
	}

	



	private void commitAnlage(KostenstellenAnlageProcess anlProcess){
		Kostenstelle kst = anlProcess.getAddedKostenstelle();
		if(kst != null){
			kostenstellenListe.add(kst);
		}else{
			logger.warn("Kostenstelle Anlage Commit, aber keine neue Kostenstelle im Process gefunden");
		}
		anlProcess.destroy();			
	}


	private void commitAendern(KostenstellenAendernProcess aendernProcess){
		// Kontenliste neu beschaffen
	
		controller.refreshKostenstellenListe();
		aendernProcess.destroy();			
	}
	
}
