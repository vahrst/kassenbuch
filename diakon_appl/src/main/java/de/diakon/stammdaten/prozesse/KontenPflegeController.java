package de.diakon.stammdaten.prozesse;

import java.awt.event.ActionListener;
import java.io.*;

import javax.swing.*;

import de.diakon.entities.*;
import de.diakon.global.Konstanten;
import de.diakon.persistence.PersistenceException;
import de.diakon.stammdaten.gui.KontenUebersichtPanel;
import de.vahrst.application.gui.ClientAreaPanel;
import de.vahrst.application.prozesse.*;
import de.vahrst.application.xml.Element;
/**
 * 
 * @author Thomas
 * @version 
 * @file KontenPflegeController.java
 */
public class KontenPflegeController
	extends AbstractController
	implements ActionListener {

	private KontenPflegeProcess process = null;

	private KontenListe kontenListe = null;
	private KontenUebersichtPanel uebersichtPanel = null;
	private ClientAreaPanel clpanel = null;

	/**
	 * Constructor for KontenPflegeController.
	 */
	public KontenPflegeController(KontenPflegeProcess p) {
		super();
		this.process = p;
	}

	public void reactivate() {
		logger.debug("reactivate");

		clpanel.activate(uebersichtPanel);

	}

	/**
	 * L�scht diesen Controller. Schlie�t das 
	 * Konten�bersichtPanel.
	 */
	public void destroy() {
		clpanel.remove(uebersichtPanel);
		uebersichtPanel.setVisible(false);
	}

	/**
	 * allgemeine Start-Methode dieses Controllers
	 */
	public void start() {
		logger.debug("start Controller");

		try{
			kontenListe = process.getKonten(true);

			uebersichtPanel = new KontenUebersichtPanel(kontenListe, this);
			uebersichtPanel.setVisible(true);

			clpanel = ClientAreaProvider.getClientArea(Konstanten.CLIENT_AREA_FORM);
			clpanel.add(uebersichtPanel, "KontenUebersicht");
		}catch(PersistenceException e){
			logger.error("Fehler beim Lesen der Konten", e);
		}
	}

	public void refreshKontenListe(){
		logger.debug("refreshKontenListe");
		try{
			kontenListe = process.getKonten(true);
			uebersichtPanel.setEntityListe(kontenListe);
		}catch(PersistenceException e){
			logger.error("Fehler beim Aktualisiern der Kontenliste", e);
		}
	
	}

	public AbstractProcess getProcess(){
		return process;
	}


	// die Handler-Methoden
	public void handleNeuEvent() {
		logger.debug("Action: Neu");
		process.startKontoAnlageProcess();
	}

	public void handleDeleteEvent() {
		logger.debug("Action: Delete");
		int selectedRow = uebersichtPanel.getSelectedRow();
		if (selectedRow != -1) {

			int result =
				JOptionPane.showConfirmDialog(
					uebersichtPanel,
					"Soll das ausgew�hlte Konto wirklich gel�scht werden?",
					"Konto l�schen",
					JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE);
			if (result == JOptionPane.YES_OPTION) {

				Konto kto = kontenListe.getKontoAt(selectedRow);
				try{
					process.deleteKonto(kto);
				}catch(PersistenceException e){
					String message = "Fehler beim L�schen des Kontos aufgetreten";
					logger.error(message, e);
					showErrorMessage(message);
					
				}

			}
		}
	}

	public void handleGeheZuEvent() {
		logger.debug("Action: Gehezu");
		int selectedRow = uebersichtPanel.getSelectedRow();
		if(selectedRow != -1){
			Konto kto = kontenListe.getKontoAt(selectedRow);
			process.startKontoAendernProcess(kto);
		}
	}

	public void handleImportEvent() {
		logger.debug("Action: Import");
		JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
		fc.setDialogTitle("XML-Datei ausw�hlen");
		int result = fc.showOpenDialog(uebersichtPanel);
		if(result == JFileChooser.APPROVE_OPTION){
			File file = fc.getSelectedFile();
			
			try{
				Element konten = process.loadImportFile(file.getAbsolutePath());
				int count = process.getKontoAnzahl(konten);
				if(count == 0){
					JOptionPane.showMessageDialog(uebersichtPanel, "Import-Datei enth�lt keine Konto-Daten");
				}else{
					// jetzt beginnt der Import. Das ganze in einem eigenen
					// Thread. Dazu erzeugen wir uns einen kleinen Fortschrittsdialog
					// ...
					ProgressMonitor pm = new ProgressMonitor(uebersichtPanel, "Konten importieren","",0,count);
					pm.setProgress(0);
					
					process.startImport(konten, pm);
					
					refreshKontenListe();
				}
				
			}catch(IOException e){
				logger.error("Fehler!", e);
				showErrorMessage("Fehler beim Einlesen der Kontodaten");
			}
		}
		
		
	}

	// private Service-Methoden

}
