package de.diakon.stammdaten.prozesse;

import de.diakon.entities.Kostenstelle;
import de.diakon.stammdaten.gui.KostenstellenPflegeDialog;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file KontenAnlageController.java
 */
public class KostenstellenAnlageController extends AbstractController {
	private KostenstellenAnlageProcess process;
	private KostenstellenPflegeDialog dialog;
	
	/**
	 * Constructor for KontenAnlageController.
	 */
	public KostenstellenAnlageController(KostenstellenAnlageProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#start()
	 */
	public void start() {
		logger.debug("start Controller");
		dialog = new KostenstellenPflegeDialog(
			ApplicationContext.getMainApplicationWindow(),
			true,
			this,
			"Kostenstelle anlegen",
			process.getKostenstelle(),
			true);
			
		dialog.setVisible(true);
			
			
	}

	/**
	 * @see de.diakon.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * L�scht diesen Controller.
	 */
	public void destroy(){
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog=null;
		}
	}
			
	public AbstractProcess getProcess(){
		return process;
	}
			

	// die Handler Methoden
	public void handleOkEvent(){
		logger.debug("Action: Ok");
		Kostenstelle kst = (Kostenstelle) dialog.getModel();
		process.createKostenstelle(kst);
	}
	
	public void handleAbbrechenEvent(){
		logger.debug("Action: Abbrechen");
		process.rollback();
	}
}
