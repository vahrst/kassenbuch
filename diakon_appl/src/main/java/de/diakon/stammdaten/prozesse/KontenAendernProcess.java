package de.diakon.stammdaten.prozesse;

import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.*;

/**
 * Prozess f�rs �ndern eines vorgegebenen Kontos.
 * @author Thomas
 * @version 
 * @file KontenAendernProcess.java
 */
public class KontenAendernProcess extends AbstractProcess {
	private KontenAendernController controller;
	private Konto konto;
	/**
	 * Constructor for KontenAendernProcess.
	 * @param parent
	 * @param eth
	 */
	public KontenAendernProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		Konto kto) {
		super(parent, eth);
		this.konto = kto;
		openController();
	}

	/**
	 * erzeugt das korrespondieren Controller-Objekt
	 */
	private void openController(){
		logger.debug("openController");
		
		controller = new KontenAendernController(this);
		controller.start();
		
	}
	/**
	 * @see de.diakon.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}
	
	/**
	 * liefert das zu ver�ndernde Konto
	 */
	public Konto getKonto(){
		return konto;
	}

	/**
	 * aktualisiert das vorgegebene Konto und
	 * beendet diesen Process
	 */
	public void updateKonto(Konto kto){
		try{
			new KontoHome().changeKonto(konto);
			commit();
		}catch(PersistenceException e){
			String message = "Fehler beim Aktualisieren des Kontos aufgetreten";
			logger.error(message, e);
			controller.showErrorMessage(message);
			rollback();
		}			
	}
}
