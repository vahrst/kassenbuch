package de.diakon.stammdaten.gui;

import java.awt.*;

import javax.swing.*;

import de.diakon.entities.Kostenstelle;
import de.diakon.gui.*;
import de.vahrst.application.gui.*;

/**
 * 
 * @author Thomas
 * @version 
 */
public class KostenstellenPflegeMainPanel extends StammdatenPflegeMainPanel {

	private JTextField tfNummer;
	private BaseTextField tfBezeichnung;

	private Kostenstelle model;
	/**
	 * Constructor for BuchungspanelMain.
	 */
	public KostenstellenPflegeMainPanel(Kostenstelle model, boolean neuanlage) {
		super(neuanlage);
		this.model = model;
		initialize();
	}
	

	/** 
	 * liefert das DAten-Modell (vom Typ Kostensetlle) mit
	 * den aktuellen Werten der Eingabemaske zur�ck.
	 */
	public Object getModel(){
		model.setNummer(Integer.parseInt(tfNummer.getText()));
		model.setBezeichnung(tfBezeichnung.getText());
		
		return model;
	}
	
	private void initialize(){
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		
		JLabel lb1 = new JLabel("Kostenstellen-Nummer");
		gbc.gridx = 0;
		gbc.gridy = 0;			
		this.add(lb1,gbc);
		
		JLabel lb2 = new JLabel("Bezeichnung");
		gbc.gridy = 1;
		this.add(lb2,gbc);
		


		tfNummer = new JTextField(6);
		tfNummer.setText(Integer.toString(model.getNummer()));
		tfNummer.setHorizontalAlignment(JTextField.RIGHT);
		if(!neuAnlageModus){
			tfNummer.setEditable(false);
			tfNummer.setFocusable(false);
		}
		
		gbc.gridy = 0;
		gbc.gridx = 1;
		this.add(tfNummer,gbc);
		
		tfBezeichnung = new BaseTextField(30);
		tfBezeichnung.setMaxLength(30);
		tfBezeichnung.setText(model.getBezeichnung());
		
		gbc.gridy = 1;
		this.add(tfBezeichnung,gbc);
		

		Strut st1 = new Strut();
	
		gbc.gridy = 8;
		gbc.gridx = 8;
		gbc.weightx = 1;
		gbc.weighty = 1;
		this.add(st1, gbc);
		gbc.weighty = 0;
		gbc.weightx = 0;

	}
}
