package de.diakon.stammdaten.gui;

import java.awt.event.ActionListener;

import javax.swing.table.*;

import de.diakon.gui.*;

/**
 * Dieses Panel enthält die reine Kontenübersicht-Tabelle.
 * @author Thomas
 * @version 
  */
public class KostenstellenUebersichtTablePanel extends StammdatenUebersichtTablePanel {


	/**
	 * Constructor for TestTablePanel.
	 */
	public KostenstellenUebersichtTablePanel(TableModel tm, ActionListener controller) {
		super(tm, controller);
	}




	protected TableColumnModel getTableColumnModel() {

		TableColumnModel tcm = new DefaultTableColumnModel();

		TableColumn kstColumn = new TableColumn(0, 100);
		
		kstColumn.setHeaderValue("Kostenstelle");
		kstColumn.setMaxWidth(100);
		tcm.addColumn(kstColumn);

		TableColumn bezColumn = new TableColumn(1);
		bezColumn.setHeaderValue("Bezeichnung");
		tcm.addColumn(bezColumn);
		
		return tcm;
	}

}
