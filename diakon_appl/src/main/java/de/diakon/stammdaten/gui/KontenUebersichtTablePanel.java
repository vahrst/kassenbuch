package de.diakon.stammdaten.gui;

import java.awt.event.ActionListener;

import javax.swing.table.*;

import de.diakon.gui.*;

/**
 * Dieses Panel enthält die reine Kontenübersicht-Tabelle.
 * @author Thomas
 * @version 
  */
public class KontenUebersichtTablePanel extends StammdatenUebersichtTablePanel {


	/**
	 * Constructor for TestTablePanel.
	 */
	public KontenUebersichtTablePanel(TableModel tm, ActionListener controller) {
		super(tm, controller);
	}




	protected TableColumnModel getTableColumnModel() {

		TableColumnModel tcm = new DefaultTableColumnModel();

		TableColumn ktoColumn = new TableColumn(0, 60);
		ktoColumn.setHeaderValue("Konto-Nr.");
		tcm.addColumn(ktoColumn);

		TableColumn bezColumn = new TableColumn(1, 200);
		bezColumn.setHeaderValue("Bezeichnung");
		tcm.addColumn(bezColumn);
		
		TableColumn shColumn = new TableColumn(4, 60);
		shColumn.setHeaderValue("S/H Vorgabe");
		tcm.addColumn(shColumn);
		
		TableColumn kstColumn = new TableColumn(2);
		kstColumn.setHeaderValue("Kostenstelle");
		tcm.addColumn(kstColumn);
		
		TableColumn debColumn = new TableColumn(3);
		debColumn.setHeaderValue("Debitor");
		tcm.addColumn(debColumn);

		return tcm;
	}

}
