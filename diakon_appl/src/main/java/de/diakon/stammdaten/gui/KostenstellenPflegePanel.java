package de.diakon.stammdaten.gui;

import de.diakon.entities.Kostenstelle;
import de.diakon.gui.*;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 */
public class KostenstellenPflegePanel extends StammdatenPflegePanel {

	/**
	 * Constructor for KontenAnlagePanel.
	 */
	public KostenstellenPflegePanel(AbstractController controller, String title, Kostenstelle model, boolean neuanlage) {
		super(controller, title, model, neuanlage);
	}

	protected StammdatenPflegeMainPanel createPflegeMainPanel(Object model, boolean neuanlage){
		return new KostenstellenPflegeMainPanel((Kostenstelle)model, neuanlage);
	}
}
