package de.diakon.stammdaten.gui;

import de.diakon.entities.Konto;
import de.diakon.gui.*;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file KontenAnlagePanel.java
 */
public class KontenPflegePanel extends StammdatenPflegePanel {
	

	/**
	 * Constructor for KontenAnlagePanel.
	 */
	public KontenPflegePanel(AbstractController controller, String title, Konto model, boolean neuanlage) {
		super(controller, title, model, neuanlage);
	}

	protected StammdatenPflegeMainPanel createPflegeMainPanel(Object model, boolean neuanlage){
		return new KontenPflegeMainPanel((Konto)model, neuanlage);
	}
	
}
