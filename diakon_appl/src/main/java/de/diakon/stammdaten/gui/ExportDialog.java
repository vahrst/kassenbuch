package de.diakon.stammdaten.gui;

import java.awt.*;

import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file ExportDialog.java
 */
public class ExportDialog extends AbstractDialog {
	private ExportPanel panel;

	/**
	 * Constructor for ExportDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public ExportDialog(
		Frame owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for ExportDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public ExportDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, title, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for ExportDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public ExportDialog(
		Dialog owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}


	private void init(AbstractController controller){
		this.setTitle("Export Stammdaten");
		this.setSize(550,200);
		
		panel = new ExportPanel(controller);
		getContentPane().add(panel, BorderLayout.CENTER);

		
		this.center();	
	}


	public ExportPanel getMainPanel(){
		return panel;	
	}

}
