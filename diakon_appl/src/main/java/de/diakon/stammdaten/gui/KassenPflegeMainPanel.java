package de.diakon.stammdaten.gui;

import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import de.diakon.gui.StammdatenPflegeMainPanel;
import de.diakon.stammdaten.prozesse.Kassendaten;
import de.vahrst.application.gui.BaseTextField;
import de.vahrst.application.gui.Strut;
import de.vahrst.application.gui.TextFieldNumeric;

/**
 * 
 * @author Thomas
 * @version 
 */
public class KassenPflegeMainPanel extends StammdatenPflegeMainPanel {

	private BaseTextField tfBezeichnung;
	private TextFieldNumeric tfKassenkonto;
	private BaseTextField tfMandant;
	private TextFieldNumeric tfBuchungskreis;

	/**
	 * Constructor for KassenNeuanlageMainPanel.
	 */
	public KassenPflegeMainPanel(Kassendaten vorgabe) {
		super(true);  // nur Neuanlage
		initialize(vorgabe);
	}
	

	/** 
	 * liefert das DAten-Modell (vom Typ Konto) mit
	 * den aktuellen Werten der Eingabemaske zur�ck.
	 */
	public Object getModel(){
		Kassendaten daten = new Kassendaten();
		daten.setBezeichnung(tfBezeichnung.getText());
		daten.setBuchungskreis(Integer.parseInt(tfBuchungskreis.getText()));
		daten.setKassenkonto(Integer.parseInt(tfKassenkonto.getText()));
		daten.setMandant(tfMandant.getText());
		
		return daten;
	}
	
	private void initialize(Kassendaten vorgabe){
		
		tfBezeichnung = new BaseTextField(30);
		tfBezeichnung.setMaxLength(30);

		Strut st1 = new Strut();
		
		JLabel lblBuchungskreis = new JLabel("Buchungskreis");
		JLabel lblMandant = new JLabel("Mandant");
		JLabel lblKassenkonto = new JLabel("Kassenkonto");
		JLabel lb1 = new JLabel("Kassen-Bezeichnung");
		
		tfMandant = new BaseTextField();
		tfMandant.setMaxLength(10);
		
		tfBuchungskreis = new TextFieldNumeric();
		tfBuchungskreis.setCompleteWithZeroes(true);
		tfBuchungskreis.setIntegerPart(3);
		tfBuchungskreis.setIntegerGrouping(false);
		tfBuchungskreis.setFractionPart(0);
		tfBuchungskreis.setText("000");
		
		tfKassenkonto = new TextFieldNumeric();
		tfKassenkonto.setIntegerGrouping(false);
		tfKassenkonto.setFractionPart(0);
		tfKassenkonto.setText("0");
		tfKassenkonto.setIntegerPart(8);
		
		JLabel lblEinstellungenFrMonatsabschluss = new JLabel("Einstellungen f\u00FCr Monatsabschluss");
		lblEinstellungenFrMonatsabschluss.setFont(new Font("Dialog", Font.BOLD, 14));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(st1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(4)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addComponent(lblEinstellungenFrMonatsabschluss)
							.addGroup(groupLayout.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(lb1)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(tfBezeichnung, GroupLayout.PREFERRED_SIZE, 348, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblBuchungskreis)
								.addComponent(lblMandant)
								.addComponent(lblKassenkonto))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(tfKassenkonto, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
									.addGap(358))
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
									.addGroup(groupLayout.createSequentialGroup()
										.addComponent(tfMandant, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGap(358))
									.addGroup(groupLayout.createSequentialGroup()
										.addComponent(tfBuchungskreis, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
										.addGap(393)))))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(st1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(tfBezeichnung, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lb1))))
					.addGap(24)
					.addComponent(lblEinstellungenFrMonatsabschluss)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblBuchungskreis)
						.addComponent(tfBuchungskreis, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblMandant)
						.addComponent(tfMandant, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblKassenkonto)
						.addComponent(tfKassenkonto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(73))
		);
		setLayout(groupLayout);

		updateView(vorgabe);
		
	}

	private void updateView(Kassendaten vorgabe){
		tfBezeichnung.setText(vorgabe.getBezeichnung());
		tfBuchungskreis.setText(Integer.toString(vorgabe.getBuchungskreis()));
		tfKassenkonto.setText(Integer.toString(vorgabe.getKassenkonto()));
		tfMandant.setText(vorgabe.getMandant());
	}
	
	public TextFieldNumeric getTfKassenkonto() {
		return tfKassenkonto;
	}
	public BaseTextField getTfMandant() {
		return tfMandant;
	}
	public TextFieldNumeric getTfBuchungskreis() {
		return tfBuchungskreis;
	}
}
