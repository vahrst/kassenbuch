package de.diakon.stammdaten.gui;

import java.awt.*;

import javax.swing.JLabel;

import de.diakon.gui.*;
import de.diakon.stammdaten.prozesse.Kassendaten;
import de.vahrst.application.gui.*;
import de.vahrst.application.types.Waehrungsbetrag;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JPanel;

/**
 * 
 * @author Thomas
 * @version 
 */
public class KassenNeuanlageMainPanel extends StammdatenPflegeMainPanel {

	private BaseTextField tfBezeichnung;
	private TextFieldNumeric tfMonat;
	private TextFieldNumeric tfJahr;
	private TextFieldWaehrung tfAnfangssaldo;
	private TextFieldNumeric tfKassenkonto;
	private BaseTextField tfMandant;
	private TextFieldNumeric tfBuchungskreis;

	/**
	 * Constructor for KassenNeuanlageMainPanel.
	 */
	public KassenNeuanlageMainPanel(Kassendaten vorgabe) {
		super(true);  // nur Neuanlage
		initialize(vorgabe);
	}
	

	/** 
	 * liefert das DAten-Modell (vom Typ Konto) mit
	 * den aktuellen Werten der Eingabemaske zur�ck.
	 */
	public Object getModel(){
		Kassendaten daten = new Kassendaten();
		daten.setBezeichnung(tfBezeichnung.getText());
		daten.setMonat(Integer.parseInt(tfMonat.getText()));
		daten.setJahr(Integer.parseInt(tfJahr.getText()));
		daten.setAnfangssaldo(tfAnfangssaldo.getBetrag());
		daten.setBuchungskreis(Integer.parseInt(tfBuchungskreis.getText()));
		daten.setKassenkonto(Integer.parseInt(tfKassenkonto.getText()));
		daten.setMandant(tfMandant.getText());
		
		return daten;
	}
	
	private void initialize(Kassendaten vorgabe){
		
		JLabel lb1 = new JLabel("Kassen-Bezeichnung");
		JLabel lb2 = new JLabel("aktueller Monat");
		JLabel lb3 = new JLabel("aktuelles Jahr");
		JLabel lb4 = new JLabel("Anfangssaldo");
		tfBezeichnung = new BaseTextField(30);
		tfBezeichnung.setMaxLength(30);
		tfMonat = new TextFieldNumeric(2);
		tfMonat.setIntegerPart(2);
		tfMonat.setFractionPart(0);
		tfMonat.setCompleteWithZeroes(true);
		tfMonat.setText(Integer.toString(vorgabe.getMonat()));
		tfJahr = new TextFieldNumeric(4);
		tfJahr.setIntegerPart(4);
		tfJahr.setFractionPart(0);
		tfJahr.setCompleteWithZeroes(true);
		tfJahr.setIntegerGrouping(false);
		tfJahr.setText(Integer.toString(vorgabe.getJahr()));
		tfAnfangssaldo = new TextFieldWaehrung();
		tfAnfangssaldo.setBetrag(new Waehrungsbetrag());
		tfAnfangssaldo.setSigned(true);
		

		Strut st1 = new Strut();
		
		JLabel lblBuchungskreis = new JLabel("Buchungskreis");
		
		JLabel lblMandant = new JLabel("Mandant");
		
		JLabel lblKassenkonto = new JLabel("Kassenkonto");
		
		tfMandant = new BaseTextField();
		tfMandant.setMaxLength(10);
		
		tfBuchungskreis = new TextFieldNumeric();
		tfBuchungskreis.setCompleteWithZeroes(true);
		tfBuchungskreis.setIntegerPart(3);
		tfBuchungskreis.setIntegerGrouping(false);
		tfBuchungskreis.setFractionPart(0);
		tfBuchungskreis.setText("000");
		
		tfKassenkonto = new TextFieldNumeric();
		tfKassenkonto.setIntegerGrouping(false);
		tfKassenkonto.setFractionPart(0);
		tfKassenkonto.setText("0");
		tfKassenkonto.setIntegerPart(8);
		
		JLabel lblEinstellungenFrMonatsabschluss = new JLabel("Einstellungen f\u00FCr Monatsabschluss");
		lblEinstellungenFrMonatsabschluss.setFont(new Font("Dialog", Font.BOLD, 14));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(st1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(4)
					.addComponent(lb1)
					.addGap(8)
					.addComponent(tfBezeichnung, GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE)
					.addGap(12))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(4)
					.addComponent(lb2)
					.addGap(43)
					.addComponent(tfMonat, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(4)
					.addComponent(lb3)
					.addGap(58)
					.addComponent(tfJahr, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(5)
					.addComponent(lb4)
					.addGap(58)
					.addComponent(tfAnfangssaldo, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(4)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblEinstellungenFrMonatsabschluss)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblBuchungskreis)
							.addGap(52)
							.addComponent(tfBuchungskreis, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)))
					.addGap(288))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(4)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblMandant)
							.addGap(93)
							.addComponent(tfMandant, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addComponent(lblKassenkonto)
							.addGap(63)
							.addComponent(tfKassenkonto, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(st1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(1)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(lb1))
						.addComponent(tfBezeichnung, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(lb2))
						.addComponent(tfMonat, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(4)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(lb3))
						.addComponent(tfJahr, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(lb4))
						.addComponent(tfAnfangssaldo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(31)
					.addComponent(lblEinstellungenFrMonatsabschluss)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(lblBuchungskreis))
						.addComponent(tfBuchungskreis, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(6)
							.addComponent(lblMandant))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(4)
							.addComponent(tfMandant, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(lblKassenkonto))
						.addComponent(tfKassenkonto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		setLayout(groupLayout);

	}
	public TextFieldNumeric getTfKassenkonto() {
		return tfKassenkonto;
	}
	public BaseTextField getTfMandant() {
		return tfMandant;
	}
	public TextFieldNumeric getTfBuchungskreis() {
		return tfBuchungskreis;
	}
}
