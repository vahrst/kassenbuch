package de.diakon.stammdaten.gui;

import java.awt.*;

import javax.swing.JLabel;

import de.diakon.entities.Konto;
import de.diakon.gui.StammdatenPflegeMainPanel;
import de.vahrst.application.gui.*;
import de.vahrst.application.types.*;

/**
 * 
 * @author Thomas
 * @version 
 */
public class KontenPflegeMainPanel extends StammdatenPflegeMainPanel {

	public TextFieldNumericInteger tfKonto;
	private BaseTextField tfBezeichnung;
	private VaCheckBox cbKostenstelle;
	private VaCheckBox cbDebitor;
	private VaComboBox cbSollHaben;

	private Konto model;
	/**
	 * Constructor for BuchungspanelMain.
	 */
	public KontenPflegeMainPanel(Konto model, boolean neuanlage) {
		super(neuanlage);
		this.model = model;
		initialize();
	}
	

	/** 
	 * liefert das Daten-Modell (vom Typ Konto) mit
	 * den aktuellen Werten der Eingabemaske zur�ck.
	 */
	public Object getModel(){
		model.setKontonummer(tfKonto.getIntegerValue());
		model.setBezeichnung(tfBezeichnung.getText());
		model.setKostenstelleErforderlich(cbKostenstelle.isSelected());
		model.setDebitor(cbDebitor.isSelected());
		model.setSollHabenVorgabe((SollHaben)cbSollHaben.getSelectedItem());		
		
		return model;
	}
	
	private void initialize(){
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		
		JLabel lb1 = new JLabel("Konto-Nummer");
		gbc.gridx = 0;
		gbc.gridy = 0;			
		this.add(lb1,gbc);
		
		JLabel lb2 = new JLabel("Bezeichnung");
		gbc.gridy = 1;
		this.add(lb2,gbc);
		
		JLabel lb3 = new JLabel("Kostenst. erforderlich");
		gbc.gridy = 2;
		this.add(lb3, gbc);
		
		JLabel lb4 = new JLabel("Debitor");
		gbc.gridy = 3;
		this.add(lb4, gbc);
		
		JLabel lb5 = new JLabel("S/H Vorgabe");
		gbc.gridy = 4;
		this.add(lb5, gbc);
		



		tfKonto = new TextFieldNumericInteger(6);
		tfKonto.setIntegerValue(model.getKontonummer());
		tfKonto.setHorizontalAlignment(TextFieldNumericInteger.RIGHT);
		if(!neuAnlageModus){
			tfKonto.setEditable(false);
			tfKonto.setFocusable(false);
		}
		
		gbc.gridy = 0;
		gbc.gridx = 1;
		this.add(tfKonto,gbc);
		
		tfBezeichnung = new BaseTextField(30);
		tfBezeichnung.setMaxLength(30);
		tfBezeichnung.setText(model.getBezeichnung());
		
		gbc.gridy = 1;
		this.add(tfBezeichnung,gbc);
		
		cbKostenstelle = new VaCheckBox("", model.isKostenstelleErforderlich());
		gbc.gridy = 2;
		this.add(cbKostenstelle,gbc);
		
		cbDebitor = new VaCheckBox("", model.isDebitor());
		gbc.gridy = 3;
		this.add(cbDebitor, gbc);

		
		cbSollHaben = new VaComboBox();
		cbSollHaben.addItem(SollHabenHome.SOLL);
		cbSollHaben.addItem(SollHabenHome.HABEN);
		cbSollHaben.setSelectedItem(model.getSollHabenVorgabe());
		gbc.gridy = 4;
//		gbc.fill = gbc.HORIZONTAL;
		this.add(cbSollHaben, gbc);

		Strut st1 = new Strut();
	
		gbc.gridy = 8;
		gbc.gridx = 8;
		gbc.weightx = 1;
		gbc.weighty = 1;
		this.add(st1, gbc);
		gbc.weighty = 0;
		gbc.weightx = 0;

	}
}
