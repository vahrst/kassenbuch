package de.diakon.stammdaten.gui;

import java.awt.event.ActionListener;

import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import de.diakon.gui.*;
import de.vahrst.application.prozesse.AbstractController;

/**
 * �bersicht �ber alle Konten.
 * @author Thomas
 * @version 
 */
public class KostenstellenUebersichtPanel extends StammdatenUebersichtPanel implements ListSelectionListener{



	/**
	 * Constructor for UebersichtPanel.
	 */
	public KostenstellenUebersichtPanel(TableModel tm, AbstractController controller) {
		super(tm, controller);
	}


	protected String getHeaderTitle(){
		return "Kostenstellen �bersicht";
	}
	
	protected StammdatenUebersichtTablePanel getTablePanel(TableModel tm, ActionListener controller){
		return new KostenstellenUebersichtTablePanel(tm, controller);
	}
	
}
