package de.diakon.stammdaten.gui;

import java.awt.event.ActionListener;

import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import de.diakon.gui.*;
import de.vahrst.application.prozesse.AbstractController;

/**
 * �bersicht �ber alle Konten.
 * @author Thomas
 * @version 
 */
public class KontenUebersichtPanel extends StammdatenUebersichtPanel implements ListSelectionListener{


	/**
	 * Constructor for UebersichtPanel.
	 */
	public KontenUebersichtPanel(TableModel tm, AbstractController controller) {
		super(tm, controller);
	}


	protected String getHeaderTitle(){
		return "Konten �bersicht";
	}
	
	protected StammdatenUebersichtTablePanel getTablePanel(TableModel tm, ActionListener controller){
		return new KontenUebersichtTablePanel(tm, controller);
	}
	
}
