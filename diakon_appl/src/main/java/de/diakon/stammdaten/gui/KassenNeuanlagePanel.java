package de.diakon.stammdaten.gui;

import de.diakon.gui.*;
import de.diakon.stammdaten.prozesse.Kassendaten;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file KassenNeuanlagePanel.java
 */
public class KassenNeuanlagePanel extends StammdatenPflegePanel {

	/**
	 * Constructor for KassenNeuanlagePanel.
	 * @param controller
	 * @param title
	 * @param model
	 * @param neuanlage
	 */
	public KassenNeuanlagePanel(
		AbstractController controller,
		String title,
		Object model) {
		super(controller, title, model, true);
	}

	/**
	 * @see de.diakon.stammdaten.gui.StammdatenPflegePanel#getPflegeMainPanel(Entity, boolean)
	 */
	protected StammdatenPflegeMainPanel createPflegeMainPanel(
		Object model,
		boolean neuanlage) {
		return new KassenNeuanlageMainPanel((Kassendaten) model);
	}

}
