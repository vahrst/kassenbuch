package de.diakon.stammdaten.gui;

import java.awt.*;

import de.diakon.entities.Kostenstelle;
import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 */
public class KostenstellenPflegeDialog extends AbstractDialog {
	private KostenstellenPflegePanel pflegePanel = null;

	/**
	 * Constructor for KontenAnlageDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public KostenstellenPflegeDialog(
		Frame owner,
		boolean modal,
		AbstractController controller,
		String title,
		Kostenstelle model,
		boolean neuanlage)
		throws HeadlessException {
		super(owner, modal, controller);
		
		this.setTitle(title);
		this.initialize(controller, title, model, neuanlage);
	}


	private void initialize(AbstractController controller, String title, Kostenstelle model, boolean neuanlage){
		this.setSize(650,200);
		this.center();
		pflegePanel = new KostenstellenPflegePanel(controller, title, model, neuanlage);
		this.getContentPane().add(pflegePanel, BorderLayout.CENTER);
		
	}
	
	public Object getModel(){
		return pflegePanel.getModel();
	}
}
