package de.diakon.stammdaten.gui;

import java.awt.*;

import javax.swing.*;

import de.vahrst.application.gui.*;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file DruckDebitorenPanel.java
 */
public class ExportPanel extends FormPanel {
	private VaButton btStarten;
	private VaButton btAbbrechen;

	public  BaseTextField tfExportfile;
	public  VaCheckBox cbExportKonten;
	public  VaCheckBox cbExportKostenstellen;

	/**
	 * Constructor for DruckDebitorenPanel.
	 */
	public ExportPanel(AbstractController controller) {
		super();
		this.setPaintHeaderArea(false);
		
		initialize(controller);
	}

	/**
	 * initialisiert dieses Panel
	 */
	private void initialize(AbstractController controller){
		this.setLayout(new BorderLayout());
		
		this.add(createDatenPanel(), BorderLayout.CENTER);
		this.add(createButtonPanel(controller), BorderLayout.SOUTH);	
		
	}

	/**
	 * erzeugt das Eingabepanel
	 */
	private JPanel createDatenPanel(){
		JPanel datenPanel = new JPanel();
		datenPanel.setOpaque(false);
		
		datenPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);
		
		JLabel lb1 = new JLabel("Export Konten ");
		gbc.gridx = 0;
		gbc.gridy = 0;			
		datenPanel.add(lb1,gbc);
		
		JLabel lb2 = new JLabel("Export Kostenstellen ");
		gbc.gridy = 1;
		datenPanel.add(lb2,gbc);
		
		JLabel lb3 = new JLabel("Dateiname ");
		gbc.gridy = 2;
		datenPanel.add(lb3,gbc);
		
	
		cbExportKonten = new VaCheckBox();
		cbExportKonten.setSelected(true);
		gbc.gridy = 0;
		gbc.gridx = 1;
		datenPanel.add(cbExportKonten,gbc);

		cbExportKostenstellen = new VaCheckBox();
		cbExportKostenstellen.setSelected(true);
		gbc.gridy = 1;
		gbc.gridx = 1;
		datenPanel.add(cbExportKostenstellen,gbc);

		tfExportfile = new BaseTextField();
		String filename = System.getProperty("user.dir")+
			System.getProperty("file.separator") + "export.xml";
		tfExportfile.setText(filename);

		gbc.gridy = 2;
		gbc.gridx = 1;
		gbc.weightx = 1.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		datenPanel.add(tfExportfile, gbc);
		

		Strut st1 = new Strut();
	
		gbc.gridy = 8;
		gbc.gridx = 8;
		gbc.weightx = 0;
		gbc.weighty = 1;
		datenPanel.add(st1, gbc);
		gbc.weighty = 0;
		gbc.weightx = 0;

		return datenPanel;

	}



	/**
	 * Erzeugt das ButtonPanel
	 */
	protected JPanel createButtonPanel(AbstractController controller){
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		buttonPanel.setMinimumSize(new Dimension(10, 24));
		buttonPanel.setPreferredSize(new Dimension(10, 24));
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		btStarten = new VaButton("Start");
		btStarten.setActionCommand("Start");
		btStarten.addActionListener(controller);
		buttonPanel.add(btStarten);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btAbbrechen = new VaButton("Abbrechen");
		btAbbrechen.setActionCommand("Abbrechen");
		btAbbrechen.addActionListener(controller);
		buttonPanel.add(btAbbrechen);


		return buttonPanel;		
	}
}
