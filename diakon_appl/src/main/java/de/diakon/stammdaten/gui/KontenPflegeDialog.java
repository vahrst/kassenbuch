package de.diakon.stammdaten.gui;

import java.awt.*;

import de.diakon.entities.Konto;
import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file KontenAnlageDialog.java
 */
public class KontenPflegeDialog extends AbstractDialog {
	private KontenPflegePanel pflegePanel = null;

	/**
	 * Constructor for KontenAnlageDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public KontenPflegeDialog(
		Frame owner,
		boolean modal,
		AbstractController controller,
		String title,
		Konto model,
		boolean neuanlage)
		throws HeadlessException {
		super(owner, modal, controller);
		
		this.setTitle(title);
		this.initialize(controller, title, model, neuanlage);
	}


	private void initialize(AbstractController controller, String title, Konto model, boolean neuanlage){
		this.setSize(650,300);
		this.center();
		pflegePanel = new KontenPflegePanel(controller, title, model, neuanlage);
		this.getContentPane().add(pflegePanel, BorderLayout.CENTER);
		
	}
	
	public Object getModel(){
		return pflegePanel.getModel();
	}
	
	public KontenPflegeMainPanel getMainPanel(){
		return (KontenPflegeMainPanel) pflegePanel.getPflegeMainPanel();
	}
}
