package de.diakon.stammdaten.gui;

import java.awt.*;

import de.diakon.stammdaten.prozesse.Kassendaten;
import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 */
public class KassenPflegeDialog extends AbstractDialog {
	private KassenPflegePanel pflegePanel = null;

	/**
	 * Constructor for KassenNeuanlageDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public KassenPflegeDialog(
		Frame owner,
		boolean modal,
		AbstractController controller,
		String title,
		Kassendaten vorgabe)
		throws HeadlessException {
		super(owner, modal, controller);
		
		this.setTitle(title);
		this.initialize(controller, title, vorgabe);
	}


	private void initialize(AbstractController controller, String title, Kassendaten vorgabe){
		this.setSize(650,300);
		this.center();
		pflegePanel = new KassenPflegePanel(controller, title, vorgabe);
		this.getContentPane().add(pflegePanel, BorderLayout.CENTER);
		
	}
	
	public Object getModel(){
		return pflegePanel.getModel();
	}
}
