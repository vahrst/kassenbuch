package de.diakon.entities;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import de.diakon.persistence.*;
import de.vahrst.application.types.SollHabenHome;
/**
 * Home-Klasse f�r Konten
 * @author Thomas
 * @version 
 * @file KontoHome.java
 */
public class KontoHome extends AbstractHome{
	private static Logger logger = Logger.getLogger(KontoHome.class);
	
	private static KontenListe kontoCache = null;

	public static String TABLENAME = "KONTEN";
	private static String COL_KONTONR = "kontonr";
	private static String COL_BEZEICHNUNG = "bezeichnung";
	private static String COL_KST = "kostenstelle";
	private static String COL_DEBITOR = "debitor";
	private static String COL_SHVORGABE = "shvorgabe";

	private static String insertSQL = null;
	private static String selectSQL = null;
	private static String deleteSQL = null;
	private static String updateSQL = null;

	public static final int DUMMY_KTONUMMER = 0;

	private static Konto dummyKonto = new Konto(DUMMY_KTONUMMER, "Konto nicht vorhanden", false, false, SollHabenHome.SOLL);

	/**
	 * Constructor for KontoHome.
	 */
	public KontoHome() {
		super();
	}

	/**
	 * liefert das Konto zur vorgegebenen Kontonummer oder
	 * null, wenn f�r die vorgegebene Kontonummer kein Konto
	 * existiert
	 */
	public Konto getKontoByKontonummer(int kontonummer)throws PersistenceException{
		try{
			loadKonten();
			return kontoCache.getKontoById(kontonummer);
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}
	
	/** 
	 * liefert das Konto zur vorgegebene Kontonummer, oder das DummyKonto,
	 * wenn f�r die vorgegebene Kontonummer kein Konto existiert.
	 */
	public Konto getKontoByKontonummerNotNull(int kontonummer) throws PersistenceException{
		Konto kto = getKontoByKontonummer(kontonummer);
		if(kto == null)
			kto = dummyKonto;
		return kto;
	}
	


	/**
	 * Liefert eine ArrayList aller Konten, sortiert nach
	 * Kontonummer.
	 */
	public KontenListe getAllKonten()throws PersistenceException{
		try{
			loadKonten();
			return new KontenListe(kontoCache);
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
		
	}

	/**
	 * liefert eine KontenListe mit allen Konten, die mit dem vorgegebenen Text beginnen.
	 * @param pattern
	 * @return KontenListe
	 * @throws PersistenceException
	 */
	public KontenListe findKontenByName(String pattern) throws PersistenceException{
		if(pattern == null || pattern.length() == 0){
			return new KontenListe();
		}
		String patternLC = pattern.toLowerCase();	
		try{
			loadKonten();
			KontenListe result = new KontenListe();
			Iterator<Konto> iter = getAllKonten().getktoListe().iterator();
			while(iter.hasNext()){
				Konto kto = iter.next();
				if(kto.getBezeichnungLowercase().startsWith(patternLC)){
					result.add(kto);
				}
			}
			
			return result;
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
			
	}

	public void addKonto(Konto kto) throws PersistenceException{
		try{
			loadKonten();
			insertKontoIntoTable(kto);
			kontoCache.add(kto);	
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}

	public void deleteKonto(Konto kto)throws PersistenceException{
		try{
			loadKonten();
			deleteKontoFromTable(kto);
			kontoCache.remove(kto);	
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}
	
	public void changeKonto(Konto kto)throws PersistenceException{
		try{
			loadKonten();
			updateKontoInTable(kto);
			kontoCache.updateKonto(kto);
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}
	/**
	 * l�dt die Liste aller Konten in den internen
	 * statischen Cache.
	 */
	private void loadKonten() throws SQLException{
		if(kontoCache != null)
			return;

		Connection conn = getConnection();
		String sql = getSelectSQL();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		List<Konto> liste = new ArrayList<Konto>();
		while(rs.next()){
			Konto kto = createKonto(rs);
			liste.add(kto);
		}
		
		kontoCache = new KontenListe();
		kontoCache.add(liste);
			
	}


	// -------------------------------------------------
	// hier sind die DB-Zugriffe
	// -------------------------------------------------
	/**
	 * aktualisiert das vorgegebene Konto
	 */
	private void updateKontoInTable(Konto kto) throws SQLException{
		logger.debug("update Konto");
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getUpdateSQL());
		stmt.setString(1,kto.getBezeichnung());
		stmt.setBoolean(2, kto.isKostenstelleErforderlich());
		stmt.setBoolean(3,kto.isDebitor());
		stmt.setInt(4,kto.getSollHabenVorgabe().getId());
		stmt.setInt(5, kto.getKontonummer());
		
		stmt.executeUpdate();
		stmt.close();
		
	}

	/**
	 * l�scht das vorgegebene Konto aus der Kontentabelle
	 */
	private void deleteKontoFromTable(Konto kto) throws SQLException{
		logger.debug("delete Konto");
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getDeleteSQL());
		stmt.setInt(1, kto.getKontonummer());
		logger.debug(stmt.toString());
		stmt.executeUpdate();
		stmt.close();
	}


	/** 
	 * f�gt das vorgegebene Konto in die Kontentabelle ein
	 */
	private void insertKontoIntoTable(Konto kto) throws SQLException{
		logger.debug("insert Konto");
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getInsertSQL());
		stmt.setInt(1, kto.getKontonummer());
		stmt.setString(2, kto.getBezeichnung());
		stmt.setBoolean(3, kto.isKostenstelleErforderlich());
		stmt.setBoolean(4, kto.isDebitor());
		stmt.setInt(5, kto.getSollHabenVorgabe().getId());
		
		logger.debug(stmt.toString());			
		stmt.executeUpdate();
		stmt.close();					
	}
	
	/**
	 *  erzeugt aus einem ResultSet-Eintrag ein Konto
	 */
	private Konto createKonto(ResultSet rs) throws SQLException{
		int id = rs.getInt(COL_KONTONR);
		String bez = rs.getString(COL_BEZEICHNUNG);
		boolean kst = rs.getBoolean(COL_KST);
		boolean debitor = rs.getBoolean(COL_DEBITOR);
		int shId = rs.getInt(COL_SHVORGABE);
		
		Konto kto = new Konto(id, bez, kst, debitor, SollHabenHome.getById(shId));
		return kto;
	}

	
	/**
	 * Liefert die DDL f�r die Anlage der Konten-Tabelle
	 */
	public String getDDL(){
		StringBuffer sb = new StringBuffer();
		sb.append("create table ").append(TABLENAME);
		sb.append(" (").append(COL_KONTONR).append( " int unique primary key not null, ");
		sb.append(COL_BEZEICHNUNG).append(" char(30) not null, ");
		sb.append(COL_KST).append(" boolean, ");
		sb.append(COL_DEBITOR).append(" boolean, ");
		sb.append(COL_SHVORGABE).append(" int)");
		
		return sb.toString();
	}
	
	/**
	 * liefert den SQL-String f�r insert
	 */
	private String getInsertSQL(){
		if(insertSQL == null){
			StringBuffer sb = new StringBuffer();
			sb.append("insert into ").append(TABLENAME);
			sb.append(" values(?,?,?,?,?)");
			insertSQL = sb.toString();
			
		}
		return insertSQL;
	}
	
	/**
	 * liefert den SQL-String f�r selects
	 */
	private String getSelectSQL(){
		if(selectSQL == null){
			selectSQL = "select * from " + TABLENAME + " order by " + COL_KONTONR;
		}
		return selectSQL;
	}

	/**
	 * liefert den SQL-STring f�r delete
	 */
	private String getDeleteSQL(){
		if(deleteSQL == null){
			deleteSQL = "delete from " + TABLENAME + " where " + COL_KONTONR + " = ?";
		}
		return deleteSQL;
	}

	/**
	 * liefert den SQL-STring f�r update
	 */
	private String getUpdateSQL(){
		if(updateSQL == null){
			StringBuffer sb = new StringBuffer();
			sb.append("update ").append(TABLENAME).append(" set ");
			sb.append(COL_BEZEICHNUNG).append("=?, ");
			sb.append(COL_KST).append("=?, ");
			sb.append(COL_DEBITOR).append("=?, ");
			sb.append(COL_SHVORGABE).append("=? ");
			sb.append("where ").append(COL_KONTONR).append("=?");
			updateSQL = sb.toString();
		}
		return updateSQL;
	}

}
