package de.diakon.entities;

import java.sql.*;

import org.apache.log4j.Logger;

import de.diakon.persistence.*;
/**
 * Home-Klasse f�r Kostenstellen
 * @author m500194
 *
 */
public class KostenstelleHome extends AbstractHome {
	private static Logger logger = Logger.getLogger(KostenstelleHome.class);

	private static KostenstellenListe kstCache = null;


	public static String TABLENAME = "KOSTENSTELLEN";
	private static String COL_KSTNUMMER = "nummer";
	private static String COL_BEZEICHNUNG = "bezeichnung";

	private static String insertSQL = null;
	private static String selectSQL = null;
	private static String deleteSQL = null;
	private static String updateSQL = null;

	/**
	 * Constructor for KostenstelleHome.
	 */
	public KostenstelleHome() {
		super();
	}


	/**
	 * liefert das Konto zur vorgegebenen Kontonummer oder
	 * null, wenn f�r die vorgegebene Kontonummer kein Konto
	 * existiert
	 */
	public Kostenstelle getKostenstelleByKostenstellenummer(int Kostenstellenummer)throws PersistenceException{
		try{
			loadKostenstellen();
			return kstCache.getKostenstelleById(Kostenstellenummer);
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}
	
	/**
	 * Liefert eine ArrayList aller Konten, sortiert nach
	 * Kostenstellenummer.
	 */
	public KostenstellenListe getAllKostenstellen()throws PersistenceException{
		try{
			loadKostenstellen();
			return new KostenstellenListe(kstCache);
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
		
	}

	public void addKostenstelle(Kostenstelle kst) throws PersistenceException{
		try{
			loadKostenstellen();
			insertKostenstelleIntoTable(kst);
			kstCache.add(kst);	
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}

	public void deleteKostenstelle(Kostenstelle kst)throws PersistenceException{
		try{
			loadKostenstellen();
			deleteKostenstelleFromTable(kst);
			kstCache.remove(kst);	
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}
	
	public void changeKostenstelle(Kostenstelle kst)throws PersistenceException{
		try{
			loadKostenstellen();
			updateKostenstelleInTable(kst);
			kstCache.updateKostenstelle(kst);
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}
	/**
	 * l�dt die Liste aller Konten in den internen
	 * statischen Cache.
	 */
	private void loadKostenstellen() throws SQLException{
		if(kstCache != null)
			return;

		Connection conn = getConnection();
		String sql = getSelectSQL();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		KostenstellenListe temp = new KostenstellenListe();
		while(rs.next()){
			Kostenstelle kst = createKostenstelle(rs);
			temp.add(kst);
		}
		
		kstCache = temp;				
			
	}


	// -------------------------------------------------
	// hier sind die DB-Zugriffe
	// -------------------------------------------------
	/**
	 * aktualisiert das vorgegebene Kostenstelle
	 */
	private void updateKostenstelleInTable(Kostenstelle kst) throws SQLException{
		logger.debug("update Kostenstelle");
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getUpdateSQL());
		stmt.setString(1,kst.getBezeichnung());
		stmt.setInt(2, kst.getNummer());
		
		stmt.executeUpdate();
		stmt.close();
		
	}

	/**
	 * l�scht das vorgegebene Kostenstelle aus der Kontentabelle
	 */
	private void deleteKostenstelleFromTable(Kostenstelle kst) throws SQLException{
		logger.debug("delete Kostenstelle");
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getDeleteSQL());
		stmt.setInt(1, kst.getNummer());
		logger.debug(stmt.toString());
		stmt.executeUpdate();
		stmt.close();
	}


	/** 
	 * f�gt das vorgegebene Kostenstelle in die Kontentabelle ein
	 */
	private void insertKostenstelleIntoTable(Kostenstelle kst) throws SQLException{
		logger.debug("insert Kostenstelle");
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getInsertSQL());
		stmt.setInt(1, kst.getNummer());
		stmt.setString(2, kst.getBezeichnung());
		
		logger.debug(stmt.toString());			
		stmt.executeUpdate();
		stmt.close();					
	}
	
	/**
	 *  erzeugt aus einem ResultSet-Eintrag eine Kostenstelle
	 */
	private Kostenstelle createKostenstelle(ResultSet rs) throws SQLException{
		int id = rs.getInt(COL_KSTNUMMER);
		String bez = rs.getString(COL_BEZEICHNUNG);
		
		Kostenstelle kst = new Kostenstelle(id, bez);
		return kst;
	}




	// SQLs
	/**
	 * Liefert die DDL f�r die Anlage der Kostenstellen-Tabelle
	 */
	public String getDDL(){
		StringBuffer sb = new StringBuffer();
		sb.append("create table ").append(TABLENAME);
		sb.append(" (").append(COL_KSTNUMMER).append( " int unique primary key not null, ");
		sb.append(COL_BEZEICHNUNG).append(" char(30) not null) ");
		
		return sb.toString();
	}
	


	/**
	 * liefert den SQL-String f�r insert
	 */
	private String getInsertSQL(){
		if(insertSQL == null){
			StringBuffer sb = new StringBuffer();
			sb.append("insert into ").append(TABLENAME);
			sb.append(" values(?,?)");
			insertSQL = sb.toString();
			
		}
		return insertSQL;
	}
	
	/**
	 * liefert den SQL-String f�r selects
	 */
	private String getSelectSQL(){
		if(selectSQL == null){
			selectSQL = "select * from " + TABLENAME + " order by " + COL_KSTNUMMER;
		}
		return selectSQL;
	}

	/**
	 * liefert den SQL-STring f�r delete
	 */
	private String getDeleteSQL(){
		if(deleteSQL == null){
			deleteSQL = "delete from " + TABLENAME + " where " + COL_KSTNUMMER + " = ?";
		}
		return deleteSQL;
	}

	/**
	 * liefert den SQL-STring f�r update
	 */
	private String getUpdateSQL(){
		if(updateSQL == null){
			StringBuffer sb = new StringBuffer();
			sb.append("update ").append(TABLENAME).append(" set ");
			sb.append(COL_BEZEICHNUNG).append("=? ");
			sb.append("where ").append(COL_KSTNUMMER).append("=?");
			updateSQL = sb.toString();
		}
		return updateSQL;
	}

}
