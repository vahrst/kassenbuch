package de.diakon.entities;

import java.util.List;

import de.diakon.buchung.gui.Kontobezeichnung;
import de.vahrst.application.types.Waehrungsbetrag;

/**
 * Tabellenmodell f�r die Buchungsliste. Die dahinterliegenden
 * Daten werden als ArrayList von Buchungss�tzen verwaltet.
 * @author Thomas
 * @version 
 * @file BuchungslisteTableModel.java
 */
public class BuchungslisteTableModel extends EntityListe<Buchungssatz> {

	


	/**
	 * Constructor for BuchungslisteTableModel.
	 */
	public BuchungslisteTableModel() {
		super();
	}

	public void setBuchungsliste(List<Buchungssatz> liste){
		this.entities = liste;
		fireTableDataChanged();
	}


	/**
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return 8;
	}

	/**
	 * @see javax.swing.table.TableModel#getColumnClass(int)
	 */
	public Class<?> getColumnClass(int columnIndex) {
		switch(columnIndex){
			case 0: 
			case 1: 
			case 2: return Integer.class;
			case 3: return Kontobezeichnung.class;
			case 4: return String.class;
			case 5: return Integer.class;
			case 6: return Waehrungsbetrag.class;
			case 7: return Boolean.class;
		}
		return Object.class;
	}

	/**
	 * @see javax.swing.table.TableModel#isCellEditable(int, int)
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	/**
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		Buchungssatz buchung = entities.get(rowIndex);
		switch(columnIndex){
			case 0: return new Integer(buchung.getLfdNummer());
			case 1: return new Integer(buchung.getTag());
			case 2: return new Integer(buchung.getKontonummer());
			case 3: 
				if(!buchung.isDummySatz()){ 
					return new Kontobezeichnung(buchung.getKontoBezeichnung(), false);
				}else{
					return new Kontobezeichnung("BUCHUNG FEHLT!", true);
				}
			case 4: return buchung.getZusatztext();
			case 5: return new Integer(buchung.getKostenstellenNummer());
			case 6: return buchung.getBetrag();
			case 7: return new Boolean(buchung.isSoll());
		}		
		return null;
	}

}



