package de.diakon.entities;

import java.util.Comparator;

import de.diakon.abschluss.prozesse.BuchungssatzKassenbuchdruck;

/**
 * Comparator, um Buchungssätze nach Konto und laufender
 * Nummer zu sortieren.
 * @author Thomas
 * @version 
 */
public class BuchungenNachKontoSortComparator implements Comparator<BuchungssatzKassenbuchdruck> {

	/**
	 * Constructor for BuchungenNachKontoSortComparator.
	 */
	public BuchungenNachKontoSortComparator() {
		super();
	}


	@Override
	public int compare(BuchungssatzKassenbuchdruck b1,
			BuchungssatzKassenbuchdruck b2) {

		int kto1 = b1.getKonto();
		int kto2 = b2.getKonto();
		
		if(kto1 != kto2)
			return kto1 - kto2;
			
		return b1.getLfdnr() - b2.getLfdnr();
	}

}
