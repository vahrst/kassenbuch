package de.diakon.entities;

import de.vahrst.application.entities.Entity;
import de.vahrst.application.xml.*;

/**
 * Repräsentiert eine Kostenstelle mit ihrer Nummer und
 * ihrer Bezeichnung
 * @author m500194
 *
 */
public class Kostenstelle extends Entity implements Cloneable{

	/** die Kostenstellen-Nummer, max. 6 stellig */
	private int nummer;
	
	/** die Bezeichnung dieser Kostenstelle */
	private String bezeichnung;

	/**
	 * Constructor for Kostenstelle.
	 */
	public Kostenstelle() {
		super();
	}

	/**
	 * Konstruktor mit Vorgabewerten
	 */
	public Kostenstelle(int id, String bezeichnung){
		super();
		this.nummer = id;
		this.bezeichnung = bezeichnung;
	}
	
	/**
	 * Konstruktor mit XML-Kostenstellen-Element als
	 * Vorgabe
	 */
	public Kostenstelle(Element kstElement) throws PlausibilitaetsException{
		super();
		parseXML(kstElement);
	}

	public Object clone(){
		return new Kostenstelle(nummer, bezeichnung);
	}

	/**
	 * Returns the bezeichnung.
	 * @return String
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * Returns the nummer.
	 * @return int
	 */
	public int getNummer() {
		return nummer;
	}

	/**
	 * Sets the bezeichnung.
	 * @param bezeichnung The bezeichnung to set
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * Sets the nummer.
	 * @param nummer The nummer to set
	 */
	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public Element toXML(Document factory){
		Element kstElement = factory.createElement("Kostenstelle");
		
		Element kstnummerElement = factory.createElement("Nummer");
		kstnummerElement.appendChild(factory.createTextNode(Integer.toString(nummer)));
		Element bezeichnungElement = factory.createElement("Name");
		bezeichnungElement.appendChild(factory.createTextNode(bezeichnung));
		
		kstElement.appendChild(kstnummerElement);
		kstElement.appendChild(bezeichnungElement);

		return kstElement;	
	}


	// -------------------------------------------------------
	// private Methoden
	// -------------------------------------------------------
	private void parseXML(Element kstElement) throws PlausibilitaetsException{
		try{
			nummer = Integer.parseInt(kstElement.getFirstElementByName("Nummer").getTextNotNull());	
			bezeichnung = kstElement.getFirstElementByName("Name").getTextNotNull().trim();
		}catch(Exception e){
			throw new PlausibilitaetsException(e);
		}
	}
}
