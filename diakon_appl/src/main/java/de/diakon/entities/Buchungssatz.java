package de.diakon.entities;

import de.vahrst.application.entities.Entity;
import de.vahrst.application.types.*;
import de.vahrst.application.xml.Document;
import de.vahrst.application.xml.Element;



/**
 * Repräsentiert einen Buchungssatz.
 * @author Thomas
 * @version 
 * @file Buchungssatz.java
 */
public class Buchungssatz extends Entity implements Comparable<Buchungssatz>{
	public static final String ASPEKT_TAG = "BS_ASPEKT_TAG";
	public static final String ASPEKT_KONTO = "BS_ASPEKT_KONTO";
	public static final String ASPEKT_KST = "BS_ASPEKT_KST";
	public static final String ASPEKT_ZUSATZTEXT = "BS_ASPEKT_ZUSATZTEXT";


	private int lfdNummer;
	
	/** Tag im aktuellen Monat für diese Buchung */
	private int tag;
	
	private Konto konto;
	
	private String zusatztext;
	
	private Kostenstelle kostenstelle;
	
	private Waehrungsbetrag betrag;
	
	private SollHaben sh;

	
	private boolean isDummy = false;

	/**
	 * Constructor for Buchungssatz.
	 */
	public Buchungssatz() {
		super();
	}

	public Buchungssatz(int lfdnr, int tag, Konto konto, String zusatztext, Kostenstelle kostenstelle, Waehrungsbetrag betrag, SollHaben sh){
		super();
		this.lfdNummer = lfdnr;
		this.tag = tag;
		this.konto = konto;
		this.zusatztext = zusatztext;
		this.kostenstelle = kostenstelle;
		this.betrag = betrag;
		this.sh = sh;
	}

	/**
	 * Liefert das Konto
	 */
	public Konto getKonto(){
		return konto;
	}
	
	/**
	 * Liefert die Bezeichnung zur Kontonummer
	 * @return String
	 */
	public String getKontoBezeichnung(){
		return konto.getBezeichnung();
	}
	
	/**
	 * Returns the kontonummer.
	 * @return int
	 */
	public int getKontonummer() {
		return konto.getKontonummer();
	}

	/**
	 * Liefert die Kostenstellen-Nummer
	 */
	public int getKostenstellenNummer(){
		if(kostenstelle != null){
			return kostenstelle.getNummer();
		}	else {
			return 0;
		}
	}

	/**
	 * Returns the kostenstelle.
	 * @return int
	 */
	public Kostenstelle getKostenstelle() {
		return kostenstelle;
	}

	/**
	 * Returns the soll.
	 * @return boolean
	 */
	public SollHaben getSH() {
		return sh;
	}

	/**
	 * Returns the tag.
	 * @return int
	 */
	public int getTag() {
		return tag;
	}

	/**
	 * Returns the zusatztext.
	 * @return String
	 */
	public String getZusatztext() {
		return zusatztext;
	}

	/**
	 * Sets the konto
	 * @param konto
	 */
	public void setKonto(Konto kto) {
		this.konto = kto;
	}

	/**
	 * Sets the kostenstelle.
	 * @param kostenstelle The kostenstelle to set
	 */
	public void setKostenstelle(Kostenstelle kostenstelle) {
		this.kostenstelle = kostenstelle;
	}

	/**
	 * Sets the soll.
	 * @param soll The soll to set
	 */
	public void setSH(SollHaben sh) {
		this.sh = sh;
	}

	public boolean isSoll(){
		return this.sh.equals(SollHabenHome.SOLL);	
	}
	
	/**
	 * Sets the tag.
	 * @param tag The tag to set
	 */
	public void setTag(int tag) {
		this.tag = tag;
	}

	/**
	 * Sets the zusatztext.
	 * @param zusatztext The zusatztext to set
	 */
	public void setZusatztext(String zusatztext) {
		this.zusatztext = zusatztext;
	}

	/**
	 * Returns the betrag.
	 * @return BigDecimal
	 */
	public Waehrungsbetrag getBetrag() {
		return betrag;
	}

	/**
	 * Sets the betrag.
	 * @param betrag The betrag to set
	 */
	public void setBetrag(Waehrungsbetrag betrag) {
		this.betrag = betrag;
	}

	/**
	 * Returns the lfdNummer.
	 * @return int
	 */
	public int getLfdNummer() {
		return lfdNummer;
	}

	/**
	 * Sets the lfdNummer.
	 * @param lfdNummer The lfdNummer to set
	 */
	public void setLfdNummer(int lfdNummer) {
		this.lfdNummer = lfdNummer;
	}

	/**
	 * liefert eine formatierte Darstellung des Buchungsdatum, unter Vorgabe
	 * von Monat und Jahr.
	 */
	public String getBuchungsdatumFormatiert(int monat, int jahr){
		if(isDummySatz()){
			return "";
		}
		
		StringBuffer sb = new StringBuffer(10);
		if(tag < 10)
			sb.append("0");
		sb.append(Integer.toString(tag)).append(".");
		if(monat < 10)
			sb.append("0");
		sb.append(Integer.toString(monat)).append(".").append(Integer.toString(jahr));
		
		return sb.toString();
	}	
			
	/**
	 * String-Darstellung dieses Buchungssatzes
	 */
	public String toString(){
		StringBuffer sb = new StringBuffer(40);
		sb.append("Buchungssatz Nr. ");
		sb.append(lfdNummer).append(" Konto: ").append(getKontonummer());
		if(kostenstelle != null)
			sb.append(" Kostenstelle: ").append(kostenstelle.getNummer());
			
		return sb.toString();
	}
	
	/**
	 * liefert eine XML-Darstellung dieses Buchungssatzes 
	 * @param factory
	 * @return
	 */
	public Element toXML(Document factory){
		Element bsElement = factory.createElement("Satz");

		bsElement.setAttribute("LfdNr", Integer.toString(lfdNummer));
		if(kostenstelle != null){
			bsElement.setAttribute("Kst", Integer.toString(kostenstelle.getNummer()));
		}
		bsElement.setAttribute("Kto", Integer.toString(konto.getKontonummer()));
		bsElement.setAttribute("Betrag", betrag.formatOhneTausenderPunkt());
		bsElement.setAttribute("SH", isSoll() ? "S" : "H");
		bsElement.setAttribute("Tag", Integer.toString(tag));
		
		
		Element zusatzTextElement = factory.createElement("Text");
		zusatzTextElement.appendChild(factory.createTextNode(zusatztext));
		bsElement.appendChild(zusatzTextElement);
		
		return bsElement;	
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Buchungssatz other) {
		int result = this.lfdNummer - other.lfdNummer;
		if(result == 0){
			long idDiff = this.getObjectId() - other.getObjectId();
			if(idDiff < 0){
				result = -1;
			}else if(idDiff > 0 ){
				result = 1;
			}else{
				result = 0;
			}
		}
		return result;
	}		

	/**
	 * gibt an, ob dies ein Dummy-Buchungssatz ist, der einen fehlenden DB-Eintrag
	 * repräsentiert.
	 * @return
	 */
	public boolean isDummySatz(){
		return isDummy;
	}
	
	protected void setDummyFlag(boolean arg){
		isDummy = arg;
	}
}
