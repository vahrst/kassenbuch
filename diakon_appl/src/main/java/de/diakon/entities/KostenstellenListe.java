package de.diakon.entities;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
/**
 * Container-Klasse f�r Kostenstellen. Implementiert das TableModel
 * Interface. Damit kann die Kostenstellenliste direkt als TableModel
 * in GUI-Oberfl�chen verwendet werden.
 * @author Thomas
 * @version 
 * @file KontenListe.java
 */
public class KostenstellenListe extends EntityListe<Kostenstelle> {
	static Logger logger = Logger.getLogger(KostenstellenListe.class);



	// ----------------------------------------------------

	/**
	 * Constructor for KostenstellenListe.
	 */
	public KostenstellenListe() {
		super();
	}

	public KostenstellenListe(KostenstellenListe liste){
		super();
		this.entities = new ArrayList<Kostenstelle> (liste.entities);
	}

	/**
	 * liefert die Liste der Kostenstellen als ArrayList
	 */
	public List<Kostenstelle> getKstListe() {
		return entities;
	}

	/**
	 * liefert das Konto mit der vorgegebenen Nummer, oder
	 * null, wenn das Konto nicht existiert
	 */
	public Kostenstelle getKostenstelleById(int id){
		for(int i=0;i<entities.size();i++){
			Kostenstelle kst = entities.get(i);
			if(kst.getNummer() == id)
				return kst;
		}
		return null;
	}

	/**
	 * f�gt eine Kostenstelle in die entities hinzu:
	 */
	public void add(Kostenstelle kostenstelle) {
		int insertpos = entities.size();
		for(int i=0;i< entities.size();i++){
			Kostenstelle kst = entities.get(i);
			if(kst.getNummer() > kostenstelle.getNummer()){
				insertpos = i;
				i=entities.size();
			}
		}
		entities.add(insertpos, kostenstelle);
		
		fireTableRowsInserted(insertpos, insertpos);
	}


	/**
	 * entfernt eine Kostenstelle aus der entities
	 */
	public void remove(Kostenstelle kst) {
		int index = entities.indexOf(kst);
		removeRow(index);
	}


	/**
	 * tauscht die vorgegebene Kostenstelle gegen ein bereits
	 * in der Liste existierende Kst mit der gleichen
	 * Nummer aus.
	 */
	public void updateKostenstelle(Kostenstelle kstNeu){
		for(int i=0;i<entities.size();i++){
			Kostenstelle kst = entities.get(i);
			if(kst.getNummer() == kstNeu.getNummer()){
				entities.set(i, kstNeu);
				fireTableDataChanged();
				return;
			}
		}	
	}

	/**
	 * liefert die Kostenstelle in der vorgegebenen Zeile
	 */
	public Kostenstelle getKostenstelleAt(int index){
		return entities.get(index);
	}
	
	/**
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return 2;
	}

	/**
	 * @see javax.swing.table.TableModel#getColumnClass(int)
	 */
	public Class<?> getColumnClass(int columnIndex) {
		switch(columnIndex){
			case 0: return Integer.class;
			case 1: return String.class;
		}
		return Object.class;
	}


	/**
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		Kostenstelle kst = entities.get(rowIndex);
		switch(columnIndex){
			case 0: return new Integer(kst.getNummer());
			case 1: return kst.getBezeichnung();
		}		
		return null;
	}

}
