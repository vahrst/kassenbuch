package de.diakon.entities;

import de.vahrst.application.entities.Entity;
import de.vahrst.application.types.*;
import de.vahrst.application.xml.*;

/**
 * repr�sentiert ein Konto mit Kontonummer, Bezeichung
 * und den steuernden Zusatzattributen.
 * @author m500194
 *
 */
public class Konto extends Entity implements Cloneable{
	public static final String ASPEKT_KONTONUMMER = "KTO_ASPEKT_NUMMER";

	private int kontonummer;
	private String bezeichnung;
	private String bezeichnungLC;
	private boolean kostenstelleErforderlich = false;
	private boolean debitor = false;
	private SollHaben sollHabenVorgabe = SollHabenHome.SOLL;

	/**
	 * Constructor for Konto.
	 */
	public Konto() {
		super();
	}

	/**
	 * Konstruktor, der ein XML-Element mit Kontendaten 
	 * entgegennimmt
	 */
	public Konto(Element kontoElement) throws PlausibilitaetsException{
		super();	

		parseXML(kontoElement);
	}

	/**
	 * noch ein Konstruktor mit allen Feldern
	 */
	public Konto(int kontonummer, String bezeichnung, boolean kst, boolean debitor, SollHaben shVorgabe){
		super();
		this.kontonummer = kontonummer;
		this.bezeichnung = bezeichnung;
		this.kostenstelleErforderlich = kst;
		this.debitor = debitor;
		this.sollHabenVorgabe = shVorgabe;
	}

	public Object clone(){
		return new Konto(kontonummer, bezeichnung, kostenstelleErforderlich, debitor, sollHabenVorgabe);
	}

	// ----------------------------------------------------
	// Getter und Setter 
	// ----------------------------------------------------
	/**
	 * Returns the bezeichnung.
	 * @return String
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * liefert die Kontobezeichnung in lowercase
	 * @return
	 */
	public String getBezeichnungLowercase(){
		if(bezeichnungLC == null && bezeichnung != null)
			bezeichnungLC = bezeichnung.toLowerCase();
		return bezeichnungLC;
	}
	

	/**
	 * Returns the debitor.
	 * @return boolean
	 */
	public boolean isDebitor() {
		return debitor;
	}

	/**
	 * Returns the kontonummer.
	 * @return int
	 */
	public int getKontonummer() {
		return kontonummer;
	}

	/**
	 * Returns the kostenstelleErforderlich.
	 * @return boolean
	 */
	public boolean isKostenstelleErforderlich() {
		return kostenstelleErforderlich;
	}

	/**
	 * Returns the sollHabenVorgabe.
	 * @return SollHaben
	 */
	public SollHaben getSollHabenVorgabe() {
		return sollHabenVorgabe;
	}

	/**
	 * Sets the bezeichnung.
	 * @param bezeichnung The bezeichnung to set
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
		bezeichnungLC = null;
	}

	/**
	 * Sets the debitor.
	 * @param debitor The debitor to set
	 */
	public void setDebitor(boolean debitor) {
		this.debitor = debitor;
	}

	/**
	 * Sets the kontonummer.
	 * @param kontonummer The kontonummer to set
	 */
	public void setKontonummer(int kontonummer) {
		this.kontonummer = kontonummer;
	}

	/**
	 * Sets the kostenstelleErforderlich.
	 * @param kostenstelleErforderlich The kostenstelleErforderlich to set
	 */
	public void setKostenstelleErforderlich(boolean kostenstelleErforderlich) {
		this.kostenstelleErforderlich = kostenstelleErforderlich;
	}

	/**
	 * Sets the sollHabenVorgabe.
	 * @param sollHabenVorgabe The sollHabenVorgabe to set
	 */
	public void setSollHabenVorgabe(SollHaben sollHabenVorgabe) {
		this.sollHabenVorgabe = sollHabenVorgabe;
	}

	/**
	 * gibt an, ob dieses Konto das Dummy-Konto ist
	 * @return boolean
	 */
	public boolean isDummyKonto(){
		return kontonummer == KontoHome.DUMMY_KTONUMMER;
	}

	/**
	 * String-Darstellung dieses Kontos
	 */
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("Konto [").append(kontonummer).append(": ").append(bezeichnung);
		sb.append("][Kost.: ").append(kostenstelleErforderlich);
		sb.append("][Debitor: ").append(debitor);
		sb.append("][S/H Vorgabe: ").append(sollHabenVorgabe);
		sb.append("]");
		return sb.toString();
	}

	/**
	 * Erzeugt aus den Attributen dieses Kontos
	 * ein XML-Element.
	 * @param factory
	 * @return Element
	 */
	public Element toXML(Document factory){
		Element kontoElement = factory.createElement("Konto");
		
		Element kontonummerElement = factory.createElement("Nummer");
		kontonummerElement.appendChild(factory.createTextNode(Integer.toString(kontonummer)));
		Element bezeichnungElement = factory.createElement("Text");
		bezeichnungElement.appendChild(factory.createTextNode(bezeichnung));
		Element kstElement = factory.createElement("Kostenstelle");
		kstElement.appendChild(factory.createTextNode(kostenstelleErforderlich ? "Y":"N"));
		Element debitorElement 	= factory.createElement("Debitor");
		debitorElement.appendChild(factory.createTextNode(debitor ? "Y":"N"));
		Element vorgabeElement = factory.createElement("Vorgabe");
		vorgabeElement.appendChild(factory.createTextNode(sollHabenVorgabe == SollHabenHome.SOLL ? "S":"H"));
		
		kontoElement.appendChild(kontonummerElement);
		kontoElement.appendChild(bezeichnungElement);
		kontoElement.appendChild(kstElement);
		kontoElement.appendChild(debitorElement);
		kontoElement.appendChild(vorgabeElement);
		
		return kontoElement;	
	}

	// -------------------------------------------------------
	// private Methoden
	// -------------------------------------------------------
	/**
	 * f�llt diese Attribute dieses Kontos 
	 * anhand des vorgegebenen XML-Dokuments.
	 * @param kontoElement
	 * @throws PlausibilitaetsException
	 */
	private void parseXML(Element kontoElement) throws PlausibilitaetsException{
		String yn = null;
		try{
			kontonummer = Integer.parseInt(kontoElement.getFirstElementByName("Nummer").getTextNotNull());	
			bezeichnung = kontoElement.getFirstElementByName("Text").getTextNotNull().trim();
			yn = kontoElement.getFirstElementByName("Kostenstelle").getTextNotNull();
			kostenstelleErforderlich = ("Y".equalsIgnoreCase(yn));
			yn = kontoElement.getFirstElementByName("Debitor").getTextNotNull();
			debitor = ("Y".equalsIgnoreCase(yn));
			yn = kontoElement.getFirstElementByName("Vorgabe").getTextNotNull();
			if("S".equalsIgnoreCase(yn)){
				sollHabenVorgabe = SollHabenHome.SOLL;
			}else{
				sollHabenVorgabe = SollHabenHome.HABEN;
			}
		}catch(Exception e){
			throw new PlausibilitaetsException(e);
		}
	}
}
