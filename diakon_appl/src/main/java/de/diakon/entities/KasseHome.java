package de.diakon.entities;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import de.diakon.persistence.*;
/**
 * Home-Klasse f�r Kassen
 * @author Thomas
 * @version 
 */
public class KasseHome extends AbstractHome{
	private static Logger logger = Logger.getLogger(KasseHome.class);
	
	public static String TABLENAME = "KASSEN";
	private static String COL_ID = "id";
	private static String COL_BEZEICHNUNG = "bezeichnung";
	private static String COL_MONAT = "monat";
	private static String COL_JAHR = "jahr";
	private static String COL_KASSENKONTO = "kassenkonto";
	private static String COL_BUCHUNGSKREIS = "buchungskreis";
	private static String COL_MANDANT = "mandant";
	

	private static String insertSQL = null;
	private static String selectSQL = null;
	private static String selectAllSQL = null;
	private static String updateSQL = null;

	/**
	 * Constructor for KasseHome
	 */
	public KasseHome() {
		super();
	}

	/**
	 * liefert die Kasse zur vorgegebenen id
	 * null, wenn f�r die vorgegebene id keine Kasse
	 * existiert
	 */
	public Kasse getKasseById(long id)throws PersistenceException{
		try{
			return selectKasseInTable(id);
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}
	
	/**
	 * Liefert eine ArrayList aller Kassen, sortiert nach
	 * Id.
	 */
	public List<Kasse> getAllKassen()throws PersistenceException{
		try{
			Connection conn = getConnection();
			String sql = getSelectAllSQL();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
		
			List<Kasse> liste = new ArrayList<Kasse>();
			while(rs.next()){
				Kasse kasse = createKasse(rs);
				liste.add(kasse);
			}
			return liste;
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}

	public void addKasse(Kasse kasse) throws PersistenceException{
		try{
			insertKasseIntoTable(kasse);
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}

	
	public void changeKasse(Kasse kasse)throws PersistenceException{
		try{
			updateKasseInTable(kasse);
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}


	// -------------------------------------------------
	// hier sind die DB-Zugriffe
	// -------------------------------------------------
	/**
	 * sucht die Kasse mit der vorgegebenen Id:
	 */
	private Kasse selectKasseInTable(long id) throws SQLException{
		Kasse kasse = null;
		
		logger.debug("select Kasse");
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getSelectSQL());
		stmt.setLong(1, id);
		ResultSet rs = stmt.executeQuery();
		if(rs.next()){
			kasse = createKasse(rs);
		}
		stmt.close();
		return kasse;
	}
		
		


	/**
	 * aktualisiert die vorgegebene Kasse
	 */
	private void updateKasseInTable(Kasse kasse) throws SQLException{
		logger.debug("update Kasse");
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getUpdateSQL());
		stmt.setString(1,kasse.getName());
		stmt.setInt(2, kasse.getMonat());
		stmt.setInt(3, kasse.getJahr());
		stmt.setInt(4, kasse.getKassenkonto());
		stmt.setInt(5, kasse.getBuchungskreis());
		stmt.setString(6, kasse.getMandant());
		stmt.setLong(7, kasse.getObjectId());
		
		stmt.executeUpdate();
		stmt.close();
		
	}

	/** 
	 * f�gt die vorgegebene Kasse in die Kassentabelle ein
	 */
	private void insertKasseIntoTable(Kasse kasse) throws SQLException{
		logger.debug("insert Kasse");
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getInsertSQL());
		stmt.setLong(1, kasse.getObjectId());
		stmt.setString(2, kasse.getName());
		stmt.setInt(3, kasse.getMonat());
		stmt.setInt(4, kasse.getJahr());
		stmt.setInt(5, kasse.getKassenkonto());
		stmt.setInt(6, kasse.getBuchungskreis());
		stmt.setString(7, kasse.getMandant());

		
		
		
		logger.debug(stmt.toString());			
		stmt.executeUpdate();
		stmt.close();					
	}
	
	/**
	 *  erzeugt aus einem ResultSet-Eintrag ein Objekt vom Typ Kasse
	 */
	private Kasse createKasse(ResultSet rs) throws SQLException{
		long id = rs.getLong(COL_ID);
		String name = rs.getString(COL_BEZEICHNUNG);
		int monat = rs.getInt(COL_MONAT);
		int jahr = rs.getInt(COL_JAHR);
		int kassenkonto = rs.getInt(COL_KASSENKONTO);
		int buchungskreis = rs.getInt(COL_BUCHUNGSKREIS);
		String mandant = rs.getString(COL_MANDANT);
		
		Kasse kasse = new Kasse(name, monat, jahr, kassenkonto, buchungskreis, mandant);
		kasse.setObjectId(id);
		return kasse;
	}

	
	/**
	 * Liefert die DDL f�r die Anlage der Kassen-Tabelle
	 */
	public String getDDL(){
		StringBuffer sb = new StringBuffer();
		sb.append("create table ").append(TABLENAME);
		sb.append(" (").append(COL_ID).append( " long unique primary key not null, ");
		sb.append(COL_BEZEICHNUNG).append(" char(30) not null, ");
		sb.append(COL_MONAT).append(" int not null, ");
		sb.append(COL_JAHR).append(" int not null,");
		sb.append(COL_KASSENKONTO).append(" int not null,");
		sb.append(COL_BUCHUNGSKREIS).append(" int not null,");
		sb.append(COL_MANDANT).append(" char(20))");
		
		return sb.toString();
	}
	
	/**
	 * erg�nzt die Kassentabelle um die notwendingen Spalten.
	 * @param conn
	 * @throws SQLException
	 */
	public void updateKassenTabelle(Connection conn) throws SQLException {
		DatabaseMetaData metaData = conn.getMetaData();
		ResultSet columns = metaData.getColumns(null, null, KasseHome.TABLENAME, null);
		
		int zaehler = 0;
		while(columns.next()){
			zaehler++;
		}
		columns.close();
		
		if(zaehler < 7){
			Statement stmt = conn.createStatement();
			if(zaehler == 4){
				stmt.executeUpdate("ALTER TABLE " + TABLENAME + " ADD " + COL_KASSENKONTO + " int default 0 not null");
				zaehler++;
			}
			if(zaehler == 5){
				stmt.executeUpdate("ALTER TABLE " + TABLENAME + " ADD " + COL_BUCHUNGSKREIS + " int default 0 not null");
				zaehler++;
			}
			if(zaehler == 6){
				stmt.executeUpdate("ALTER TABLE " + TABLENAME + " ADD " + COL_MANDANT + " CHAR(20) default '' not null  ");
				zaehler++;
			}
			stmt.close();
		}
	}
	
	
	/**
	 * liefert den SQL-String f�r insert
	 */
	private String getInsertSQL(){
		if(insertSQL == null){
			StringBuffer sb = new StringBuffer();
			sb.append("insert into ").append(TABLENAME);
			sb.append(" values(?,?,?,?,?,?,?)");
			insertSQL = sb.toString();
			
		}
		return insertSQL;
	}
	

	/**
	 * liefert den SQL-String f�r select aller Kassen
	 */
	private String getSelectAllSQL(){
		if(selectAllSQL == null){
			selectAllSQL = "select * from " + TABLENAME + " order by " + COL_BEZEICHNUNG;
		}
		return selectAllSQL;
	}

	/**
	 * liefert den SQL-String f�r select einer Kassen nach id
	 */
	private String getSelectSQL(){
		if(selectSQL == null){
			selectSQL = "select * from " + TABLENAME + " where " + COL_ID + " = ?";
		}
		return selectSQL;
	}


	/**
	 * liefert den SQL-STring f�r update
	 */
	private String getUpdateSQL(){
		if(updateSQL == null){
			StringBuffer sb = new StringBuffer();
			sb.append("update ").append(TABLENAME).append(" set ");
			sb.append(COL_BEZEICHNUNG).append("=?, ");
			sb.append(COL_MONAT).append("=?, ");
			sb.append(COL_JAHR).append("=?, ");
			sb.append(COL_KASSENKONTO).append("=?, ");
			sb.append(COL_BUCHUNGSKREIS).append("=?, ");
			sb.append(COL_MANDANT).append("=? ");
			sb.append("where ").append(COL_ID).append("=?");
			updateSQL = sb.toString();
		}
		return updateSQL;
	}

}
