package de.diakon.entities;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import de.diakon.persistence.PersistenceException;
import de.vahrst.application.types.SollHaben;
import de.vahrst.application.types.SollHabenHome;
import de.vahrst.application.types.Waehrungsbetrag;
/**
 * 
 * @author Thomas
 * @version 
 * @file BuchungssatzHome.java
 */
public class BuchungssatzHome extends AbstractHome {
	private static Logger logger = Logger.getLogger(BuchungsperiodeHome.class);
	
	public static String TABLENAME = "BUCHUNGEN";
	private static String COL_ID = "id";  // Object-Id
	private static String COL_PERIODE_ID = "periode_ref";  // die Objekct-Id der Buchungsperiode
	private static String COL_NUMMER = "nummer";
	private static String COL_TAG = "tag";
	private static String COL_KONTO = "kontonr";
	private static String COL_ZUSATZTEXT = "zusatztext";
	private static String COL_KST = "kostenstelle";
	private static String COL_BETRAG = "betrag";
	private static String COL_SH = "sh";
	

	private static String insertSQL = null;
	private static String selectSQL = null;
	private static String updateSQL = null;

	
	private KontoHome kontoHome = null;
	private KostenstelleHome kstHome = null;
	
	/**
	 * Constructor for BuchungssatzHome.
	 */
	public BuchungssatzHome() {
		super();
	}


	/** lazy Getter f�r KontoHome */
	private KontoHome getKontoHome(){
		if(kontoHome == null)
			kontoHome = new KontoHome();
			
		return kontoHome;
	}
	/** lazy Getter f�r KostenstelleHome */
	private KostenstelleHome getKostenstelleHome(){
		if(kstHome == null)
			kstHome = new KostenstelleHome();
		return kstHome;
	}

	
	/**
	 * liefert die Liste aller Buchungss�tze f�r die vorgegebene
	 * Buchungsperiode
	 */
	public List<Buchungssatz> getAllBuchungssaetze(Buchungsperiode periode) throws PersistenceException{
		try{
			List<Buchungssatz> liste = new ArrayList<Buchungssatz>();

			Connection conn = getConnection();
			PreparedStatement stmt = conn.prepareStatement(getSelectAllSQL());

			stmt.setLong(1, periode.getObjectId());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				liste.add(createBuchungssatz(rs));
			}
			stmt.close();

			return liste;			
		}catch(SQLException e){
			throw new PersistenceException(e);
		}
	}





	/**
	 * f�gt den vorgebenen Buchungssatz f�r die vorgegebene Buchungsperiode
	 * in die TAbelle ein.
	 */
	public void addBuchungssatz(Buchungssatz satz, Buchungsperiode periode)throws PersistenceException{
		try{
			Connection conn = getConnection();
			PreparedStatement stmt = conn.prepareStatement(getInsertSQL());
			
			stmt.setLong(1, satz.getObjectId());
			stmt.setLong(2, periode.getObjectId());
			stmt.setInt(3, satz.getLfdNummer());
			stmt.setInt(4, satz.getTag());
			stmt.setInt(5, satz.getKontonummer());
			stmt.setString(6, satz.getZusatztext());
			stmt.setInt(7, satz.getKostenstellenNummer());
			stmt.setInt(8, satz.getBetrag().getCent());
			stmt.setInt(9, satz.getSH().getId());			
			
			stmt.executeUpdate();
			
			stmt.close();	
		}catch(SQLException e){
			throw new PersistenceException(e);
		}	
	}

	/**
	 * aktualisiert die Daten des vorgebenen Buchungssatz 
	 * f�r die vorgegebene Buchungsperiode in der TAbelle.
	 */
	public void updateBuchungssatz(Buchungssatz satz, Buchungsperiode periode)throws PersistenceException{
		
		try{
			Connection conn = getConnection();
			PreparedStatement stmt = conn.prepareStatement(getUpdateSQL());
			
			stmt.setInt(1, satz.getTag());
			stmt.setInt(2, satz.getKontonummer());
			stmt.setString(3, satz.getZusatztext());
			stmt.setInt(4, satz.getKostenstellenNummer());
			stmt.setInt(5, satz.getBetrag().getCent());
			stmt.setInt(6, satz.getSH().getId());			
			stmt.setLong(7, periode.getObjectId());

			// wir akualisieren nnicht �ber die Object-Id des Buchungssatzes, weil
			// bei �nderungen in der Regel ein neues Buchungssatz-Objekt erzeugt
			// wird, das eine neue Objekct -Id hat. Daher hier der Zugriff �ber
			// die Laufende Nummer( 
			stmt.setInt(8, satz.getLfdNummer());

			
			int updCount = stmt.executeUpdate();
			logger.debug("aktualisiert: " + updCount);
			
			stmt.close();	
		}catch(SQLException e){
			logger.error("Fehler bei updateBuchungssatz", e);
			throw new PersistenceException(e);
		}
	}

	public Buchungssatz createDummyBuchungssatz(int lfdNummer){
		Konto dummyKonto = null;
		try{
			dummyKonto = new KontoHome().getKontoByKontonummerNotNull(KontoHome.DUMMY_KTONUMMER);
		}catch(Exception e){
		}
		Waehrungsbetrag betrag = new Waehrungsbetrag(0);
		SollHaben sh = SollHabenHome.SOLL;
		
		Buchungssatz bs = new Buchungssatz(lfdNummer, 0, dummyKonto, "", null, betrag, sh);
		bs.setDummyFlag(true);
		return bs;
	}

	
	
	/**
	 *  erzeugt aus einem ResultSet-Eintrag ein Objekt vom Typ Buchungssatz
	 */
	private Buchungssatz createBuchungssatz(ResultSet rs) throws SQLException, PersistenceException{
		long objId = rs.getLong(COL_ID);
		int nummer = rs.getInt(COL_NUMMER);
		int tag = rs.getInt(COL_TAG);
		int ktoNummer = rs.getInt(COL_KONTO);
		String zusatz = rs.getString(COL_ZUSATZTEXT);
		int kstNummer = rs.getInt(COL_KST);
		int betr = rs.getInt(COL_BETRAG);
		int sh = rs.getInt(COL_SH);
		
		Konto kto = getKontoHome().getKontoByKontonummerNotNull(ktoNummer);
		Kostenstelle kst = getKostenstelleHome().getKostenstelleByKostenstellenummer(kstNummer);
		
		Buchungssatz b = new Buchungssatz(nummer, tag, kto, zusatz, kst, new Waehrungsbetrag(betr), SollHabenHome.getById(sh));
		b.setObjectId(objId);
		return b;
	}
	
	
	// -----------------------------------------------------------------------
	// SQL
	// -----------------------------------------------------------------------

	/**
	 * Liefert die DDL f�r die Anlage der Buchungsperioden-Tabelle
	 */
	public String getDDL(){
		StringBuffer sb = new StringBuffer();
		sb.append("create table ").append(TABLENAME);
		sb.append(" (").append(COL_ID).append( " long unique primary key not null, ");
		sb.append(COL_PERIODE_ID).append(" long not null, ");
		sb.append(COL_NUMMER).append(" int not null, ");
		sb.append(COL_TAG).append(" int not null, ");
		sb.append(COL_KONTO).append(" int not null, ");
		sb.append(COL_ZUSATZTEXT).append(" char(30), ");
		sb.append(COL_KST).append(" int not null, ");
		sb.append(COL_BETRAG).append(" int not null, ");
		sb.append(COL_SH).append(" int not null) ");
		
		return sb.toString();
	}
	
	/**
	 * liefert den SQL-String f�r insert
	 */
	private String getInsertSQL(){
		if(insertSQL == null){
			StringBuffer sb = new StringBuffer();
			sb.append("insert into ").append(TABLENAME);
			sb.append(" values(?,?,?,?,?,?,?,?,?)");
			insertSQL = sb.toString();
			
		}
		return insertSQL;
	}
	

	/**
	 * liefert den SQL-String f�r select der Buchungsperioden einer Kasse
	 * @return String die SQL f�r das Select-Statement
	 */
	private String getSelectAllSQL(){
		if(selectSQL == null){
			selectSQL = "select * from " + TABLENAME + " where " + COL_PERIODE_ID + " = ? order by " + COL_NUMMER;
		}
		return selectSQL;
	}

//	/**
//	 * liefert den SQL-String f�r select einer Buchungsperioden einer Kasse
//	 * @return String die SQL f�r das Select-Statement
//	 */
//	private String getSelectSQL(){
//		if(selectSQL == null){
//			selectSQL = "select * from " + TABLENAME + " where " + COL_PERIODE_ID + "=? and " + COL_NUMMER +"=? ";
//		}
//		return selectSQL;
//	}



	/**
	 * liefert den SQL-STring f�r update
	 */
	private String getUpdateSQL(){
		if(updateSQL == null){
			StringBuffer sb = new StringBuffer();
			sb.append("update ").append(TABLENAME).append(" set ");
			sb.append(COL_TAG).append("=?, ");
			sb.append(COL_KONTO).append("=?, ");
			sb.append(COL_ZUSATZTEXT).append("=?, ");
			sb.append(COL_KST).append("=?, ");
			sb.append(COL_BETRAG).append("=?, ");
			sb.append(COL_SH).append("=? ");
			sb.append("where ").append(COL_PERIODE_ID).append("=? and ");
			sb.append(COL_NUMMER).append("=?");
			updateSQL = sb.toString();
		}
		return updateSQL;
	}

	/**
	 * Iteriert �ber alle Buchungss�tze f�r die vorgegebene Buchungsperiode und
	 * errechnet den Buchungssaldo dieses Monats.
	 * @param periodeId
	 * @return
	 */
    public Waehrungsbetrag getBerechnetenSaldoFuerPeriode(Long periodeId) {
		try {
			Connection conn = getConnection();
			PreparedStatement stmt = conn.prepareStatement(getSelectAllSQL());

			int saldo = 0;

			stmt.setLong(1, periodeId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				int betrag = rs.getInt(COL_BETRAG);
				int sh = rs.getInt(COL_SH);

				if (sh == 0) {
					saldo -= betrag;
				} else {
					saldo += betrag;
				}
			}
			rs.close();
			stmt.close();
			return new Waehrungsbetrag(saldo);
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
    }
}
