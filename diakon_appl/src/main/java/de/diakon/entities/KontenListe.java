package de.diakon.entities;

import java.util.*;

import org.apache.log4j.Logger;

import de.vahrst.application.types.SollHaben;
/**
 * Container-Klasse f�r Konten. Implementiert das TableModel
 * Interface. Damit kann die Kontenliste direkt als TableModel
 * in GUI-Oberfl�chen verwendet werden.
 * @author Thomas
 * @version 
 * @file KontenListe.java
 */
public class KontenListe extends EntityListe<Konto> {
	static Logger logger = Logger.getLogger(KontenListe.class);


	class KontoSorter implements Comparator<Konto>{
		/**
		 * @see java.util.Comparator#compare(Object, Object)
		 */
		public int compare(Konto k1, Konto k2) {
			return k1.getKontonummer() - k2.getKontonummer();
		}

	}

	// ----------------------------------------------------

	/**
	 * Constructor for KontenListe.
	 */
	public KontenListe() {
		super();
	}

	public KontenListe(KontenListe liste){
		super();
		this.entities = new ArrayList<Konto>(liste.entities);
	}

	/**
	 * liefert die Liste der Konten als ArrayList
	 */
	public List<Konto> getktoListe() {
		return entities;
	}

	/**
	 * liefert das Konto mit der vorgegebenen Nummer, oder
	 * null, wenn das Konto nicht existiert. Wird von KontoHome
	 * verwendet.
	 */
	public Konto getKontoById(int id){
		for(int i=0;i<entities.size();i++){
			Konto kto = entities.get(i);
			if(kto.getKontonummer() == id)
				return kto;
		}
		return null;
	}

	/**
	 * f�gt ein Konto in die entities hinzu:
	 */
	public void add(Konto konto) {
		
		int insertpos = entities.size();
		for(int i=0;i< entities.size();i++){
			Konto kto = entities.get(i);
			if(kto.getKontonummer() > konto.getKontonummer()){
				insertpos = i;
				i=entities.size();
			}
		}
		entities.add(insertpos, konto);
		
		fireTableRowsInserted(insertpos, insertpos);
	}

	/**
	 * f�gt eine Liste von Konten 'en bloc' hinzu. Dieser werden
	 * hier sortiert.
	 */
	public void add(List<Konto> liste){
		entities = new ArrayList<Konto>(liste);
		Collections.sort(entities, new KontoSorter());
		fireTableDataChanged();
			
	}


	/**
	 * entfernt ein Konto aus der entities
	 */
	public void remove(Konto konto) {
		int index = entities.indexOf(konto);
		removeRow(index);
	}


	/**
	 * tauscht das vorgegebene Konto gegen ein bereits
	 * in der Liste existierendes Konto mit der gleichen
	 * Nummer aus.
	 */
	public void updateKonto(Konto kontoNeu){
		for(int i=0;i<entities.size();i++){
			Konto kto = entities.get(i);
			if(kto.getKontonummer() == kontoNeu.getKontonummer()){
				entities.set(i, kontoNeu);
				fireTableDataChanged();
				return;
			}
		}	
	}

	/**
	 * liefert das Konto in der vorgegebenen Zeile
	 */
	public Konto getKontoAt(int index){
		return entities.get(index);
	}
	
	/**
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return 5;
	}

	/**
	 * @see javax.swing.table.TableModel#getColumnClass(int)
	 */
	public Class<?> getColumnClass(int columnIndex) {
		switch(columnIndex){
			case 0: return Integer.class;
			case 1: return String.class;
			case 2:
			case 3: return Boolean.class;
			case 4: return SollHaben.class;
		}
		return Object.class;
	}

	/**
	 * @see javax.swing.table.TableModel#isCellEditable(int, int)
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	/**
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		Konto kto = entities.get(rowIndex);
		switch(columnIndex){
			case 0: return new Integer(kto.getKontonummer());
			case 1: return kto.getBezeichnung();
			case 2: return new Boolean(kto.isKostenstelleErforderlich());
			case 3: return new Boolean(kto.isDebitor());
			case 4: return kto.getSollHabenVorgabe();
		}		
		return null;
	}

		}
