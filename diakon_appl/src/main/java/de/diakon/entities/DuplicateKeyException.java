package de.diakon.entities;

import de.diakon.persistence.PersistenceException;

/**
 * 
 * @author Thomas
 * @version 
 * @file DuplicateKeyExcepiton.java
 */
public class DuplicateKeyException extends PersistenceException {

	/**
	 * Constructor for DuplicateKeyExcepiton.
	 */
	public DuplicateKeyException() {
		super();
	}

	/**
	 * Constructor for DuplicateKeyExcepiton.
	 * @param message
	 */
	public DuplicateKeyException(String message) {
		super(message);
	}

	/**
	 * Constructor for DuplicateKeyExcepiton.
	 * @param message
	 * @param cause
	 */
	public DuplicateKeyException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor for DuplicateKeyExcepiton.
	 * @param cause
	 */
	public DuplicateKeyException(Throwable cause) {
		super(cause);
	}

}
