package de.diakon.entities;

import java.lang.ref.WeakReference;
import java.util.*;

import javax.swing.event.*;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;

/**
 * 
 * @author Thomas
 * @version 
 * @file EntityListe.java
 */
public abstract class EntityListe<E> implements TableModel {
	private Logger logger;
	
	/**
	 * Die interne Liste der Entities. Als ArrayList
	 */
	protected List<E> entities = new ArrayList<E>();
	
	/**
	 * Die Liste der TableModelListener
	 */
	private List<WeakReference<TableModelListener>> listenerList = new ArrayList<WeakReference<TableModelListener>>();

	/**
	 * Constructor for EntityListe.
	 */
	public EntityListe() {
		super();
		logger = Logger.getLogger(getClass());
		
	}

	public void removeRow(int index){
		if(index != -1 && index < entities.size()){
			entities.remove(index);
			fireTableRowsDeleted(index, index);
		}
		
	}



	/**
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() {
		return entities.size();
	}





	/**
	 * @see javax.swing.table.TableModel#getColumnName(int)
	 */
	public String getColumnName(int columnIndex) {
		return null;
	}




	/**
	 * @see javax.swing.table.TableModel#isCellEditable(int, int)
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}






	/**
	 * @see javax.swing.table.TableModel#setValueAt(Object, int, int)
	 */
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	}




	//
	//  Managing Listeners
	//

	/**
	 * Adds a listener to the list that's notified each time a change
	 * to the data model occurs.
	 *
	 * @param	l		the TableModelListener
	 */
	public void addTableModelListener(TableModelListener l) {
		logger.debug("TableModelListener added: " + l);
		listenerList.add(new WeakReference<TableModelListener>( l ));

		removeDeadReferences();
		
	}



	/**
	 * Removes a listener from the list that's notified each time a
	 * change to the data model occurs.
	 *
	 * @param	l		the TableModelListener
	 */
	public void removeTableModelListener(TableModelListener l) {
		Iterator<WeakReference<TableModelListener>> iter = listenerList.iterator();
		while(iter.hasNext()){
			WeakReference<TableModelListener> wr = iter.next();
			TableModelListener tl = wr.get();
			if(tl == null || tl == l){
				iter.remove();
			}
		}
	}



	/**
	 * Returns an array of all the table model listeners 
	 * registered on this model.
	 *
	 * @return all of this model's <code>TableModelListener</code>s 
	 *         or an empty
	 *         array if no table model listeners are currently registered
	 *
	 * @see #addTableModelListener
	 * @see #removeTableModelListener
	 *
	 * @since 1.4
	 */
//	public TableModelListener[] getTableModelListeners() {
//		return (TableModelListener[]) listenerList.getListeners(
//			TableModelListener.class);
//	}

	//
	//  Fire methods
	//




	/**
	 * Notifies all listeners that all cell values in the table's
	 * rows may have changed. The number of rows may also have changed
	 * and the <code>JTable</code> should redraw the
	 * table from scratch. The structure of the table (as in the order of the
	 * columns) is assumed to be the same.
	 *
	 * @see TableModelEvent
	 * @see EventListenerList
	 * @see javax.swing.JTable#tableChanged(TableModelEvent)
	 */
	public void fireTableDataChanged() {
		fireTableChanged(new TableModelEvent(this));
	}



	/**
	 * Notifies all listeners that the table's structure has changed.
	 * The number of columns in the table, and the names and types of
	 * the new columns may be different from the previous state.
	 * If the <code>JTable</code> receives this event and its
	 * <code>autoCreateColumnsFromModel</code>
	 * flag is set it discards any table columns that it had and reallocates
	 * default columns in the order they appear in the model. This is the
	 * same as calling <code>setModel(TableModel)</code> on the
	 * <code>JTable</code>.
	 *
	 * @see TableModelEvent
	 * @see EventListenerList
	 */
	public void fireTableStructureChanged() {
		fireTableChanged(new TableModelEvent(this, TableModelEvent.HEADER_ROW));
	}



	/**
	 * Notifies all listeners that rows in the range
	 * <code>[firstRow, lastRow]</code>, inclusive, have been inserted.
	 *
	 * @param  firstRow  the first row
	 * @param  lastRow   the last row
	 *
	 * @see TableModelEvent
	 * @see EventListenerList
	 *
	 */
	public void fireTableRowsInserted(int firstRow, int lastRow) {
		fireTableChanged(
			new TableModelEvent(
				this,
				firstRow,
				lastRow,
				TableModelEvent.ALL_COLUMNS,
				TableModelEvent.INSERT));
	}



	/**
	 * Notifies all listeners that rows in the range
	 * <code>[firstRow, lastRow]</code>, inclusive, have been updated.
	 *
	 * @param firstRow  the first row
	 * @param lastRow   the last row
	 *
	 * @see TableModelEvent
	 * @see EventListenerList
	 */
	public void fireTableRowsUpdated(int firstRow, int lastRow) {
		fireTableChanged(
			new TableModelEvent(
				this,
				firstRow,
				lastRow,
				TableModelEvent.ALL_COLUMNS,
				TableModelEvent.UPDATE));
	}



	/**
	 * Notifies all listeners that rows in the range
	 * <code>[firstRow, lastRow]</code>, inclusive, have been deleted.
	 *
	 * @param firstRow  the first row
	 * @param lastRow   the last row
	 *
	 * @see TableModelEvent
	 * @see EventListenerList
	 */
	public void fireTableRowsDeleted(int firstRow, int lastRow) {
		logger.debug("fireEvent: rowsDeleted: " + firstRow + " - " + lastRow);
		logger.debug("size = " + getRowCount());
		fireTableChanged(
			new TableModelEvent(
				this,
				firstRow,
				lastRow,
				TableModelEvent.ALL_COLUMNS,
				TableModelEvent.DELETE));
	}



	/**
	 * Notifies all listeners that the value of the cell at 
	 * <code>[row, column]</code> has been updated.
	 *
	 * @param row  row of cell which has been updated
	 * @param column  column of cell which has been updated
	 * @see TableModelEvent
	 * @see EventListenerList
	 */
	public void fireTableCellUpdated(int row, int column) {
		fireTableChanged(new TableModelEvent(this, row, row, column));
	}



	/**
	 * Forwards the given notification event to all
	 * <code>TableModelListeners</code> that registered
	 * themselves as listeners for this table model.
	 *
	 * @param e  the event to be forwarded
	 *
	 * @see #addTableModelListener
	 * @see TableModelEvent
	 * @see EventListenerList
	 */
	public void fireTableChanged(TableModelEvent e) {
		if(!listenerList.isEmpty()){
			logger.debug("fireTableChanged");
		}
		
		Iterator<WeakReference<TableModelListener>> iter = listenerList.iterator();
		while(iter.hasNext()){
			WeakReference<TableModelListener> wr = iter.next();
			TableModelListener tl = wr.get();
			logger.debug("TableListener: " + tl);
			if(tl == null){
				iter.remove();
				logger.debug("leere WeakReference aus TableListenerListe entfernt");
			}else{
				tl.tableChanged(e);
			}
		}
	}

	private void removeDeadReferences(){
		Iterator<WeakReference<TableModelListener>> iter = listenerList.iterator();
		while(iter.hasNext()){
			WeakReference<TableModelListener> wr = iter.next();
			TableModelListener tl = wr.get();
			if(tl == null){
				iter.remove();
				logger.debug("leere WeakReference aus TableListenerListe entfernt");
			}
		}
		
	}

}
