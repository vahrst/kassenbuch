package de.diakon.entities;

import java.beans.*;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import javax.swing.table.TableModel;

import org.apache.log4j.Logger;

import de.diakon.global.Konstanten;
import de.diakon.main.PropertiesManager;
import de.diakon.persistence.PersistenceException;
/**
 * Die 'Kopfdaten' der aktuellen Kasse. Name, aktuelle
 * Salden und aktuelle Buchungsperiode
 * @author Thomas
 * @version 
 */
public class KassenKontext {
	static Logger logger = Logger.getLogger(KassenKontext.class);
	
	public static final String PCEVENT_KASSE_CHANGED = "KASSE";
	public static final String PCEVENT_SALDEN = "SALDEN";

	/** Die Kasse dieses Kassen-Kontext */
	private Kasse kasse;

	/** Die laufende Buchungsperiode (aktueller Monat)*/
	private Buchungsperiode laufendePeriode;

	/** fehlende Buchungsperionden, zu denen aber Buchungss�tze vorhanden sind */
	private List<Long> fehlendeBuchungsperioden;
	

	private PropertyChangeSupport pcSupport = null;

	/** 
	 * Das Tabellenmodell f�r die Anzeige von Buchungss�tzen. 
	 * Dieses wird hier zentral gehalten und bei �nderungen der Kasse
	 * oder der Buchungsperiode passend neu gef�llt.
	 */
	private BuchungslisteTableModel buchungslisteTableModel = new BuchungslisteTableModel();	


	/**
	 * Constructor for KasseKontext.
	 */
	public KassenKontext() {
		super();
		pcSupport = new PropertyChangeSupport(this);
	}

	// ----------------------------------------------------------
	// PropertyChangeSupport
	// ----------------------------------------------------------
	public void addPropertyChangeListener(PropertyChangeListener l){
		pcSupport.addPropertyChangeListener(l);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener l){
		pcSupport.removePropertyChangeListener(l);
	}


	/**
	 * wechselt die Kasse. Die neue Kasse wird anhand der vorgegebenen
	 * Object-Id gelesen.
	 */
	public void setKasse(long kassenId) throws PersistenceException{
		Kasse k = new KasseHome().getKasseById(kassenId);
		if(k == null){
			String message = "Kasse mit der vorgegebenen Id nicht gefunden: " + kassenId;
			logger.error(message);
			throw new PersistenceException(message);
		}else{
			setKasse(k);
		}
	}


	/**
	 * wechselt die Kasse. Die Kassenstammdaten sowie die beiden
	 * Buchungsperioden werden neu eingelesen
	 */
	public void setKasse(Kasse kasse)throws PersistenceException{
		this.kasse = kasse;


		laufendePeriode = new BuchungsperiodeHome().getAktuelleBuchungsperiodeForKasse(kasse);
//		ArrayList liste = new BuchungsperiodeHome().getBuchungsperiodenForKasse(kasse);



	
		if(laufendePeriode == null){
			throw new PersistenceException("aktuelle Buchungsperiode nicht gefunden!");
		}
			
		buchungslisteTableModel.setBuchungsliste(laufendePeriode.getBuchungssaetze());

		fehlendeBuchungsperioden = new BuchungsperiodeHome().getBuchungsperiodenIdsVonVerwaistenBuchungssaetzen();
		// Als Warning ausgeben:
		for(Long id : fehlendeBuchungsperioden){
			logger.warn("Buchungss�tze mit dieser nicht vorhandenen Periode-Referenz gefunden: " + id + "!!");
		}

	
		pcSupport.firePropertyChange(PCEVENT_KASSE_CHANGED, null, kasse);
		
		Properties props = PropertiesManager.getProperties();
		props.put(Konstanten.PROPS_LAST_KASSE_ID, Long.toString(kasse.getObjectId()));
		
		try{
			PropertiesManager.storeProperties(props);
		}catch(IOException e){
			logger.warn("Fehler beim Sichern der Appl.-Properties", e);
		}
		
	}


	/**
	 * liefert die Buchungsperiode, in der gerade gebucht wird.
	 * Dies ist die laufendePeriode, wenn korrekturModus = false ist und
	 * die Vorperiode, wenn korrekturModus = true ist;
	 */
	public Buchungsperiode getAktuelleBuchungsperiode(){
		return laufendePeriode;
	}

	/**
	 * Liefert die Liste der fehlenden Buchungsperioden, zu denen aber Buchungss�tze
	 * existieren...
	 * @return
	 */
	public List<Long> getFehlendeBuchungsperioden() {
		return fehlendeBuchungsperioden;
	}

	/**
	 * liefert das Tabellen-Modell f�r die Buchungsliste.
	 */
	public TableModel getBuchungslisteTableModel(){
		return buchungslisteTableModel;
	}

	/**
	 * liefert monat/jahr der aktuellen Buchungsperiode im Format mm/jjjj
	 */
	public String getAktuelleBuchungsperiodeFormatiert(){
		Buchungsperiode p = getAktuelleBuchungsperiode();
		if(p != null)
			return p.getBuchungsPeriodeFormatiert();
		else
			return "";
	}
	
	/**
	 * liefert monat/jahr f�r die n�chste Buchungsperiode im Format mm/jjjj
	 */
	public String getNaechsteBuchungsperiodeFormatiert(){
		Buchungsperiode p = getAktuelleBuchungsperiode();
		if(p != null)
			return p.getNaechsteBuchungsPeriodeFormatiert();
		else
			return "";	
	}

	/**
	 * liefert monat/jahr f�r die vorherige Buchungsperiode im Format mm/jjjj
	 */
	public String getVorherigeBuchungsperiodeFormatiert(){
		Buchungsperiode p = getAktuelleBuchungsperiode();
		if(p != null)
			return p.getVorherigeBuchungsPeriodeFormatiert();
		else
			return "";	
	}
	

	/**
	 * f�gt f�r die aktuelle Buchungsperiode einen Buchungsatz hinzu. aktualisiert
	 * bei Bedarf auch die Folgeperiode (Falls wir im Korrektur-Modus sind).
	 */	
	public void addBuchungssatz(Buchungssatz satz)throws PlausibilitaetsException , PersistenceException{
		// technische Transaktion: Buchungssatz schreiben:
		int nr = satz.getLfdNummer();
			
		new BuchungssatzHome().addBuchungssatz(satz, laufendePeriode);				
		laufendePeriode.addBuchungssatz(satz);
			
		buchungslisteTableModel.fireTableRowsInserted(nr-1,nr-1);
		pcSupport.firePropertyChange(PCEVENT_SALDEN, null, laufendePeriode.getSaldo());

	}


	/**
	 * aktualisiert den vorgegebenen Buchungssatz f�r die aktuelle Periode.
	 */
	public void updateBuchungssatz(Buchungssatz satz) throws PlausibilitaetsException, PersistenceException{
		int nr = satz.getLfdNummer();
		BuchungssatzHome bsHome = new BuchungssatzHome();
		
		// wenn der urspr�ngliche Buchungssatz ein DummySatz war, d.h. ein
		// Satz, zu dem es noch keinen DB-Eintrag gab, dann m�ssen wir jetzt
		// den Satz in der DB-Tabelle anlegen:
		if(laufendePeriode.isDummyBuchung(nr)){
			bsHome.addBuchungssatz(satz, laufendePeriode);
		}else{
			bsHome.updateBuchungssatz(satz, laufendePeriode);
		}
		laufendePeriode.replaceBuchungssatz(satz);
		
		buchungslisteTableModel.fireTableRowsUpdated(nr-1, nr-1);
		pcSupport.firePropertyChange(PCEVENT_SALDEN, null, laufendePeriode.getSaldo());
		
	}		

	// ------------------------------------------------------------------
	// die einfachen Getter und Setter
	// ------------------------------------------------------------------
	/**
	 * liefert die Kassen-Stammdaten
	 */
	public Kasse getKasse(){
		return kasse;	
	}
	
	/** liefert die Id der aktuellen Kasse, oder 0 */
	public long getKassenId(){
		if(kasse == null){
			return 0;
		}else{
			return kasse.getObjectId();
		}
	}
	
	/** liefert die n�chste Freie Buchungsnummer */
	public int getNaechsteBuchungsNummer(){
		return getAktuelleBuchungsperiode().getNaechsteBuchungsNummer();
	}
		
	/**
	 * gibt an, ob f�r die aktuelle Kasse eine Buchungsperiode im Vormonat
	 * existiert
	 */
	public boolean hasVormonat(){
		if(laufendePeriode == null){
			return false;
		}else{
			return laufendePeriode.hasVormonat();
		}
	}

	/**
	 * liefert eine ID, wenn der Vormonat nicht vorhanden ist (d.h. keine Buchungsperiode f�r den
	 * Vormonat vorhanden ist) und in der Liste der fehlenden BuchungsperiodeIds ein Eintrag
	 * enthalten ist, der vom Zeitraum her passend ist...
	 * @return
	 */
	public Long getVormonatReparaturKandidat(){
		if(laufendePeriode != null && !laufendePeriode.hasVormonat() && !fehlendeBuchungsperioden.isEmpty()){
			// pr�fen, ob eine der fehlenden Buchungsperioden passt.
			int monat = laufendePeriode.getMonat()-1;
			int jahr = laufendePeriode.getJahr();
			if(monat == 0){
				monat = 12;
				jahr--;
			}
			logger.info("Suche Reparatur-Kandidat f�r Vormonat " + monat + "." + jahr );



			for(Long missingId : fehlendeBuchungsperioden){
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(missingId);
				if(cal.get(Calendar.YEAR) == jahr && cal.get(Calendar.MONTH)+1 == monat){
					logger.info("Gefunden. Id: " + missingId + "( = " + cal.toString());
					return missingId;
				}
			}
			logger.info("nicht gefunden...");
		}
		return null;
	}

	
	/**
	 * Die daten der aktuellen Kasse wurde aktualisiert. Der Kontext kann diese Info weiterleiten.
	 * @return
	 */
	public void kasseAktualisiert(){
		pcSupport.firePropertyChange(PCEVENT_KASSE_CHANGED, null, kasse);
	}
}
