package de.diakon.entities;

import java.util.*;

import de.diakon.persistence.PersistenceException;
import de.vahrst.application.entities.Entity;
import de.vahrst.application.types.*;
import de.vahrst.application.xml.Document;
import de.vahrst.application.xml.Element;

/**
 * Repr�sentiert eine Buchungsperiode. Enh�lt Monat und Jahr dieser Periode
 * sowie die Salden.
 * @author Thomas
 * @version 
 * @file Buchungsperiode.java
 */
public class Buchungsperiode extends Entity {

	// ein paar symbolische Konstanten:
	
	public static final String PCEVENT_SALDEN = "SALDEN";


	// Eigene Stammdaten:
	
	/** Buchungsmonat */
	private int monat;
	
	/** Buchungsjahr */
	private int jahr;
	
	/** Der Anfangssaldo f�r diese Buchungsperiode */
	private Waehrungsbetrag anfangsSaldo;

	/** existiert eine Buchungsperiode im Vormonat */
	private boolean vormonat = false;

	private List<String> fehlermeldungen = null;
	
	
	// Beziehungsdaten:

	/** Die Liste der Buchungss�tze f�r diese Periode */
	private List<Buchungssatz> buchungsliste = null;

	// lokale Daten.
	/** Summe der Einnahmen */
	private Waehrungsbetrag habenSaldo = new Waehrungsbetrag(0);
	
	/** Summe der Ausgaben */
	private Waehrungsbetrag sollSaldo = new Waehrungsbetrag(0);
	
	/** Kassensaldo */
	private Waehrungsbetrag saldo ;
	


	/**
	 * Constructor for Buchungsperiode.
	 */
	public Buchungsperiode() {
		super();
		// pcSupport = new PropertyChangeSupport(this);
	}

	/**
	 * Konstruktor mit Vorgabewerten f�r diese
	 * Buchungsperiode
	 */
	public Buchungsperiode(int monat, int jahr, Waehrungsbetrag anfangssaldo){
		super();
		// upport = new PropertyChangeSupport(this);
		this.monat = monat;
		this.jahr = jahr;
		this.anfangsSaldo = anfangssaldo;
		this.saldo = new Waehrungsbetrag(anfangssaldo);
	}

	/**
	 * liefert Monat und Jahr dieser BuchungsPeriode im 
	 * Format mm/jjjj
	 */
	public String getBuchungsPeriodeFormatiert(){
		return formatMMJJJJ(monat, jahr);
	}

	/**
	 * liefert Monat und Jahr der n�chsten Buchungsperiode im Format
	 * mm/jjjj
	 */
	public String getNaechsteBuchungsPeriodeFormatiert(){
		int locMonat = monat +1;
		int locJahr = jahr;
		if(locMonat > 12){
			locMonat = 1;
			locJahr++;
		}
		return formatMMJJJJ(locMonat, locJahr);		
	}

	/**
	 * liefert Monat und Jahr der vorherigen Buchungsperiode im Format
	 * mm/jjjj oder 'nicht vorhanden", wenn keine Vormonats-Periode existiert
	 */
	public String getVorherigeBuchungsPeriodeFormatiert(){
		if(!hasVormonat())
			return "nicht vorhanden";
		
		int locMonat = monat -1;
		int locJahr = jahr;
		if(locMonat == 0){
			locMonat = 12;
			locJahr--;
		}
		return formatMMJJJJ(locMonat, locJahr);		
	}
	

	private String formatMMJJJJ(int monat, int jahr){
		if(monat == 0 && jahr == 0)
			return "";
			
		StringBuffer sb = new StringBuffer(7);
		if(monat < 10)
			sb.append("0");
		sb.append(monat);
		sb.append("/");
		sb.append(jahr);
		return sb.toString();		
		
	}

	/**
	 * f�gt einen Buchungssatz hinzu. Hier wird nicht geschrieben, das �bernimmt
	 * immer der Kassenkontext.
	 */
	public void addBuchungssatz(Buchungssatz satz) throws PlausibilitaetsException{
		// erstmal Plausibilisierung
		if(satz.getLfdNummer() != buchungsliste.size()+1){
			logger.error("Buchungsnummern nicht konsistent, Aktuelle Anzahl: " + buchungsliste.size() + " neue Buchung mit Nummer: " + satz.getLfdNummer());
			throw new PlausibilitaetsException(new Fehlermeldung(0,"Da stimmt was mit den Buchungsnummern nicht!",null));	
		}
		
		buchungsliste.add(satz);
		
		if(satz.isSoll()){
			sollSaldo.add(satz.getBetrag());
			saldo.minus(satz.getBetrag());
		}else{		
			habenSaldo.add(satz.getBetrag());
			saldo.add(satz.getBetrag());
		}
	}

	/**
	 * ersetzt den vorhandenen BS durch den vorgegebenen Buchungssatz mit
	 * der gleichen Nummer
	 */
	public void replaceBuchungssatz(Buchungssatz satz) throws PlausibilitaetsException{
		int nummer = satz.getLfdNummer();
		if(nummer <1 || nummer > buchungsliste.size()){
			logger.error("Replace einer nicht existierenden Buchungsnummer");
			throw new PlausibilitaetsException(new Fehlermeldung(0,"Da stimmt was mit den Buchungsnummern nicht!",null));	
		}
		Buchungssatz bsalt = buchungsliste.get(nummer-1);
		if(bsalt.getLfdNummer() != satz.getLfdNummer()){
			logger.error("Alte Buchungssatz enthielt falsche Nummer !");
			throw new PlausibilitaetsException(new Fehlermeldung(0,"Da stimmt was mit den Buchungsnummern nicht!",null));	
		}
		
		buchungsliste.set(nummer-1, satz);
		if(!satz.getBetrag().equals(bsalt.getBetrag()) || satz.isSoll() != bsalt.isSoll()){
			updateSalden();
		}
	}

	/**
	 * pr�ft, ob f�r die aktuelle Periode der vorgegebene Buchungstag 
	 * erlaubt ist
	 */
	public void checkBuchungstag(int tag) throws PlausibilitaetsException{
		Calendar cal = new GregorianCalendar(jahr, monat-1, tag);
		if(cal.get(Calendar.MONTH) != monat -1 || cal.get(Calendar.DAY_OF_MONTH) != tag)	{
			Fehlermeldung m = new Fehlermeldung(0, "Buchungstag in diesem Monat nicht g�ltig", Buchungssatz.ASPEKT_TAG);
			throw new PlausibilitaetsException(m);
		}
	}


	/**
	 * aktualisiert die Salden
	 */
	private void updateSalden(){
		// wenn die Buchungsliste noch nicht eingelesen wurde,
		// m�ssen wir hier nichts tun.
		if(buchungsliste == null)
			return;
		
		saldo = new Waehrungsbetrag(anfangsSaldo);
		sollSaldo = new Waehrungsbetrag(0);
		habenSaldo = new Waehrungsbetrag(0);
			
		// Salden neu berechnen
		for(int i=0;i<buchungsliste.size();i++){
			Buchungssatz satz = buchungsliste.get(i);
			if(satz.isSoll()){
				sollSaldo.add(satz.getBetrag());
				saldo.minus(satz.getBetrag());
			}else{
				habenSaldo.add(satz.getBetrag());
				saldo.add(satz.getBetrag());
			}
		}
		// pcSupport.firePropertyChange(PCEVENT_SALDEN, null, saldo);		
	}

	/**
	 * liefert das Sortierfeld f�r die TAbelle: 999999-jjjjmm
	 */
	public int getSortField(){
		return 999999-(jahr*100+monat);
	}

	// ----------------------------------------------------------
	// Getter (und Setter )
	// ----------------------------------------------------------
	/**
	 * Returns the anfangsSaldo.
	 * @return Waehrungsbetrag
	 */
	public Waehrungsbetrag getAnfangsSaldo() {
		return anfangsSaldo;
	}

	/**
	 * Returns the jahr.
	 * @return int
	 */
	public int getJahr() {
		return jahr;
	}

	/**
	 * Returns the monat.
	 * @return int
	 */
	public int getMonat() {
		return monat;
	}

	/** Liefert den SollSaldo 	 */
	public Waehrungsbetrag getSollSaldo(){
		return sollSaldo;
	}

	/** Liefert den HabenSaldo   */
	public Waehrungsbetrag getHabenSaldo(){
		return habenSaldo;
	}
	
	/** Liefert den Aktuellen Saldo */
	public Waehrungsbetrag getSaldo(){
		return saldo;
	}
	
	/**
	 * Liefert die n�chste zu verwendende Buchungsnummer.
	 */
	public int getNaechsteBuchungsNummer(){
	   return buchungsliste.size() + 1;
	}


	/**
	 * Setzt den Anfangssaldo. In diesem Fall m�ssen auch die anderen Salden
	 * neu berechnet werden
	 */
	public void setAnfangssaldo(Waehrungsbetrag anfangsSaldo){
		if(anfangsSaldo != null){
			this.anfangsSaldo = anfangsSaldo;
			updateSalden();
		}
	}

	/**
	 * Liefert die Liste der Buchungss�tze. Lazy Init. Beim Ersten Zugriff
	 * werden die Buchungss�tze aus der Tabelle gelesen.
	 */
	public List<Buchungssatz> getBuchungssaetze()throws PersistenceException{
		if(buchungsliste == null){
			List<Buchungssatz> buchungen = new BuchungssatzHome().getAllBuchungssaetze(this);
			createBuchungsliste(buchungen);

			updateSalden();
		}
		
		return buchungsliste;
	}

	/**
	 * Erzeugt die intern verwaltete Buchungsliste anhand der vorgegebenen
	 * Liste von Buchungen, so wie sie aus der DB kommen. Hintergrund: 
	 * Es besteht die M�glichkeit, dass durch DB-Fehler Buchungen fehlen. F�r diesen
	 * fehlenden Buchungsnummern werden Dummy-Buchungen in die Liste eingetragen.
	 * @param buchungen
	 */
	private void createBuchungsliste(List<Buchungssatz> buchungen){
		// sicherheitshalber die Inputliste sortieren:
		boolean fehlendeSaetze = false;

		Collections.sort(buchungen);
		buchungsliste = new ArrayList<Buchungssatz>();
		Iterator<Buchungssatz> iter = buchungen.iterator();
		while(iter.hasNext()){
			Buchungssatz bs = iter.next();
			// Lfd. Nummern der Buchungen beginnen bei 1. D.h.:
			// Satz 1 => buchungsliste slot 0
			// Satz 2 => buchungsliste slot 1
			if (bs.getLfdNummer() > buchungsliste.size()+1){
				// au, L�cke entdeckt!!
				// mit Dummy-S�tzen auff�llen.
				fehlendeSaetze = true;
				BuchungssatzHome bsHome = new BuchungssatzHome();
				while(buchungsliste.size() < bs.getLfdNummer()-1){
					int fehlendeNummer = buchungsliste.size()+1;
					Buchungssatz dummy = bsHome.createDummyBuchungssatz(fehlendeNummer);
					buchungsliste.add(dummy);
				}
			}
			
			// jetzt doppelten Eintrag pr�fen: buchungsnummer == buchungsliste.size()
			// in diesem Fall wird die buchung verworfen.
			if(bs.getLfdNummer() == buchungsliste.size()){
				logger.error("Doppelte Buchungsnummer entdeckt: " + bs.getLfdNummer());
			}else{
				buchungsliste.add(bs);
			}	
		}
		if(fehlendeSaetze){
			addFehlermeldung("Es wurden fehlende Buchungss�tze erkannt!");
		}
	}

	
	/**
	 * liefert eine XML-Darstellung dieser Buchungsperiode 
	 * @param factory
	 * @return
	 */
	public Element toXML(Document factory){
		Element periodeElement = factory.createElement("Buchungsperiode");
		
		periodeElement.setAttribute("Jahr", Integer.toString(jahr));
		periodeElement.setAttribute("Monat", Integer.toString(monat));
		periodeElement.setAttribute("Anfangssaldo", saldo.formatOhneTausenderPunkt());
		
		return periodeElement;	
	}

	
	
	/**
	 * f�gt die vorgegebene Meldung in die Liste der Fehlermeldungen ein.
	 * @param meldung
	 */
	private void addFehlermeldung(String meldung){
		if(fehlermeldungen == null){
			fehlermeldungen = new ArrayList<String>(2);
		}
		fehlermeldungen.add(meldung);
	}

	/**
	 * gibt an, ob die Buchung mit der vorgegebenen buchungsnummer eine
	 * DummyBuchung ist.
	 * @param buchungsnummer
	 * @return
	 */
	public boolean isDummyBuchung(int buchungsnummer){
		if(buchungsnummer <1 || buchungsnummer > buchungsliste.size()){
			return false;
		}
		Buchungssatz bs = buchungsliste.get(buchungsnummer-1);
		return bs.isDummySatz();
		
	}

	public boolean hasVormonat(){
		return vormonat;
	}
	
	public void setVormonat(boolean arg){
		vormonat = arg;
	}
	
	public boolean hasFehlermeldungen(){
		return fehlermeldungen != null && fehlermeldungen.size() > 0;
	}
	
	public String[] getFehlermeldungen(){
		if(fehlermeldungen == null){
			return new String[0];
		}
		String[] result = new String[fehlermeldungen.size()];
		for(int i=0;i<fehlermeldungen.size();i++){
			result[i] = fehlermeldungen.get(i);
		}
		return result;
	}
}
