package de.diakon.entities;

import java.util.ArrayList;
import java.util.List;

import de.vahrst.application.types.Fehlermeldung;

/**
 * 
 * @author Thomas
 * @version 
 * @file PlausibilitaetsException.java
 */
public class PlausibilitaetsException extends Exception {

	private List<Fehlermeldung> fehlermeldungen = new ArrayList<Fehlermeldung>();

	/**
	 * Constructor for PlausibilitaetsException.
	 */
	public PlausibilitaetsException() {
		super();
	}

	/**
	 * Constructor for PlausibilitaetsException.
	 * @param message
	 */
	public PlausibilitaetsException(Fehlermeldung meldung) {
		super(meldung.getMessage());
		fehlermeldungen.add(meldung);
	}

	/**
	 * Constructor for PlausibilitaetsException.
	 * @param message
	 * @param cause
	 */
	public PlausibilitaetsException(Fehlermeldung meldung, Throwable cause) {
		super(meldung.getMessage(), cause);
		fehlermeldungen.add(meldung);
	}

	/**
	 * Constructor for PlausibilitaetsException.
	 * @param cause
	 */
	public PlausibilitaetsException(Throwable cause) {
		super(cause);
	}


	/** Liefert die Liste aller Fehlermeldungen */
	public List<Fehlermeldung> getMeldungsListe(){
		return fehlermeldungen;
	}
	
	public void addFehlermeldung(Fehlermeldung m){
		fehlermeldungen.add(m);
	}
		
}
