package de.diakon.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import de.diakon.persistence.PersistenceException;
import de.vahrst.application.types.Waehrungsbetrag;
/**
 * Home-Klasse f�r Buchungsperioden
 * @author Thomas
 * @version 
 */
public class BuchungsperiodeHome extends AbstractHome{
	private static Logger logger = Logger.getLogger(BuchungsperiodeHome.class);

	public static String TABLENAME = "BUCHUNGSPERIODEN";
	private static String COL_ID = "id";
	private static String COL_KASSENID = "kasse_ref";
	private static String COL_MONAT = "monat";
	private static String COL_JAHR = "jahr";
	private static String COL_ANFANGSSALDO = "anf_saldo";
	private static String COL_SORTFIELD = "sort";

	private static String insertSQL = null;
	private static String selectSQL = null;
	private static String selectAllForKasseSQL = null;
	// private static String selectAllSQL = null;
	private static String updateSQL = null;

	/**
	 * Constructor for BuchungsperiodeHome
	 */
	public BuchungsperiodeHome() {
		super();
	}

	/** 
	 * Liefert f�r die vorgegebene Kasse und die vorgegeben Periode (monat und
	 * jahr) die Buchungsperiode, falls vorhanden, sonst null. Es werden
	 * keine Buchungss�tze gelesen und keine Salden aktualisiert.
	 */
	private Buchungsperiode getBuchungsperiode(Kasse kasse, int monat, int jahr) throws SQLException{
		Buchungsperiode p = null;
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getSelectSQL());
		stmt.setLong(1, kasse.getObjectId());
		stmt.setInt(2, monat);
		stmt.setInt(3, jahr);

		ResultSet rs = stmt.executeQuery();

		if (rs.next()) {
			p = createBuchungsperiode(rs);
		}
		stmt.close();
		return p;
	}

	/**
	 * ermittelt alle Buchungss�tze, zu denen *keine* Buchungsperiode vorhanden ist und
	 * liefert eine Liste der fehlenden Buchungsperionden-Ids
	 * @return
	 */
	public List<Long> getBuchungsperiodenIdsVonVerwaistenBuchungssaetzen() throws PersistenceException {
		try {
			Connection conn = getConnection();
// Dieses Statement ist extrem langsam:
//			PreparedStatement stmt = conn.prepareStatement("select distinct t1.periode_ref from buchungen t1 where not exists( select t2.id from buchungsperioden t2 where t1.periode_ref = t2.id)");

			// Also machen wir zwei Selects und gleichen im Speicher ab...

			PreparedStatement stmt = conn.prepareStatement("select distinct t1.periode_ref from buchungen t1");
			ResultSet resultSet = stmt.executeQuery();

			Set<Long> periodeRefs = new HashSet<Long>();
			while (resultSet.next()) {
				periodeRefs.add(resultSet.getLong(1));
			}
			resultSet.close();
			stmt.close();

			// jetzt alle Buchungsperionden einlesen:
			stmt = conn.prepareStatement("select distinct id from buchungsperioden");
			resultSet = stmt.executeQuery();
			while(resultSet.next()){
				Long id = resultSet.getLong(1);
				periodeRefs.remove(id);
			}

			// Alle Eintr�ge, die jetzt noch in periodeRefs enthalten sind, geh�ren zu
			// Buchungss�tzen, zu denen die Buchungsperiode fehlt.
			return new ArrayList<Long>(periodeRefs);

		} catch (SQLException sqle) {
			throw new PersistenceException(sqle);
		}
	}

	/**
	 * liefert die aktuelle Buchungsperiode f�r die vorgegebene Kasse
	 */
	public Buchungsperiode getAktuelleBuchungsperiodeForKasse(Kasse kasse)
		throws PersistenceException {
		Buchungsperiode p = null;
		try {
			p = getBuchungsperiode(kasse, kasse.getMonat(), kasse.getJahr());
			
			if(p != null){
				// erst beim Lesen der Buchungss�tze werden die Salden in der 
				// Buchungsperiode p richtig gesetzt:
				p.getBuchungssaetze();
				
				// nachschauen, ob es eine Buchungsperiode im Vormonat gibt.
				setVormonatsFlag(p, kasse);
			}
			return p;

		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
	}

	public void addBuchungsperiode(Buchungsperiode p, Kasse kasse)
		throws PersistenceException {
		try {
			insertIntoTable(p, kasse);
			setVormonatsFlag(p, kasse);
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
	}

	public void changeBuchungsperiode(Buchungsperiode p)
		throws PersistenceException {
		try {
			updateInTable(p);
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
	}

	public void createOrUpdateBuchungsperiode(
		Kasse kasse,
		int monat,
		int jahr,
		Waehrungsbetrag anfSaldo)
		throws PersistenceException {

		try{
			Buchungsperiode p = getBuchungsperiode(kasse, monat, jahr);
			if(p == null){
				p = new Buchungsperiode(monat, jahr, anfSaldo);
				addBuchungsperiode(p, kasse);
			}else{	
				p.setAnfangssaldo(anfSaldo);
				changeBuchungsperiode(p);
			}
			
		}catch(SQLException e){
			throw new PersistenceException(e);
		}

	}

	private void setVormonatsFlag(Buchungsperiode p, Kasse kasse) throws SQLException{
		int monat = p.getMonat()-1;
		int jahr = p.getJahr();
		if(monat == 0){
			monat = 12;
			jahr--;
		}
		
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getSelectSQL());
		stmt.setLong(1, kasse.getObjectId());
		stmt.setInt(2, monat);
		stmt.setInt(3, jahr);

		ResultSet rs = stmt.executeQuery();

		p.setVormonat(rs.next()); // true/false
		stmt.close();
		
	}

	// -------------------------------------------------
	// hier sind die DB-Zugriffe
	// -------------------------------------------------

	/**
	 * aktualisiert die vorgegebene Buchungsperiode
	 */
	private void updateInTable(Buchungsperiode p) throws SQLException {
		logger.debug("update Buchungsperiode");
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getUpdateSQL());
		stmt.setInt(1, p.getAnfangsSaldo().getCent());
		stmt.setLong(2, p.getObjectId());

		stmt.executeUpdate();
		stmt.close();

	}

	/** 
	 * f�gt die vorgegebene Buchungsperiode f�r die zugeh�rige Kasse
	 */
	private void insertIntoTable(Buchungsperiode p, Kasse kasse)
		throws SQLException {
		logger.debug("insert Buchungsperiode");
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getInsertSQL());

		stmt.setLong(1, p.getObjectId());
		stmt.setLong(2, kasse.getObjectId());
		stmt.setInt(3, p.getMonat());
		stmt.setInt(4, p.getJahr());
		stmt.setInt(5, p.getAnfangsSaldo().getCent());
		stmt.setInt(6, p.getSortField());

		logger.debug(stmt.toString());
		stmt.executeUpdate();
		stmt.close();
	}

	/**
	 *  erzeugt aus einem ResultSet-Eintrag ein Objekt vom Typ Buchungsperiode
	 */
	private Buchungsperiode createBuchungsperiode(ResultSet rs)
		throws SQLException {
		long objId = rs.getLong(COL_ID);
		int monat = rs.getInt(COL_MONAT);
		int jahr = rs.getInt(COL_JAHR);
		int cent = rs.getInt(COL_ANFANGSSALDO);

		Buchungsperiode p =
			new Buchungsperiode(monat, jahr, new Waehrungsbetrag(cent));
		p.setObjectId(objId);
		return p;
	}

	/**
	 * Liefert die DDL f�r die Anlage der Buchungsperioden-Tabelle
	 */
	public String getDDL() {
		StringBuffer sb = new StringBuffer();
		sb.append("create table ").append(TABLENAME);
		sb.append(" (").append(COL_ID).append(
			" long unique primary key not null, ");
		sb.append(COL_KASSENID).append(" long not null, ");
		sb.append(COL_MONAT).append(" int not null, ");
		sb.append(COL_JAHR).append(" int not null, ");
		sb.append(COL_ANFANGSSALDO).append(" int not null, ");
		sb.append(COL_SORTFIELD).append(" int not null) ");

		return sb.toString();
	}

	/**
	 * liefert den SQL-String f�r insert
	 */
	private String getInsertSQL() {
		if (insertSQL == null) {
			StringBuffer sb = new StringBuffer();
			sb.append("insert into ").append(TABLENAME);
			sb.append(" values(?,?,?,?,?,?)");
			insertSQL = sb.toString();

		}
		return insertSQL;
	}

//	/**
//	 * liefert den SQL-String f�r select der Buchungsperioden einer Kasse
//	 * @return String die SQL f�r das Select-Statement
//	 */
//	private String getSelectAllSQL() {
//		if (selectSQL == null) {
//			selectSQL =
//				"select * from "
//					+ TABLENAME
//					+ " where "
//					+ COL_KASSENID
//					+ " = ? order by "
//					+ COL_SORTFIELD;
//		}
//		return selectSQL;
//	}

	/**
	 * liefert den SQL-String f�r select einer Buchungsperioden einer Kasse
	 * @return String die SQL f�r das Select-Statement
	 */
	private String getSelectSQL() {
		if (selectSQL == null) {
			selectSQL =
				"select * from "
					+ TABLENAME
					+ " where "
					+ COL_KASSENID
					+ "=? and "
					+ COL_MONAT
					+ "=? and "
					+ COL_JAHR
					+ "=?";
		}
		return selectSQL;
	}

	/**
	 * liefert den SQL-String f�r select aller Buchungsperioden einer Kasse
	 * @return String die SQL f�r das Select-Statement
	 */
	private String getSelectAllForKasseSQL() {
		if (selectAllForKasseSQL == null) {
			selectAllForKasseSQL =
				"select * from " 
					+ TABLENAME
					+ " where "
					+ COL_KASSENID
					+ "=? ORDER BY " + COL_SORTFIELD;
		}
		return selectAllForKasseSQL;
	}



	/**
	 * liefert den SQL-STring f�r update
	 */
	private String getUpdateSQL() {
		if (updateSQL == null) {
			StringBuffer sb = new StringBuffer();
			sb.append("update ").append(TABLENAME).append(" set ");
			sb.append(COL_ANFANGSSALDO).append("=? ");
			sb.append("where ").append(COL_ID).append("=?");
			updateSQL = sb.toString();
		}
		return updateSQL;
	}

	
	/**
	 * liefert eine Liste alle Buchungsperioden f�r die vorgegebene Kasse, absteigend sortiert.
	 * @param kasse
	 * @return
	 */
	public List<Buchungsperiode> getAllBuchungsperiodenForKasse(Kasse kasse) throws PersistenceException{
		List<Buchungsperiode> liste = new ArrayList<Buchungsperiode>();
		try{
			Connection conn = getConnection();
			PreparedStatement stmt = conn.prepareStatement(getSelectAllForKasseSQL());
			stmt.setLong(1, kasse.getObjectId());
	
			ResultSet rs = stmt.executeQuery();
	
			while(rs.next()){
				Buchungsperiode p = createBuchungsperiode(rs);
				liste.add(p);
			}
			stmt.close();
		}catch(SQLException sqle){
			throw new PersistenceException(sqle);
		}
		return liste;
	}

	/**
	 * erzeugt einen Buchungsperiode-Satz mit den vorgegebenen Daten.
	 * @param buchungssaldoId
	 * @param korrekturmonat
	 * @param korrekturjahr
	 * @param anfangssaldo
	 * @param kasseId
	 */
	public void repairBuchungsperiode(Long buchungssaldoId, int korrekturmonat, int korrekturjahr, Waehrungsbetrag anfangssaldo, long kasseId) throws SQLException{
		Connection conn = getConnection();
		PreparedStatement stmt = conn.prepareStatement(getInsertSQL());

		stmt.setLong(1, buchungssaldoId);
		stmt.setLong(2, kasseId);
		stmt.setInt(3, korrekturmonat);
		stmt.setInt(4, korrekturjahr);
		stmt.setInt(5, anfangssaldo.getCent());

		int sort = 999999-(korrekturjahr*100+korrekturmonat);
		stmt.setInt(6, sort);
		logger.debug(stmt.toString());
		stmt.executeUpdate();
		stmt.close();


	}
}
