package de.diakon.entities;

import de.vahrst.application.entities.Entity;
import de.vahrst.application.xml.Document;
import de.vahrst.application.xml.Element;

/**
 * Beschreibt eine Kasse mit Id und Namen
 * @author Thomas
 * @version 
 */
public class Kasse extends Entity {

	
	/** der Name dieser Kasse */
	private String name;
	
	/** aktuelle Monat */
	private int monat;
	
	/** aktuelles Jahr */
	private int jahr;

	
	private int kassenkonto;
	private int buchungskreis;
	private String mandant = "";
	
	
	/**
	 * Constructor for Kasse.
	 */
	public Kasse() {
		super();
	}
	
	/**
	 * Konstruktor mit Vorgabewerten 
	 */
	public Kasse(String name, int monat, int jahr, int kassenkonto, int buchungskreis, String mandant){
		super();
		this.name = name;
		this.monat = monat;
		this.jahr = jahr;
		this.kassenkonto = kassenkonto;
		this.buchungskreis = buchungskreis;
		this.mandant = mandant;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer(40);
		sb.append("Kasse [").append(name).append("] [");
		sb.append(monat).append(".").append(jahr).append("]");
		return sb.toString();	
	}

	/**
	 * liefert eine XML-Darstellung dieser Kasse-Entity
	 * @param factory
	 * @return
	 */
	public Element toXML(Document factory){
		Element kasseElement = factory.createElement("Kasse");
		
		kasseElement.setAttribute("Name", name);
		kasseElement.setAttribute("Jahr", Integer.toString(jahr));
		kasseElement.setAttribute("Monat", Integer.toString(monat));
		kasseElement.setAttribute("Kassenkonto", Integer.toString(kassenkonto));
		kasseElement.setAttribute("Buchungskreis", Integer.toString(buchungskreis));
		kasseElement.setAttribute("Mandant",  mandant);
		
		return kasseElement;	
	}
	
	
	
	// ------------------------------------------------------------
	// Getter und Setter 
	// -------------------------------------------------------------



	/**
	 * Returns the name.
	 * @return String
	 */
	public String getName() {
		return name;
	}


	/**
	 * Sets the name.
	 * @param name The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the jahr.
	 * @return int
	 */
	public int getJahr() {
		return jahr;
	}

	/**
	 * Returns the monat.
	 * @return int
	 */
	public int getMonat() {
		return monat;
	}

	/**
	 * Sets the jahr.
	 * @param jahr The jahr to set
	 */
	public void setJahr(int jahr) {
		this.jahr = jahr;
	}

	/**
	 * Sets the monat.
	 * @param monat The monat to set
	 */
	public void setMonat(int monat) {
		this.monat = monat;
	}

	public int getKassenkonto() {
		return kassenkonto;
	}
	
	public void setKassenkonto(int kassenkonto) {
		this.kassenkonto = kassenkonto;
	}
	
	public int getBuchungskreis() {
		return buchungskreis;
	}
	public void setBuchungskreis(int buchungskreis) {
		this.buchungskreis = buchungskreis;
	}
	public String getMandant() {
		return mandant;
	}
	public void setMandant(String mandant) {
		this.mandant = mandant;
	}
	
}
