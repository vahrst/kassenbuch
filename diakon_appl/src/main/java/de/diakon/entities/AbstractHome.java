package de.diakon.entities;

import java.sql.Connection;
import java.sql.SQLException;

import de.diakon.persistence.PersistenceManager;

/**
 * abstrakte Oberklasse f�r Home-Klassen.
 * @author Thomas und Andrea
 *
 */
public class AbstractHome {

	/**
	 * liefert eine Connection vom PersistenceManager
	 * @return
	 */
	protected Connection getConnection() throws SQLException{
		return PersistenceManager.getPersistenceManager().getConnection();		
	}
}
