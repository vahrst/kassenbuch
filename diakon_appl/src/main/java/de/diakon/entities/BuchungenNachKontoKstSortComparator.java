package de.diakon.entities;

import java.util.Comparator;

import org.apache.log4j.Logger;
/**
 * Comparator, um Buchungssätze nach Konto und Kostenstelle 
 * zu sortieren.
 * @author Thomas
 * @version 
 */
public class BuchungenNachKontoKstSortComparator implements Comparator<Buchungssatz> {
	public static Logger logger = Logger.getLogger(BuchungenNachKontoSortComparator.class);

	/**
	 * Constructor for BuchungenNachKontoSortComparator.
	 */
	public BuchungenNachKontoKstSortComparator() {
		super();
	}

	/**
	 * @see java.util.Comparator#compare(Object, Object)
	 */
	public int compare(Buchungssatz b1, Buchungssatz b2) {

		
		int kto1 = b1.getKontonummer();
		int kto2 = b2.getKontonummer();

		int kst1 = b1.getKostenstellenNummer();
		int kst2 = b2.getKostenstellenNummer();

		int tag1 = b1.getTag();
		int tag2 = b2.getTag();
		
		if(kto1 != kto2)
			return  kto1 - kto2;

		if(kst1 != kst2)
			return kst1 - kst2;

		if(tag1 != tag2)
			return tag1 - tag2;
			
		return b1.getLfdNummer() - b2.getLfdNummer();			
		
	}

}
