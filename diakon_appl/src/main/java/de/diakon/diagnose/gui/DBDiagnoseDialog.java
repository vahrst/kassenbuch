package de.diakon.diagnose.gui;
import java.awt.*;

import com.lutris.instantdb.SQLBuilder.SQLBuilder;
import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file DBDiagnoseDialog.java
 */
public class DBDiagnoseDialog extends AbstractDialog {

	/**
	 * Constructor for DBDiagnoseDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public DBDiagnoseDialog(
		Frame owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init();
	}

	/**
	 * Constructor for DBDiagnoseDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public DBDiagnoseDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, title, modal, controller);
		
		init();
	}

	/**
	 * Constructor for DBDiagnoseDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public DBDiagnoseDialog(
		Dialog owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init();
	}


	private void init(){
		SQLBuilder sqlBuilderPanel = new SQLBuilder();

		this.setContentPane(sqlBuilderPanel.getContentPane());
		
		this.setSize(640,520);
		this.center();
	}

}
