package de.diakon.diagnose.prozesse;

import de.diakon.diagnose.gui.DBDiagnoseDialog;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file DBDiagnoseController.java
 */
public class DBDiagnoseController extends AbstractController {
	private DBDiagnoseProcess process;
	private DBDiagnoseDialog dialog;

	/**
	 * Constructor for DBDiagnoseController.
	 */
	public DBDiagnoseController(DBDiagnoseProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		dialog = new DBDiagnoseDialog(ApplicationContext.getMainApplicationWindow(), "SQLBuilder", true, this);
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#handleViewClosedEvent(Object)
	 */
	public void handleViewClosedEvent(Object source) {
		process.beenden();
	}

}
