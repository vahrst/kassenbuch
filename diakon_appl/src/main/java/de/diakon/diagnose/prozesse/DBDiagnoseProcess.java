package de.diakon.diagnose.prozesse;

import java.sql.SQLException;

import de.diakon.persistence.PersistenceManager;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.EndTransactionHandler;

/**
 * 
 * @author Thomas
 * @version 
 * @file DBDiagnoseProcess.java
 */
public class DBDiagnoseProcess extends AbstractProcess {
	private DBDiagnoseController controller;

	/**
	 * Constructor for DBDiagnoseProcess.
	 * @param parent
	 * @param eth
	 */
	public DBDiagnoseProcess(
		AbstractProcess parent,
		EndTransactionHandler eth) {
		super(parent, eth);
		
		
		PersistenceManager.getPersistenceManager().close();
		
		controller = new DBDiagnoseController(this);
		controller.start();
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	
	public void beenden(){
		logger.info("beende DBDiagnose");
		try{
			PersistenceManager.getPersistenceManager().getConnection();
		}catch(SQLException e){
			logger.warn("Fehler beim erneuten �ffnen einer Connection");
		}
		rollback();	
	}

}
