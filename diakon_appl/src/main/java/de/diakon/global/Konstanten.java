package de.diakon.global;

/**
 * Symbolische Konstanten 
 * @author Thomas
 * @version 
 */
public interface Konstanten {
	public static final String MENU_FILE = "MENU_FILE";
	public static final String MENU_HELP = "MENU_HELP";
	
	public static final String ACTION_BEENDEN = "ACT_BEENDEN";
	public static final String ACTION_PROPERTIES = "ACT_PROPERTIES";
	public static final String ACTION_DATENSICHERUNG = "ACT_DATENSICHERUNG";

	public static final String ACTION_BUCHEN = "ACT_BUCHEN";
	public static final String ACTION_UEBERSICHT = "ACT_UEBERSICHT";

	public static final String ACTION_KASSENPFLEGE = "ACT_KASSE_PFLEGEN";
	public static final String ACTION_MONATSABSCHLUSS = "ACT_MONATSABSCHLUSS";
	public static final String ACTION_VORMONAT = "ACT_VORMONAT";
	public static final String ACTION_REPAIR_VORMONAT = "ACT_REPAIR_VORMONAT";
	public static final String ACTION_DRUCK_KASSENBUCH = "ACT_DRUCK_KASSENBUCH";
	public static final String ACTION_DRUCK_DEBITOREN = "ACT_DRUCK_DEBITOREN";
	public static final String ACTION_DRUCK_SUMMENLISTE = "ACT_DRUCK_SUMMENLISTE";
	public static final String ACTION_EXPORT_BUCHUNGEN = "ACT_EXPORT_BUCHUNGEN";
	public static final String ACTION_EXPORT_DB = "ACT_EXPORT_DB";
	
	public static final String ACTION_KASSEWECHSEL = "ACT_KASSE_WECHSEL";
	public static final String ACTION_KASSEANLEGEN = "ACT_KASSE_ANLEGEN";

	public static final String ACTION_HILFE = "ACT_HILFE";
	public static final String ACTION_ABOUT = "ACT_ABOUT";
	public static final String ACTION_DIAGNOSE = "ACT_DIAGNOSE";

	public static final String ACTION_KOSTENSTELLEN = "ACT_KOSTENSTELLEN";
	public static final String ACTION_KONTEN = "ACT_KONTEN";
	public static final String ACTION_EXPORT_STAMMDATEN = "ACT_EXPORT_STAMMDATEN";

	public static final String ACTION_TEST1 = "ACT_TEST1";
	public static final String ACTION_TEST2= "ACT_TEST2";
	
	
	public static final String CLIENT_AREA_FORM = "formular";
	
	
	
	// Konstanten f�r die Properties
	public static final String PROPS_DB_FILE = "DatenbankSteuerDatei";
	public static final String PROPS_BACKUP_DIR = "Sicherungsverzeichnis";
	public static final String PROPS_BACKUP_GENCOUNT = "Anzahl_Sicherungskopien";
	public static final String PROPS_AUTO_BACKUP = "Automatisches_Backup";
	public static final String PROPS_LAST_KASSE_ID = "LetzteKasse_Id";
	public static final String PROPS_KONTOLISTE = "Automatische_Kontoliste";
	public static final String PROPS_KLAENGE = "Klaenge";
	public static final String PROPS_CSV_EXPORT_CHARSET = "CSV_Export_Charset";
	public static final String PROPS_SCREEN_X = "Screen_X";
	public static final String PROPS_SCREEN_Y = "Screen_Y";
	public static final String PROPS_EXPORT_DIR = "Exportverzeichnis";

}
