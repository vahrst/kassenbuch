package de.diakon.gui;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

import de.vahrst.application.gui.*;
import de.vahrst.application.prozesse.AbstractController;

/**
 * Oberklasse f�r Stammdaten-Pflegepanels
 * @author Thomas
 * @version 
 */
public abstract class StammdatenPflegePanel extends FormPanel {
	protected VaButton btOk;
	protected VaButton btAbbrechen;

	protected AbstractController controller;

	private StammdatenPflegeMainPanel pflegeMainPanel;

	/**
	 * Constructor for StammdatenPflegePanel.
	 */
	public StammdatenPflegePanel(AbstractController controller, String title, Object model, boolean neuanlage) {
		super();
		this.controller = controller;
		setPaintHeaderArea(false);
		initialize(title, model, neuanlage);
	}


	private void initialize(String title, Object model, boolean neuanlage){
		this.setLayout(new BorderLayout());
		this.setTitle(title);
		
		this.add(createButtonPanel(controller), BorderLayout.SOUTH);

		pflegeMainPanel = createPflegeMainPanel(model, neuanlage);

		this.add(pflegeMainPanel, BorderLayout.CENTER);		
		
	}
	
	/**
	 * erzeugt das Button-Panel
	 */
	protected JPanel createButtonPanel(ActionListener controller){
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
//		buttonPanel.setBackground(ColorKonstanten.COLOR_BUTTONPANEL);
		buttonPanel.setMinimumSize(new Dimension(10, 24));
		buttonPanel.setPreferredSize(new Dimension(10, 24));
//		buttonPanel.setBorder(new EmptyBorder(3,0,0,0));			
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		btOk = new VaButton("Ok");
		btOk.setActionCommand("Ok");
		btOk.addActionListener(controller);
		buttonPanel.add(btOk);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btAbbrechen = new VaButton("Abbrechen");
		btAbbrechen.setActionCommand("Abbrechen");
		btAbbrechen.addActionListener(controller);
		buttonPanel.add(btAbbrechen);


		return buttonPanel;		
	}
	
	public Object getModel(){
		return pflegeMainPanel.getModel();
	}
	
	protected abstract StammdatenPflegeMainPanel createPflegeMainPanel(Object model, boolean neuanlage);
	
	/**
	 * liefert das PflegeMainPanel
	 */
	public StammdatenPflegeMainPanel getPflegeMainPanel(){
		return pflegeMainPanel;
	}
}
