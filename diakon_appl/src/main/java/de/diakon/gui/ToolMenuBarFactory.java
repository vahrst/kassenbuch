package de.diakon.gui;

import javax.swing.*;

/**
 * Factoryklasse mit statischer Methode f�r die Bereitstellung
 * der Toolbar und der Men�bar
 * @author Thomas
 * @version 
 * @file ToolbarFactory.java
 */
public class ToolMenuBarFactory {

	/**
	 * Constructor for ToolbarFactory.
	 */
	public ToolMenuBarFactory() {
		super();
	}


	/**
	 * liefert die Toolbar
	 * @return JToolBar
	 */
	public static JToolBar getToolBar(){
		
		return null;
	}
	
	/**
	 * liefert das Anwendungsmenu
	 * @return JMenuBar
	 */
	public static JMenuBar getMenuBar(){
		return null;
	}
}
