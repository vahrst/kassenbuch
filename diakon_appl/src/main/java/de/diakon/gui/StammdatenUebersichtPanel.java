package de.diakon.gui;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;

import de.vahrst.application.gui.*;
import de.vahrst.application.prozesse.AbstractController;
/**
 * 
 * @author Thomas
 * @version 
 * @file StammdatenUebersichtPanel.java
 */
public abstract class StammdatenUebersichtPanel extends FormPanel implements ListSelectionListener {
	private static Logger logger = Logger.getLogger(StammdatenUebersichtPanel.class);

	private StammdatenUebersichtTablePanel tablePanel = null;

	protected VaButton btGeheZu;
	protected VaButton btNeu;
	protected VaButton btDelete;
	protected VaButton btImport;

	/**
	 * Constructor for StammdatenUebersichtPanel.
	 */
	public  StammdatenUebersichtPanel(TableModel tm, AbstractController controller) {
		super();
		initialize(tm, controller);
	}

	private void initialize(TableModel tm, AbstractController controller) {
		this.setLayout(new BorderLayout());
		this.setBackground(ColorKonstanten.COLOR_FORMPANEL_BG);

		this.setTitle(getHeaderTitle());
//		this.add(getHeaderPanel(), BorderLayout.NORTH);


		JPanel buttonPanel = createButtonPanel(controller);
		this.add(buttonPanel, BorderLayout.SOUTH);

		
		tablePanel = getTablePanel(tm, controller);
		tablePanel.addSelectionListener(this);
		this.add(tablePanel, BorderLayout.CENTER);

		boolean rowSelected = getSelectedRow() != -1;
		setButtonState(rowSelected);		
	}


	/**
	 * Notwendige Methode f�r ListSelectionListener
	 */
	public void valueChanged(ListSelectionEvent e){
		ListSelectionModel lsm = (ListSelectionModel) e.getSource();
		boolean rowSelected = !lsm.isSelectionEmpty();
		setButtonState(rowSelected);		
	}


	public int getSelectedRow(){
		return tablePanel.getSelectedRow();
	}


	public void setEntityListe(TableModel tm){
		tablePanel.setTableModel(tm);
	}



	protected void setButtonState(boolean rowSelected){
		if(btGeheZu != null)
			btGeheZu.setEnabled(rowSelected);
			
		if(btDelete != null)
			btDelete.setEnabled(rowSelected);
	}

	protected JPanel createButtonPanel(AbstractController controller){
		logger.debug("createButtonPanel");
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
//		buttonPanel.setBackground(ColorKonstanten.COLOR_BUTTONPANEL);
		buttonPanel.setMinimumSize(new Dimension(10, 24));
		buttonPanel.setPreferredSize(new Dimension(10, 24));
//		buttonPanel.setBorder(new EmptyBorder(3,0,0,0));			
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		btGeheZu = new VaButton("GeheZu");
		btGeheZu.setActionCommand("GeheZu");
		btGeheZu.addActionListener(controller);
		buttonPanel.add(btGeheZu);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btNeu = new VaButton("Neu");
		btNeu.setActionCommand("Neu");
		btNeu.addActionListener(controller);
		buttonPanel.add(btNeu);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btDelete = new VaButton("L�schen");
		btDelete.setActionCommand("Delete");
		btDelete.addActionListener(controller);
		buttonPanel.add(btDelete);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btImport = new VaButton("Import");
		btImport.setActionCommand("Import");
		btImport.addActionListener(controller);
		buttonPanel.add(btImport);

		return buttonPanel;		
	}


/*
	protected JPanel getHeaderPanel(){
		JPanel headerPanel = new JPanel();
		headerPanel.setBackground(ColorKonstanten.COLOR_HEADER);
		headerPanel.setMinimumSize(new Dimension(10, 25));
		headerPanel.setPreferredSize(new Dimension(10, 25));
		headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.X_AXIS));

		JLabel headerText = new JLabel(getTitle());
		headerText.setFont(new Font(null, Font.BOLD, 12));
		headerText.setOpaque(false);
		headerPanel.add(headerText);
		return headerPanel;
	}
*/
	protected abstract StammdatenUebersichtTablePanel getTablePanel(TableModel tm, ActionListener controller);

	protected abstract String getHeaderTitle();
}
