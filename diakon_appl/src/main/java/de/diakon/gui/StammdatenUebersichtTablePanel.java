package de.diakon.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import de.vahrst.application.gui.ColorKonstanten;
import de.vahrst.application.gui.Table;

/**
 * Oberklassen f�r Panels mit der Stammdaten-�bersicht-Tabelle.
 * @author Thomas
 * @version 
 */
public abstract class StammdatenUebersichtTablePanel extends JPanel {
	// private static Logger logger = Logger.getLogger(StammdatenUebersichtTablePanel.class);


	// private TableModel tableModel = null;
	protected Table table = null;

	/**
	 * Constructor for TestTablePanel.
	 */
	public StammdatenUebersichtTablePanel(TableModel tm, ActionListener controller) {
		super();
		initialize(tm, controller);
	}


	public int getSelectedRow(){
		return table.getSelectedRow();
	}

	public void addSelectionListener(ListSelectionListener l){
		table.getSelectionModel().addListSelectionListener(l);
	}

	public void setTableModel(TableModel tm){
		table.setModel(tm);
	}

	private void initialize(TableModel tableModel, ActionListener controller) {
		setLayout(new BorderLayout());

		TableColumnModel tcm = getTableColumnModel();
		table = new Table(tableModel, tcm);
		table.addActionListener(controller);
		
		table.getColumnModel().setColumnSelectionAllowed(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JScrollPane scpane = new JScrollPane(table);
		scpane.getViewport().setBackground(ColorKonstanten.COLOR_FORMPANEL_BG);

		this.add(scpane, BorderLayout.CENTER);

				
	}

	protected abstract TableColumnModel getTableColumnModel();
	
	
}
