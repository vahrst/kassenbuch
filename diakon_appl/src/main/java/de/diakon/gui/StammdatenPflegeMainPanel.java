package de.diakon.gui;

import javax.swing.JPanel;

/**
 * 
 * @author Thomas
 * @version 
 */
public abstract class StammdatenPflegeMainPanel extends JPanel {

	protected boolean neuAnlageModus = false;
	
	/**
	 * Constructor for StammdatenPflegeMainPanel
	 */
	public StammdatenPflegeMainPanel(boolean neuanlage) {
		super();
		this.neuAnlageModus = neuanlage;
		this.setOpaque(false);
	}
	
	
	/**
	 * liefert das Daten-Modell mit den aktuellen Werten
	 * der Eingabemaske zur�ck.
	 */
	public abstract Object getModel();

}
