package de.diakon.abschluss.prozesse;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Repr�sentiert die Daten einer Druckzeile f�r den Ausdruck der Kassenliste.
 * @author thomas
 *
 */
public class BuchungssatzKassenbuchdruck {

	private Date datum;
	private int lfdnr;
	private int konto;
	private int kst;
	
	private BigDecimal haben;
	private BigDecimal soll;
	
	private String kontobez;
	private String zusatztext;
	
	public BuchungssatzKassenbuchdruck(Date datum, int lfdnr, int konto, int kst, BigDecimal haben, BigDecimal soll, String kontobez, String zusatztext){
		super();
		this.datum = datum;
		this.lfdnr = lfdnr;
		this.konto = konto;
		this.kst = kst;
		this.haben = haben;
		this.soll = soll;
		this.kontobez = kontobez;
		this.zusatztext = zusatztext;
	}

	public Date getDatum() {
		return datum;
	}
	public int getLfdnr() {
		return lfdnr;
	}
	public int getKonto() {
		return konto;
	}
	public int getKst() {
		return kst;
	}
	public BigDecimal getHaben() {
		return haben;
	}
	public BigDecimal getSoll() {
		return soll;
	}
	public String getKontobez() {
		return kontobez;
	}
	public String getZusatztext() {
		return zusatztext;
	}
	
	public BigDecimal getSaldo(){
		if(haben != null){
			return haben;
		}else{
			return soll.negate();
		}
	}
	
	public void setSaldo(BigDecimal saldo){
		if(saldo.longValue() >= 0 ){
			haben = saldo;
		}else{
			soll = saldo.negate();
		}
	}
	
}
