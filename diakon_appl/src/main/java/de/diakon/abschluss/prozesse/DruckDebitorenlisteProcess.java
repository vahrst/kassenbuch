package de.diakon.abschluss.prozesse;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import de.diakon.entities.BuchungenNachKontoSortComparator;
import de.diakon.entities.Buchungsperiode;
import de.diakon.entities.Buchungssatz;
import de.diakon.entities.KassenKontext;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.EndTransactionHandler;

/**
 * 
 * @author Thomas
 * @version
 * @file DruckKassenbuchProcess.java
 */
public class DruckDebitorenlisteProcess extends AbstractProcess {
	private DruckDebitorenlisteController controller;
	private KassenKontext kontext;

	/**
	 * Constructor for DruckKassenbuchProcess.
	 * 
	 * @param parent
	 * @param eth
	 */
	public DruckDebitorenlisteProcess(AbstractProcess parent,
			EndTransactionHandler eth, KassenKontext kontext) {
		super(parent, eth);

		this.kontext = kontext;
		openController();

	}

	private void openController() {
		controller = new DruckDebitorenlisteController(this);
		controller.start();
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	/**
	 * Startet den Ausdruck f�r den vorgegebenen Debitoren-Bereich. Wenn vorschau = true ist,
	 * wird kein Druck veranlasst, sondern ein Vorschaufenster ge�ffnet.
	 * @param vondeb
	 * @param bisdeb
	 * @param vorschau
	 * @throws PersistenceException
	 */
	public void startDruck(int vondeb, int bisdeb, boolean vorschau) throws PersistenceException {
		// wir erzeugen uns erstmal eine Kopie der Buchungsliste, nur mit den
		// Debitoren-Buchungen:
		List<BuchungssatzKassenbuchdruck> druckdaten = getDruckdaten(vondeb, bisdeb);

		if (druckdaten.isEmpty()) {
			controller
					.showErrorMessage("Im vorgegebenen Kontobereich gibt es keine Debitoren-Buchungen");
			commit();
			return;
		}

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("buchungsmonat", kontext.getAktuelleBuchungsperiodeFormatiert());
		parameters.put("kassenbuch", kontext.getKasse().getName());
		
		try{

			JRDataSource dataSource = new JRBeanCollectionDataSource(druckdaten); 
			
			InputStream in = this.getClass().getResourceAsStream("/reports/debitoren.jrxml");
			JasperReport report = JasperCompileManager.compileReport(in);

			JasperPrint filledReport = JasperFillManager.fillReport(report, parameters, dataSource);
			
			if(vorschau){
				JasperViewer.viewReport(filledReport, false);				
			}else{
				JasperPrintManager.printReport(filledReport, true);
			}
		
		}catch(Exception e){
			logger.error("Fehler bei der Druckaufbereitung der Buchungss�tze", e);
			controller.showErrorMessage("Fehler bei der Druckaufbereitung der Buchungss�tze");
		}

		commit();
	}

	/**
	 * lievert eine eine Liste von BuchungssatzKassenbuchdruck Instanzen,
	 * gefiltert nach Debitoren, eingegrenzt auf die vorgegebenen von- und
	 * bis-Werte, sortiert nach Kontonummer und Lfd. Nummer.
	 * 
	 * @return
	 * @throws PersistenceException
	 */
	private List<BuchungssatzKassenbuchdruck> getDruckdaten(int vondeb, int bisdeb)
			throws PersistenceException {
		List<BuchungssatzKassenbuchdruck> resultList = new ArrayList<BuchungssatzKassenbuchdruck>();

		Buchungsperiode p = kontext.getAktuelleBuchungsperiode();
		int monat = p.getMonat();
		int jahr = p.getJahr();

		GregorianCalendar cal = new GregorianCalendar();
		List<Buchungssatz> buchungsliste = p.getBuchungssaetze();

		for (Buchungssatz bs : buchungsliste) {
			if (bs.getKonto().isDebitor() && bs.getKontonummer() >= vondeb
					&& bs.getKontonummer() <= bisdeb) {

				cal.set(jahr, monat - 1, bs.getTag());
				Date datum = cal.getTime();
				BigDecimal soll = null;
				BigDecimal haben = null;
				BigDecimal betrag = BigDecimal.valueOf(bs.getBetrag().getCent(), 2);
				if (bs.isSoll()) {
					soll = betrag;
				} else {
					haben = betrag;
				}

				BuchungssatzKassenbuchdruck drucksatz = new BuchungssatzKassenbuchdruck(
						datum, bs.getLfdNummer(), bs.getKontonummer(),
						bs.getKostenstellenNummer(), haben, soll, bs.getKontoBezeichnung(),
						bs.getZusatztext());

				resultList.add(drucksatz);
			}
		}

		// danach sortieren wir die Liste nach Konto-Nummer und laufende Nummer
		Collections.sort(resultList, new BuchungenNachKontoSortComparator());

		return resultList;
	}

}
