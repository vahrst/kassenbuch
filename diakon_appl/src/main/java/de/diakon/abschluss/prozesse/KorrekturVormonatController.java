package de.diakon.abschluss.prozesse;

import de.diakon.abschluss.gui.KorrekturVormonatDialog;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file KorrekturVormonatController.java
 */
public class KorrekturVormonatController extends AbstractController {
	private KorrekturVormonatProcess process;
	private KorrekturVormonatDialog dialog;

	/**
	 * Constructor for KorrekturVormonatController.
	 */
	public KorrekturVormonatController(KorrekturVormonatProcess p) {
		super();
		this.process = p;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		MonatsabschlussDaten daten = process.getDaten();
		dialog = new KorrekturVormonatDialog(ApplicationContext.getMainApplicationWindow(),true, this, daten);
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

	// Die EventHandler
	public void handleStartEvent(){
		try{
			process.setzeAufVormonat();
		}catch(PersistenceException e){
			showErrorMessage("Beim Zurücksetzen ist ein Fehler aufgetreten");
			process.rollback();
		}
	}
	
	public void handleAbbrechenEvent(){
		process.rollback();
	}

}
