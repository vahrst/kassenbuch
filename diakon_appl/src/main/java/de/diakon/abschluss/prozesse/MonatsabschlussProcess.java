package de.diakon.abschluss.prozesse;

import de.diakon.entities.*;
import de.diakon.persistence.*;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file MonatsabschlussProcess.java
 */
public class MonatsabschlussProcess extends AbstractProcess {
	private MonatsabschlussController controller;
	
	private KassenKontext kontext;
	
	/**
	 * Constructor for MonatsabschlussProcess.
	 * @param parent
	 * @param eth
	 */
	public MonatsabschlussProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		KassenKontext kontext) {
		super(parent, eth);
		
		this.kontext = kontext;
		openController();
	}

	private void openController(){
		controller = new MonatsabschlussController(this);
		controller.start();	
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	
	public MonatsabschlussDaten getDaten(){
		MonatsabschlussDaten daten = new MonatsabschlussDaten();
		daten.kassenname = kontext.getKasse().getName();
		daten.monatAlt = kontext.getAktuelleBuchungsperiodeFormatiert();
		daten.monatNeu = kontext.getNaechsteBuchungsperiodeFormatiert();
		
		return daten;
	}


	/**
	 * jetzt wird der Monatsabschluss durchgeführt.
	 */
	public void monatsabschluss() throws PersistenceException{

		try{
			PersistenceManager.getPersistenceManager().startTransaction();	
			Kasse kasse = kontext.getKasse();
			Buchungsperiode aktPeriode = kontext.getAktuelleBuchungsperiode();
				
			int monat = aktPeriode.getMonat();
			int jahr = aktPeriode.getJahr();
				
			monat++;
			if(monat > 12){
				monat = 1;
				jahr++;
			}

			BuchungsperiodeHome pHome = new BuchungsperiodeHome();
			pHome.createOrUpdateBuchungsperiode(kasse, monat, jahr, aktPeriode.getSaldo());
				
			kasse.setJahr(jahr);
			kasse.setMonat(monat);
			new KasseHome().changeKasse(kasse);

			PersistenceManager.getPersistenceManager().commitTransaction();			

			kontext.setKasse(kasse);
			log.info("Monatsabschluss durchgeführt: " + kasse.toString());
			commit();
		}catch(PersistenceException e){
			logger.error("Fehler in Monatsabschluss", e);
			PersistenceManager.getPersistenceManager().rollbackTransaction();
			throw e;
		}		
		
	}
}
