package de.diakon.abschluss.prozesse;

import de.diakon.abschluss.gui.KorrekturVormonatDialog;
import de.diakon.abschluss.gui.ReparaturVormonatSaldenDialog;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;

/**
 * 
 * @author Thomas
 * @version 
 * @file KorrekturVormonatController.java
 */
public class ReparaturVormonatSaldenController extends AbstractController {
	private ReparaturVormonatSaldenProcess process;
	private ReparaturVormonatSaldenDialog dialog;

	/**
	 * Constructor for KorrekturVormonatController.
	 */
	public ReparaturVormonatSaldenController(ReparaturVormonatSaldenProcess p) {
		super();
		this.process = p;
	}

	/**
	 * @see AbstractController#start()
	 */
	public void start() {
		ReparaturVormonatssaldenDaten daten = process.getDaten();
		dialog = new ReparaturVormonatSaldenDialog(ApplicationContext.getMainApplicationWindow(),true, this, daten);
		dialog.setVisible(true);
	}

	/**
	 * @see AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

	// Die EventHandler
	public void handleStartEvent(){
		try{
			process.reparieren();
		}catch(PersistenceException e){
			showErrorMessage("Beim Zurücksetzen ist ein Fehler aufgetreten");
			process.rollback();
		}
	}
	
	public void handleAbbrechenEvent(){
		process.rollback();
	}

}
