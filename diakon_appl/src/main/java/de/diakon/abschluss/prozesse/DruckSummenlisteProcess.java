package de.diakon.abschluss.prozesse;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import de.diakon.entities.BuchungenNachKontoKstSortComparator;
import de.diakon.entities.Buchungsperiode;
import de.diakon.entities.Buchungssatz;
import de.diakon.entities.KassenKontext;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.EndTransactionHandler;

/**
 * 
 * @author Thomas
 * @version 
 * @file DruckSummenlisteProcess.java
 */
public class DruckSummenlisteProcess extends AbstractProcess {
	private DruckSummenlisteController controller;
	private KassenKontext kontext;
	boolean zeilenschalter = false;  // f�r graue Zeilen

	/**
	 * Constructor for DruckSummenlisteProcess.
	 * @param parent
	 * @param eth
	 */
	public DruckSummenlisteProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		KassenKontext kontext) {
		super(parent, eth);

		this.kontext = kontext;
		openController();
	}

	private void openController() {
		logger.debug("openController");
		controller = new DruckSummenlisteController(this);
		controller.start();
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	private List<Buchungssatz> getSortedList(int vonkonto, int biskonto)
		throws PersistenceException {
		// wir erzeugen uns erstmal eine Kopie der Buchungsliste, mit den vorgegeben Kontenbereich
		List<Buchungssatz> buchungsliste =
			kontext.getAktuelleBuchungsperiode().getBuchungssaetze();
		List<Buchungssatz> selectionListe = new ArrayList<Buchungssatz>();

		for (Buchungssatz b: buchungsliste) {
			if (b.getKontonummer() >= vonkonto
				&& b.getKontonummer() <= biskonto) {
				selectionListe.add(b);
			}
		}

		// jetzt sortieren wir die Liste nach Konto-Nummer und Kostenstellen-Nummer
		Collections.sort(
			selectionListe,
			new BuchungenNachKontoKstSortComparator());

		return selectionListe;
	}

	/**
	 * Startet die Formatierung und den Druck der Liste gem�� der vorgegebenen Parameter.
	 * @param vonkonto
	 * @param biskonto
	 * @param einzelbuchungen
	 * @param vorschau
	 * @throws PersistenceException
	 */
	public void startDruck(int vonkonto, int biskonto, boolean einzelbuchungen, boolean vorschau)
		throws Exception {
		List<Buchungssatz> liste = getSortedList(vonkonto, biskonto);

		if (liste.size() == 0) {
			controller.showErrorMessage(
				"Im vorgegebenen Kontobereich gibt es keine Buchungen");
			commit();
			return;
		}

		List<BuchungssatzKassenbuchdruck> druckdaten = getDruckdaten(liste);
		
		if(einzelbuchungen){
			druckKontoKstSummen(druckdaten, true, vorschau);
		}else{
			druckKontoKstSummen(druckdaten, false, vorschau);
		}
		
		commit();
	}
	
	
	/**
	 * Druck die Summenliste mit Einzelbuchungen f�r die vorgegebene (sortierte)
	 * Buchungsliste
	 * @param liste
	 * @param boolean
	 */
	private void druckKontoKstSummen(List<BuchungssatzKassenbuchdruck> druckdaten, boolean detailliert, boolean vorschau) throws Exception{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("buchungsmonat", kontext.getAktuelleBuchungsperiodeFormatiert());
		parameters.put("kassenbuch", kontext.getKasse().getName());

		String jasperfile = detailliert ? "/reports/ktokst_summen_detailiert.jrxml" : "/reports/ktokst_summen_kumuliert.jrxml";
		
		JRDataSource dataSource = new JRBeanCollectionDataSource(druckdaten); 
			
		InputStream in = this.getClass().getResourceAsStream(jasperfile);
		JasperReport report = JasperCompileManager.compileReport(in);

		JasperPrint filledReport = JasperFillManager.fillReport(report, parameters, dataSource);
			
		if(vorschau){
			JasperViewer.viewReport(filledReport, false);				
		}else{
			JasperPrintManager.printReport(filledReport, true);
		}
	
	}	
	
	
	/**
	 * liefert eine Liste der Buchungss�tze des aktuellen Monats als Instanzen
	 * von BuchungssatzKassenbuchdruck.
	 * @return
	 * @throws PersistenceException 
	 */
	private List<BuchungssatzKassenbuchdruck> getDruckdaten(List<Buchungssatz> liste) throws PersistenceException {
		List<BuchungssatzKassenbuchdruck> resultList = new ArrayList<BuchungssatzKassenbuchdruck>();
		
		Buchungsperiode p = kontext.getAktuelleBuchungsperiode();
		int monat = p.getMonat();
		int jahr = p.getJahr();
		
		GregorianCalendar cal = new GregorianCalendar();
		
		for(Buchungssatz bs : liste){
			cal.set(jahr, monat-1, bs.getTag());
			Date datum = cal.getTime();  
			BigDecimal soll = null;
			BigDecimal haben = null;
			BigDecimal betrag = BigDecimal.valueOf(bs.getBetrag().getCent(), 2);
			if(bs.isSoll()){
				soll = betrag;
			}else{
				haben = betrag;
			}
			
			BuchungssatzKassenbuchdruck drucksatz = new BuchungssatzKassenbuchdruck(
					datum, 
					bs.getLfdNummer(),
					bs.getKontonummer(),
					bs.getKostenstellenNummer(),
					haben,
					soll,
					bs.getKontoBezeichnung(),
					bs.getZusatztext());
			resultList.add(drucksatz);
			
		}
		return resultList;
	}


}
