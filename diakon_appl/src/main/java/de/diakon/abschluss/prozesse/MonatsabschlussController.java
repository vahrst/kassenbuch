package de.diakon.abschluss.prozesse;

import de.diakon.abschluss.gui.MonatsabschlussDialog;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file MonatsabschlussController.java
 */
public class MonatsabschlussController extends AbstractController {
	private MonatsabschlussProcess process;
	
	private MonatsabschlussDialog dialog;

	/**
	 * Constructor for MonatsabschlussController.
	 */
	public MonatsabschlussController(MonatsabschlussProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		
		dialog = new MonatsabschlussDialog(ApplicationContext.getMainApplicationWindow(), true, this, process.getDaten());
		dialog.setVisible(true);		
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}


	// Die EventHandler
	public void handleStartEvent(){
		try{
			process.monatsabschluss();
		}catch(PersistenceException e){
			showErrorMessage("Beim Monatsabschlu▀ ist ein Fehler aufgetreten");
			process.rollback();
		}
	}
	
	public void handleAbbrechenEvent(){
		process.rollback();
	}

}
