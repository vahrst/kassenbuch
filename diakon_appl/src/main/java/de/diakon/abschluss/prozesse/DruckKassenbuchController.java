package de.diakon.abschluss.prozesse;

import de.diakon.abschluss.gui.DruckKassenbuchDialog;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;

/**
 * 
 * @author Thomas
 * @version 
 * @file DruckKassenbuchController.java
 */
public class DruckKassenbuchController extends AbstractController {
	private DruckKassenbuchProcess process;
	private DruckKassenbuchDialog dialog;
	
	/**
	 * Constructor for DruckKassenbuchController.
	 */
	public DruckKassenbuchController(DruckKassenbuchProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		dialog = new DruckKassenbuchDialog(ApplicationContext.getMainApplicationWindow(),true, this);
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

	public void handleAbbrechenEvent(){
		process.rollback();
	}
	
	
	/**
	 * Eventhandler Methode f�r ActionButton 'Vorschau'
	 */
	public void handleVorschauEvent(){
		handleDruckEvent(true);
	}
	
	/**
	 * Eventhandler Methode f�r Button 'start'
	 */
	public void handleStartEvent(){
		handleDruckEvent(false);
	}

	/**
	 * Gemeinschame Methode f�r handleStart und handleVorschau. 
	 * @param vorschau
	 */
	private void handleDruckEvent(boolean vorschau){
		try{
			process.startDruck(vorschau);
		}catch(Exception e){
			logger.error("Fehler beim Druck des Kassenbuchs", e);
			showErrorMessage("Fehler beim Druck des Kassenbuchs aufgetreten.");
			process.rollback();
		}

	}
	
	
}
