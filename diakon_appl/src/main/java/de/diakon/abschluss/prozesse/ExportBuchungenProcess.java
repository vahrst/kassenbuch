package de.diakon.abschluss.prozesse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import de.diakon.entities.BuchungenNachKontoKstSortComparator;
import de.diakon.entities.Buchungsperiode;
import de.diakon.entities.Buchungssatz;
import de.diakon.entities.Kasse;
import de.diakon.entities.KassenKontext;
import de.diakon.entities.Konto;
import de.diakon.entities.KontoHome;
import de.diakon.global.Konstanten;
import de.diakon.main.PropertiesManager;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.EndTransactionHandler;
import de.vahrst.application.types.SollHaben;
import de.vahrst.application.types.SollHabenHome;
import de.vahrst.application.types.Waehrungsbetrag;

/**
 * Process f�r CSV-Export der Summenliste 
 * @author Thomas
 * @version 
 */
public class ExportBuchungenProcess extends AbstractProcess {
	private ExportBuchungenController controller;
	private KassenKontext kontext;
	boolean zeilenschalter = false;  // f�r graue Zeilen

	/**
	 * @param parent
	 * @param eth
	 */
	public ExportBuchungenProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		KassenKontext kontext) {
		super(parent, eth);

		this.kontext = kontext;
		openController();
	}

	private void openController() {
		logger.debug("openController");
		controller = new ExportBuchungenController(this);
		controller.start();
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	/**
	 * liefert eine Liste aller Buchungss�tze der aktuellen Periode, sortiert nach Konto und Kostenstelle.
	 * @return
	 * @throws PersistenceException
	 */
	private List<Buchungssatz> getSortedList()
		throws PersistenceException {
		// wir erzeugen uns erstmal eine Kopie der Buchungsliste, mit den vorgegeben Kontenbereich
		List<Buchungssatz> buchungsliste =
			kontext.getAktuelleBuchungsperiode().getBuchungssaetze();
		List<Buchungssatz> selectionListe = new ArrayList<Buchungssatz>(buchungsliste);


		// jetzt sortieren wir die Liste nach Konto-Nummer und Kostenstellen-Nummer
		Collections.sort(
			selectionListe,
			new BuchungenNachKontoKstSortComparator());

		return selectionListe;
	}

	/**
	 * Liefert den Dateinamen, der sich aus Kassenname und Periode zusammensetzt.
	 * @return
	 */
	public String getDefaultFilename(){
		Kasse kasse = kontext.getKasse();
		
		String kassenname = kasse.getName();
		String kassenname2 = kassenname.replace(' ', '_');
		kassenname2 = kassenname2.replace('\\', '_');
		kassenname2 = kassenname2.replace('/', '_');
		int monat = kasse.getMonat();
		int jahr = kasse.getJahr();
		
		StringBuilder sb = new StringBuilder();
		sb.append("Kontokst-Summen_");
		sb.append(kassenname2).append("_");
		sb.append(String.format("%4d%02d", jahr, monat));
		sb.append(".csv");
		
		return sb.toString();
				
		
	}
	
	public String getExportDirectory(){
		String exportdir = PropertiesManager.getProperty(Konstanten.PROPS_EXPORT_DIR);
		return exportdir;
	}
	
	/**
	 * Startet die Formatierung und die Ausgabe der Buchungsdaten in das vorgegebene File.
	 * @param vonkonto
	 * @param biskonto
	 * @param einzelbuchungen
	 * @param vorschau
	 * @throws PersistenceException
	 */
	public void startExport(String directory, String filename)	throws Exception {
		logger.info("CSV-Export f�r: " + directory + " - " + filename);

		Properties props = PropertiesManager.getProperties();
		props.setProperty(Konstanten.PROPS_EXPORT_DIR, directory);
		PropertiesManager.storeProperties(props);
		
		Kasse kasse = kontext.getKasse();
		
		String charset = PropertiesManager.getProperty(Konstanten.PROPS_CSV_EXPORT_CHARSET).trim();
		String mandant = kasse.getMandant().trim();
		
		String belegnummer = getBelegnummer(kasse.getBuchungskreis());
		String belegdatum = getBelegdatum();
		
		
		File f = new File(directory, filename);
		FileOutputStream fout = new FileOutputStream(f);
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fout, charset)); 
		
		List<Buchungssatz> liste = getSortedList();
		
		
		writer.write(createCSVKopfzeile());
		writer.write("\n");

		Waehrungsbetrag ausgleichBetrag = new Waehrungsbetrag(0);

		for(Buchungssatz bs : liste){
			String buchungsZeile = createCSVZeile(bs, mandant, belegnummer, belegdatum);
			writer.write(buchungsZeile);
			writer.write("\n");
			
			if(bs.isSoll()){
				ausgleichBetrag.minus(bs.getBetrag());
			}else{
				ausgleichBetrag.add(bs.getBetrag());
			}
		}

		// Ausgleichsbuchungssatz erzeugen:
		SollHaben sh = SollHabenHome.SOLL;
		if(ausgleichBetrag.getCent() < 0){
			ausgleichBetrag.invert();
			sh = SollHabenHome.HABEN;
		}
		
		Konto ausgleichsKonto = new Konto(kasse.getKassenkonto(), "", false, false, SollHabenHome.HABEN);
		
		Buchungssatz ausgleichsBuchungssatz = new Buchungssatz(0, 1, ausgleichsKonto, "Gegenbuchung Kasse", null, ausgleichBetrag, sh);

		String buchungsZeile = createCSVZeile(ausgleichsBuchungssatz, mandant, belegnummer, belegdatum);
		writer.write(buchungsZeile);
		writer.write("\n");

				
		
		
		
		writer.close();
		
		
	}

	private String createCSVZeile(Buchungssatz bs, String mandant, 	String belegnummer, String belegdatum) {
		Konto kto = bs.getKonto();
		String koart = kto.isDebitor() ? "1" : "0"; // 1 = Debitor, 0 = Sach. 
		String ktonummer = String.format("%06d", bs.getKontonummer());
		String kstnummer = "";
		if(bs.getKostenstellenNummer() != 0){
			kstnummer = String.format("%06d", bs.getKostenstellenNummer());
		}
		String sh = bs.isSoll() ? "S" : "H";
		String betrag = bs.getBetrag().formatOhneTausenderPunkt();
		
		StringBuilder sb = new StringBuilder();

		sb.append(mandant).append(";");  // MA, Mandant
		sb.append("**;");  // BK, immer 2 Sternchen
		sb.append("02;");  // Belegart, immer 02
		sb.append(belegnummer).append(";"); // Beleg, 8 stellig
		sb.append(belegdatum).append(";");  // Buchungsdatum (= Belegdatum) = 01.mm.jjjj
		sb.append(belegdatum).append(";");  // Belegdatum, 01.mm.jjjj
		sb.append(koart).append(";");  // KontoArt, KOART, ART. Sach = 0/S, Debitor = 1/D
		sb.append(ktonummer).append(";"); // KONTO, 6stellig
		sb.append(sh).append(";"); // "S/H
		sb.append(betrag).append(";"); // 			"BETRAG;
		sb.append("\"").append(bs.getZusatztext()).append("\";");
		sb.append(kstnummer).append(";"); // KOSTENSTELLE, 6-stelling
		sb.append("0;"); //GEGKONTO
		sb.append(";"); // AKTIONSCODE
		sb.append(";"); // AUTOMGEGENBUCHUNG
		sb.append(";"); // FOLGE
		sb.append(";"); // GRUPPE
		sb.append("0;"); // GEGART
		sb.append(";"); // KOSTENART
		sb.append(";"); // CPDKONTOART
		sb.append(";"); // EXTERNEBELEGNUMMER
		sb.append("98;"); // BS
		sb.append(";"); // CPDKONTONUMMER
		sb.append(";"); // CPDNAME1;");
		sb.append(";"); // CPDSPRACHE;");
		sb.append(";"); // CPDLAENDERSCHLUESSEL;");
		sb.append(";"); // ZUSTXTID1;");
		sb.append(";"); // ZUSTXTTEXT1;");
		sb.append(";"); // ZUSTXTINTERN1;");
		sb.append(";"); // OPCODE;");
		sb.append(";"); // OPZUORDNUNG;");
		sb.append(""); // OPWEITEREDATEN");
		
		return sb.toString();
	}

	/**
	 * erzeugt die CSV Kopfzeile
	 * @return
	 */
	private String createCSVKopfzeile() {
		StringBuilder sb = new StringBuilder();
		sb.append("MA;");
		sb.append("BK;");
		sb.append("BELEGART;");
		sb.append("BELEG;");
		sb.append("BUCHDAT;");
		sb.append("BELDAT;");
		sb.append("KOART;");
		sb.append("KONTO;");
		sb.append("S/H;");
		sb.append("BETRAG;");
		sb.append("TEXT;");
		sb.append("KOSTENSTELLE;");
		sb.append("GEGKONTO;");
		sb.append("AKTIONSCODE;");
		sb.append("AUTOMGEGENBUCHUNG;");
		sb.append("FOLGE;");
		sb.append("GRUPPE;");
		sb.append("GEGART;");
		sb.append("KOSTENART;");
		sb.append("CPDKONTOART;");
		sb.append("EXTERNEBELEGNUMMER;");
		sb.append("BS;");
		sb.append("CPDKONTONUMMER;");
		sb.append("CPDNAME1;");
		sb.append("CPDSPRACHE;");
		sb.append("CPDLAENDERSCHLUESSEL;");
		sb.append("ZUSTXTID1;");
		sb.append("ZUSTXTTEXT1;");
		sb.append("ZUSTXTINTERN1;");
		sb.append("OPCODE;");
		sb.append("OPZUORDNUNG;");
		sb.append("OPWEITEREDATEN");
		
		
		return sb.toString();
	}

	/**
	 * liefert das Belegdatum im Format 01.mm.jjjj f�r den Export.
	 * @return
	 */
	private String getBelegdatum() {
		Buchungsperiode buchungsperiode = kontext.getAktuelleBuchungsperiode();
		return String.format("01.%02d.%04d", buchungsperiode.getMonat(), buchungsperiode.getJahr() );
	}

	/**
	 * liefert die Belegnummer f�r diesen Export, zusammengesetzt aus Jahr, Buchungskreis + Monat.
	 * @return
	 */
	private String getBelegnummer(int buchungskreis) {
		
		Buchungsperiode buchungsperiode = kontext.getAktuelleBuchungsperiode();
		
		int jahrZweistellig = buchungsperiode.getJahr() % 100;
		
		return String.format("%02d%03d%03d", jahrZweistellig, buchungskreis, buchungsperiode.getMonat());
	}
	

}
