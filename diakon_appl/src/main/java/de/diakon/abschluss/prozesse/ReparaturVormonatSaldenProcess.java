package de.diakon.abschluss.prozesse;

import de.diakon.entities.*;
import de.diakon.persistence.PersistenceException;
import de.diakon.persistence.PersistenceManager;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.EndTransactionHandler;
import de.vahrst.application.types.Waehrungsbetrag;
import org.omg.PortableServer.POAPackage.WrongAdapterHelper;

import java.sql.SQLException;

/**
 * 
 * @author Thomas
 */
public class ReparaturVormonatSaldenProcess extends AbstractProcess {
	private ReparaturVormonatSaldenController controller;
	private KassenKontext kontext;

	private ReparaturVormonatssaldenDaten daten;

	/**
	 * Constructor for {@link ReparaturVormonatSaldenProcess}.
	 * @param parent
	 * @param eth
	 */
	public ReparaturVormonatSaldenProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		KassenKontext kontext	) {
		super(parent, eth);

		this.kontext = kontext;
		openController();
	}

	private void openController(){
		controller = new ReparaturVormonatSaldenController(this);
		controller.start();
	}

	public ReparaturVormonatssaldenDaten getDaten(){
		if(daten == null) {
			daten = new ReparaturVormonatssaldenDaten();
			daten.init(kontext);

			Waehrungsbetrag saldo = berechneKorrekturAnfangssaldo(kontext);
			daten.anfangssaldo = saldo;
		}
		return daten;
	}

	private Waehrungsbetrag berechneKorrekturAnfangssaldo(KassenKontext kontext) {
		// 1. den Anfangssaldo der aktuellen Periode holen:
		Waehrungsbetrag anfangsSaldo = kontext.getAktuelleBuchungsperiode().getAnfangsSaldo();

		// 2. alle Buchungss�tze der fehlenden Buchungsperiode lesen
		Long periodeId = kontext.getVormonatReparaturKandidat();
		Waehrungsbetrag diffSaldo = new BuchungssatzHome().getBerechnetenSaldoFuerPeriode(periodeId);

		anfangsSaldo.minus(diffSaldo);
		return anfangsSaldo;
	}


	public void reparieren()throws PersistenceException{
		try{
			ReparaturVormonatssaldenDaten daten = getDaten();

			PersistenceManager.getPersistenceManager().startTransaction();
			new BuchungsperiodeHome().repairBuchungsperiode(
					daten.buchungssaldoId,
					daten.korrekturmonat,
					daten.korrekturjahr,
					daten.anfangssaldo,
					kontext.getKasse().getObjectId()
			);

			PersistenceManager.getPersistenceManager().commitTransaction();

			// Kasse neu setzen, das f�hrt zu einer Aktualisierung aller Status-Felder
			// ua. hasVormonat etc.
			Kasse kasse = kontext.getKasse();
			kontext.setKasse(kasse);


			log.info("Buchungssaldo satz rekonstruiert!!");
			commit();

		}catch(SQLException e){
			logger.error("Fehler in setzeAufVormonat", e);
			PersistenceManager.getPersistenceManager().rollbackTransaction();
			throw new PersistenceException(e);
		}

	}

	/**
	 * @see AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

}
