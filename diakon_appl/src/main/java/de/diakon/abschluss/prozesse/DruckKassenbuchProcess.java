package de.diakon.abschluss.prozesse;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import de.diakon.entities.Buchungsperiode;
import de.diakon.entities.Buchungssatz;
import de.diakon.entities.KassenKontext;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;
import de.vahrst.application.prozesse.EndTransactionHandler;

/**
 * 
 * @author Thomas
 * @version 
 * @file DruckKassenbuchProcess.java
 */
public class DruckKassenbuchProcess extends AbstractProcess {
	private DruckKassenbuchController controller;
	private KassenKontext kontext;
	
	/**
	 * Constructor for DruckKassenbuchProcess.
	 * @param parent
	 * @param eth
	 */
	public DruckKassenbuchProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		KassenKontext kontext) {
		super(parent, eth);

		this.kontext = kontext;		
		openController();
		

	}
	
	private void openController(){
		controller = new DruckKassenbuchController(this);
		controller.start();
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

	/**
	 * Formatiert den Ausdruck und startet ihn.
	 */
	public void startDruck(boolean vorschau) throws Exception{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("buchungsmonat", kontext.getAktuelleBuchungsperiodeFormatiert());
		parameters.put("kassenbuch", kontext.getKasse().getName());
		BigDecimal anfangssaldo = BigDecimal.valueOf(kontext.getAktuelleBuchungsperiode().getAnfangsSaldo().getCent(), 2);
		BigDecimal endsaldo = BigDecimal.valueOf(kontext.getAktuelleBuchungsperiode().getSaldo().getCent(), 2);
		parameters.put("bestand_alt", anfangssaldo);
		parameters.put("bestand_neu", endsaldo);

		
		List<BuchungssatzKassenbuchdruck> druckdaten = getDruckdaten();
		JRDataSource dataSource = new JRBeanCollectionDataSource(druckdaten); 
			
		InputStream in = this.getClass().getResourceAsStream("/reports/kassenbuch.jrxml");
		JasperReport report = JasperCompileManager.compileReport(in);

		JasperPrint filledReport = JasperFillManager.fillReport(report, parameters, dataSource);
			
		if(vorschau){
			JasperViewer.viewReport(filledReport, false);				
		}else{
			JasperPrintManager.printReport(filledReport, true);
		}


		commit();
		
	}
	
	
	
	/**
	 * liefert eine Liste der Buchungssätze des aktuellen Monats als Instanzen
	 * von BuchungssatzKassenbuchdruck.
	 * @return
	 * @throws PersistenceException 
	 */
	private List<BuchungssatzKassenbuchdruck> getDruckdaten() throws PersistenceException {
		List<BuchungssatzKassenbuchdruck> resultList = new ArrayList<BuchungssatzKassenbuchdruck>();
		
		Buchungsperiode p = kontext.getAktuelleBuchungsperiode();
		int monat = p.getMonat();
		int jahr = p.getJahr();
		
		GregorianCalendar cal = new GregorianCalendar();
		
		
		List<Buchungssatz> liste = p.getBuchungssaetze();
		for(Buchungssatz bs : liste){
			cal.set(jahr, monat-1, bs.getTag());
			Date datum = cal.getTime();  
			BigDecimal soll = null;
			BigDecimal haben = null;
			BigDecimal betrag = BigDecimal.valueOf(bs.getBetrag().getCent(), 2);
			if(bs.isSoll()){
				soll = betrag;
			}else{
				haben = betrag;
			}
			
			BuchungssatzKassenbuchdruck drucksatz = new BuchungssatzKassenbuchdruck(
					datum, 
					bs.getLfdNummer(),
					bs.getKontonummer(),
					bs.getKostenstellenNummer(),
					haben,
					soll,
					bs.getKontoBezeichnung(),
					bs.getZusatztext());
			resultList.add(drucksatz);
			
		}
		return resultList;
	}


	
}
