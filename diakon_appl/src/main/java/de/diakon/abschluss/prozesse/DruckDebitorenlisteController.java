package de.diakon.abschluss.prozesse;

import javax.swing.JOptionPane;

import de.diakon.abschluss.gui.*;
import de.diakon.persistence.PersistenceException;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file DruckKassenbuchController.java
 */
public class DruckDebitorenlisteController extends AbstractController {
	private DruckDebitorenlisteProcess process;
	private DruckDebitorenDialog dialog;

	/**
	 * Constructor for DruckKassenbuchController.
	 */
	public DruckDebitorenlisteController(DruckDebitorenlisteProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		dialog = new DruckDebitorenDialog(ApplicationContext.getMainApplicationWindow(),true, this);
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if(dialog != null){
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

	public void handleAbbrechenEvent(){
		process.rollback();
	}
	
	
	/**
	 * Eventhandler Methode f�r ActionButton 'Vorschau'
	 */
	public void handleVorschauEvent(){
		handleDruckEvent(true);
	}
	
	/**
	 * Eventhandler Methode f�r Button 'start'
	 */
	public void handleStartEvent(){
		handleDruckEvent(false);
	}

	/**
	 * Gemeinschame Methode f�r handleStart und handleVorschau. 
	 * @param vorschau
	 */
	private void handleDruckEvent(boolean vorschau){
		DruckDebitorenPanel panel = dialog.getMainPanel();
		int vondeb = panel.tfVonDebitor.getIntegerValue();
		int bisdeb = panel.tfBisDebitor.getIntegerValue();
		
		if(vondeb > bisdeb){
			JOptionPane.showMessageDialog(dialog, "Von ist gr��er als Bis!");
			return;
		}
		
		try{
			process.startDruck(vondeb, bisdeb, vorschau);
		}catch(PersistenceException e){
			showErrorMessage("Fehler beim Lesen der Buchungss�tze");
			process.rollback();
		}

	}
}
