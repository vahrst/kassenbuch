package de.diakon.abschluss.prozesse;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import de.diakon.abschluss.gui.ExportSummenlisteDialog;
import de.diakon.abschluss.gui.ExportSummenlistePanel;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;

/**
 * 
 * @author Thomas
 * @version 
 * @file DruckSummenlisteController.java
 */
public class ExportBuchungenController extends AbstractController {
	private ExportBuchungenProcess process;
	private ExportSummenlisteDialog dialog;

	
	/**
	 * Constructor for DruckSummenlisteController.
	 */
	public ExportBuchungenController(ExportBuchungenProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		dialog =
			new ExportSummenlisteDialog(
				ApplicationContext.getMainApplicationWindow(),
				"CSV-Export Summenliste",
				true,
				this);

		ExportSummenlistePanel panel = dialog.getMainPanel();
		panel.getTfDirectory().setText(process.getExportDirectory());
		panel.getTfFilename().setText(process.getDefaultFilename());
		
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if (dialog != null) {
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}


	public void handleAbbruchEvent(){
		process.rollback();
	}
	
	public void handleDirectoryChooserEvent(){
		ExportSummenlistePanel panel = dialog.getMainPanel();
		String directory = panel.getTfDirectory().getText();

		JFileChooser chooser = new JFileChooser(); 
    chooser.setCurrentDirectory(new File(directory));
    chooser.setDialogTitle("Auswahl Zielverzeichnis");
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    // disable the "All files" option.
    chooser.setAcceptAllFileFilterUsed(false);

    int result = chooser.showOpenDialog(dialog);
    if(result ==  JFileChooser.APPROVE_OPTION) { 
    	String dir = chooser.getSelectedFile().getAbsolutePath();
    	panel.getTfDirectory().setText(dir);
    }
	
	
	}
	
	
	public void handleStartEvent(){
		ExportSummenlistePanel panel = dialog.getMainPanel();
		String directory = panel.getTfDirectory().getText();
		String filename = panel.getTfFilename().getText();
		
		try{
			process.startExport(directory, filename);
			JOptionPane.showMessageDialog(dialog, "Kassenbuchungen wurden exportiert");
			process.commit();
		}catch(Exception e){
			logger.error("Fehler beim Export der Buchungen aufgetreten!", e);
			showErrorMessage("Fehler beim Export der Kassenbuchungen aufgetreten.");
			process.rollback();
		}
	}

}
