package de.diakon.abschluss.prozesse;

/**
 * 
 * @author Thomas
 * @version 
 * @file MonatsabschlussDaten.java
 */
public class MonatsabschlussDaten {
	public String kassenname;
	public String monatAlt;
	public String monatNeu;
	
	/**
	 * Constructor for MonatsabschlussDaten.
	 */
	public MonatsabschlussDaten() {
		super();
	}

}
