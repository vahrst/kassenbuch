package de.diakon.abschluss.prozesse;

import javax.swing.JOptionPane;

import de.diakon.abschluss.gui.DruckSummenlisteDialog;
import de.diakon.abschluss.gui.DruckSummenlistePanel;
import de.vahrst.application.global.ApplicationContext;
import de.vahrst.application.prozesse.AbstractController;
import de.vahrst.application.prozesse.AbstractProcess;

/**
 * 
 * @author Thomas
 * @version 
 * @file DruckSummenlisteController.java
 */
public class DruckSummenlisteController extends AbstractController {
	private DruckSummenlisteProcess process;
	private DruckSummenlisteDialog dialog;

	/**
	 * Constructor for DruckSummenlisteController.
	 */
	public DruckSummenlisteController(DruckSummenlisteProcess process) {
		super();
		this.process = process;
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {
		dialog =
			new DruckSummenlisteDialog(
				ApplicationContext.getMainApplicationWindow(),
				"Druck Summenliste",
				true,
				this);
		dialog.setVisible(true);
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {
		if (dialog != null) {
			dialog.setVisible(false);
			dialog.dispose();
			dialog = null;
		}
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}


	public void handleAbbrechenEvent(){
		process.rollback();
	}
	
	public void handleVorschauEvent(){
		handleDruckEvent(true);
	}
	
	public void handleStartEvent(){
		handleDruckEvent(false);
	}
	
	/**
	 * behandelt Druck- und Vorschau der Summenliste
	 * @param vorschau
	 */
	private void handleDruckEvent(boolean vorschau){
		DruckSummenlistePanel panel = dialog.getMainPanel();
		int vonkonto = panel.tfVonKonto.getIntegerValue();
		int biskonto = panel.tfBisKonto.getIntegerValue();
		boolean einzelbuchungen = panel.cbEinzelbuchungen.isSelected();
		
		if(vonkonto > biskonto){
			JOptionPane.showMessageDialog(dialog, "Von ist gr��er als Bis!");
			return;
		}
		
		try{
			process.startDruck(vonkonto, biskonto, einzelbuchungen, vorschau);
		}catch(Exception e){
			logger.error("Fehler beim Druck der Summenliste!", e);
			showErrorMessage("Fehler beim Druck der Summenliste aufgetreten.");
			process.rollback();
		}
	}

}
