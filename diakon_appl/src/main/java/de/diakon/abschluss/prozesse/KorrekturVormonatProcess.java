package de.diakon.abschluss.prozesse;

import de.diakon.entities.*;
import de.diakon.persistence.*;
import de.vahrst.application.prozesse.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file KorrekturVormonatProcess.java
 */
public class KorrekturVormonatProcess extends AbstractProcess {
	private KorrekturVormonatController controller;
	private KassenKontext kontext;
	/**
	 * Constructor for KorrekturVormonatProcess.
	 * @param parent
	 * @param eth
	 */
	public KorrekturVormonatProcess(
		AbstractProcess parent,
		EndTransactionHandler eth,
		KassenKontext kontext	) {
		super(parent, eth);
		
		this.kontext = kontext;
		openController();
	}
	
	private void openController(){
		controller = new KorrekturVormonatController(this);
		controller.start();	
	}

	public MonatsabschlussDaten getDaten(){
		MonatsabschlussDaten daten = new MonatsabschlussDaten();
		daten.kassenname = kontext.getKasse().getName();
		daten.monatAlt = kontext.getAktuelleBuchungsperiodeFormatiert();
		daten.monatNeu = kontext.getVorherigeBuchungsperiodeFormatiert();
		
		return daten;
	}

	public void setzeAufVormonat()throws PersistenceException{
		try{
			PersistenceManager.getPersistenceManager().startTransaction();	
			Kasse kasse = kontext.getKasse();
			Buchungsperiode aktPeriode = kontext.getAktuelleBuchungsperiode();
				
			int monat = aktPeriode.getMonat();
			int jahr = aktPeriode.getJahr();
				
			monat--;
			if(monat == 0){
				monat = 12;
				jahr--;
			}

			kasse.setJahr(jahr);
			kasse.setMonat(monat);
			new KasseHome().changeKasse(kasse);

			PersistenceManager.getPersistenceManager().commitTransaction();			

			kontext.setKasse(kasse);
			log.info("Buchungsperiode auf Vormonat zur�ckgesetzt f�r: " + kasse.toString());
			commit();
		}catch(PersistenceException e){
			logger.error("Fehler in setzeAufVormonat", e);
			PersistenceManager.getPersistenceManager().rollbackTransaction();
			throw e;
		}		
		
	}

	/**
	 * @see de.vahrst.application.prozesse.AbstractProcess#getController()
	 */
	public AbstractController getController() {
		return controller;
	}

}
