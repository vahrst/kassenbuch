package de.diakon.abschluss.prozesse;

import de.diakon.entities.Buchungsperiode;
import de.diakon.entities.KassenKontext;
import de.vahrst.application.types.Waehrungsbetrag;

/**
 * Info-Datensatz, der die Daten des zu rekonstruierenden Vormonatsalden-Satzes
 * enth�lt.
 */
public class ReparaturVormonatssaldenDaten {

    public String kassenname;
    public int korrekturmonat;
    public int korrekturjahr;

    public Long buchungssaldoId;
    public Waehrungsbetrag anfangssaldo;

    public void init(KassenKontext kontext) {
        this.kassenname = kontext.getKasse().getName();
        this.buchungssaldoId = kontext.getVormonatReparaturKandidat();
        Buchungsperiode p = kontext.getAktuelleBuchungsperiode();
        this.korrekturmonat = p.getMonat()-1;
        this.korrekturjahr = p.getJahr();
        if(this.korrekturmonat == 0){
            this.korrekturmonat = 12;
            this.korrekturjahr--;
        }

    }

    public String formatKorrekurPeriode() {
        return "" + korrekturmonat + "/" + korrekturjahr;
    }
}
