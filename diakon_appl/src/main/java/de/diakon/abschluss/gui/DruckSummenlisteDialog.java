package de.diakon.abschluss.gui;

import java.awt.*;

import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 */
public class DruckSummenlisteDialog extends AbstractDialog {
	private DruckSummenlistePanel panel;

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public DruckSummenlisteDialog(
		Frame owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public DruckSummenlisteDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, title, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public DruckSummenlisteDialog(
		Dialog owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}

	
	private void init(AbstractController controller){
		this.setTitle("Druck Summenliste");
		this.setSize(400,250);
		
		panel = new DruckSummenlistePanel(controller);
		getContentPane().add(panel, BorderLayout.CENTER);

		
		this.center();	
	}


	public DruckSummenlistePanel getMainPanel(){
		return panel;	
	}
}
