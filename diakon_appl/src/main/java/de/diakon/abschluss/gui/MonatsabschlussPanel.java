package de.diakon.abschluss.gui;

import java.awt.*;

import javax.swing.*;

import de.diakon.abschluss.prozesse.MonatsabschlussDaten;
import de.vahrst.application.gui.*;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file MonatsabschlussPanel.java
 */
public class MonatsabschlussPanel extends FormPanel {
	protected VaButton btStarten;
	protected VaButton btAbbrechen;

	/**
	 * Constructor for MonatsabschlussPanel.
	 */
	public MonatsabschlussPanel(AbstractController controller, MonatsabschlussDaten daten) {
		super();
		this.setPaintHeaderArea(false);
		
		initialize(controller, daten);
	}

	/**
	 * initialisiert dieses Panel
	 */
	private void initialize(AbstractController controller, MonatsabschlussDaten daten){
		this.setLayout(new BorderLayout());
		
		
		this.add(createInfoPanel(daten), BorderLayout.CENTER);
		this.add(createButtonPanel(controller), BorderLayout.SOUTH);	
		
	}

	/**
	 * Erzeugt das Infopanel 
	 */
	private JPanel createInfoPanel(MonatsabschlussDaten daten){
		JPanel infoPanel = new JPanel();
		infoPanel.setOpaque(false);
		
		infoPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);

		gbc.gridx = 0;
		gbc.gridy = 0;
		JLabel lb1 = new JLabel("Monatsabschlu� f�r Kasse: ");
		infoPanel.add(lb1, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		JLabel lb2 = new JLabel("alte Buchungsperiode: ");
		infoPanel.add(lb2, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		JLabel lb3 = new JLabel("neue Buchungsperiode: ");
		infoPanel.add(lb3, gbc);
		
		gbc.fill=GridBagConstraints.HORIZONTAL;
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		JLabel lbName = new JLabel(daten.kassenname);
		lbName.setBackground(Color.WHITE);
		lbName.setOpaque(true);
		infoPanel.add(lbName, gbc);		

		gbc.gridy = 1;
		JLabel lbMonatAlt = new JLabel(daten.monatAlt);
		lbMonatAlt.setBackground(Color.WHITE);
		lbMonatAlt.setOpaque(true);
		infoPanel.add(lbMonatAlt, gbc);		

		gbc.gridy = 2;
		JLabel lbMonatNeu = new JLabel(daten.monatNeu);
		lbMonatNeu.setBackground(Color.WHITE);
		lbMonatNeu.setOpaque(true);
		infoPanel.add(lbMonatNeu, gbc);		


		gbc.fill=GridBagConstraints.NONE;

		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 4;
		JLabel lb4 = new JLabel("Bitte stellen Sie sicher, dass f�r den aktuellen Monat alle Auswertungen");
		lb4.setFont(new Font(null, Font.ITALIC, 12));
		lb4.setForeground(Color.RED);
		infoPanel.add(lb4, gbc);

		gbc.gridx = 0;
		gbc.gridy = 5;
		JLabel lb5 = new JLabel("gedruckt wurden! ");
		lb5.setFont(new Font(null, Font.ITALIC, 12));
		lb5.setForeground(Color.RED);
		infoPanel.add(lb5, gbc);


		gbc.gridwidth = 1;
		gbc.gridx = 7;
		gbc.gridy = 7;
		gbc.weightx = 1;
		gbc.weighty = 1;
		Strut st1 = new Strut();
		infoPanel.add(st1, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.fill=GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 8;
		gbc.insets = new Insets(10,4,10,4);
		Liner liner = new Liner();
		infoPanel.add(liner, gbc);
		
		return infoPanel;	
	}


	/**
	 * Erzeugt das ButtonPanel
	 */
	protected JPanel createButtonPanel(AbstractController controller){
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		buttonPanel.setMinimumSize(new Dimension(10, 24));
		buttonPanel.setPreferredSize(new Dimension(10, 24));
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		btStarten = new VaButton("Start");
		btStarten.setActionCommand("Start");
		btStarten.addActionListener(controller);
		buttonPanel.add(btStarten);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btAbbrechen = new VaButton("Abbrechen");
		btAbbrechen.setActionCommand("Abbrechen");
		btAbbrechen.addActionListener(controller);
		buttonPanel.add(btAbbrechen);


		return buttonPanel;		
	}

}
