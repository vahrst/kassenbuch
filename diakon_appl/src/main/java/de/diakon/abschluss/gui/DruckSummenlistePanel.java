package de.diakon.abschluss.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

import de.vahrst.application.gui.FormPanel;
import de.vahrst.application.gui.Strut;
import de.vahrst.application.gui.TextFieldNumericInteger;
import de.vahrst.application.gui.VaButton;
import de.vahrst.application.gui.VaCheckBox;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 */
public class DruckSummenlistePanel extends FormPanel {
	private VaButton btStarten;
	private VaButton btAbbrechen;
	private VaButton btVorschau;

	public  TextFieldNumericInteger tfVonKonto;
	public  TextFieldNumericInteger tfBisKonto;
	public VaCheckBox cbEinzelbuchungen;

	/**
	 * Constructor for DruckDebitorenPanel.
	 */
	public DruckSummenlistePanel(AbstractController controller) {
		super();
		this.setPaintHeaderArea(false);
		
		initialize(controller);
	}

	/**
	 * initialisiert dieses Panel
	 */
	private void initialize(AbstractController controller){
		this.setLayout(new BorderLayout());
		
		this.add(createDatenPanel(), BorderLayout.CENTER);
		this.add(createButtonPanel(controller), BorderLayout.SOUTH);	
		
	}

	/**
	 * erzeugt das Eingabepanel
	 */
	private JPanel createDatenPanel(){
		JPanel datenPanel = new JPanel();
		datenPanel.setOpaque(false);
		JLabel lb1 = new JLabel("von Konto-Nummer");
		JLabel lb2 = new JLabel("bis Konto-Nummer");
		JLabel lb3 = new JLabel("Einzelbuchungen drucken");
		tfVonKonto = new TextFieldNumericInteger(6);
		tfVonKonto.setIntegerPart(6);
		tfVonKonto.setIntegerValue(0);
		tfBisKonto = new TextFieldNumericInteger(6);
		tfBisKonto.setIntegerPart(6);
		tfBisKonto.setIntegerValue(999999);
		

		Strut st1 = new Strut();
		
		cbEinzelbuchungen = new VaCheckBox();
		GroupLayout gl_datenPanel = new GroupLayout(datenPanel);
		gl_datenPanel.setHorizontalGroup(
			gl_datenPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_datenPanel.createSequentialGroup()
					.addGroup(gl_datenPanel.createParallelGroup(Alignment.TRAILING)
						.addGroup(Alignment.LEADING, gl_datenPanel.createSequentialGroup()
							.addContainerGap()
							.addComponent(lb3)
							.addPreferredGap(ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
							.addComponent(cbEinzelbuchungen, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(st1, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(Alignment.LEADING, gl_datenPanel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_datenPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lb2)
								.addComponent(lb1))
							.addGap(46)
							.addGroup(gl_datenPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(tfBisKonto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tfVonKonto, GroupLayout.DEFAULT_SIZE, 56, Short.MAX_VALUE))))
					.addGap(188))
		);
		gl_datenPanel.setVerticalGroup(
			gl_datenPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_datenPanel.createSequentialGroup()
					.addComponent(st1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addGroup(gl_datenPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(tfVonKonto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lb1))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_datenPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(tfBisKonto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lb2))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_datenPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lb3)
						.addComponent(cbEinzelbuchungen, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(191, Short.MAX_VALUE))
		);
		datenPanel.setLayout(gl_datenPanel);

		return datenPanel;

	}



	/**
	 * Erzeugt das ButtonPanel
	 */
	protected JPanel createButtonPanel(AbstractController controller){
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		buttonPanel.setMinimumSize(new Dimension(10, 24));
		buttonPanel.setPreferredSize(new Dimension(10, 24));
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		btStarten = new VaButton("Start");
		btStarten.setActionCommand("Start");
		btStarten.addActionListener(controller);
		buttonPanel.add(btStarten);

		buttonPanel.add(Box.createHorizontalStrut(5));
		
		
		btVorschau = new VaButton();
		btVorschau.setActionCommand("vorschau");
		btVorschau.setText("Vorschau");
		btVorschau.addActionListener(controller);
		buttonPanel.add(btVorschau);
		
		buttonPanel.add(Box.createHorizontalStrut(5));

		btAbbrechen = new VaButton("Abbrechen");
		btAbbrechen.setActionCommand("Abbrechen");
		btAbbrechen.addActionListener(controller);
		
		Strut strut = new Strut();
		buttonPanel.add(strut);
		buttonPanel.add(btAbbrechen);


		return buttonPanel;		
	}
}
