package de.diakon.abschluss.gui;

import java.awt.*;

import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file DruckDebitorenDialog.java
 */
public class DruckDebitorenDialog extends AbstractDialog {
	private DruckDebitorenPanel panel;

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public DruckDebitorenDialog(
		Frame owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public DruckDebitorenDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, title, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public DruckDebitorenDialog(
		Dialog owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}

	
	private void init(AbstractController controller){
		this.setTitle("Druck Debitorenliste");
		this.setSize(400,250);
		
		panel = new DruckDebitorenPanel(controller);
		getContentPane().add(panel, BorderLayout.CENTER);

		
		this.center();	
	}


	public DruckDebitorenPanel getMainPanel(){
		return panel;	
	}
}
