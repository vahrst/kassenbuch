package de.diakon.abschluss.gui;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.HeadlessException;

import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * Dialog f�r Druckauswahl Druck Kassenbuch.
 * @author Thomas
 * @version 
 */
public class ExportSummenlisteDialog extends AbstractDialog {
	private ExportSummenlistePanel panel;

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 * @wbp.parser.constructor
	 */
	public ExportSummenlisteDialog(
		Frame owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public ExportSummenlisteDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, title, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public ExportSummenlisteDialog(
		Dialog owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}

	
	private void init(AbstractController controller){
		this.setTitle("Export Summenlisten");
		this.setSize(550,240);
		
		panel = new ExportSummenlistePanel(controller);
		getContentPane().add(panel, BorderLayout.CENTER);
		this.center();	
	}

	/**
	 * liefert das Panel mit den Eingabelfeldern.
	 * @return
	 */
	public ExportSummenlistePanel getMainPanel(){
		return panel;	
	}
}
