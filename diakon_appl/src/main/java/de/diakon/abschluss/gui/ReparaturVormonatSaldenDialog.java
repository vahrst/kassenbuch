package de.diakon.abschluss.gui;

import de.diakon.abschluss.prozesse.MonatsabschlussDaten;
import de.diakon.abschluss.prozesse.ReparaturVormonatssaldenDaten;
import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

import java.awt.*;

/**
 * 
 * @author Thomas
 * @version 
 */
public class ReparaturVormonatSaldenDialog extends AbstractDialog {

	/**
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public ReparaturVormonatSaldenDialog(
		Frame owner,
		boolean modal,
		AbstractController controller,
		ReparaturVormonatssaldenDaten daten)
		throws HeadlessException {
		super(owner, modal, controller);

		init(controller, daten);
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public ReparaturVormonatSaldenDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller,
		ReparaturVormonatssaldenDaten daten)
		throws HeadlessException {
		super(owner, title, modal, controller);
		
		init(controller, daten);
	}

	private void init(AbstractController controller, ReparaturVormonatssaldenDaten daten){
		this.setSize(500,220);
		this.setResizable(false);
		this.setTitle("Reparatur fehlende Vormonatssalden");
		
		ReparaturVormonatSaldenPanel panel = new ReparaturVormonatSaldenPanel(controller, daten);
		getContentPane().add(panel, BorderLayout.CENTER);
		
		center();	
	}
	
}
