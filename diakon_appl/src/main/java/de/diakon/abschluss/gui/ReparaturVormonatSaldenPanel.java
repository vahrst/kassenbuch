package de.diakon.abschluss.gui;

import de.diakon.abschluss.prozesse.MonatsabschlussDaten;
import de.diakon.abschluss.prozesse.ReparaturVormonatssaldenDaten;
import de.vahrst.application.gui.FormPanel;
import de.vahrst.application.gui.Liner;
import de.vahrst.application.gui.Strut;
import de.vahrst.application.gui.VaButton;
import de.vahrst.application.prozesse.AbstractController;

import javax.swing.*;
import java.awt.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file MonatsabschlussPanel.java
 */
public class ReparaturVormonatSaldenPanel extends FormPanel {
	protected VaButton btStarten;
	protected VaButton btAbbrechen;

	/**
	 * Constructor for MonatsabschlussPanel.
	 */
	public ReparaturVormonatSaldenPanel(AbstractController controller, ReparaturVormonatssaldenDaten daten) {
		super();
		this.setPaintHeaderArea(false);
		
		initialize(controller, daten);
	}

	/**
	 * initialisiert dieses Panel
	 */
	private void initialize(AbstractController controller, ReparaturVormonatssaldenDaten daten){
		this.setLayout(new BorderLayout());

		this.add(createInfoPanel(daten), BorderLayout.CENTER);
		this.add(createButtonPanel(controller), BorderLayout.SOUTH);	
		
	}

	/**
	 * Erzeugt das Infopanel 
	 */
	private JPanel createInfoPanel(ReparaturVormonatssaldenDaten daten){
		JPanel infoPanel = new JPanel();
		infoPanel.setOpaque(false);
		
		infoPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(1,4,1,4);

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		JLabel infoLabel1 = new JLabel("Fehlender Buchungssaldo wird rekonstruiert");
		infoPanel.add(infoLabel1, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		JLabel infoLabel2 = new JLabel("Bitte Anfangssaldo pr�fen!");
		infoPanel.add(infoLabel2, gbc);

		gbc.gridwidth = 1;

		gbc.gridx = 0;
		gbc.gridy = 2;
		JLabel lb1 = new JLabel("Kasse: ");
		infoPanel.add(lb1, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		JLabel lb2 = new JLabel("Buchungsperiode: ");
		infoPanel.add(lb2, gbc);

		gbc.gridx = 0;
		gbc.gridy = 4;
		JLabel lb3 = new JLabel("Buchungsperiode ID : ");
		infoPanel.add(lb3, gbc);

		gbc.gridx = 0;
		gbc.gridy = 5;
		JLabel lb4 = new JLabel("Anfangssaldo (rekonstruiert): ");
		infoPanel.add(lb4, gbc);
		
		gbc.fill=GridBagConstraints.HORIZONTAL;
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		JLabel lbName = new JLabel(daten.kassenname);
		lbName.setBackground(Color.WHITE);
		lbName.setOpaque(true);
		infoPanel.add(lbName, gbc);		

		gbc.gridy = 3;
		JLabel lbPeriode = new JLabel(daten.formatKorrekurPeriode());
		lbPeriode.setBackground(Color.WHITE);
		lbPeriode.setOpaque(true);
		infoPanel.add(lbPeriode, gbc);

		gbc.gridy = 4;
		JLabel lbId = new JLabel(Long.toString(daten.buchungssaldoId));
		lbId.setBackground(Color.WHITE);
		lbId.setOpaque(true);
		infoPanel.add(lbId, gbc);

		gbc.gridy = 5;
		JLabel lbSaldo = new JLabel(daten.anfangssaldo.format());
		lbSaldo.setBackground(Color.WHITE);
		lbSaldo.setOpaque(true);
		infoPanel.add(lbSaldo, gbc);


		gbc.fill=GridBagConstraints.NONE;

/*		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 5;
		JLabel lb4 = new JLabel("Bitte stellen Sie sicher, dass f�r den aktuellen Monat alle Auswertungen");
		lb4.setFont(new Font(null, Font.ITALIC, 12));
		lb4.setForeground(Color.RED);
		infoPanel.add(lb4, gbc);

		gbc.gridx = 0;
		gbc.gridy = 6;
		JLabel lb5 = new JLabel("gedruckt wurden! ");
		lb5.setFont(new Font(null, Font.ITALIC, 12));
		lb5.setForeground(Color.RED);
		infoPanel.add(lb5, gbc);
*/

		gbc.gridwidth = 1;
		gbc.gridx = 7;
		gbc.gridy = 7;
		gbc.weightx = 1;
		gbc.weighty = 1;
		Strut st1 = new Strut();
		infoPanel.add(st1, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.fill=GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 8;
		gbc.insets = new Insets(10,4,10,4);
		Liner liner = new Liner();
		infoPanel.add(liner, gbc);
		
		return infoPanel;	
	}


	/**
	 * Erzeugt das ButtonPanel
	 */
	protected JPanel createButtonPanel(AbstractController controller){
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		buttonPanel.setMinimumSize(new Dimension(10, 24));
		buttonPanel.setPreferredSize(new Dimension(10, 24));
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		btStarten = new VaButton("Start");
		btStarten.setActionCommand("Start");
		btStarten.addActionListener(controller);
		buttonPanel.add(btStarten);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btAbbrechen = new VaButton("Abbrechen");
		btAbbrechen.setActionCommand("Abbrechen");
		btAbbrechen.addActionListener(controller);
		buttonPanel.add(btAbbrechen);


		return buttonPanel;		
	}

}
