package de.diakon.abschluss.gui;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.HeadlessException;

import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * Dialog f�r Druckauswahl Druck Kassenbuch.
 * @author Thomas
 * @version 
 */
public class DruckKassenbuchDialog extends AbstractDialog {
	private DruckKassenbuchPanel panel;

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 * @wbp.parser.constructor
	 */
	public DruckKassenbuchDialog(
		Frame owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public DruckKassenbuchDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, title, modal, controller);
		
		init(controller);
	}

	/**
	 * Constructor for DruckDebitorenDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public DruckKassenbuchDialog(
		Dialog owner,
		boolean modal,
		AbstractController controller)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller);
	}

	
	private void init(AbstractController controller){
		this.setTitle("Druck Kassenbuch");
		this.setSize(500,150);
		
		panel = new DruckKassenbuchPanel(controller);
		getContentPane().add(panel, BorderLayout.CENTER);
		this.center();	
	}

	/**
	 * liefert das Panel mit den Eingabelfeldern.
	 * @return
	 */
	public DruckKassenbuchPanel getMainPanel(){
		return panel;	
	}
}
