package de.diakon.abschluss.gui;

import java.awt.*;

import javax.swing.*;

import de.vahrst.application.gui.*;
import de.vahrst.application.prozesse.AbstractController;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
 * 
 * @author Thomas
 * @version
 * @file DruckDebitorenPanel.java
 */
public class DruckKassenbuchPanel extends FormPanel {
	private VaButton btStarten;
	private VaButton btAbbrechen;
	private VaButton btVorschau;

	/**
	 * Constructor for DruckDebitorenPanel.
	 */
	public DruckKassenbuchPanel(AbstractController controller) {
		super();
		this.setPaintHeaderArea(false);

		initialize(controller);
	}

	/**
	 * initialisiert dieses Panel
	 */
	private void initialize(AbstractController controller) {
		this.setLayout(new BorderLayout());

		this.add(createDatenPanel(), BorderLayout.CENTER);
		this.add(createButtonPanel(controller), BorderLayout.SOUTH);

	}

	/**
	 * erzeugt das Eingabepanel
	 */
	private JPanel createDatenPanel() {
		JPanel datenPanel = new JPanel();
		datenPanel.setOpaque(false);

		Strut st1 = new Strut();

		JLabel lblNewLabel = new JLabel(
				"Bitte w\u00E4hlen Sie 'Start' f\u00FCr direkten Druck oder 'Vorschau'");
		GroupLayout gl_datenPanel = new GroupLayout(datenPanel);
		gl_datenPanel.setHorizontalGroup(gl_datenPanel.createParallelGroup(
				Alignment.LEADING)
				.addGroup(
						gl_datenPanel
								.createSequentialGroup()
								.addGroup(
										gl_datenPanel
												.createParallelGroup(Alignment.LEADING)
												.addComponent(st1, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addGroup(
														gl_datenPanel
																.createSequentialGroup()
																.addGap(12)
																.addComponent(lblNewLabel,
																		GroupLayout.DEFAULT_SIZE, 426,
																		Short.MAX_VALUE))).addContainerGap()));
		gl_datenPanel.setVerticalGroup(gl_datenPanel.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_datenPanel
						.createSequentialGroup()
						.addComponent(st1, GroupLayout.PREFERRED_SIZE,
								GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(lblNewLabel).addGap(128)));
		datenPanel.setLayout(gl_datenPanel);

		return datenPanel;

	}

	/**
	 * Erzeugt das ButtonPanel
	 */
	protected JPanel createButtonPanel(AbstractController controller) {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		buttonPanel.setMinimumSize(new Dimension(10, 24));
		buttonPanel.setPreferredSize(new Dimension(10, 24));
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		btStarten = new VaButton("Start");
		btStarten.setActionCommand("Start");
		btStarten.addActionListener(controller);
		buttonPanel.add(btStarten);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btVorschau = new VaButton("Vorschau");
		btVorschau.setActionCommand("vorschau");
		btVorschau.addActionListener(controller);
		buttonPanel.add(btVorschau);

		buttonPanel.add(Box.createHorizontalStrut(5));

		btAbbrechen = new VaButton("Abbrechen");
		btAbbrechen.setActionCommand("Abbrechen");
		btAbbrechen.addActionListener(controller);
		buttonPanel.add(btAbbrechen);

		return buttonPanel;
	}
}
