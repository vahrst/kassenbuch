package de.diakon.abschluss.gui;


import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import de.vahrst.application.gui.BaseTextField;
import de.vahrst.application.gui.FormPanel;
import de.vahrst.application.gui.VaButton;
import de.vahrst.application.prozesse.AbstractController;

public class ExportSummenlistePanel extends FormPanel {
	private BaseTextField tfDirectory;
	private BaseTextField tfFilename;

	
	/**
	 * Create the panel.
	 */
	public ExportSummenlistePanel(AbstractController controller) {
		setPaintHeaderArea(false);
		
		VaButton vbtnStartExport = new VaButton();
		vbtnStartExport.setActionCommand("Start");
		vbtnStartExport.addActionListener(controller);
		vbtnStartExport.setText("Start Export");
		
		VaButton vbtnAbbruch = new VaButton();
		vbtnAbbruch.setText("Abbruch");
		vbtnAbbruch.setActionCommand("Abbruch");
		vbtnAbbruch.addActionListener(controller);
		
		JLabel lblExportiertDieKonten = new JLabel("Exportiert die Konten- und Kostenstellensummen des aktuellen Monats");
		
		JLabel lblExportverzeichnis = new JLabel("Export-Verzeichnis");
		
		JLabel lblDateiname = new JLabel("Dateiname");
		
		JLabel label = new JLabel("");
		
		VaButton btDirectory = new VaButton();
		btDirectory.setText("...");
		btDirectory.setActionCommand("directoryChooser");
		btDirectory.addActionListener(controller);
		
		tfDirectory = new BaseTextField();
		tfDirectory.setEditable(false);
		tfDirectory.setFocusable(false);
		
		tfFilename = new BaseTextField();
		tfFilename.setFocusable(false);
		tfFilename.setEditable(false);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(vbtnStartExport, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(vbtnAbbruch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(label)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addComponent(lblExportiertDieKonten, Alignment.LEADING)
							.addGroup(groupLayout.createSequentialGroup()
								.addComponent(lblExportverzeichnis)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
									.addComponent(tfFilename, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE)
									.addComponent(tfDirectory, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGap(18)
								.addComponent(btDirectory, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)))
						.addComponent(lblDateiname))
					.addGap(22))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblExportiertDieKonten)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btDirectory, 0, 0, Short.MAX_VALUE)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblExportverzeichnis)
							.addComponent(tfDirectory, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addGap(12)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDateiname)
						.addComponent(tfFilename, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(label)
					.addPreferredGap(ComponentPlacement.RELATED, 181, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(vbtnAbbruch, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
						.addComponent(vbtnStartExport, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)))
		);
		setLayout(groupLayout);

	}
	public BaseTextField getTfDirectory() {
		return tfDirectory;
	}
	public BaseTextField getTfFilename() {
		return tfFilename;
	}
}
