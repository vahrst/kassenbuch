package de.diakon.abschluss.gui;

import java.awt.*;

import de.diakon.abschluss.prozesse.MonatsabschlussDaten;
import de.vahrst.application.gui.AbstractDialog;
import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file MonatsabschlussDialog.java
 */
public class MonatsabschlussDialog extends AbstractDialog {

	/**
	 * Constructor for MonatsabschlussDialog.
	 * @param owner
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public MonatsabschlussDialog(
		Frame owner,
		boolean modal,
		AbstractController controller,
		MonatsabschlussDaten daten)
		throws HeadlessException {
		super(owner, modal, controller);
		
		init(controller, daten);
	}

	/**
	 * Constructor for MonatsabschlussDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @param controller
	 * @throws HeadlessException
	 */
	public MonatsabschlussDialog(
		Frame owner,
		String title,
		boolean modal,
		AbstractController controller,
		MonatsabschlussDaten daten)
		throws HeadlessException {
		super(owner, title, modal, controller);
		
		init(controller, daten);
	}

	private void init(AbstractController controller, MonatsabschlussDaten daten){
		this.setSize(500,220);
		this.setResizable(false);
		this.setTitle("Monatsabschlu▀");
		
		MonatsabschlussPanel panel = new MonatsabschlussPanel(controller, daten);
		getContentPane().add(panel, BorderLayout.CENTER);
		
		center();	
	}
	
}
