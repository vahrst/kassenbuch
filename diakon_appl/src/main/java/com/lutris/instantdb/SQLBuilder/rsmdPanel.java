/*-----------------------------------------------------------------------------
 * Copyright (C) 1997-2001 Lutris Technologies, Inc.       All Rights Reserved.
 *
 * This software and documentation is the confidential and proprietary
 * information of Lutris Technologies, Inc. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with Lutris Technologies.
 *-----------------------------------------------------------------------------
 * $Id: rsmdPanel.java,v 1.1.2.3 2001/05/23 22:26:43 andy Exp $
 *-----------------------------------------------------------------------------
 */
package com.lutris.instantdb.SQLBuilder;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * Provides the tab showing the result set meta-data
 */
class rsmdPanel extends JPanel {

	Connection		con;						// the database connection
	ResourceBundle res = ResourceBundle.getBundle("com.lutris.instantdb.SQLBuilder.Res");
	SQLBuilder		parent;						// the parent application
	JTable			outputTable;				// the table for outputting the results
	JScrollPane		scrollpane;					// the scroll pane containing the results table
	Statement		stmt;						// the statement used to make requests
	ResultSet		rs;							// the ResultSet from the last query
	ResultSetMetaData rsmd;						// the data we'll display
	JComboBox		columns;					// used to select a column for meta-data
	ReflectMetaData	dataModel;					// supplies data for the diaplyed table

	/**
	 *
	 */
	rsmdPanel (SQLBuilder sb) {

          super( new GridBagLayout() );

		parent = sb;										// save parent application

          GridBagConstraints gbc = new GridBagConstraints();
          gbc.fill = GridBagConstraints.BOTH;
          gbc.insets = new Insets( 4,4,4,4 );

		// add the driver list box and label
          JLabel jlabel = new JLabel(res.getString("Select_a_column"), JLabel.RIGHT );
          add( jlabel, gbc );

          // create the columns drop down box
          columns = new JComboBox();
          gbc.weightx = 1.0;
          gbc.gridwidth = GridBagConstraints.REMAINDER;
          add( columns, gbc );

          outputTable = new JTable();
          JScrollPane jscrollpane = new JScrollPane( outputTable );
          gbc.weightx = 0;
          gbc.weighty = 1.0;
          add( jscrollpane, gbc );

          columns.addActionListener( new theListener() );

	} // method rsmdPanel

	/**
	 *
	 */
	void onQuery (ResultSet r) {
		try {
			rs = r	;										// save the results set
			rsmd = rs.getMetaData();						// get the data to be displayed
               if ( columns.getItemCount() > 0 )
               {
     			columns.removeAllItems();						// clear out existing column list
               }
			for (int i=1; i<=rsmd.getColumnCount(); i++) {	// add each available column to list
				columns.addItem(rsmd.getColumnName(i));	
			} // for
		} catch (Exception e) {
			System.out.println (res.getString("Error_setting_Results") + e.toString());
		} // try-catch
	} // method onQuery

	/**
	 * Used to handle events
	 */
	class theListener implements ActionListener{

		public void actionPerformed (ActionEvent a) {
			try {
				if (a.getSource() == columns) {				// if a new column selected
					int selection = columns.getSelectedIndex();	// get currently selected table
					outputTable.setModel (
						new ReflectMetaData(rsmd,selection+1));// change data model
				} // if
			} catch (Exception e) {
				System.out.println (e.toString());
			} // try-catch
		} // method actionPerformed

	} // inner class ButtonListener 


} // class dbmdPanel 

