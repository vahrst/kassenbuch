/*-----------------------------------------------------------------------------
 * Copyright (C) 1997-2001 Lutris Technologies, Inc.       All Rights Reserved.
 *
 * This software and documentation is the confidential and proprietary
 * information of Lutris Technologies, Inc. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with Lutris Technologies.
 *-----------------------------------------------------------------------------
 * $Id: Res.java,v 1.1.2.4 2001/05/23 22:26:43 andy Exp $
 *-----------------------------------------------------------------------------
 */
package com.lutris.instantdb.SQLBuilder;


public class Res extends java.util.ListResourceBundle {
	static final Object[][] contents = {
	{ "Browse_Database", "Browse Database" },
	{ "jdbc_idb_", "jdbc:idb:" },
	{ "Connecting_", "Connecting..." },
	{ "Disconnect", "Disconnect" },
	{ "Connected_to", "Connected to " },
	{ "Error", "Error" },
	{ "com_lutris_instantdb", "com.lutris.instantdb.jdbc.idbDriver" },
	{ "sun_jdbc_odbc", "sun.jdbc.odbc.JdbcOdbcDriver" },
	{ "Driver", "Driver" },
	{ "URL", "URL" },
	{ "Username", "Username" },
	{ "Password", "Password" },
	{ "Browse", "Browse" },
	{ "Connect", "Connect" },
	{ "Disconnecting_", "Disconnecting..." },
	{ "Disconnected", "Disconnected" },
	{ "SELECT_FROM", "SELECT * FROM " },
	{ "FROM", " FROM " },
	{ "BLANK", " " },
	{ "Tables", "Tables" },
	{ "Columns", "Columns" },
	{ "Enter_row_limit_", "Enter row limit:" },
	{ "Submit", "Submit" },
	{ "Query_", "Query:" },
	{ "SELECT", "SELECT " },
	{ "KEY", "%" },
	{ "COLUMN_NAME", "COLUMN_NAME" },
	{ "Error_getting_columns", "Error getting columns" },
	{ "TABLE_NAME", "TABLE_NAME" },
	{ "Error_getting_tables", "Error getting tables" },
	{ "True", "True" },
	{ "False", "False" },
	{ "int", "int" },
	{ "boolean", "boolean" },
	{ "java_lang_String", "java.lang.String" },
	{ "Property", "Property" },
	{ "Value", "Value" },
	{ "Select_a_column", "Select a column" },
	{ "Error_setting_Results", "Error setting Results meta-data:\n" },
	{ "Could_not_load", "Could not load LookAndFeel: " },
	{ "Connection", "Connection" },
	{ "Connects_Disconnects", "Connects/Disconnects to a database" },
	{ "Dbase_Data", "Dbase Data" },
	{ "Database_Meta_data", "Database Meta-data" },
	{ "Query", "Query" },
	{ "Performs_database", "Performs database queries" },
	{ "Results_Data", "Results Data" },
	{ "Result_Set_Meta_data", "Result Set Meta-data" },
	{ "Lutris_SQLBuilder", "Lutris SQLBuilder" },
	{ "North", "North" },
	{ "Center", "Center" },
	{ "South", "South" },
	{ "Error_disconnecting_", "Error disconnecting: " }};

	public Object[][] getContents() {
		return contents;
	}
}

