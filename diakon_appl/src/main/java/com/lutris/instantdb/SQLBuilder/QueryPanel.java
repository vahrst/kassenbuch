/*-----------------------------------------------------------------------------
 * Copyright (C) 1997-2001 Lutris Technologies, Inc.       All Rights Reserved.
 *
 * This software and documentation is the confidential and proprietary
 * information of Lutris Technologies, Inc. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with Lutris Technologies.
 *-----------------------------------------------------------------------------
 * $Id: QueryPanel.java,v 1.1.2.3 2001/05/23 22:26:42 andy Exp $
 *-----------------------------------------------------------------------------
 */
package com.lutris.instantdb.SQLBuilder;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;

import com.lutris.instantdb.jdbc.idbResultsSet;



/**
 * Provides the tab allowing queries to be made.
 */
class QueryPanel extends JPanel {

	JComboBox		tables;						// combo box holding tables in database
	ResourceBundle res = ResourceBundle.getBundle("com.lutris.instantdb.SQLBuilder.Res");
//	ResourceBundle res = ResourceBundle.getBundle("com.lutris.instantdb.SQLBuilder.Res");
	JComboBox		columns;					// combo box holding columns in this table
	JComboBox		sqlCommands;				// combo box holding currently valid sql
	JTextField		sqlText;					// holds the SQL text box
	JTextField		rowCount;					// holds the row count
	JButton			submitButton;				// submits the SQL
	ActionListener	theHandler;					// handles events
	JFrame			applicationFrame;			// frame of the main application
	JLabel			statusBar;					// the parent's status bar
	Connection		con;						// the database connection
	SQLBuilder		parent;						// the parent application
	JTable			outputTable;				// the table for outputting the results
	JScrollPane		scrollpane;					// the scroll pane containing the results table
//	ResultModel		data;						// fetches data from the ResultSet and provides to the JTable
	Statement		stmt;						// the statement used to make requests
	ResultSet		rs;							// the ResultSet from the last query
	String[]		selectedCols;				// used when columns selected individually
	StringBuffer	selectBuf;					// used to build up column selection
	int				selectCount;				// count of selected columns
	boolean			firstTime;					// prevents colum selection on table selction


	/**
	 * Used to handle button clicks
	 */
	class theQueryListener implements ActionListener{

		public void actionPerformed (ActionEvent a) {
			try {
				if (a.getSource() == tables) {				// if a new table selected
					getColumns();							// get its columns
					String curTable = (String)tables.getSelectedItem();	// get currently selected table
					String sql = res.getString("SELECT_FROM")+curTable;	// set default query
					sqlText.setText (sql);					// display it
					selectCount=0;							// only used if cols selected
					firstTime=false;						// ensures next col selct works
				} else if (a.getSource() == columns) {
					if (firstTime) {						// just reacting to table selection
						firstTime=false;
						return;
					} // if
					String curCol = (String)columns.getSelectedItem();	// get currently selected col
					selectBuf.setLength(7);					// pear back to SELECT
					selectedCols[selectCount++]=curCol;
					for (int i=0; i<selectCount; i++) {		// for each selected column
						if (i!=0) selectBuf.append (',');
						selectBuf.append (selectedCols[i]);	// add to query
					} // for
					String curTable = (String)tables.getSelectedItem();	// get currently selected table
					selectBuf.append (res.getString("FROM")+curTable);
					sqlText.setText (selectBuf.toString());	// display it
				} else if (a.getSource() == submitButton) {
					String rowCountStr = rowCount.getText();// get any row count
					if (!rowCountStr.equals("")) {			// if a row count set
						stmt.setMaxRows (Integer.parseInt(rowCountStr));
					} else {
						stmt.setMaxRows (0);
					} // if-else
					if (stmt.execute(sqlText.getText())) {
						// Result set available
						rs = stmt.getResultSet();
						outputTable.setModel (new ResultModel());
						parent.onQuery (rs);
					} // if
				} // if-else
			} catch (Exception e) {
				System.out.println (e.toString());
			} // try-catch
		} // method actionPerformed
	} // inner class theQueryListener 

	/**
	 *
	 */
	QueryPanel (SQLBuilder sb) {

        super( new GridBagLayout() );

        parent = sb;
        applicationFrame = parent.getFrame();
        statusBar = parent.getStatusBar();
        theHandler = new theQueryListener();

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets( 2,2,2,2 );
        gbc.fill = GridBagConstraints.BOTH;

        // lbl tables
        gbc.gridx = 0;
        gbc.gridy = 0;
        JLabel lbl = new JLabel( res.getString("Tables") );
        add( lbl, gbc );

        // lbl columns
        lbl = new JLabel( res.getString("Columns") );
        gbc.gridx = 0;
        gbc.gridx = 1;
        add( lbl, gbc );

        // lbl row limit
        gbc.gridx = 2;
        lbl = new JLabel( res.getString("Enter_row_limit_") );
        add( lbl, gbc );

        gbc.gridy = 1;

        // combo tables
        gbc.gridx = 0;
        tables = new JComboBox( );
        add( tables, gbc );

        // combo cols
        gbc.gridx = 1;
        columns = new JComboBox( );
        add( columns, gbc );

        // txt row
        gbc.gridx = 2;
        rowCount = new JTextField( 20 );
        add( rowCount, gbc );

        // submit
        gbc.gridx = 3;
        submitButton = new JButton( res.getString("Submit") );
        add( submitButton, gbc );

        gbc.gridy = 2;

        // query line
        lbl = new JLabel(res.getString("Query_"));
        gbc.gridx = 0;
        gbc.anchor = GridBagConstraints.EAST;
        add( lbl, gbc );
        sqlText = new JTextField( 50 );
        gbc.gridx = 1;
        gbc.gridwidth = 4; //gbc.REMAINDER;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        add( sqlText, gbc );

        gbc.gridy = 3;

        // table
        gbc.gridx = 0;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.gridheight = GridBagConstraints.REMAINDER;
        gbc.weighty = 1.0;
        outputTable = new JTable();
//        outputTable.setAutoResizeMode( JTable.AUTO_RESIZE_NEXT_COLUMN );
        JScrollPane jscrollpane = new JScrollPane( outputTable );
        add( jscrollpane, gbc );

        // setup listeners
        tables.addActionListener( theHandler );
        columns.addActionListener( theHandler );
        submitButton.addActionListener( theHandler );
        sqlText.addActionListener( theHandler );

        selectBuf = new StringBuffer(res.getString("SELECT"));

	} // method ConnectionPanel

	/**
	 * Adds a column of results to a combo list box
	 */
	void addResultsToList (ResultSet rs, JComboBox list, String colName) throws SQLException {
          if ( list.getItemCount() > 0 )
          {
     		list.removeAllItems();								// clear out the combo list
          }
		ResultSetMetaData rsmd = rs.getMetaData ();			// get results set info
		rsmd.getColumnCount ();				// get number of columns
		int reqdCol = rs.findColumn (colName);

		// add each row from col to list
		while (rs.next()) {									// keep fetching result rows
			firstTime=true;
			list.addItem (rs.getString(reqdCol));			// add to cur row
		} // while

	} // method addResultsToList

	/**
	 *
	 */
	void getColumns () {
		try {
			String curTable = (String)tables.getSelectedItem();	// get currently selected table
			DatabaseMetaData dbmd = con.getMetaData();
			ResultSet rs = dbmd.getColumns(null,null,curTable,res.getString("KEY"));
			addResultsToList (rs, columns, res.getString("COLUMN_NAME"));
			selectedCols = new String[columns.getItemCount()];
			rs.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, 
				e.toString(), res.getString("Error_getting_columns"), JOptionPane.DEFAULT_OPTION);
		} // try-catch
	} // method getColumns

	/**
	 * Gets the list of tables.
	 */
	void getTables () {
		try {
			DatabaseMetaData dbmd = con.getMetaData();
			ResultSet rs = dbmd.getTables (null,null,res.getString("KEY"),null);
			addResultsToList (rs, tables, res.getString("TABLE_NAME"));
			rs.close();
			getColumns();
			firstTime=false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, 
				e.toString(), res.getString("Error_getting_tables"), JOptionPane.DEFAULT_OPTION);
		} // try-catch
	} // method getTables



	/**
	 * Invoked by ConnectionPanel when a database connect takes place.
	 */
	void onConnect (Connection c) {
		con = c;											// save the Connection
		getTables();										// populate the tables combo
		try {
			stmt = con.createStatement ();
		} catch (Exception e) {
		} // try-catch
	} // method onConnect


	/**
	 * Used to provide data for the results array table.
	 */
	class ResultModel extends AbstractTableModel {

		String blank = res.getString("BLANK");

		boolean resultsOK () throws SQLException {
			if (rs==null) return false;						// check for no results
			rs.getMetaData();								// if this fails then rs is closed
			return true;									// rs present and not closed
		}

		public int getColumnCount() { 
			try {
				if (!resultsOK()) return 10; 				// guess anything
				return (rs.getMetaData()).getColumnCount();	// return proper number of cols
			} catch (Exception e) {
				return 10;
			} // try-catch
		}

		public int getRowCount() { 
			try {
				if (!resultsOK()) return 100; 				// guess anything
				if (!(rs instanceof idbResultsSet)) return 1000;// guess anything
				return ((idbResultsSet)rs).getRowCount();
			} catch (Exception e) {
				return 100;
			} // try-catch
		}

		public Object getValueAt(int row, int col) { 
			try {
				if (!resultsOK()) return blank;				// empty
				if (!(rs instanceof idbResultsSet)) {		// if not an InstantDB database
					if (!rs.next()) return blank;				// too many rows - so just blank them out
					return rs.getString(col+1);
				} else {
					((idbResultsSet)rs).setCurRow (row+1);	// move to requested row
					rs.next();
					return rs.getString(col+1);
				} // if-else
			} catch (Exception e) {
				return blank;
			} // try-catch
		}

		public String getColumnName (int i) {
			try {
				if (!resultsOK()) return new Integer(i).toString();
				return rs.getMetaData().getColumnName(i+1);
			} catch (Exception e) {
				return new Integer(i).toString();
			} // try-catch
		}
		
	} // iner class ResultModel 

} // class QueryPanel 

