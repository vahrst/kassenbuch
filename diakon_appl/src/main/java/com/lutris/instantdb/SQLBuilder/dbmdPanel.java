/*-----------------------------------------------------------------------------
 * Copyright (C) 1997-2001 Lutris Technologies, Inc.       All Rights Reserved.
 *
 * This software and documentation is the confidential and proprietary
 * information of Lutris Technologies, Inc. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with Lutris Technologies.
 *-----------------------------------------------------------------------------
 * $Id: dbmdPanel.java,v 1.1.2.3 2001/05/23 22:26:43 andy Exp $
 *-----------------------------------------------------------------------------
 */
package com.lutris.instantdb.SQLBuilder;

import java.awt.BorderLayout;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * Provides the tab showing the database meta-data
 */
class dbmdPanel extends JPanel {

	Connection		con;						// the database connection
	SQLBuilder		parent;						// the parent application
	JTable			outputTable;				// the table for outputting the results
	JScrollPane		scrollpane;					// the scroll pane containing the results table
	Statement		stmt;						// the statement used to make requests
	ResultSet		rs;							// the ResultSet from the last query
	DatabaseMetaData dbmd;						// the data we'll display



	/**
	 *
	 */
	dbmdPanel (SQLBuilder sb) {
		parent = sb;										// save parent application
		setLayout( new BorderLayout() );

		outputTable = new JTable();
		JScrollPane scrollpane = new JScrollPane(outputTable);
		add ( scrollpane, BorderLayout.CENTER );
	} // method dbmdPanel


	/**
	 * Invoked by ConnectionPanel when a database connect takes place.
	 */
	void onConnect (Connection c) {
		try {
			con = c;										// save the Connection
			dbmd = con.getMetaData();						// get the data to be displayed
			outputTable.setModel (new ReflectMetaData(dbmd));
		} catch (Exception e) {
		} // try-catch
	} // method onConnect


} // class dbmdPanel 

