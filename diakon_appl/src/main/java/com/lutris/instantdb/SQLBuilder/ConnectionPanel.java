/*-----------------------------------------------------------------------------
 * Copyright (C) 1997-2001 Lutris Technologies, Inc.       All Rights Reserved.
 *
 * This software and documentation is the confidential and proprietary
 * information of Lutris Technologies, Inc. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with Lutris Technologies.
 *-----------------------------------------------------------------------------
 * $Id: ConnectionPanel.java,v 1.1.2.4 2001/05/23 22:26:42 andy Exp $
 *-----------------------------------------------------------------------------
 */
package com.lutris.instantdb.SQLBuilder;

import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * Provides the tab containing the connection details.
 */
class ConnectionPanel extends JPanel {

	static ResourceBundle res = ResourceBundle.getBundle("com.lutris.instantdb.SQLBuilder.Res");
	static String[]	drivers = {					// used to populate the above list
		res.getString("com_lutris_instantdb"),
		res.getString("sun_jdbc_odbc")};
	JComboBox		driverList;					// list of available JDBC drivers
	JTextField		urlText;					// holds the url text box
	JTextField		userText;					// holds the username text box
	JTextField		passText;					// holds the password text box
	JButton			browseButton;				// pops up the browse dialog box
	JButton			connectButton;				// connects to the database
	ActionListener	buttonHandler;				// handles button events
	JFrame			applicationFrame;			// frame of the main application
	JLabel			statusBar;					// the parent's status bar
	Connection		con;						// the database connection
	SQLBuilder		parent;						// the parent application

	/**
	 *
	 */
	ConnectionPanel (SQLBuilder sb) {
		parent = sb;										// save parent application
		applicationFrame = parent.getFrame();				// needed by button listener
		statusBar = parent.getStatusBar();					// used to set status

		driverList = new JComboBox();						// create the drivers drop down box
		for (int i=0; i<drivers.length; i++) {				// add each available driver to list
			driverList.addItem(drivers[i]);	
		} // for
		
		setLayout(null);

		// add the driver list box and label
		JLabel driverLabel = new JLabel (res.getString("Driver"));
		add (driverLabel);
		driverLabel.setBounds (50,50,80,20);
		add (driverList);
		driverList.setBounds (140,50,150,20);

		// add the various text boxes
		
		urlText = addLabelledTextBox (res.getString("URL"), 80);
		urlText.setText("jdbc:idb:diakon.prp");
		userText = addLabelledTextBox (res.getString("Username"), 100);
		passText = addLabelledTextBox (res.getString("Password"), 120);

		// add the browse and connect/disconnect buttons
		buttonHandler = new ButtonListener ();
		browseButton = addButton (res.getString("Browse"), 100);
		connectButton = addButton (res.getString("Connect"), 210);

		setSize(250,120);
	} // method ConnectionPanel


	/**
	 *
	 */
	JTextField addLabelledTextBox (String label, int y) {
		JLabel l = new JLabel (label);
		add (l);
		l.setBounds (50,y,80,20);
		
		JTextField tf = null;
		if (label.equals(res.getString("Password"))) {
			tf = new JPasswordField();
		} else {
			tf = new JTextField ();
		} // if-else
		add (tf);
		tf.setBounds (140,y,150,20);
		
		return tf;
	} // method addLabelledTextBox

	/**
	 *
	 */
	JButton addButton (String label, int x) {
		JButton b = new JButton (label);
		add (b);
		b.setBounds (x, 170, 100, 30);
		b.addActionListener (buttonHandler);
		return b;
	} // method addButton

	/**
	 *
	 */
	void onDisconnect () throws SQLException {
		if (con != null) {
			statusBar.setText (res.getString("Disconnecting_"));
			con.close();
			con=null;
			connectButton.setText (res.getString("Connect"));
			statusBar.setText (res.getString("Disconnected"));
		} // if
	} // method onDisconnect


	/**
	 * Used to handle button clicks
	 */
	class ButtonListener implements ActionListener{

		public void actionPerformed (ActionEvent e) {
			if (e.getSource() == browseButton) {			// check which buttone
				/* 
				JFileChooser chooser = new JFileChooser();
				int retval = chooser.showDialog(applicationFrame);
				*/
				FileDialog d = new FileDialog (
					applicationFrame, res.getString("Browse_Database"), FileDialog.LOAD);
				d.setVisible(true);
				urlText.setText (res.getString("jdbc_idb_")
					+d.getDirectory()+d.getFile());
			} else {										// must be the connect/disconnect button
				String oldStatus = statusBar.getText();		// save status in case of error
				try {
					if (con==null) {						// if not connected to a database
						statusBar.setText (res.getString("Connecting_"));
						statusBar.paintImmediately (0,0,1000,30);
						String theDriver = (String)driverList.getSelectedItem();
						Class.forName (theDriver).newInstance();// load the driver class
						if (userText.getText().equals(""))
							con = DriverManager.getConnection (urlText.getText());
						else
							con = DriverManager.getConnection (urlText.getText(),
                                         userText.getText(),
                                         passText.getText());
						parent.onConnect(con);
						connectButton.setText (res.getString("Disconnect"));
						statusBar.setText (res.getString("Connected_to")+urlText.getText());
					} else {								// must be a disconnect request
						onDisconnect();
					} // if-else
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, 
						ex.toString(), res.getString("Error"), JOptionPane.DEFAULT_OPTION);
					statusBar.setText(oldStatus);
				} // try-catch
			} // if-else
		} // method actionPerformed

	} // inner class ButtonListener 

} // class ConnectionPanel 

