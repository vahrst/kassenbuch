/*-----------------------------------------------------------------------------
 * Copyright (C) 1997-2001 Lutris Technologies, Inc.       All Rights Reserved.
 *
 * This software and documentation is the confidential and proprietary
 * information of Lutris Technologies, Inc. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with Lutris Technologies.
 *-----------------------------------------------------------------------------
 * $Id: ReflectMetaData.java,v 1.1.2.3 2001/05/23 22:26:43 andy Exp $
 *-----------------------------------------------------------------------------
 */
package com.lutris.instantdb.SQLBuilder;

// -> jdk1.1 imports
// import com.sun.java.swing.*;
// import com.sun.java.swing.table.*;
// import com.sun.java.swing.event.*;
// -> jdk1.2 imports
import java.lang.reflect.Method;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

/**
 * Used to populate a met-data table.
 * Reflects all the supplied objects methods and then invokes upon request.
 */
class ReflectMetaData extends AbstractTableModel {

	String			blank = "";
	static ResourceBundle res = ResourceBundle.getBundle("com.lutris.instantdb.SQLBuilder.Res");
	Method[]		mdMethods;					// the methods of the meta data class
	Object			metaData;					// the actual meta-data object
	Object[]		params;						// parameters to methods (if any)

	/**
	 * constructor
	 *
	 * Gets all the database meta data class methods
	 */
	 ReflectMetaData (Object o) {
		 super();
		 metaData = o;										// save the meta Data object
		 try {
			 Class<?> mdClass = metaData.getClass();
			 mdMethods = mdClass.getDeclaredMethods();
		 } catch (Exception e) {
		 } // try-catch
	 } // method dbmdModel

	/**
	 * constructor
	 *
	 * Used to set method parameter
	 */
	 ReflectMetaData (Object o, int i) {
		 this (o);
		 params = new Object[1];							// create room for one parameter
		 params [0] = new Integer (i);						// and set to integer value
	 } // method dbmdModel



	public int getColumnCount() {return 2;}
	public int getRowCount() {return mdMethods.length;} 

	/**
	 *
	 */
	String toBoolStr (Boolean b) {
		String[] strs = {res.getString("True"),res.getString("False")};
		if (b.booleanValue())
			return strs[0];
		else
			return strs[1];
	} // method toBoolStr

	public Object getValueAt(int row, int col) { 
		try {
			if (col==0) return mdMethods[row].getName();

			Method curMethod = mdMethods[row];
			Class<?>[] methodParams = curMethod.getParameterTypes();
			int paramCount = methodParams.length;

			// Check for a parameters
			Object[] theParams = null;
			if (paramCount > 1) return blank;
			if (paramCount == 1) {
				if (params==null) return blank;
				if (!methodParams[0].getName().equals(res.getString("int"))) return blank;
				theParams = params;
			} // if

			String returnType = curMethod.getReturnType().getName();
			if (returnType.equals(res.getString("boolean"))) {
				return toBoolStr ((Boolean)curMethod.invoke (metaData, theParams));
			} // if
			if (returnType.equals(res.getString("java_lang_String"))) {
				return curMethod.invoke (metaData, theParams);
			} // if
			if (returnType.equals(res.getString("int"))) {
				return curMethod.invoke (metaData, theParams);
			} // if
			return blank;
		} catch (Exception e) {
			System.out.println (e.toString());
			return blank;
		} // try-catch
	}

	public String getColumnName (int i) {
		if (i==0)
			return res.getString("Property");
		else
			return res.getString("Value");
	}
	
} // ReflectMetaData

