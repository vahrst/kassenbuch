/*-----------------------------------------------------------------------------
 * Copyright (C) 1997-2001 Lutris Technologies, Inc.       All Rights Reserved.
 *
 * This software and documentation is the confidential and proprietary
 * information of Lutris Technologies, Inc. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with Lutris Technologies.
 *-----------------------------------------------------------------------------
 * $Id: SQLBuilder.java,v 1.1.2.4 2001/05/23 22:26:43 andy Exp $
 *-----------------------------------------------------------------------------
 */
package com.lutris.instantdb.SQLBuilder;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;




public class SQLBuilder extends JPanel {
    static JFrame frame;
	static ResourceBundle res;
	static SQLBuilder panel ;

	static final int WIDTH = 640;
	static final int HEIGHT = 520;

	JTabbedPane				tabs;				// the main tabbed panel
	JLabel					statusBar;			// the status bar
	ConnectionPanel			connectPanel;		// the panel that handles connections
	QueryPanel				qPanel;				// panel where queries are done
	dbmdPanel				dbmd;				// displays the database meta data
	rsmdPanel				rsmd;				// displays the ResultSet meta data
	String[]				landfNames;			// names of available look and feels
	String[]				landfClassNames;	// names of available look and feel classes
	JRadioButton[]			landfButtons;		// the look and feel buttons

	/**
	 *
	 */
	JRadioButton addButton (String label, ButtonGroup g, String command, ActionListener a) {
		JRadioButton b = new JRadioButton (label);
		b.setActionCommand (command);
		g.add (b);
		add(b);
		b.addActionListener (a);
		return b;
	} // method addButton

	/**
	 * Accessor for statusBar.
	 */
	JLabel getStatusBar () {
		return statusBar;
	} // method getStatusBar

	/**
	 * accessor for frame.
	 */
	JFrame getFrame () {
		return frame;
	} // method getFrame


	/**
	 * Invoked by the connection panel when a connection
	 * is made to the database.
	 */
	void onConnect (Connection c) {
		qPanel.onConnect (c);
		dbmd.onConnect(c);
	} // method onConnect

	/**
	 * Invoked by the query panel when a query completes.
	 */
	void onQuery (ResultSet r) {
		rsmd.onQuery (r);
	} // method onQuery


    public SQLBuilder() {
		res = ResourceBundle.getBundle("com.lutris.instantdb.SQLBuilder.Res");


		// Create the buttons.
		ButtonGroup group = new ButtonGroup();
		RadioListener myListener = new RadioListener();
		statusBar = new JLabel (res.getString("Disconnected"));

		// find the available look and feels
		UIManager.LookAndFeelInfo[] lnfs = UIManager.getInstalledLookAndFeels();
		landfNames = new String[lnfs.length];
		landfClassNames = new String[lnfs.length];
		landfButtons = new JRadioButton[lnfs.length];
		for (int i=0; i<lnfs.length; i++) {
			landfNames[i] = lnfs[i].getName();
			landfClassNames[i] = lnfs[i].getClassName();
			landfButtons[i] = addButton (landfNames[i], group, landfClassNames[i], myListener);
			landfButtons[i].setEnabled(false);
		} // for

	

		tabs = new JTabbedPane ();
		connectPanel = new ConnectionPanel(this);
		dbmd = new dbmdPanel (this);
		qPanel = new QueryPanel(this);
		rsmd = new rsmdPanel (this);
		tabs.addTab (res.getString("Connection"),null,connectPanel,res.getString("Connects_Disconnects"));
		tabs.addTab (res.getString("Dbase_Data"),null,dbmd,res.getString("Database_Meta_data"));
		tabs.addTab (res.getString("Query"),null,qPanel,res.getString("Performs_database"));
		tabs.addTab (res.getString("Results_Data"),null,rsmd,res.getString("Result_Set_Meta_data"));

		tabs.setSelectedIndex(0);
    }


    /** An ActionListener that listens to the radio buttons. */
    class RadioListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String lnfName = e.getActionCommand();
			try {
				UIManager.setLookAndFeel(lnfName);
				SwingUtilities.updateComponentTreeUI(frame);
				frame.setSize(WIDTH, HEIGHT);
				//frame.pack();
			} catch (Exception exc) {
				JRadioButton button = (JRadioButton)e.getSource();
				button.setEnabled(false);
				System.err.println(res.getString("Could_not_load") + lnfName);
			}
			updateState();
		} // actionPerformed
	} // class RadioListener 

    public void updateState() {
		String lnfName = UIManager.getLookAndFeel().getName();
		for (int i=0; i<landfClassNames.length; i++) {
			if (landfNames[i].equals(lnfName)) {
				landfButtons[i].setSelected(true);
				break;
			} // if
		} // for
    }



    public static void main(String s[]) {
		panel = new SQLBuilder();

		frame = new JFrame(res.getString("Lutris_SQLBuilder"));
		
		frame.addWindowListener(new WindowEventHandler(panel));
		frame.setContentPane(panel.getContentPane());
//		frame.getContentPane().add(res.getString("North"), panel);
//		frame.getContentPane().add(res.getString("Center"), panel.tabs);
//		frame.getContentPane().add(res.getString("South"), panel.statusBar);
		frame.setSize(WIDTH,HEIGHT);
		frame.setVisible(true);
		
		panel.updateState();
    }
    
    public JPanel getContentPane(){
    	JPanel contentPane = new JPanel();
    	contentPane.setLayout(new BorderLayout());
    	contentPane.add(BorderLayout.NORTH, this);
    	contentPane.add(BorderLayout.CENTER, tabs);
    	contentPane.add(BorderLayout.SOUTH, statusBar);
    	
    	return contentPane;
    }
}

class WindowEventHandler extends WindowAdapter {

	SQLBuilder	panel;
	ResourceBundle res = ResourceBundle.getBundle("com.lutris.instantdb.SQLBuilder.Res");

	WindowEventHandler (SQLBuilder sqlbuild) {
		panel = sqlbuild;
	} // method WindowEventHandler

	public void windowClosing(WindowEvent e) {
		try {
			panel.connectPanel.onDisconnect();
		} catch (Exception ex) {
			System.out.println (res.getString("Error_disconnecting_")+ex.toString());
		} // try-catch
		System.exit(0);
	}
} // class WindowEventHandler 

