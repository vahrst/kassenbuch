package de.diakon.printertest;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import de.diakon.abschluss.prozesse.BuchungssatzKassenbuchdruck;


public class JasperTestMain {

	private static class KontoKstComparator implements Comparator<BuchungssatzKassenbuchdruck>{

		@Override
		public int compare(BuchungssatzKassenbuchdruck o1,
				BuchungssatzKassenbuchdruck o2) {

			long kk1 = o1.getKonto() * 100000 + o1.getKst();
			long kk2 = o2.getKonto() * 100000 + o2.getKst();
			
			if(kk1 > kk2){
				return 1;
			}else{
				if(kk1 < kk2){
					return -1;
				}else{
					return 0;
				}
			}
		}
		
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
//	InputStream in = JasperTestMain.class.getResourceAsStream("/kassenbuch.jrxml");
	InputStream in = JasperTestMain.class.getResourceAsStream("/debitoren.jrxml");

		JasperDesign jd = JRXmlLoader.load(in);
//		jd.setPageHeight(400);
		
		JasperReport report = JasperCompileManager.compileReport(jd);
		
//		InputStream in = JasperTestMain.class.getResourceAsStream("/kassenbuch.jasper");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("buchungsmonat", "11/2011");
		parameters.put("kassenbuch", "Test-Kassenbuch");
		
		JRDataSource dataSource = new JRBeanCollectionDataSource(getDataSorted()); 
		
		JasperPrint fillReport = JasperFillManager.fillReport(report, parameters, dataSource);
		
		//JasperPrintManager.printReport(fillReport, true);
		
		JasperViewer.viewReport(fillReport);
		
	}
	
	private static List<BuchungssatzKassenbuchdruck> getData(){
		List<BuchungssatzKassenbuchdruck> liste = new ArrayList<BuchungssatzKassenbuchdruck>();
		liste.add( new BuchungssatzKassenbuchdruck(Date.valueOf("2003-03-01"), 1, 44411, 100, null, BigDecimal.valueOf(23500, 2), "Beherbung Gäste", "Feriengäste") );
		liste.add( new BuchungssatzKassenbuchdruck(Date.valueOf("2003-03-01"), 2, 79100, 700, null, BigDecimal.valueOf(125, 2), "Kassendifferenz", null) );
		liste.add( new BuchungssatzKassenbuchdruck(Date.valueOf("2003-03-01"), 3, 510023, 0, BigDecimal.valueOf(2500, 2),null, "Braun, Wilhelm", "Taschengeld") );
		liste.add( new BuchungssatzKassenbuchdruck(Date.valueOf("2003-03-01"), 4, 79100, 100, BigDecimal.valueOf(2500, 2),null, "Kassendifferenz", null) );
		liste.add( new BuchungssatzKassenbuchdruck(Date.valueOf("2003-03-02"), 5, 79100, 100, BigDecimal.valueOf(35, 2),null, "Kassendifferenz", "nochmal eine Korrektur") );
		liste.add( new BuchungssatzKassenbuchdruck(Date.valueOf("2003-03-02"), 6, 79100, 700, null, BigDecimal.valueOf(123, 2), "Kassendifferenz", "test") );
		liste.add( new BuchungssatzKassenbuchdruck(Date.valueOf("2003-03-15"), 7, 64705, 700, BigDecimal.valueOf(13750, 2),null, "Vergütung/Honorar sonst. Dienste", null) );
		liste.add( new BuchungssatzKassenbuchdruck(Date.valueOf("2003-03-15"), 8, 13300, 0, null, BigDecimal.valueOf(500000, 2), "Kassenauffüllung / Verr.-konto", "Kassenauffüllung") );
		liste.add( new BuchungssatzKassenbuchdruck(Date.valueOf("2003-03-15"), 9, 44500, 100, null, BigDecimal.valueOf(1730, 2), "Einnahmen Cafe", null) );
		liste.add( new BuchungssatzKassenbuchdruck(Date.valueOf("2003-03-31"), 10, 12300, 700, null, BigDecimal.valueOf(3350, 2), "Forderungen MA", "Trullalala") );
		liste.add( new BuchungssatzKassenbuchdruck(Date.valueOf("2003-03-25"), 11, 510098, 0, null, BigDecimal.valueOf(7765, 2), "Kriebel, Charlotte", null) );
		
		return liste;
	}

	private static List<BuchungssatzKassenbuchdruck> getDataSorted(){
		List<BuchungssatzKassenbuchdruck> liste = getData();
		
		Collections.sort(liste, new KontoKstComparator());
		
		return liste;
	}

}
