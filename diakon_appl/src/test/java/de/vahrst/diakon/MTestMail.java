package de.vahrst.diakon;

import java.awt.Desktop;
import java.net.URI;
import java.net.URISyntaxException;

public class MTestMail {
	
	public static void main(String[] args) throws Exception{
		
		if (Desktop.isDesktopSupported()) {
      Desktop desktop = Desktop.getDesktop();
      // Now enable buttons for actions that are supported.
      if(desktop.isSupported(Desktop.Action.MAIL)){
      	StringBuilder sb = new StringBuilder();
      	sb.append("subject=Support mail");
      	sb.append("&");
      	sb.append("body=Trullalala und so weiter");
      	sb.append("&");
      	sb.append("Attach=\"/home/thomas/kassenbuch.csv\"");

      	String queryPart = uriTest3(sb.toString());
      	
      	URI uriMail = new URI("mailto:thomas@vahrst.de?" + queryPart );
      	desktop.mail(uriMail);
      	
      	
      }
      
		}        		
		
	}

	private static String uriTest3(String path) throws Exception {
    String encodedUri = null;
    URI uri = null;
        uri = new URI("http","bbc.co.uk","/search/news/",path,null);
    encodedUri = uri.getRawQuery();
    return encodedUri;
}
	
}
