package de.vahrst.application.types;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.Test;

/**
 * Testf�lle f�r Waehrungsbetrag
 * @author thomas
 *
 */
public class TestWaehrungsbetrag {
	
	@Test
	public void testFormat(){
		StringBuilder sb = new StringBuilder();
		// von -1200,00 bis 1200,00 testen: 
		for(int i=-120000; i<=120000; i++){
			Waehrungsbetrag b = new Waehrungsbetrag(i);
			
			String format = b.format();

			sb.setLength(0);
			
			int nk = Math.abs(i%100);
			int vk = Math.abs(i/100);
			
			if(i<0){
				sb.append("-");
			}
			
			if(vk > 999){
				sb.append(vk/1000).append(".");

				int vk1 = vk%1000;
				if(vk1 < 100){
					sb.append("0");
				}
				if(vk1 < 10){
					sb.append("0");
				}
				sb.append(vk1);
			}else{
				sb.append(vk);
			}
			sb.append(",");

			if(nk < 10){
				sb.append("0");
			}
			sb.append(nk);
			
			
//			BigInteger bi = BigInteger.valueOf(i);
//			BigDecimal bd = new BigDecimal(bi, 2);
			assertEquals("Fehler f�r i=" + i , sb.toString(), format);
		}
	}

	@Test
	public void testFormatOhneTausenderPunkt(){
		StringBuilder sb = new StringBuilder();
		// von -1200,00 bis 1200,00 testen: 
		for(int i=-120000; i<=120000; i++){
			Waehrungsbetrag b = new Waehrungsbetrag(i);
			
			String format = b.formatOhneTausenderPunkt();

			sb.setLength(0);

			int nk = Math.abs(i%100);
			int vk = Math.abs(i/100);
			
			if(i<0){
				sb.append("-");
			}
			
			sb.append(vk).append(",");

			if(nk < 10){
				sb.append("0");
			}
			sb.append(nk);
			
			
//			BigInteger bi = BigInteger.valueOf(i);
//			BigDecimal bd = new BigDecimal(bi, 2);
			assertEquals("Fehler f�r i=" + i , sb.toString(), format);
		}
	}
	
	@Test
	public void testFormat2(){
		Waehrungsbetrag b = new Waehrungsbetrag(123459);
		assertEquals("1.234,59", b.format());
	}
}
