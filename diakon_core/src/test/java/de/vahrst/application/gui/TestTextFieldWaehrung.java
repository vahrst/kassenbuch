package de.vahrst.application.gui;

import junit.framework.TestCase;

import de.vahrst.application.gui.TextFieldWaehrung;
import de.vahrst.application.types.Waehrungsbetrag;

/**
 * 
 * @author Thomas
 * @version 
 * @file TestTextFieldWaehrung.java
 */
public class TestTextFieldWaehrung extends TestCase {

	/**
	 * Constructor for TestTextFieldWaehrung.
	 * @param arg0
	 */
	public TestTextFieldWaehrung(String arg0) {
		super(arg0);
	}


	public void testRundungsfehler(){
		TextFieldWaehrung tfw = new TextFieldWaehrung();
	
		for(int cent =0; cent < 99999; cent++){
			Waehrungsbetrag a = new Waehrungsbetrag(cent);
			tfw.setBetrag( a);
			Waehrungsbetrag b = tfw.getBetrag();
			
			assertEquals(cent, b.getCent());
		}		
	}

	public void testMehrAlsZweiNachkommastellen(){
		TextFieldWaehrung tfw = new TextFieldWaehrung();
		
		tfw.setFormattedText("10,12345");
		assertEquals(1012, tfw.getBetrag().getCent());
		
		tfw.setFormattedText("10,555555");
		assertEquals(1056, tfw.getBetrag().getCent());
	}
}
