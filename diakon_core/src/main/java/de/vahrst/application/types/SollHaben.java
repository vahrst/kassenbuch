package de.vahrst.application.types;

/**
 * Werteobjekt mit den beiden Zustšnden Soll und Haben.
 * @author m500194
 *
 */
public class SollHaben extends Schluesselobjekt {


	/**
	 * Constructor for SollHaben.
	 */
	SollHaben(int id, String kbez, String lbez) {
		super(id, kbez, lbez);
	}

}
