package de.vahrst.application.types;

/**
 * Repräsentiert eine Fehlermeldung
 * @author Thomas
 * @version 
 * @file Fehlermeldung.java
 */
public class Fehlermeldung {

	private String message;
	private int nummer;
	
	/**
	 * optional: Dieses Feld kann Informationen zu einem
	 * Aspekt einer Entität aufnehmen
	 */
	private String aspekt;

	/**
	 * Konstruktor
	 */
	public Fehlermeldung(int nummer, String message) {
		this(nummer, message, null);
		
	}

	public Fehlermeldung(int nummer, String message, String aspekt){
		super();
		this.nummer = nummer;
		this.message = message;
		this.aspekt = aspekt;
	}

	public String getMessage(){
		return message;
	}
	
	public String getAspekt(){
		return aspekt;
	}
	
	public void setAspekt(String arg){
		aspekt = arg;
	}
	
	public int getNummer(){
		return nummer;
	}
}
