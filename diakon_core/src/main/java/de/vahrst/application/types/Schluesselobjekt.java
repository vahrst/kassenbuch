/*
 * Created on 13.01.2004
 *
 */
package de.vahrst.application.types;

/**
 * Allgemeine Superklasse f�r Schluesselobjekte
 * @author Thomas
 */
public abstract class Schluesselobjekt {
	private int id;
	private String kurzbezeichnung;
	private String langbezeichnung;

	/**
	 * Constructor for SollHaben.
	 */
	protected Schluesselobjekt(int id, String kbez, String lbez) {
		super();
		this.id = id;
		this.kurzbezeichnung = kbez;
		this.langbezeichnung = lbez;
	}

	/**
	 * Returns the id.
	 * @return int
	 */
	public int getId() {
		return id;
	}

	/**
	 * Returns the kurzbezeichnung.
	 * @return String
	 */
	public String getKurzbezeichnung() {
		return kurzbezeichnung;
	}

	/**
	 * Returns the langbezeichnung.
	 * @return String
	 */
	public String getLangbezeichnung() {
		return langbezeichnung;
	}

	/**
	 * String-Darstellung
	 */
	public String toString(){
		return getLangbezeichnung();
	}
	
}
