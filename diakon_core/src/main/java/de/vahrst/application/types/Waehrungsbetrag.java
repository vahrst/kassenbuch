package de.vahrst.application.types;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
/**
 * Kapselt einen W�hrungsbetrag.
 * @author Thomas
 * @version 
 */
public class Waehrungsbetrag {

	private int cent = 0;


	/**
	 * Constructor for Waehrungsbetrag.
	 */
	public Waehrungsbetrag() {
		super();
	}
	
	/** 
	 * erzeugt anhand des vorgegebenen Waehrungsbetrag ein
	 * neues Objekt.
	 */
	public Waehrungsbetrag(Waehrungsbetrag b){
		this(b.getCent());
	}
	
	public Waehrungsbetrag(int cent){
		this.cent = cent;
	}

	/**
	 * Gibt den Waehrungsbetrag als Cent-Wert zru�ck
	 * @return int
	 */
	public int getCent(){
		return cent;
	}
	
	/**
	 * liefert eine formatierte Darstellung des Betrags, im Format
	 * "#,##0.00"
	 * @return String
	 */
	public String format(){
		DecimalFormat formatter = new DecimalFormat("#,##0.00");
		String f = formatter.format(toBigDecimal());
		return f;
	}

	
	/**
	 * liefert eine formatierte Darstellung des Betrags, im Format
	 * "###0.00"
	 * @return String
	 */
	public String formatOhneTausenderPunkt(){
		DecimalFormat formatter = new DecimalFormat("###0.00");
		String f = formatter.format(toBigDecimal());
		return f;
	}

	
	/**
	 * liefert eine BigDecimal-Instanz des Waehrungsbetrags
	 * @return
	 */
	protected BigDecimal toBigDecimal(){
		BigInteger bi = BigInteger.valueOf(cent);
		BigDecimal bd = new BigDecimal(bi, 2);
		return bd;
	}
	
	
	/**
	 * liefert eine formatierte Darstellung des Betrags + 
	 * W�hrungszeichen (Euro)
	 * @return String
	 */
	public String formatWithCurrency(){
		DecimalFormat formatterCurrency = new DecimalFormat("#,##0.00 \u20ac");  //20ac = Euro-Zeichen
		String f = formatterCurrency.format(toBigDecimal());
		return f;
	}
	
	/** Addiert den vorgegeaben Waehrungsbetrag hinzu */
	public void add(Waehrungsbetrag b){
		this.cent += b.getCent();
	}
	
	/** subtrahiert den vorgegeben Waehrungsbetrag */
	public void minus(Waehrungsbetrag b){
		this.cent -= b.getCent();
	}
	
	/** invertiert das vorzeichen des Waherungsbetrags */
	public void invert(){
		this.cent = -cent;
	}
	
	public boolean equals(Object o){
		if(! (o instanceof Waehrungsbetrag)) return false;
		Waehrungsbetrag other = (Waehrungsbetrag) o;
		return other.cent == this.cent;	
	}
}
