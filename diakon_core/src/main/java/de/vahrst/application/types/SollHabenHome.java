package de.vahrst.application.types;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory-Klasse f�r die Werteobjekte SollHaben.
 * @author m500194
 *
 */
public class SollHabenHome {

	public static SollHaben SOLL = new SollHaben(0, "Soll", "Ausgabe");
	public static SollHaben HABEN = new SollHaben(1, "Haben", "Einnahme");

	/**
	 * Constructor for SollHabenHome.
	 */
	public SollHabenHome() {
		super();
	}


	public static List<SollHaben> getSollHabenListe(){
		List<SollHaben> liste = new ArrayList<SollHaben>();
		liste.add(SOLL);
		liste.add(HABEN);
		return liste;
	}
	
	public static SollHaben getById(int id){
		switch(id){
			case 0: return SOLL;
			case 1: return HABEN;
			default: return null;
		}
	}
}
