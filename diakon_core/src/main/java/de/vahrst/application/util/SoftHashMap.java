/*
 */
package de.vahrst.application.util;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * Hashmap, die softreferences halt. Diese kann z.B. als Cache f�r
 * gro�e Objekte verwendet werden, die vom GC bei Speicherknappheit
 * wieder entfernt werden k�nnen.
 * <p>
 * Ein Aufr�umen der Entrys geschieht bei jedem Zugriff (get/put)
 * <p>
 * Implementierung stammt von http://www.gruntz.ch/papers/references/SoftHashMap.java
 * @author m500194
 */
public class SoftHashMap extends AbstractMap {

	private Map m = null;
	private ReferenceQueue q = new ReferenceQueue();

	public SoftHashMap() {
		m = new HashMap();
	}
	public SoftHashMap(int initialCapacity) {
		m = new HashMap(initialCapacity);
	}
	public SoftHashMap(int initialCapacity, float loadFactor) {
		m = new HashMap(initialCapacity, loadFactor);
	}

	public Object get(Object key) {
		Object res = m.get(key);
		return res == null ? null : ((Reference) res).get();
	}

	public Object put(Object key, Object value) {
		processQueue();
		Reference ref = new SoftEntry(key, value, q);
		Object res = m.put(key, ref);
		return res == null ? null : ((Reference) res).get();
	}

	private Set entrySet = null;

	public Set entrySet() {
		if (entrySet == null)
			entrySet = new EntrySet();
		return entrySet;
	}

	private void processQueue() {
		Reference r;
		while ((r = q.poll()) != null) {
			SoftEntry e = (SoftEntry) r;
			m.remove(e.key);
		}
	}

	public int size() {
		return entrySet().size();
	}

	public Object remove(Object key) {
		processQueue();
		Object res = m.remove(key);
		return res == null ? null : ((Reference) res).get();
	}

	public void clear() {
		processQueue();
		m.clear();
	}

	private static class SoftEntry extends SoftReference {
		private Object key; // neccessary so that freed objects can be removed
		private SoftEntry(Object key, Object value, ReferenceQueue q) {
			super(value, q);
			this.key = key;
		}
	}

	private class EntrySet extends AbstractSet {
		private Set entrySet = m.entrySet();

		public int size() {
			int s = 0;
			for (Iterator i = iterator(); i.hasNext(); i.next())
				s++;
			return s;
		}

		public boolean isEmpty() {
			// default implementation computes size which is inefficient
			return !(iterator().hasNext());
		}

		public boolean remove(Object o) {
			processQueue();
			return super.remove(o);
		}

		public Iterator iterator() {

			return new Iterator() {
				Iterator it = entrySet.iterator();
				Entry next = null;
				Object value = null;
				/* Strong reference to key, so that the GC
											will leave it alone as long as this Entry exists */

				public boolean hasNext() {
					while (it.hasNext()) {
						final Entry e = (Entry) it.next();
						SoftEntry se = (SoftEntry) e.getValue();
						value = null;
						if ((se != null) && ((value = se.get()) == null)) {
							/* Weak key has been cleared by GC */
							continue;
						}
						next = new Map.Entry() {
							public Object getKey() {
								return e.getKey();
							}
							public Object getValue() {
								return value;
							}
							public Object setValue(Object v) {
								Object res = value;
								value = v;
								e.setValue(new SoftEntry(e.getKey(), value, q));
								return res;
							}
							public boolean equals(Object x) {
								if (!(x instanceof Map.Entry))
									return false;
								Map.Entry e = (Map.Entry) x;
								Object key = getKey();
								return key == null ? e.getKey() == null : key.equals(e.getKey())
									&& value == null ? e.getValue() == null : value.equals(e.getValue());
							}

							public int hashCode() {
								Object key = getKey();
								return (((key == null) ? 0 : key.hashCode()) ^ ((value == null) ? 0 : value.hashCode()));
							}

						};
						return true;
					}
					return false;
				}

				public Object next() {
					if ((next == null) && !hasNext())
						throw new NoSuchElementException();
					Entry e = next;
					next = null;
					return e;
				}

				public void remove() {
					it.remove();
				}

			};
		}
	}
}
