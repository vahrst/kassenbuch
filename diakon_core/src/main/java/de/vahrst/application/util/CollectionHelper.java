package de.vahrst.application.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * Stellt diverse Services im Zusammenhang mit Collections wie Vector, Array
 * etc. bereit.
 * <p>
 * 
 * @author Guyet, Sven
 * @version 1.0.1 (Guyet, Sven - 30.07.2002 10:12)
 */
public abstract class CollectionHelper {
	/**
	 * Erweitere den �bergebenen Vector um alle Elemente des �bergebenen Arrays
	 * und gebe den Vector zur�ck.
	 * 
	 * @return java.util.Vector der erweiterte Vector
	 * @param aVector
	 *          java.util.Vector der zu erweiternde Vector
	 * @param anArray
	 *          java.lang.Object[] das Array mit den an den Vector anzuh�ngenden
	 *          Elementen
	 *          <p>
	 * @author Guyet, Sven
	 */
	public static Vector<?> append(Vector<Object> aVector, Object[] anArray) {
		for (int i = 0; i < anArray.length; ++i)
			aVector.addElement(anArray[i]);
		return aVector;
	}

	/**
	 * Erweitere den �bergebenen Vector um das �bergebene Element und gebe den
	 * Vector zur�ck.
	 * 
	 * @return java.util.Vector der erweiterte Vector
	 * @param aVector
	 *          java.util.Vector der zu erweiternde Vector
	 * @param anObject
	 *          java.lang.Object das an den Vector anzuh�ngende Element
	 *          <p>
	 * @author Guyet, Sven
	 */
	public static Vector<?> append(Vector<Object> aVector, Object anObject) {
		aVector.addElement(anObject);
		return aVector;
	}

	/**
	 * Gebe ein Object-Array zur�ck, welches die Elemente des �bergebenen Vectors
	 * enth�lt.
	 * 
	 * @return java.lang.Object[] das Array mit den Elementen des Vectors
	 * @param aVector
	 *          java.util.Vector der in ein Array zu konvertierende Vector
	 *          <p>
	 * @author Guyet, Sven
	 */
	public static Object[] asArray(Vector<Object> aVector) {
		int count = aVector.size();
		Object[] anArray = new Object[count];
		Enumeration<Object> e = aVector.elements();

		for (int i = 0; i < count; i++)
			anArray[i] = e.nextElement();
		return anArray;
	}

	/**
	 * Liefert den Inhalt des vorgegebenen Iterators als List zur�ck.
	 * 
	 * @return List
	 * @param iter
	 *          Iterator
	 */
	public static List<?> asList(Iterator<Object> iter) {
		List<Object> list = new ArrayList<Object>();
		while (iter.hasNext()) {
			list.add(iter.next());
		}

		return list;
	}

	/**
	 * Liefert den Inhalt des vorgegebenen Arrays als List zur�ck.
	 * 
	 * @return List
	 * @param a
	 *          Object[]
	 */
	public static List asList(Object[] a) {
		ArrayList list = new ArrayList(a.length);
		for (int i = 0; i < a.length; i++) {
			list.add(a[i]);
		}
		return list;
	}

	/**
	 * Gebe ein String-Array zur�ck, welches die Elemente der �bergebenen
	 * Collection enth�lt.
	 * 
	 * @return String[] Das Array mit den Elementen der Collection
	 * @param collie
	 *          Collection Die in ein Array zu konvertierende Collection. Alle
	 *          Elemente m�ssen auf String gecastet werden k�nnen.
	 *          <p>
	 * @author Vahrst, Thomas
	 */
	public static String[] asStringArray(Collection collie) {
		return (String[]) collie.toArray(new String[collie.size()]);
	}

	/**
	 * Pr�ft ob das �bergebene Objekt im Objekt-Array vorhanden ist und gibt das
	 * aus dem Array zur�ck. ACHTUNG: In dieser Methode wird linear gesucht. Bei
	 * Arrays, deren Inhalt sortiert ist, empfiehlt es sich, die search-MEthode
	 * aus java.util.Arrays zu verwenden. <br>
	 * MERKE : Intern wird die equals() Methode aufgerufen um die Objekte zu
	 * vergleichen. Wenn also in einem beliebigen Objekt die equals() Methode
	 * nicht implementiert ist kann es sein, da� das Objekt nicht gefunden wird.
	 * Dann kommt es hier ggf. zu einem falschen Ergebnis. Siehe dazu auch ->
	 * Object.equals(Object o)
	 */
	public static final Object contains(Object[] objects, Object object) {
		if ((objects == null) || (objects.length < 1) || (object == null))
			return null;

		Class arrayTyp = objects.getClass().getComponentType();
		// pr�fen, ob das vorgebene Objekt �berhautpt als Typ im Array vorkommen
		// kann:
		if (!(object.getClass().isAssignableFrom(arrayTyp))) {
			return null;
		}

		int anzahlObjekte = objects.length;

		for (int i = 0; i < anzahlObjekte; i++) {
			Object o = objects[i];

			if (object.equals(o)) {
				return o;
			}
		}
		return null;
	}

	/**
	 * Pr�ft, ob der �bergebene String im String-Array vorhanden ist und gibt den
	 * betreffenden String aus dem Array zur�ck. MERKE : Intern wird die equals()
	 * bzw. equalsIgnoreCase() Methode aufgerufen um die Objekte zu vergleichen.
	 * Siehe dazu auch -> Object.equals(Object o)
	 */
	public static final String contains(String[] stringArray, String s,
			boolean ignoreCase) {
		if ((stringArray == null) || (stringArray.length < 1) || (s == null))
			return null;

		int anzahlStrings = stringArray.length;

		for (int i = 0; i < anzahlStrings; i++) {
			String element = stringArray[i];
			if (ignoreCase) {
				if (s.equalsIgnoreCase(element)) {
					return element;
				}
			} else {
				if (s.equals(element)) {
					return element;
				}
			}
		}
		return null;
	}

	/**
	 * Gebe ein String-Array zur�ck, welches die Elemente des �bergebenen Vectors
	 * enth�lt.
	 * 
	 * @return java.lang.String[] das Array mit den Elementen des Vectors
	 * @param aVector
	 *          java.util.Vector der in ein Array zu konvertierende Vector. Alle
	 *          Vector-Elemente m�ssen auf String gecastet werden k�nnen.
	 *          <p>
	 * @author Guyet, Sven
	 */
	public static String[] asStringArray(Vector aVector) {
		int count = aVector.size();
		String[] anArray = new String[count];
		Enumeration e = aVector.elements();

		for (int i = 0; i < count; i++)
			anArray[i] = (String) e.nextElement();
		return anArray;
	}

	/**
	 * Sortiere die �bergebene Elementmenge und gebe einen Iterator auf das
	 * Ergebnis zur�ck.
	 * <p>
	 * Erfordert eine Collection von Elementen, die sortierbar sind (also das
	 * Comparable Interface implementieren); anderenfalls muss ein expliziter
	 * Comparator mitgegeben werden.
	 * <p>
	 * Beschreibung: Auch wenn's auf den ersten Blick umst�ndlich erscheint, ist
	 * diese Implementierung schneller, als z.B. direkt eine sortierte Collection
	 * wie TreeSet zu nehmen und die Elemente dort einzuf�gen.
	 * 
	 * @return java.util.Iterator Iterator der sortierten Ergebnismenge
	 * @param coll
	 *          java.util.Collection Die zu sortierende Elementmenge von Objekten,
	 *          die das Comparable Interface implementieren oder, falls dies nicht
	 *          der Fall ist, vom mitgegebenen comparator sortiert werden k�nnen.
	 * @param comparator
	 *          java.util.Comparator (optionales) Objekt, welches die
	 *          Sortierreihenfolge festlegt. Ist dieser Parameter null, werden die
	 *          Objekte aus coll anhand ihrer Standardsortierung sortiert, d.h.
	 *          die Objekte m�ssen in diesem Fall das Comparable Interface
	 *          implementieren.
	 *          <p>
	 * @author Guyet, Sven
	 */
	public final static Iterator sort(Collection coll, Comparator comp) {
		return asSortedCollection(coll, comp).iterator();
	}

	/**
	 * Sortiere die �bergebene Elementmenge und gebe einen Iterator auf das
	 * Ergebnis zur�ck.
	 * <p>
	 * Erfordert einen Iterator einer Collection von Elementen, die sortierbar
	 * sind (also das Comparable Interface implementieren); anderenfalls muss ein
	 * expliziter Comparator mitgegeben werden.
	 * <p>
	 * Beschreibung: Auch wenn's auf den ersten Blick umst�ndlich erscheint, ist
	 * diese Implementierung schneller, als z.B. direkt eine sortierte Collection
	 * wie TreeSet zu nehmen und die Elemente dort einzuf�gen.
	 * 
	 * @return java.util.Iterator Iterator der sortierten Ergebnismenge
	 * @param it
	 *          java.util.Iterator Iterator f�r die zu sortierende Elementmenge
	 *          von Objekten, die das Comparable Interface implementieren oder,
	 *          falls dies nicht der Fall ist, vom mitgegebenen comparator
	 *          sortiert werden k�nnen.
	 * @param comp
	 *          java.util.Comparator (optionales) Objekt, welches die
	 *          Sortierreihenfolge festlegt. Ist dieser Parameter null, werden die
	 *          Objekte des Iterators anhand ihrer Standardsortierung sortiert,
	 *          d.h. die Objekte m�ssen in diesem Fall das Comparable Interface
	 *          implementieren.
	 *          <p>
	 * @author Guyet, Sven
	 */
	public final static Iterator sort(Iterator it, Comparator comp) {
		ArrayList tempArray = new ArrayList();
		while (it.hasNext())
			tempArray.add(it.next());
		return sort(tempArray, comp);
	}

	/**
	 * Sortiere die �bergebene Elementmenge und gebe die resultierende Collection
	 * zur�ck.
	 * <p>
	 * Erfordert eine Collection von Elementen, die sortierbar sind (also das
	 * Comparable Interface implementieren); anderenfalls muss ein expliziter
	 * Comparator mitgegeben werden.
	 * <p>
	 * Beschreibung: Auch wenn's auf den ersten Blick umst�ndlich erscheint, ist
	 * diese Implementierung schneller, als z.B. direkt eine sortierte Collection
	 * wie TreeSet zu nehmen und die Elemente dort einzuf�gen.
	 * 
	 * @return java.util.Collection Eine Collection der sortierten Elemente aus
	 *         coll
	 * @param coll
	 *          java.util.Collection Die zu sortierende Elementmenge von Objekten,
	 *          die das Comparable Interface implementieren oder, falls dies nicht
	 *          der Fall ist, vom mitgegebenen comparator sortiert werden k�nnen.
	 * @param comparator
	 *          java.util.Comparator (optionales) Objekt, welches die
	 *          Sortierreihenfolge festlegt. Ist dieser Parameter null, werden die
	 *          Objekte aus coll anhand ihrer Standardsortierung sortiert, d.h.
	 *          die Objekte m�ssen in diesem Fall das Comparable Interface
	 *          implementieren.
	 *          <p>
	 * @author Guyet, Sven
	 */
	public final static Collection asSortedCollection(Collection coll,
			Comparator comp) {
		Object[] tempArray = coll.toArray();
		if (comp == null) {
			Arrays.sort(tempArray);
		} else {
			Arrays.sort(tempArray, comp);
		}
		return Arrays.asList(tempArray);
	}

	/**
	 * Gebe ein Object-Array zur�ck, welche eine Teilmenge der Elemente des
	 * �bergebenen Arrays enth�lt, und zwar 'count' Elemente, beginnend an der
	 * Position 'offset'.
	 * 
	 * @return java.lang.Object[] das neue Array mit der Element-Teilmenge
	 * @param anArray
	 *          java.lang.Object[] das Array, aus welchem die Teilmenge zu
	 *          entnehmen ist
	 * @param offset
	 *          int Position des ersten, zu �bernehmenden Elements in anArray
	 * @param count
	 *          int Anzahl der von anArray zu �bernehmenden Elemente
	 *          <p>
	 * @author Guyet, Sven
	 */
	public static Object[] subArray(Object[] anArray, int offset, int count) {
		Object[] newArray = new Object[count];
		System.arraycopy(anArray, offset, newArray, 0, count);
		return newArray;
	}
	
	/**
	 * entfernt alle null-Referenzen aus der vorgegebenen ArrayList und
	 * liefert eine neue ArrayList zur�ck.
	 * <p>
	 * Dieser Service wurde bewusst f�r den Typ ArrayList implementiert. 
	 * F�r eine allgemeing�ltigere Version (d.h. f�r java.util.List) 
	 * 
	 * @param liste
	 * @return
	 */
	public static ArrayList trim(ArrayList liste){
		if(liste == null){
			return new ArrayList(0);
		}
		
		ArrayList result = new ArrayList(liste.size());
		for(int i=0;i<liste.size();i++){
			if(liste.get(i) != null){
				result.add(liste.get(i));
			}
		}
		
		return result;
	}
	
}