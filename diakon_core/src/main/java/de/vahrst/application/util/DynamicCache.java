package de.vahrst.application.util;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

/**
 * 'intelligenter' Cache, der immer die zuletzt ben�tigten Elemente
 * (bis zu einer maximal-Anzahl' im Speicher vorh�lt.
 * <p>
 * Strategie:<br>
 * Der Datenpuffer ist in Form einer verketteten Liste organisiert, wobei
 * immer das zuletzt zugegriffene Element an den Anfang der Kette gestellt wird.
 * Wenn neue Elemente gespeichert werden sollen, wird immer das letzte Element der
 * Kette entfernt.
 * <p>
 * Die Speicherung von Elementen geschieht in zwei Stufen. Wenn ein
 * Element neu in den Cache gestellt wird, wird es zuerst in eine Liste
 * f�r Neuzug�nge gestellt. Erst beim zweiten (bzw. x-ten)
 * Zugriff wird das Element in die Hauptliste gestellt. Auf diese Weise wird vermieden,
 * dass durch ein kurzfristiges Setzen von vielen Elementen in den Cache alle 'alt eingesessenen'
 * Elemente rausgekegelt werden.
 * <p>
 * Bei jedem Zugriff wird immer in beiden Datenpuffern nach dem passenden Element gesucht. Die Suche geschieht
 * sequentiell in den beiden LinkedLists, daher sollte die Kapazit�t des Caches nicht zu gro� gew�hlt werden.
 * <p>
 * Creation date: (04.02.02 10:32:10)
 * @author: Vahrst, Thomas
 */
public class DynamicCache implements Serializable {
	/** 
	 * die max. Anzahl Eintrage je Datenpuffer. Dies bezieht sich auf den Hauptpuffer und
	 * den Neuzugangs-Puffer. Das bedeutet, der Cache kann max. die doppelte Anzahl Elemente
	 * halten
	 */
	private int maxElements = 20;

	/**
	 * ab wann geh�rt ein Element 'zum alten Eisen', d.h. ab dem wievielten Zugriff wird
	 * das Element von der zugangsliste in die Liste der 'etablierten' Element �bertragen
	 * (Das Setzen eines neuen Elements in den Cache z�hlt auch bereits als Zugriff)
	 */
	private int anzahlZugriffeAlsNeuzugang = 1;
	
	/**
	 * Liste der neu gesetzten Elemente. Das neueste Element steht auf Position 0.
	 */
	private final LinkedList neuzugangsListe = new LinkedList();

	/**
	 * Liste derjenigen Elemente, auf die bereits mehrfach zugegriffen wurde.
	 * Das zuletzt zugegriffene Element steht auf Position 0.
	 */
	private final LinkedList hauptliste = new LinkedList();

	/**
	 * Name des Caches. 
	 */
	private String name;
	
	/**
	 * Default-Konstruktor. Erzeugt einen DynamicCache mit
	 * maxElements = 20 und anzahlZugriffeAlsNeuzugang = 1
	 * Creation date: (04.02.02 15:28:25)
	 */
	public DynamicCache(String name) {
		super();
		this.name = name;	
	}

	/**
	 * DynamicCache Konstruktor. Erzeugt einen DynamicCache mit den vorgegebenen Parametern.
	 */
	public DynamicCache(String name, int maxElements, int anzahlZugriffeAlsNeuzugang) {
		this(name);
		this.maxElements = maxElements;
		this.anzahlZugriffeAlsNeuzugang = anzahlZugriffeAlsNeuzugang;
	}

	/**
	 * �ber diese Methode kann ein Testprogramm (junit) an die internen
	 * Daten dieses Caches gelangen. Die daten werden an das mitgegebene TestInterface
	 * �bergeben.
	 * Creation date: (04.02.02 14:21:34)
	 * @param ti test.vahrst.cache.TestInterface
	 */
	public void _testConnector(DynamicCacheTestInterface ti) {
		ti.setVariablen(neuzugangsListe, hauptliste);	
	}

	/**
	 * l�scht den Cache
	 * Creation date: (04.02.02 15:09:49)
	 */
	public void clear() {
		neuzugangsListe.clear();
		hauptliste.clear();	
	}

	/**
	 * sucht in der vorgegebenen Liste nach einem Element mit dem vorgegebenen Key und liefert die Position des
	 * gesuchten Elements zur�ck. -1, wenn nicht gefunden. Key-Abfrage mit 'equals'
	 * Creation date: (04.02.02 14:40:48)
	 * @return int
	 * @param key java.lang.Object
	 */
	private int find(LinkedList list, Object key) {
		Iterator iter = list.iterator();
		int zaehler = 0;
		while(iter.hasNext()){
			DynamicCacheEntry element  = (DynamicCacheEntry) iter.next();
			if (element.getKey() != null && element.getKey().equals(key)) {
				return zaehler;
			}
			zaehler++;
		}
		return -1;
	}

	/**
	 * Liefert den Wert, der zu dem gesuchten Key passt, falls dieser im
	 * Cache vorliegt. Sonst wird null zur�ckgegeben.
	 * Creation date: (04.02.02 11:19:55)
	 * @return java.lang.Object
	 * @param key java.lang.Object
	 */
	public synchronized Object get(Object key) {
		LinkedList sucheListe = neuzugangsListe;
		
		// wir suchen erstmal nach dem Element:
		int pos = find(sucheListe, key);
		if(pos == -1){
			sucheListe = hauptliste;		
			pos = find(sucheListe, key);
		}
	
		if (pos == -1) {
			return null;
		}
	
		DynamicCacheEntry element = (DynamicCacheEntry) sucheListe.remove(pos);
		if (sucheListe.equals(neuzugangsListe)) {
			// der Key befindet sich noch in der Neuzugangsliste
			element.incrementAccessCount();
	
			// mu� der Key nun in die Hauptliste?
			if (element.getAccessCount() > anzahlZugriffeAlsNeuzugang) {  // jau
				hauptliste.addFirst(element);
				if(hauptliste.size() > maxElements) {
					hauptliste.removeLast();
				}
			} else {
				neuzugangsListe.addFirst(element);
			}
		} else {
			// der Key geh�rt bereits zu den alt eingesessenen Keys
			element.incrementAccessCount();
			hauptliste.addFirst(element);
			if (hauptliste.size() > maxElements) {
				hauptliste.removeLast();
			}
		}
		
		return element.getValue();
	}

	public String getName() {
		return name;
	}

	/**
	 * Liefert einen Set mit allen Keys dieses Caches.
	 * Creation date: (20.03.03 16:30:03)
	 * @return java.util.Set
	 */
	public Set keySet() {
		HashSet hs = new HashSet(maxElements << 1);
	
		Iterator iter = neuzugangsListe.iterator();
		while(iter.hasNext()){
			DynamicCacheEntry entry = (DynamicCacheEntry) iter.next();
			hs.add(entry.getKey());
		}
	
		iter = hauptliste.iterator();
		while(iter.hasNext()){
			DynamicCacheEntry entry = (DynamicCacheEntry) iter.next();
			hs.add(entry.getKey());
		}
		return hs;
	}

	/**
	 * speichert den Key mit dem passenden Value im Cache. Falls dieser Key bereits existiert, wird
	 * nur der Value angepa�t. Es findet keine �nderung der Position des Elements im Cache statt.
	 * Neue Elemente werden in der neuzugangsListe vorne eingef�gt.
	 * Creation date: (04.02.02 13:57:09)
	 * @param key java.lang.Object
	 * @param value java.lang.Object
	 */
	public synchronized void put(Object key, Object value) {
		LinkedList sucheListe = neuzugangsListe;
		
		// wir suchen erstmal nach dem Element:
		int pos = find(sucheListe, key);
		if (pos == -1) {
			sucheListe = hauptliste;		
			pos = find(sucheListe, key);
		}
			
		// gefunden, nur value �ndern:
		if (pos != -1) {
			// Key ist bereits vorhanden
			DynamicCacheEntry element = (DynamicCacheEntry) sucheListe.get(pos);
			element.setValue(value);
			return;
		}
		DynamicCacheEntry element = new DynamicCacheEntry(key, value);
		neuzugangsListe.addFirst(element);
		if (neuzugangsListe.size() > maxElements) {
			neuzugangsListe.removeLast();
		}
	
		element.incrementAccessCount();		
	}
}