package de.vahrst.application.util;
/**
 * Interface f�r Objekte, die vom DiffHelper behandelt werden k�nnen.
 * Neben der aus Comparable geerbten Methode compareTo() muss ein solches Objekt
 * auch die Methode getKey() implementieren.
 * Creation date: (06.12.03 12:08:03)
 * @author: Guyet, Sven
 */
public interface Diffable extends Comparable {

	
/**
 * Liefert den Key dieeses Objektes. Wenn der Key zweier Diffable-Objekt identisch ist, handelt es sich um
 * zwei Versionen der gleiche logischen Instanz (z.B. Kundennummer, Termin-Id etc.)
 * Creation date: (06.12.03 12:12:55)
 * @return java.lang.Comparable
 */
Comparable getKey();
}