package de.vahrst.application.util;
/**
 * Diese Klasse enth�lt Service-Methoden, um bytes oder Byte-Arrays in Strings
 * zu formatieren oder formatiertes Strings zu byte-Arrays zu konvertieren.
 * <p>
 * @author Vahrst, Thomas
 * @version 1.0 (Guyet, Sven - 16.08.2001 14:03) 
 */
public abstract class ByteFormatter {
	 private static char hexDigit[] =  { '0','1','2','3', '4','5','6','7','8','9','A','B','C','D','E','F' };
	 private static int numberOfBytesPerLine = 16;  // 16 Hex-Werte in eine Zeile schreiben.


	/**
	 * Erzeugt aus einem Zeichen '0' bis '9' bzw 'a' - 'f' oder 'A'-'F'
	 * den entsprechenden numerischen Wert (0-15).
	 */
	public static int charToHexValue(char c){
		switch(c){
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				return c-48;
			case 'a':
			case 'A':
				return 10;
			case 'b':
			case 'B':
				return 11;
			case 'c':
			case 'C':
				return 12;
			case 'd':
			case 'D':
				return 13;
			case 'e':
			case 'E':
				return 14;
			case 'f':
			case 'F':
				return 15;
			default:
				throw new RuntimeException("ung�ltiges Hex-zeichen");
		}
	}

	/**
	 * erzeugt aus dem vorgegebenen Byte eine Stringdarstellung, 
	 * bestehend aus zwei Zeichen, jeweils im Bereich '0'-'9' und 'A'-'F'.
	 * <p>
	 * Beispiel:<br>
	 * 127 => '7F'<br>
	 * 128 => '80'<br>
	 * 255 => 'FF'
	 * 
	 * @param b
	 * @return
	 */
	public static String formatByte(byte b){
	  char[] array = { hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] };
	  return new String(array);
		
	}
	
	/**
	 * erzeugt aus dem vorgegebenen Byte-Array eine Stringdarstellung,
	 * wobei jedes Byte in Form von zwei Zeichen - jeweils im Bereich 
	 * '0'-'9' und 'A'-'F' dargestellt wird.
	 * @param bytes
	 * @return
	 */
	public static String formatBytes(byte[] bytes){
		if(bytes == null){
			return "";
		}
		
		char[] array = new char[bytes.length*2];
		for(int i=0,i2=0;i<bytes.length;i++, i2+=2){
			byte b = bytes[i];
			array[i2] = hexDigit[(b >> 4) & 0x0f];
			array[i2+1] = hexDigit[b & 0x0f];
		}
		return new String(array);
	}
	
	/**
	 * liefert eine Stringdarstellung des vorgegebenen Short-Wertes
	 */
	static public String formatShortValue(short arg) {
	  return formatBytes(NumericHelper.shortToByte(arg));
	}

	/**
	 * erzeugt aus einem String mit Hex-Zeichen ("A0F31B...") das
	 * entsprechende byte-Array.
	 * @param arg
	 * @return
	 */
	public static byte[] parseByteString(String daten){
		if(daten == null){
			return new byte[0];
		}
		byte[] result = new byte[daten.length() / 2];
		for (int i = 0, j = 0; i < daten.length(); i += 2, j++) {
			int wert1 = ByteFormatter.charToHexValue(daten.charAt(i));
			int wert2 = ByteFormatter.charToHexValue(daten.charAt(i + 1));
			result[j] = (byte) (wert1 * 16 + wert2);
		}
		return result;
	}
	
	
	/**
 	 * liefert eine formatierte Stringdarstellung des Byte-Arrays im
	 * Hex-Format, jeweils 16 Bytes pro Zeile, jedes Byte mit einem Blank
	 * getrennt.
	 * @param bytes
	 * @return
	 */
	public static String printBytes(byte[] bytes){
		return printBytes(bytes, bytes.length);
	}
	
	
	/**
	 * liefert eine formatierte Stringdarstellung des Byte-Arrays im
	 * Hex-Format, jeweils 16 Bytes pro Zeile, jedes Byte mit einem Blank
	 * getrennt.
	 * Creation date: (22.11.01 10:14:49)
	 * @return java.lang.String
	 * @param bytes byte[]
	 * @param laenge die Anzahl der auszugebenden bytes. Wenn laenge == 0
	 *    ist, wird das gesamte byte-Array ausgegeben.
	 */
	public static String printBytes(byte[] bytes, int laenge) {
		
		if(laenge < 0 || laenge > bytes.length)
			laenge = bytes.length;

		StringBuffer sb = new StringBuffer(laenge*3 + laenge / numberOfBytesPerLine);
		
		int byteZaehler = 0;
		for(int i=0; i<laenge; i++){
			sb.append(formatByte(bytes[i])).append(" ");
			
			if(++byteZaehler == numberOfBytesPerLine){
				byteZaehler = 0;
				sb.append("\n");
			}
		}
		
		return sb.toString();
	}
	
}