package de.vahrst.application.util;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
/**
 * Stellt Hilfsroutinen f�r zeitbezogene Objekte und Klassen
 * wie Date und Timestamp zur Verf�gung.
 * <p>
 * @see java.util.Date
 * @see java.sql.Date
 * @see java.sql.Time
 * @see java.sql.Timestamp
 * <p>
 * @author Guyet, Sven (IBM)
 * @version 1.119 
 */
public final class TimeAndDateHelper {
	/** Hilfsvariable fuer Zeitberechnungen */
	private static Calendar staticCal = null;
	
	/** Hilfsvariable fuer den minimalen Timestamp */
	private static Timestamp minimumTimestamp = null;
		
	private static final String TIMESTAMP_FORMAT_ERROR = "Timestamp format must be yyyy-MM-dd-HH.mm.ss.SSSSSS";
	private static final String DATE_OR_TOKEN_PARSE_ERROR = "Invalid date or token string: ";

	/**
	 * Standard-Konstruktor ist private, da diese Klasse nur statische Services
	 * bereitstellt und niemals instanziiert wird.
	 */
	private TimeAndDateHelper() {
		// Verhindere Instanziierung
	}

	/**
	 * Baue anhand des uebergebenen Formatstrings und den uebergebenen
	 * Werten fuer Tag, Monat und Jahr einen Datumsstring auf und gebe diesen
	 * zurueck. Diese Methode ist effizienter und ressourcenschonender
	 * als die sehr allgemeingueltigen Java-Mechanismen wie z.B. in SimpleDateFormat.
	 * <p>
	 * Fuer naehere Informationen @see #dateFormat(int, int, int, String, String)
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	dateFormat( {12, 4, 23},    "d.m.y")         liefert "12.4.1923" (*)
	 * 	dateFormat( {12, 4, 23},    "d.m.yy")        liefert "12.4.23"
	 * 	dateFormat( {12, 4, 23},    "d.m.yyy")       liefert "12.4.1923" (*)
	 * 	dateFormat( {12, 4, 23},    "d.m.yyyy")      liefert "12.4.1923" (*)
	 * 	dateFormat( {12, 4, 23},    "d.m.yyyyyy")    liefert "12.4.001923" (*)
	 * 	dateFormat( {12, 10, 2023}, "d.m.yy")        liefert "12.10.2023"
	 * 	dateFormat( {12, 10, 2023}, "dd.mm.yyyy")    liefert "12.10.2023"
	 * 	dateFormat( {12, 10, 2023}, "ddd.mmm.yyyyy") liefert "012.010.02023"
	 * (*) wenn der Aufruf vor dem 13.4.2003 erfolgt; nach dem 12.4.2003 wird
	 *     als Jahreszahl 20 eingesetzt.
	 * </pre>
	 * @return java.lang.String
	 *		der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param datum int[]
	 *		Ein Array mit 3 Elementen: Tag (je nach Monat/Jahr ein Wert zwischen 1 und 31),
	 *    Monat (ein Wert zwischen 1 und 12) und Jahr (ein Wert ungleich 0)
	 * @param format java.lang.String
	 *		die Vorgabe fuer die Datumsaufbereitung
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String dateFormat(int[] datum, String format) {
		// Es m�ssen genau 3 Werte (Tag, Monat, Jahr) mitgekommen sein
		if (datum.length != 3) {
			return null;
		}

		// Dann ist alles klar; leite an die allgemeine Methode weiter
		// und verwende das Standard-Token ".".
		return dateFormat(datum[0], datum[1], datum[2], format, ".");
	}

	/**
	 * Baue anhand des uebergebenen Formatstrings und den uebergebenen
	 * Werten fuer Tag, Monat und Jahr einen Datumsstring auf und gebe diesen
	 * zurueck. Diese Methode ist effizienter und ressourcenschonender
	 * als die sehr allgemeingueltigen Java-Mechanismen wie z.B. in SimpleDateFormat.
	 * <p>
	 * Fuer naehere Informationen @see #dateFormat(int, int, int, String, String, boolean)
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	dateFormat( {12, 4, 23},    "d.m.y",         ".") liefert "12.4.1923" (*)
	 * 	dateFormat( {12, 4, 23},    "d.m.yy",        ".") liefert "12.4.23"
	 * 	dateFormat( {12, 4, 23},    "d.m.yyy",       ".") liefert "12.4.1923" (*)
	 * 	dateFormat( {12, 4, 23},    "d.m.yyyy",      ".") liefert "12.4.1923" (*)
	 * 	dateFormat( {12, 4, 23},    "d.m.yyyyyy",    ".") liefert "12.4.001923" (*)
	 * 	dateFormat( {12, 10, 2023}, "d.m.yy",        ".") liefert "12.10.2023"
	 * 	dateFormat( {12, 10, 2023}, "dd.mm.yyyy",    ".") liefert "12.10.2023"
	 * 	dateFormat( {12, 10, 2023}, "ddd.mmm.yyyyy", ".") liefert "012.010.02023"
	 * (*) wenn der Aufruf vor dem 13.4.2003 erfolgt; nach dem 12.4.2003 wird
	 *     als Jahreszahl 20 eingesetzt.
	 * </pre>
	 * @return java.lang.String
	 *		der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param datum int[]
	 *		Ein Array mit 3 Elementen: Tag (je nach Monat/Jahr ein Wert zwischen 1 und 31),
	 *    Monat (ein Wert zwischen 1 und 12) und Jahr (ein Wert ungleich 0)
	 * @param format java.lang.String
	 *		die Vorgabe fuer die Datumsaufbereitung
	 * @param token String
	 *		String der Laenge 1 mit dem zu verwendenden Trennzeichen
	 *		(ueblicherweise ein Punkt bei einem normalen Datum bzw. ein Strich bei einem ISO-Datum)
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String dateFormat(int[] datum, String format, String token) {
		// Es m�ssen genau 3 Werte (Tag, Monat, Jahr) mitgekommen sein
		if (datum.length != 3) {
			return null;
		}

		// Dann ist alles klar; leite an die allgemeine Methode weiter
		return dateFormat(datum[0], datum[1], datum[2], format, token, true);
	}

	/**
	 * Baue anhand des uebergebenen Formatstrings und den uebergebenen
	 * Werten fuer Tag, Monat und Jahr einen Datumsstring auf und gebe diesen
	 * zurueck. Diese Methode ist effizienter und ressourcenschonender
	 * als die sehr allgemeingueltigen Java-Mechanismen wie z.B. in SimpleDateFormat.
	 * <p>
	 * Fuer naehere Informationen @see #dateFormat(int, int, int, String, String, boolean)
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	dateFormat(12, 4, 23,    "d.m.y")         liefert "12.4.1923" (*)
	 * 	dateFormat(12, 4, 23,    "d.m.yy")        liefert "12.4.23"
	 * 	dateFormat(12, 4, 23,    "d.m.yyy")       liefert "12.4.1923" (*)
	 * 	dateFormat(12, 4, 23,    "d.m.yyyy")      liefert "12.4.1923" (*)
	 * 	dateFormat(12, 4, 23,    "d.m.yyyyyy")    liefert "12.4.001923" (*)
	 * 	dateFormat(12, 10, 2023, "d.m.yy")        liefert "12.10.2023"
	 * 	dateFormat(12, 10, 2023, "dd.mm.yyyy")    liefert "12.10.2023"
	 * 	dateFormat(12, 10, 2023, "ddd.mmm.yyyyy") liefert "012.010.02023"
	 * (*) wenn der Aufruf vor dem 13.4.2003 erfolgt; nach dem 12.4.2003 wird
	 *     als Jahreszahl 20 eingesetzt.
	 * </pre>
	 * @return java.lang.String
	 *		der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param tag int
	 *		Tag (je nach Monat/Jahr ein Wert zwischen 1 und 31)
	 * @param monat int
	 *		Monat (ein Wert zwischen 1 und 12)
	 * @param jahr int
	 * 		Jahr (ein Wert ungleich 0)
	 * @param format java.lang.String
	 *		die Vorgabe fuer die Datumsaufbereitung wie oben beschrieben
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String dateFormat(int tag, int monat, int jahr, String format) {
		return dateFormat(tag, monat, jahr, format, ".", true);
	}

	/**
	 * Baue anhand des uebergebenen Formatstrings und den uebergebenen
	 * Werten fuer Tag, Monat und Jahr einen Datumsstring auf und gebe diesen
	 * zurueck. Diese Methode ist effizienter und ressourcenschonender
	 * als die sehr allgemeingueltigen Java-Mechanismen wie z.B. in SimpleDateFormat.
	 * <p>
	 * Fuer naehere Informationen @see #dateFormat(int, int, int, String, String, boolean)
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	dateFormat(12, 4, 23,    "d.m.y",         ".") liefert "12.4.1923" (*)
	 * 	dateFormat(12, 4, 23,    "d.m.yy",        ".") liefert "12.4.23"
	 * 	dateFormat(12, 4, 23,    "d.m.yyy",       ".") liefert "12.4.1923" (*)
	 * 	dateFormat(12, 4, 23,    "d.m.yyyy",      ".") liefert "12.4.1923" (*)
	 * 	dateFormat(12, 4, 23,    "d.m.yyyyyy",    ".") liefert "12.4.001923" (*)
	 * 	dateFormat(12, 10, 2023, "d.m.yy",        ".") liefert "12.10.2023"
	 * 	dateFormat(12, 10, 2023, "dd.mm.yyyy",    ".") liefert "12.10.2023"
	 * 	dateFormat(12, 10, 2023, "ddd.mmm.yyyyy", ".") liefert "012.010.02023"
	 * (*) wenn der Aufruf vor dem 13.4.2003 erfolgt; nach dem 12.4.2003 wird
	 *     als Jahreszahl 20 eingesetzt.
	 * </pre>
	 * @return java.lang.String
	 *		der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param tag int
	 *		Tag (ein Wert groesser 0)
	 * @param monat int
	 *		Monat (ein Wert groesser 0)
	 * @param jahr int
	 * 		Jahr (ein Wert groesser gleich 0)
	 * @param format java.lang.String
	 *		die Vorgabe fuer die Datumsaufbereitung wie oben beschrieben
	 * @param token String
	 *		String der Laenge 1 mit dem zu verwendenden Trennzeichen
	 *		(ueblicherweise ein Punkt bei einem normalen Datum bzw. ein Strich bei einem ISO-Datum
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String dateFormat(int tag, int monat, int jahr, String format, String token) {
		return dateFormat(tag, monat, jahr, format, token, true);
	}

	/**
	 * Baue anhand des uebergebenen Formatstrings und den uebergebenen
	 * Werten fuer Tag, Monat und Jahr einen Datumsstring auf und gebe diesen
	 * zurueck. Diese Methode ist effizienter und ressourcenschonender
	 * als die sehr allgemeingueltigen Java-Mechanismen wie z.B. in SimpleDateFormat.
	 * <p>
	 * Der uebergebene Formatstring kann die folgenden Zeichen enthalten:
	 * <ul>
	 * <li>d oder D	Tag
	 * <li>m oder M	Monat
	 * <li>y oder Y	Jahr
	 * <li>token		das Zeichen, das im separaten Aufrufparameter token uebergeben wurde
	 * </ul><p>		
	 * Sind die Platzhalter kuerzer als der einzusetzende Wert (z.B. ein einfaches 'd'
	 * bei einem zweistelligen Tag, so wird trotzdem der komplette Wert eingesetzt.
	 * Sind die Platzhalter laenger, so wird der Wert mit der entsprechenden Anzahl
	 * fuehrender Nullen eingesetzt.
	 * <p>
	 * Eine Besonderheit ist die Jahreszahl in Kombination mit dem 'adjustCentury' Parameter:
	 * Ist adjustCentury gleich true, und werden 2 Platzhalter fuer das Jahr angegeben
	 * (also yy oder YY), und die uebergebene Jahreszahl ist kleiner als 100, dann wird
	 * diese unveraendert mit zwei Stellen eingefuegt. Ist die Jahreszahl kleiner als
	 * 100 und es wurden ungleich 2 Platzhalter fuer das Jahr vorgegeben, so wird 
	 * zunaechst das passende Jahrhundert ermittelt, und die so resultierende 4-stellige
	 * Jahreszahl genommen. Jahreszahlen groesser als 99 werden uebernommen und nur je nach
	 * Platzhalter ggf. mit fuehrenden Nullen aufgefuellt.
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	dateFormat(12, 4, 23,    "d.m.y",         ".", true)  liefert "12.4.1923" (*)
	 * 	dateFormat(12, 4, 23,    "d.m.yy",        ".", true)  liefert "12.4.23"
	 * 	dateFormat(12, 4, 23,    "d.m.yyy",       ".", true)  liefert "12.4.1923" (*)
	 * 	dateFormat(12, 4, 23,    "d.m.yyyy",      ".", true)  liefert "12.4.1923" (*)
	 * 	dateFormat(12, 4, 23,    "d.m.yyyyyy",    ".", true)  liefert "12.4.001923" (*)
	 * 	dateFormat(12, 10, 2023, "d.m.yy",        ".", true)  liefert "12.10.2023"
	 * 	dateFormat(12, 10, 2023, "dd.mm.yyyy",    ".", true)  liefert "12.10.2023"
	 * 	dateFormat(12, 10, 2023, "ddd.mmm.yyyyy", ".", true)  liefert "012.010.02023"
	 * (*) wenn der Aufruf vor dem 13.4.2003 erfolgt; nach dem 12.4.2003 wird
	 *     als Jahreszahl 20 eingesetzt.
	 *
	 * 	dateFormat(12, 4, 23,    "d.m.y",         ".", false) liefert "12.4.23"
	 * 	dateFormat(12, 4, 23,    "d.m.yy",        ".", false) liefert "12.4.23"
	 * 	dateFormat(12, 4, 23,    "d.m.yyy",       ".", false) liefert "12.4.023"
	 * 	dateFormat(12, 4, 23,    "d.m.yyyy",      ".", false) liefert "12.4.0023"
	 * 	dateFormat(12, 4, 23,    "d.m.yyyyyy",    ".", false) liefert "12.4.000023"
	 * 	dateFormat(12, 10, 2023, "d.m.yy",        ".", false) liefert "12.10.2023"
	 * 	dateFormat(12, 10, 2023, "dd.mm.yyyy",    ".", false) liefert "12.10.2023"
	 * 	dateFormat(12, 10, 2023, "ddd.mmm.yyyyy", ".", false) liefert "012.010.02023"
	 * </pre>
	 * @return java.lang.String
	 *		der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param tag int
	 *		Tag (ein Wert groesser 0)
	 * @param monat int
	 *		Monat (ein Wert groesser 0)
	 * @param jahr int
	 * 		Jahr (ein Wert groesser gleich 0)
	 * @param format java.lang.String
	 *		die Vorgabe fuer die Datumsaufbereitung wie oben beschrieben
	 * @param token String
	 *		String der Laenge 1 mit dem zu verwendenden Trennzeichen
	 *		(ueblicherweise ein Punkt bei einem normalen Datum bzw. ein Strich bei einem ISO-Datum
	 * @param adjustCentury boolean
	 *		Flag, das angibt, ob Jahreszahlen kleiner als 100 gemaess dem oben beschriebenen
	 *		Algorithmus in 19xx bzw. 20xx umgewandelt werden sollen oder nicht.
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String dateFormat(int tag, int monat, int jahr, String format, String token, boolean adjustCentury) {
		if ((format == null) || (token == null) || (token.length() != 1)) {
			return null;
		}

		StringBuffer sb = new StringBuffer(format.length());

		// Brauchen StringTokenizer, um das format aufzubr�seln
		// Die Tokens wollen wir dabei auch.
		StringTokenizer tokenizer = new StringTokenizer(format, token, true);

		String teilString;
		char zeichen;
		int laenge;
		int wert = 0;

		while (tokenizer.hasMoreTokens()) {
			teilString = tokenizer.nextToken().toLowerCase();
			laenge = teilString.length();
			zeichen = teilString.charAt(0);

			// Wenn es mehr als ein Zeichen gibt, sollten wir
			// nachsehen, ob es alle dieselben sind
			for (int i = 1; i < laenge; i++) {
				if (zeichen != teilString.charAt(i)) {
					return null;
				}
			}

			if (teilString.equals(token)) {
				// Token wird �bernommen
				sb.append(token);
			} else {
				// Alles andere muss evtl. ein wenig formatiert werden
				switch (zeichen) {
					case 'd' :
						wert = tag;
						if (wert < 1) {
							return null;
						}
						break;
					case 'm' :
						wert = monat;
						if (wert < 1) {
							return null;
						}
						break;
					case 'y' :
						wert = jahr;
						if (wert < 0) {
							return null;
						}
						if (wert < 100 && laenge != 2 && adjustCentury) {
							wert += getCentury(jahr, monat, tag) * 100;
						}
						break;
					default :
						return null;
				}
				// wenn es einen Treffer gab, dann einf�gen
				sb.append(StringHelper.padLengthLeft(String.valueOf(wert), laenge, '0'));
			}
		}
		return sb.toString();
	}

	/**
	 * Versuche, anhand des uebergebenen Strings ein java.util.Date Objekt zu erzeugen.
	 * Der String muss eines der folgenden Formate aufweisen:<pre>
	 *		dd.mm.yyyy
	 *		dd-mm-yyyy
	 *		yyyy.mm.dd
	 *		yyyy-mm-dd
	 * </pre>
	 * Aufrufbeispiel:<pre>
	 *		String s = "11.08.1960";
	 *		java.util.Date d = TimeAndDateHelper.dateValueOf(s);
	 * </pre>
	 * @return java.util.Date
	 * 		Das anhand des uebergebenen Strings gebildete Date-Objekt
	 * @param arg String
	 * 		Der in ein Date-Objekt zu konvertierende String im oben beschriebenen Format
	 * @exception IllegalArgumentException
	 * 		Falls Teile des uebergebenen Strings keine gueltigen Werte fuer Tag, Monat oder Jahr sind
	 * @exception ParseException
	 * 		Falls der uebergebene String nicht dem obigen Format entspricht
	 * <p>
	 * @see java.util.Date
	 * <p>
	 * @author Guyet, Sven
	 */
	public static java.util.Date dateValueOf(String arg) throws ParseException {
		// Der Resultwert wird erst mal mit null initialisiert.
		// Wenn sich das bis zum Methodenende nicht �ndert, wird eine Exception geworfen
		java.util.Date date = null;

		if (arg != null && arg.length() == 10) {
			// Schon besser! Mal sehen, ob ich auch Trennzeichen finde...
			int pattern = 0;
			switch (arg.charAt(2)) {
				case '.' :
					if (arg.charAt(5) == '.') {
						pattern = 1; // DDxMMxYYYY
					}
					break;
				case '-' :
					if (arg.charAt(5) == '-') {
						pattern = 1; // DDxMMxYYYY
					}
					break;
				default :
					switch (arg.charAt(4)) {
						case '.' :
							if (arg.charAt(7) == '.') {
								pattern = 2; // YYYYxMMxDD
							}
							break;
						case '-' :
							if (arg.charAt(7) == '-') {
								pattern = 2; // YYYYxMMxDD
							}
							break;
						default :
							break;
					}
					break;
			}

			if (pattern != 0) {
				// Trennzeichen gefunden. Dann mal versuchen, die Einzelbestandteile
				// abzugreifen und ein Dateobjekt daraus zu basteln. Benutze extra
				// nicht SimpleDateFormat o.�., um performanter zu sein.
				// Exceptions k�nnen ignoriert werden, da vor dem Return noch auf 
				// date!=null gepr�ft wird
				try {
					int day, month, year;
					if (pattern == 1) {
						day = Integer.parseInt(arg.substring(0, 2));
						month = Integer.parseInt(arg.substring(3, 5));
						year = Integer.parseInt(arg.substring(6, 10));
					} else {
						year = Integer.parseInt(arg.substring(0, 4));
						month = Integer.parseInt(arg.substring(5, 7));
						day = Integer.parseInt(arg.substring(8, 10));
					}
					date = (new GregorianCalendar(year, month - 1, day)).getTime();
				} catch (Exception e) {
					// Fehler hier ignorieren
				}
			}
		}

		if (date == null) {
			throw new ParseException("Date format must be one of dd.mm.yyyy, dd-mm-yyyy, yyyy.mm.dd, yyyy-mm-dd"+"; value provided: "+arg, 0);
		}
		return date;
	}

	/**
	 * Formatiere den uebergebenen Datumsstring gemaess dem uebergebenen Formatstring
	 * um und gebe das Ergebnis zurueck. Diese Methode ist effizienter und
	 * ressourcenschonender als die sehr allgemeingueltigen Java-Mechanismen wie
	 * z.B. in SimpleDateFormat.
	 * <p>
	 * Das Eingabedatum muss im deutschen Format vorliegen, d.h. "dd.mm.yyyy";
	 * anderenfalls wird eine ParseException geworfen.
	 * <p>
	 * Der uebergebene Formatstring kann die folgenden Zeichen enthalten:
	 * <ul>
	 * <li>d oder D	Tag
	 * <li>m oder M	Monat
	 * <li>y oder Y	Jahr
	 * <li>'.'		Trennzeichen
	 * </ul><p>		
	 * Sind die Platzhalter kuerzer als der einzusetzende Wert (z.B. ein einfaches 'd'
	 * bei einem zweistelligen Tag, so wird trotzdem der komplette Wert eingesetzt.
	 * Sind die Platzhalter laenger, so wird der Wert mit der entsprechenden Anzahl
	 * fuehrender Nullen eingesetzt.
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	formatDatum("1.5.1",      "d.m.y")         liefert "1.5.1"
	 * 	formatDatum("1.5.1",      "dd.mm.yy")      liefert "01.05.01"
	 * 	formatDatum("1.5.1",      "dd.mm.yyyy")    liefert "01.05.0001"
	 * 	formatDatum("1.5.1",      "ddd.mmm.yyyyy") liefert "001.005.00001"
	 * 	formatDatum("12.04.23",   "d.m.y")         liefert "12.4.23"
	 * 	formatDatum("12.04.23",   "dd.mm.yy")      liefert "12.04.23"
	 * 	formatDatum("12.04.23",   "dd.mm.yyyy")    liefert "12.04.0023"
	 * 	formatDatum("12.04.23",   "ddd.mmm.yyyyy") liefert "012.004.00023"
	 * 	formatDatum("12.10.2023", "d.m.y")         liefert "12.10.2023"
	 * 	formatDatum("12.10.2023", "dd.mm.yy")      liefert "12.10.2023"
	 * 	formatDatum("12.10.2023", "dd.mm.yyyy")    liefert "12.10.2023"
	 * 	formatDatum("12.10.2023", "ddd.mmm.yyyyy") liefert "012.010.02023"
	 * </pre>
	 * @return java.lang.String
	 *		Der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param datum java.lang.String
	 *		Ein Datumsstring im deutschen Format, d.h. "dd.mm.yyyy"
	 * @param format java.lang.String
	 *		Die Vorgabe fuer die Datumsaufbereitung.
	 * @exception ParseException
	 *		Der Uebergabeparameter war null, oder es konnten nicht genau 3 int-Werte im Datumsstring
	 * 		gefunden werden, die durch "." voneinander getrennt sind, oder die gefundenen
	 * 		Werte entsprechen keinem gueltigen Datum.
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String formatDatum(String datum, String format) throws ParseException {
		return formatDatum(datum, format, ".");
	}

	/**
	 * Formatiere den uebergebenen Datumsstring gemaess dem uebergebenen Formatstring
	 * um und gebe das Ergebnis zurueck. Diese Methode ist effizienter und
	 * ressourcenschonender als die sehr allgemeingueltigen Java-Mechanismen wie
	 * z.B. in SimpleDateFormat.
	 * <p>
	 * Das Eingabedatum muss im deutschen Format vorliegen, d.h. "dd.mm.yyyy";
	 * anderenfalls wird eine ParseException geworfen.
	 * <p>
	 * Der uebergebene Formatstring kann die folgenden Zeichen enthalten:
	 * <ul>
	 * <li>d oder D	Tag
	 * <li>m oder M	Monat
	 * <li>y oder Y	Jahr
	 * <li>token		das Zeichen, das im separaten Aufrufparameter token uebergeben wurde
	 * </ul><p>		
	 * Sind die Platzhalter kuerzer als der einzusetzende Wert (z.B. ein einfaches 'd'
	 * bei einem zweistelligen Tag, so wird trotzdem der komplette Wert eingesetzt.
	 * Sind die Platzhalter laenger, so wird der Wert mit der entsprechenden Anzahl
	 * fuehrender Nullen eingesetzt.
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	formatDatum("1.5.1",      "d-m-y",         "-") liefert "1-5-1"
	 * 	formatDatum("1.5.1",      "dd-mm-yy",      "-") liefert "01-05-01"
	 * 	formatDatum("1.5.1",      "dd-mm-yyyy",    "-") liefert "01-05-0001"
	 * 	formatDatum("1.5.1",      "ddd-mmm-yyyyy", "-") liefert "001-005-00001"
	 * 	formatDatum("12.04.23",   "d-m-y",         "-") liefert "12-4-23"
	 * 	formatDatum("12.04.23",   "dd-mm-yy",      "-") liefert "12-04-23"
	 * 	formatDatum("12.04.23",   "dd-mm-yyyy",    "-") liefert "12-04-0023"
	 * 	formatDatum("12.04.23",   "ddd-mmm-yyyyy", "-") liefert "012-004-00023"
	 * 	formatDatum("12.10.2023", "d-m-y",         "-") liefert "12-10-2023"
	 * 	formatDatum("12.10.2023", "dd-mm-yy",      "-") liefert "12-10-2023"
	 * 	formatDatum("12.10.2023", "dd-mm-yyyy",    "-") liefert "12-10-2023"
	 * 	formatDatum("12.10.2023", "ddd-mmm-yyyyy", "-") liefert "012-010-02023"
	 * </pre>
	 * @return java.lang.String
	 *		Der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param datum java.lang.String
	 *		Ein Datumsstring im deutschen Format, d.h. "dd.mm.yyyy"
	 * @param format java.lang.String
	 *		Die Vorgabe fuer die Datumsaufbereitung.
	 * @param token String
	 *		String der Laenge 1 mit dem zu verwendenden Trennzeichen
	 *		(ueblicherweise ein Punkt bei einem normalen Datum bzw. ein Strich bei einem ISO-Datum
	 * @exception ParseException
	 *		Der Uebergabeparameter war null, oder es konnten nicht genau 3 int-Werte im Datumsstring
	 * 		gefunden werden, die durch "." voneinander getrennt sind, oder die gefundenen
	 * 		Werte entsprechen keinem gueltigen Datum.
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String formatDatum(String datum, String format, String token) throws ParseException {
		// Zunaechst erstmal die Einzelbestandteile aus dem uebergebenen Datumsstring
		// herausholen.
		int[] date = parseDatum(datum);
		// In parseDatum() wurde bereits geprueft, dass genau 3 Werte (Tag, Monat, Jahr) mitgekommen sind.
		// Daher kann ich jetzt aufbereiten. Stelle ueber den letzten Aufrufparameter sicher, dass das
		// Jahrhundert der Jahreszahl unveraendert bleibt.
		return dateFormat(date[0], date[1], date[2], format, token, false);
	}

	/**
	 * Formatiere den uebergebenen Datumsstring gemaess dem uebergebenen Formatstring
	 * um und gebe das Ergebnis zurueck. Diese Methode ist effizienter und
	 * ressourcenschonender als die sehr allgemeingueltigen Java-Mechanismen wie
	 * z.B. in SimpleDateFormat.
	 * <p>
	 * Das Eingabedatum muss im Iso-Format vorliegen, d.h. "yyyy-mm-dd";
	 * anderenfalls wird eine ParseException geworfen.
	 * <p>
	 * Der uebergebene Formatstring kann die folgenden Zeichen enthalten:
	 * <ul>
	 * <li>d oder D	Tag
	 * <li>m oder M	Monat
	 * <li>y oder Y	Jahr
	 * <li>'.'		Trennzeichen
	 * </ul><p>		
	 * Sind die Platzhalter kuerzer als der einzusetzende Wert (z.B. ein einfaches 'd'
	 * bei einem zweistelligen Tag, so wird trotzdem der komplette Wert eingesetzt.
	 * Sind die Platzhalter laenger, so wird der Wert mit der entsprechenden Anzahl
	 * fuehrender Nullen eingesetzt.
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	formatIsoDatum("1-5-1",      "d.m.y")         liefert "1.5.1"
	 * 	formatIsoDatum("1-5-1",      "dd.mm.yy")      liefert "01.05.01"
	 * 	formatIsoDatum("1-5-1",      "dd.mm.yyyy")    liefert "01.05.0001"
	 * 	formatIsoDatum("1-5-1",      "ddd.mmm.yyyyy") liefert "001.005.00001"
	 * 	formatIsoDatum("23-04-12",   "d.m.y")         liefert "12.4.23"
	 * 	formatIsoDatum("23-04-12",   "dd.mm.yy")      liefert "12.04.23"
	 * 	formatIsoDatum("23-04-12",   "dd.mm.yyyy")    liefert "12.04.0023"
	 * 	formatIsoDatum("23-04-12",   "ddd.mmm.yyyyy") liefert "012.004.00023"
	 * 	formatIsoDatum("2023-10-12", "d.m.y")         liefert "12.10.2023"
	 * 	formatIsoDatum("2023-10-12", "dd.mm.yy")      liefert "12.10.2023"
	 * 	formatIsoDatum("2023-10-12", "dd.mm.yyyy")    liefert "12.10.2023"
	 * 	formatIsoDatum("2023-10-12", "ddd.mmm.yyyyy") liefert "012.010.02023"
	 * </pre>
	 * @return java.lang.String
	 *		Der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param datum java.lang.String
	 *		Ein Datumsstring im Iso-Format, d.h. "yyyy-mm-dd"
	 * @param format java.lang.String
	 *		Die Vorgabe fuer die Datumsaufbereitung.
	 * @exception ParseException
	 *		Der Uebergabeparameter war null, oder es konnten nicht genau 3 int-Werte im Datumsstring
	 * 		gefunden werden, die durch "-" voneinander getrennt sind, oder die gefundenen
	 * 		Werte entsprechen keinem gueltigen Datum.
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String formatIsoDatum(String datum, String format) throws ParseException {
		// Zunaechst erstmal die Einzelbestandteile aus dem uebergebenen Datumsstring
		// herausholen.
		int[] date = parseIsoDatum(datum);
		// In parseIsoDatum() wurde bereits geprueft, dass genau 3 Werte (Tag, Monat, Jahr) mitgekommen sind.
		// Daher kann ich jetzt aufbereiten. Stelle ueber den letzten Aufrufparameter sicher, dass das
		// Jahrhundert der Jahreszahl unveraendert bleibt.
		return dateFormat(date[0], date[1], date[2], format, ".", false);
	}

	/**
	 * Formatiere den uebergebenen Datumsstring gemaess dem uebergebenen Formatstring
	 * um und gebe das Ergebnis zurueck. Diese Methode ist effizienter und
	 * ressourcenschonender als die sehr allgemeingueltigen Java-Mechanismen wie
	 * z.B. in SimpleDateFormat.
	 * <p>
	 * Das Eingabedatum muss im Iso-Format vorliegen, d.h. "yyyy-mm-dd";
	 * anderenfalls wird eine ParseException geworfen.
	 * <p>
	 * Der uebergebene Formatstring kann die folgenden Zeichen enthalten:
	 * <ul>
	 * <li>d oder D	Tag
	 * <li>m oder M	Monat
	 * <li>y oder Y	Jahr
	 * <li>token		das Zeichen, das im separaten Aufrufparameter token uebergeben wurde
	 * </ul><p>		
	 * Sind die Platzhalter kuerzer als der einzusetzende Wert (z.B. ein einfaches 'd'
	 * bei einem zweistelligen Tag, so wird trotzdem der komplette Wert eingesetzt.
	 * Sind die Platzhalter laenger, so wird der Wert mit der entsprechenden Anzahl
	 * fuehrender Nullen eingesetzt.
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	formatIsoDatum("1-5-1",      "d.m.y",         ".") liefert "1.5.1"
	 * 	formatIsoDatum("1-5-1",      "dd.mm.yy",      ".") liefert "01.05.01"
	 * 	formatIsoDatum("1-5-1",      "dd.mm.yyyy",    ".") liefert "01.05.0001"
	 * 	formatIsoDatum("1-5-1",      "ddd.mmm.yyyyy", ".") liefert "001.005.00001"
	 * 	formatIsoDatum("23-04-12",   "d.m.y",         ".") liefert "12.4.23"
	 * 	formatIsoDatum("23-04-12",   "dd.mm.yy",      ".") liefert "12.04.23"
	 * 	formatIsoDatum("23-04-12",   "dd.mm.yyyy",    ".") liefert "12.04.0023"
	 * 	formatIsoDatum("23-04-12",   "ddd.mmm.yyyyy", ".") liefert "012.004.00023"
	 * 	formatIsoDatum("2023-10-12", "d.m.y",         ".") liefert "12.10.2023"
	 * 	formatIsoDatum("2023-10-12", "dd.mm.yy",      ".") liefert "12.10.2023"
	 * 	formatIsoDatum("2023-10-12", "dd.mm.yyyy",    ".") liefert "12.10.2023"
	 * 	formatIsoDatum("2023-10-12", "ddd.mmm.yyyyy", ".") liefert "012.010.02023"
	 * </pre>
	 * @return java.lang.String
	 *		Der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param datum java.lang.String
	 *		Ein Datumsstring im Iso-Format, d.h. "yyyy-mm-dd"
	 * @param format java.lang.String
	 *		Die Vorgabe fuer die Datumsaufbereitung.
	 * @param token String
	 *		String der Laenge 1 mit dem zu verwendenden Trennzeichen
	 *		(ueblicherweise ein Punkt bei einem normalen Datum bzw. ein Strich bei einem ISO-Datum
	 * @exception ParseException
	 *		Der Uebergabeparameter war null, oder es konnten nicht genau 3 int-Werte im Datumsstring
	 * 		gefunden werden, die durch "-" voneinander getrennt sind, oder die gefundenen
	 * 		Werte entsprechen keinem gueltigen Datum.
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String formatIsoDatum(String datum, String format, String token) throws ParseException {
		// Zunaechst erstmal die Einzelbestandteile aus dem uebergebenen Datumsstring
		// herausholen.
		int[] date = parseIsoDatum(datum);
		// In parseIsoDatum() wurde bereits geprueft, dass genau 3 Werte (Tag, Monat, Jahr) mitgekommen sind.
		// Daher kann ich jetzt aufbereiten. Stelle ueber den letzten Aufrufparameter sicher, dass das
		// Jahrhundert der Jahreszahl unveraendert bleibt.
		return dateFormat(date[0], date[1], date[2], format, token, false);
	}

	/**
	 * Gebe die ersten beiden Stellen der auf 4 Stellen normierten uebergebenen Jahreszahl zurueck.
	 * Ist das uebergebene Jahr mehr als 2-stellig, so werden einfach die letzten beiden Stellen
	 * abgeschnitten und der Rest zurueckgegeben. Eine 2-stellige Jahresangabe wird so auf 4 Stellen
	 * normiert, dass das ergebende Jahr in den Zeitraum<br>
	 * [aktuelles Jahr - 79; aktuelles Jahr + 20]<br>
	 * faellt.
	 * <p>
	 * ACHTUNG: Diese Methode braucht die meiste Zeit dann, wenn ein Calendar-Objekt fuer
	 * die Jahrhundertberechnung gebraucht wird. Dies kann man vermeiden, wenn die Jahreszahl
	 * mehr als 2-stellig ist.
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	getCentury(23) liefert 19 (*)
	 * 	getCentury(123) liefert 1
	 * 	getCentury(2023) liefert 20
	 *  (*) wenn der Aufruf vor dem Jahr 2004 erfolgt; ab 2004 wird
	 *		  20 zurueckgegeben.
	 * </pre>
	 * @return int
	 *		die ersten beiden Stellen der auf 4 Stellen normierten Jahreszahl
	 * @param jahr int
	 *		Jahr (ein Wert groesser 0)
	 * <p>
	 * @author Guyet, Sven
	 */
	public static int getCentury(int jahr) {
		return getCentury(jahr, -1, -1);
	}

	/**
	 * Gebe fuer das uebergebene Datum die ersten beiden Stellen der auf 4 Stellen
	 * normierten Jahreszahl zurueck. Ist das uebergebene Jahr mehr als 2-stellig,
	 * so werden einfach die letzten beiden Stellen abgeschnitten und der Rest
	 * zurueckgegeben. Eine 2-stellige Jahresangabe so auf 4 Stellen normiert, dass
	 * das sich ergebende Datum in den Zeitraum heute vor 80 Jahren bis heute in 20 Jahren
	 * faellt.<br>
	 * Bei uebergebenem Tag und Monat (beide Werte groesser als Null) wird das genaue
	 * Datum fuer die Intervallbetrachtung genommen. Anderenfalls wird nur die Jahreszahl
	 * betrachtet, d.h. bei Aufruf im Jahr 2001 wird aus 21 das Jahr 2021, aus 22 das Jahr 1922.
	 * <p>
	 * ACHTUNG: Diese Methode braucht die meiste Zeit dann, wenn ein Calendar-Objekt fuer
	 * die Jahrhundertberechnung gebraucht wird. Dies kann man vermeiden, wenn die Jahreszahl
	 * mehr als 2-stellig ist.
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	getCentury(23, 4, 12) liefert 19 (*)
	 * 	getCentury(123, 4, 12) liefert 1
	 * 	getCentury(2023, 4, 12) liefert 20
	 * (*) wenn der Aufruf vor dem 13.4.2003 erfolgt; nach dem 12.4.2003 wird
	 *	   20 zurueckgegeben.
	 * </pre>
	 * @return int
	 *		die ersten beiden Stellen der auf 4 Stellen normierten Jahreszahl
	 * @param jahr int
	 *		Jahr (ein Wert groesser 0)
	 * @param monat int
	 *		Monat (ein Wert zwischen 1 und 12)
	 * @param tag int
	 * 		Tag (je nach Monat/Jahr ein Wert zwischen 1 und 31)
	 * <p>
	 * @author Guyet, Sven
	 */
	public static int getCentury(int jahr, int monat, int tag) {
		if (jahr > 99) {
			return jahr / 100;
		}

		Calendar calendar = Calendar.getInstance();
		// das heutige Jahr ermitteln.
		int aktJahr = calendar.get(Calendar.YEAR);
		// Das uebergebene Jahr mit dem heutigen
		int testJahr = (aktJahr / 100) * 100 + jahr;

		if ((monat > 0) && (tag > 0) && (testJahr == aktJahr + 20)) {
			// Wenn Tag und Monat angegeben sind, genau ueberpruefen,
			// ob das Datum innerhalb der naechsten 20 Jahre liegt.
			// Ermittle einen Integerwert fuer heutige Monat/Jahr Kombi
			int test = (calendar.get(Calendar.MONTH) + 1) * 100 + calendar.get(Calendar.DATE);

			// Im kritischen Jahr (heute plus 20) testen wir genau Monat und Tag
			if (test < (monat * 100 + tag)) {
				testJahr -= 100;
			}
		} else {
			// Ansonsten nur das Jahr �berpr�fen
			if (testJahr > aktJahr + 20) {
				testJahr -= 100;
			}
		}

		return testJahr / 100;
	}

	/**
	 * Erzeuge anhand des uebergebenen Time-Objekts einen String im Format hh:mm.
	 * <p>
	 * Aufrufbeispiel:<pre>
	 *		java.sql.Time t = new java.sql.Time(12,20,0);
	 *		String s = TimeAndDateHelper.hhmmStringOf(t);
	 * </pre>
	 * @return String
	 * 		Der aus dem Time-Parameter gebildete String im Format hh:mm
	 * @param zeit java.sql.Time
	 * 		Das in einen String obigen Formats umzuwandelnde Time-Objekt
	 * <p>
	 * @see java.sql.Time
	 * <p>
	 * @author Spaeth, Andrea
	 */
	public static String hhmmStringOf(Time zeit) {
		return zeit.toString().substring(0, 5);
	}

	/**
	 * Baue anhand des ISO-Formates ("yyyy-mm-dd") und den uebergebenen
	 * Werten fuer Tag, Monat und Jahr einen Datumsstring auf und gebe diesen
	 * zurueck. Diese Methode ist effizienter und ressourcenschonender
	 * als die sehr allgemeingueltigen Java-Mechanismen wie z.B. in SimpleDateFormat.
	 * <p>
	 * Fuer naehere Informationen s. dateFormat(int, int, int, String, String)
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	isoFormat({12, 4, 23}) liefert "1923-04-12" (*)
	 * 	isoFormat({12, 4, 19}) liefert "2019-04-12"
	 * 	isoFormat({12, 10, 2023}) liefert "2023-10-12"
	 * (*) wenn der Aufruf vor dem 13.4.2003 erfolgt; nach dem 12.4.2003 wird
	 *     als Jahreszahl 20 eingesetzt.
	 * </pre>
	 * @return java.lang.String
	 *		der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param datum int[]
	 *		Ein Array mit 3 Elementen: Tag (je nach Monat/Jahr ein Wert zwischen 1 und 31),
	 *    Monat (ein Wert zwischen 1 und 12) und Jahr (ein Wert ungleich 0)
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String isoFormat(int[] datum) {
		// Es m�ssen genau 3 Werte (Tag, Monat, Jahr) mitgekommen sein
		return (datum.length != 3) ? null : isoFormat(datum[0], datum[1], datum[2]);
	}

	/**
	 * Baue anhand des ISO-Formates ("yyyy-mm-dd") und den uebergebenen
	 * Werten fuer Tag, Monat und Jahr einen Datumsstring auf und gebe diesen
	 * zurueck. Diese Methode ist effizienter und ressourcenschonender
	 * als die sehr allgemeingueltigen Java-Mechanismen wie z.B. in SimpleDateFormat.
	 * <p>
	 * Fuer naehere Informationen s. dateFormat(int, int, int, String, String)
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	isoFormat(12, 4, 23) liefert "1923-04-12" (*)
	 * 	isoFormat(12, 4, 19) liefert "2019-04-12"
	 * 	isoFormat(12, 10, 2023) liefert "2023-10-12"
	 * (*) wenn der Aufruf vor dem 13.4.2003 erfolgt; nach dem 12.4.2003 wird
	 *     als Jahreszahl 20 eingesetzt.
	 * </pre>
	 * @return java.lang.String
	 *		der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param tag int
	 *		Tag (je nach Monat/Jahr ein Wert zwischen 1 und 31)
	 * @param monat int
	 *		Monat (ein Wert zwischen 1 und 12)
	 * @param jahr int
	 * 		Jahr (ein Wert ungleich 0)
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String isoFormat(int tag, int monat, int jahr) {
		return dateFormat(tag, monat, jahr, "yyyy-mm-dd", "-");
	}

	/**
	 * Baue anhand des ISO-Formates ("yyyy-mm-dd") und dem uebergebenen
	 * Datum einen Datumsstring auf und gebe diesen
	 * zurueck. Diese Methode ist effizienter und ressourcenschonender
	 * als die sehr allgemeingueltigen Java-Mechanismen wie z.B. in SimpleDateFormat.
	 * <p>
	 * Fuer naehere Informationen s. dateFormat(int, int, int, String, String)
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	isoFormat(new GregorianCalendar(2003, 5, 5)) liefert "2003-05-05"
	 *  isoFormat(new GregorianCalendar())           liefert das Tagesdatum im ISO Format
	 * </pre>
	 * @return java.lang.String
	 *		der String mit dem aufbereiteten Datum, oder null im Fehlerfall
	 * @param date java.util.Date
	 *		Datum das in den ISO String konvertiert weden soll
	 * @see java.util.Calendar
	 * @see java.util.GregorianCalendar
	 * <p>
	 * @author Schulte, Klaus
	 */
	public static String isoFormat(Calendar cal) {
		// Achtung: Da der Monat im Calendar mit 0 anfaengt, muss hier 1 aufaddiert werden
		return dateFormat(cal.get(Calendar.DATE), cal.get(Calendar.MONTH)+1, cal.get(Calendar.YEAR), "yyyy-mm-dd", "-");
	}

	/**
	 * Gebe zurueck, ob das uebergebene Jahr ein Schaltjahr ist.
	 * <p>
	 * Die Logik ist ein wenig kompliziert: Ein Jahr ist dann ein Schaltjahr, wenn
	 * es ohne Rest durch 400 teilbar ist. In allen anderen Faellen ist ein Jahr ein
	 * Schaltjahr genau dann, wenn es ohne Rest durch 4, aber nicht ohne Rest durch 100
	 * teilbar ist. 
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	isSchaltjahr(1996) liefert true
	 * 	isSchaltjahr(1997) liefert false
	 * 	isSchaltjahr(1900) liefert false
	 * 	isSchaltjahr(2000) liefert true
	 * </pre>
	 * @return boolean
	 *		true, falls es sich bei dem uebergebenen Jahr um ein Schaltjahr handelt, sonst false
	 * @param jahr int
	 *		das zu ueberpruefenden Jahr
	 * <p>
	 * @author Guyet, Sven
	 */
	public static final boolean isSchaltjahr(int jahr) {
		return (jahr % 4 == 0) && ((jahr % 100 != 0) || (jahr % 400 == 0));
	}

	/**
	 * Gebe zurueck, ob es sich bei der uebergebenen Kombination Tag/Monat/Jahr um ein
	 * gueltiges Datum handelt. Hierbei wird die Schaltjahr-Logik mitberuecksichtigt.
	 * <p>
	 * Weitere Informationen @see #isValidDate(int, int, int)
	 * <p>
	 * @return boolean
	 *		true, falls es sich bei den uebergebenen Daten um ein gueltiges Datum handelt, sonst false
	 * @param datum int[]
	 *		3-stelliges Array mit Tag, Monat, Jahr (in dieser Reihenfolge!) des zu ueberpruefenden Datums
	 * <p>
	 * @author Guyet, Sven
	 */
	public static final boolean isValidDate(int[] datum) {
		return (datum.length != 3) ? false : isValidDate(datum[0], datum[1], datum[2]);
	}

	/**
	 * Gebe zurueck, ob es sich bei der uebergebenen Kombination Tag/Monat/Jahr um ein
	 * gueltiges Datum handelt. Hierbei wird die Schaltjahr-Logik mitberuecksichtigt.
	 * <p>
	 * Aufrufbeispiele:<pre>
	 * 	isValidDate(31, 1, 2000) liefert true
	 * 	isValidDate(32, 1, 2000) liefert false
	 * 	isValidDate(29, 2, 1996) liefert true
	 * 	isValidDate(29, 2, 1997) liefert false
	 * 	isValidDate(29, 2, 1900) liefert false (kein Schaltjahr, da glattes Jahrhundert)
	 * 	isValidDate(29, 2, 2000) liefert true (Schaltjahr, da glattes Jahrtausend)
	 * </pre>
	 * @return boolean
	 *		true, falls es sich bei den uebergebenen Daten um ein gueltiges Datum handelt, sonst false
	 * @param tag int
	 *		Tag des zu ueberpruefenden Datums
	 * @param monat int
	 *		Monat des zu ueberpruefenden Datums
	 * @param jahr int
	 *		Jahr des zu ueberpruefenden Datums
	 * <p>
	 * @author Guyet, Sven
	 */
	public static final boolean isValidDate(int tag, int monat, int jahr) {
		// Einfache Werteregeln pruefen
		if ((monat <= 0) || (monat > 12)) {
			return false;
		}

		int[] monatsTage = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if ((tag <= monatsTage[monat - 1]) && (tag > 0)) {
			return true;
		}

		// Jetzt die Schaltjahr-Logik
		if ((tag == 29) && (monat == 2)) {
			return isSchaltjahr(jahr);
		}

		// Schade, jetzt bleibt nix mehr
		return false;
	}

	/**
	 * Bilde meine interne statische Calendar-Variable. Diese hilft bei der
	 * Date- und Timearithmetik.
	 * <p>
	 * @author: Guyet, Sven
	 */
	private static final void makeStaticCalendar() {
		staticCal = new GregorianCalendar();
	}

	/**
	 * Gebe ein int-Array zurueck, welches die Komponenten Tag, Monat und Jahr des
	 * uebergebenens Datumsstrings enthaelt. Der String muss im deutschen Format vorliegen,
	 * d.h. "dd.mm.yyyy"; anderenfalls wird eine ParseException geworfen.
	 * <p>
	 * @return int[]
	 *		Ein 3-stelliges int-Array mit Tag/Monat/Jahr des Datums in dieser Reihenfolge.
	 * @param datum java.lang.String
	 *		Ein Datumsstring im deutschen Format, d.h. "dd.mm.yyyy"
	 * @exception ParseException
	 *		Der Uebergabeparameter war null, oder es konnten nicht genau 3 int-Werte im Datumsstring
	 * 		gefunden werden, die durch "." voneinander getrennt sind, oder die gefundenen
	 * 		Werte entsprechen keinem gueltigen Datum.
	 * <p>
	 * @author Guyet, Sven
	 */
	public static int[] parseDatum(String datum) throws java.text.ParseException {
		int[] date = parseDatum(datum, ".");

		// Jetzt noch pruefen, ob ein gueltiges Datum vorliegt.
		if (!isValidDate(date)) {
			throw new ParseException("Invalid date in string: " + datum, 0);
		}
		return date;
	}

	/**
	 * Gebe ein int-Array zurueck, welches die durch den uebergebenen Token getrennten
	 * Komponenten des uebergebenen Datumsstrings enthaelt. Hier findet keine Gueltigkeits-
	 * pruefung statt, sondern eine reines Parsing des Strings.
	 * Die eigentlichen Datumspruefungen sind Aufgabe des Aufrufers.
	 * <p>
	 * @return int[]
	 *		Ein 3-stelliges int-Array mit den vom Token getrennten Integerkomponenten im
	 * 		uebergebenen String.
	 * @param datum java.lang.String
	 *		Ein Datumsstring in verschiedenen Formaten (Reihenfolge von Tag, Monat, Jahr nicht
	 *		festgelegt). Die Komponenten Tag, Monat, Jahr muessen jedoch durch den uebergebenen
	 * 		Token voneinander getrennt sein.
	 * @exception ParseException
	 *		Einer der Uebergabeparameter war null, oder es konnten nicht genau 3 int-Werte im Datumsstring
	 * 		gefunden werden, die durch den uebergebenen Token voneinander getrennt sind.
	 * <p>
	 * @author Guyet, Sven
	 */
	private static int[] parseDatum(String datum, String token) throws ParseException {
		// Simple Checks
		if ((datum == null) || (token == null)) {
			throw new ParseException(DATE_OR_TOKEN_PARSE_ERROR + "null", 0);
		}

		// Jetzt den String richtig auseinandernehmen
		StringTokenizer tokenizer = new StringTokenizer(datum, token);
		int[] datumArray = new int[3];

		try {
			for (int i = 0; i < 3; i++) {
				if (!tokenizer.hasMoreTokens()) {
					// Schade; keine Zahlen mehr da
					throw new ParseException(DATE_OR_TOKEN_PARSE_ERROR + datum, 0);
				}
				datumArray[i] = Integer.parseInt(tokenizer.nextToken());
			}
		} catch (Exception e) {
			throw new ParseException(DATE_OR_TOKEN_PARSE_ERROR + datum, 0);
		}

		if (tokenizer.hasMoreTokens()) {
			// Schade; jetzt ist mehr da als gewuenscht
			throw new ParseException(DATE_OR_TOKEN_PARSE_ERROR + datum, 0);
		}
		return datumArray;
	}

	/**
	 * Gebe ein int-Array zurueck, welches die Komponenten Tag, Monat und Jahr des
	 * uebergebenens Datumsstrings enthaelt. Der String muss im Iso-Format vorliegen,
	 * d.h. "yyyy-mm-dd"; anderenfalls wird eine ParseException geworfen.
	 * <p>
	 * @return int[]
	 *		Ein 3-stelliges int-Array mit Tag/Monat/Jahr des Datums in dieser Reihenfolge.
	 * @param isoDatum java.lang.String
	 *		Ein Datumsstring im Iso-Format, d.h. "yyyy-mm-dd"
	 * @exception ParseException
	 *		Der Uebergabeparameter war null, oder es konnten nicht genau 3 int-Werte im Datumsstring
	 * 		gefunden werden, die durch "-" voneinander getrennt sind, oder die gefundenen
	 * 		Werte entsprechen keinem gueltigen Datum.
	 * <p>
	 * @author Guyet, Sven
	 */
	public static int[] parseIsoDatum(String isoDatum) throws ParseException {
		// Wir nutzen die Funktionalit�t des parseDatum() Services, vertauschen dann
		// aber Tag und Jahr, da sie in umgekehrter Reihenfolge darin stehen
		int[] datum = parseDatum(isoDatum, "-");
		int temp = datum[2];
		datum[2] = datum[0];
		datum[0] = temp;

		// So, jetzt muessen wir aber noch das Datum pruefen.
		if (!isValidDate(datum)) {
			throw new ParseException("Invalid date in string: " + isoDatum, 0);
		}
		return datum;
	}

	/**
	 * Konvertiere den �bergebenen "DB2-Timestamp" String in ein java.sql.Timestamp Objekt. 
	 * Der String muss das Format<br>
	 *		yyyy-MM-dd-HH.mm.ss.SSSSSS<br>
	 * haben, wobei
	 * <ul>
	 * <li>yyyy	das 4-stellige Jahr ist
	 * <li>MM		der 2-stellige Monat innerhalb des Jahres ist (01-12)
	 * <li>dd		der 2-stellige Tag innerhalb des Monats ist (01-31)
	 * <li>HH		die 2-stellige Stundenanzahl ist (00-23)
	 * <li>mm		die 2-stellige Minutenanzahl ist (00-59)
	 * <li>ss		die 2-stellige Sekundenanzahl ist (00-59)
	 * <li>SSSSSS	die 6-stellige "Milli-Milli-Sekundenanzahl" ist (000000-999999)
	 * </ul><p>
	 * Wir konvertieren hier den String einfach in das Format
	 *		yyyy-MM-dd HH:mm:ss.SSSSSS
	 * welches dann durch die java.sql.Timestamp.valueOf(String) Methode verarbeitet werden kann.
	 * <p>
	 * Aufrufbeispiel:<pre>
	 * 	String s = "2000-08-11-08.30.00.123456";
	 * 	java.sql.Timestamp t = TimeAndDateHelper.timestampValueOf(s);
	 * </pre>
	 * @return Timestamp
	 * 		Das aus dem uebergebenen String gebildete Timestamp-Objekt
	 * @param s String
	 * 		Der in einen Timestamp umzuwandelnde String im oben beschriebenen Format
	 * @exception IllegalArgumentException
	 * 		Falls einer der Stringbestandteile nicht dem oben beschriebenen Format entspricht
	 * <p>
	 * @see java.sql.Timestamp
	 * <p>
	 * @author Guyet, Sven
	 */
	public static Timestamp timestampValueOf(String s) {
		if (s == null) {
			throw new IllegalArgumentException(TIMESTAMP_FORMAT_ERROR+"; value provided: null");
		}

		char[] ca = s.toCharArray();
		if ((ca.length < 19)
			|| (ca.length > 26)
			|| ((ca[10] != '-') && (ca[10] != ' '))
			|| ((ca[13] != '.') && (ca[13] != ':'))
			|| ((ca[16] != '.') && (ca[16] != ':'))
			|| ((ca[19] != '.') && (ca[19] != ','))) {
			throw new IllegalArgumentException(TIMESTAMP_FORMAT_ERROR+"; value provided: "+s);
		}

		ca[10] = ' ';
		ca[13] = ':';
		ca[16] = ':';
		ca[19] = '.';
		return Timestamp.valueOf(new String(ca));
	}

	/**
	 * Erzeuge aus dem uebergebenen String ein java.sql.Time Objekt.
	 * Der String muss das Format<br>
	 *		hh:mm<br>
	 * haben, wobei
	 * <ul>
	 * <li>hh		die 2-stellige Stundenanzahl ist (00-23)
	 * <li>mm		die 2-stellige Minutenanzahl ist (00-59)
	 * </ul><p>
	 * Aufrufbeispiel:<pre>
	 *		String s = "12:20";
	 *		java.sql.Time t = TimeAndDateHelper.timeValueOf(s);
	 * </pre>
	 * @return Time
	 * 		Das anhand des uebergebenen Strings gebildete Time-Objekt
	 * @param zeit String
	 * 		Der in ein Time-Objekt zu konvertierende String im oben beschriebenen Format
	 * @exception IllegalArgumentException
	 * 		Falls Teile des uebergebenen Strings keine gueltigen Werte fuer Stunde oder Minute sind
	 * @exception ParseException
	 * 		Falls der uebergebene String nicht dem obigen Format entspricht
	 * <p>
	 * @author Guyet, Sven
	 */
	public static Time timeValueOf(String zeit) throws ParseException {
		if ((zeit == null) || (zeit.length() != 5) || (zeit.charAt(2) != ':')) {
			throw new ParseException("Timestamp format must be HH:MM; value provided: "+zeit, 0);
		}
		return Time.valueOf(zeit + ":00");
	}

	/**
	 * Erzeuge aus den uebergebenen Werte ein java.sql.Time Objekt.
	 * Intern wird das Datum des gelieferten Objekts auf den 1.Januar 1970 gesetzt.
	 * Jede Minute, die versucht, auf Datumsbestandteile des zurueckgelieferten
	 * <code>Time</code> Objekts zuzugreifen, erhaelt eine
	 * <code>java.lang.IllegalArgumentException</code>.
	 * Der Rueckgabewert dieser Methode entspricht dem ueber den deprecated java.sql.Time
	 * Konstruktor mit den Argumenten (int, int, int) zu erzeugenden Objekt, vermeidet
	 * aber deprecated Aufrufe.
	 * <p>
	 * Aufrufbeispiel:<pre>
	 *		java.sql.Time highNoon = TimeAndDateHelper.timeValueOf(12, 0, 0);
	 * </pre>
	 * @return Time
	 * 		Das anhand der uebergebenen Stunden-, Minuten- und Sekundenwerte gebildete Time-Objekt
	 * @param hours int
	 * 		Der Stundenwert des gewuenschten Time-Objekts (0 bis 23). Werte ausserhalb dieses
	 *    Bereiches werden auf den gueltigen Wertebereich normiert, d.h. ein Uebergabewert
	 *    von 26 liefert ein Time-Objekt mit Stunde 3.
	 * @param minutes int
	 * 		Der Minutenwert des gewuenschten Time-Objekts (0 bis 59). Werte ausserhalb dieses
	 *    Bereiches justieren den Stundenwert des Time-Objekts, d.h. ein Uebergabewert
	 *    von 64 erhoeht die Stundenzahl um 1 und liefert den Minutenwert 5.
	 * @param seconds int
	 * 		Der Sekundenwert des gewuenschten Time-Objekts (0 bis 23). Werte ausserhalb dieses
	 *    Bereiches justieren den Minutenwert des Time-Objekts, d.h. ein Uebergabewert
	 *    von 64 erhoeht die Minutenzahl um 1 und liefert den Sekundenwert 5.
	 * <p>
	 * @author Guyet, Sven
	 */
	public static Time timeValueOf(int hours, int minutes, int seconds) {
		if (staticCal == null) {
			makeStaticCalendar();
		}
				
		synchronized (staticCal) {
			staticCal.clear();
			staticCal.set(1970, 0, 1, hours, minutes, seconds);
			return new Time(staticCal.getTime().getTime());
		}
	}

	/**
	 * Konvertierung eines Java-Timestamp in einen DB2-Timestamp (Character 26)
	 * und liefert das Ergebnis in Form zweier Strings (String[]) f�r Datum und Zeit zur�ck.
	 * Ausgabeformat f�r Datum (returnwert[0]):
	 * <p>
	 *		yyyy-MM-dd
	 * <p>
	 * Ausgabeformat f�r Zeit (returnwert[1]):
	 *    HH.mm.ss.SSSSSS
	 * <p><ul>
	 * <li>yyyy	Jahr		    4-stellig
	 * <li>MM		Monat		    2-stellig (01-12)
	 * <li>dd		Tag 				2-stellig (01-31)
	 * <li>HH		Stunde			2-stellig (00-23)
	 * <li>mm		Minute	 	 	2-stellig (00-59)
	 * <li>ss		Sekunde	 		2-stellig (00-59)
	 * <li>SSSSSS	Mikrosekunden 	6-stellig (000000-999999)
	 * </ul><p>
	 * Erfordert, dass das im Input-Timestamp enthaltene Jahr groesser gleich 0
	 * und maximal 4-stellig ist. Ansonsten wird eine IllegalArgumentException
	 * geworfen.
	 * <p>
	 * Aufrufbeispiel:<pre>
	 *		Timestamp javaTs = Timestamp.valueOf("2000-08-11 08:30:00.123456789");
	 *		String[] sasStrings = TimeAndDateHelper.toSASTimeAndDateStrings(javaTs);
	 * </pre>
	 * @return String[]
	 * 		[0] = Datum, [1] = Zeit
	 * @param aTimestamp  java.sql.Timestamp
	 * 		der zu konvertierende Timestamp
	 * @exception IllegalArgumentException
	 *		Wenn der uebergebene Timestamp null ist oder nicht den "Mindestanforderungen"
	 *		(Jahr groesser oder gleich 0, maximal 4-stellig) genuegt.
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String[] toSASTimeAndDateStrings(Timestamp aTimestamp) {
		StringBuffer sb = toDB2TimestampStringBuffer(aTimestamp);
		String[] result = new String[2];
		result[0] = sb.substring(0, 10); // Datum
		result[1] = sb.substring(11); // Zeit
		return result;
	}

	/**
	 * Konvertiere den �bergebenen "DB2-Timestamp" String in ein java.sql.Timestamp Objekt. 
	 * Der String muss das Format<br>
	 *		yyyy-MM-dd-HH.mm.ss.SSSSSS<br>
	 * haben, wobei
	 * <ul>
	 * <li>yyyy	das 4-stellige Jahr ist
	 * <li>MM		der 2-stellige Monat innerhalb des Jahres ist (01-12)
	 * <li>dd		der 2-stellige Tag innerhalb des Monats ist (01-31)
	 * <li>HH		die 2-stellige Stundenanzahl ist (00-23)
	 * <li>mm		die 2-stellige Minutenanzahl ist (00-59)
	 * <li>ss		die 2-stellige Sekundenanzahl ist (00-59)
	 * <li>SSSSSS	die 6-stellige "Milli-Milli-Sekundenanzahl" ist (000000-999999)
	 * </ul><p>
	 * Synonym f�r @see #timestampValueOf(String)
	 */
	public static Timestamp toJDBCTimestamp(String db2Timestamp) {
		return timestampValueOf(db2Timestamp);
	}

	/**
	 * Konvertierung eines Java-Timestamp in einen DB2-Timestamp (Character 26)
	 * wobei evtl. fehlende Mikrosekunden mit "0" aufgef�llt werden.
	 * <p>
	 * Der Ausgabestring hat das Format:
	 * <p>
	 *		yyyy-MM-dd-HH.mm.ss.SSSSSS
	 * <p><ul>
	 * <li>yyyy	Jahr		    4-stellig
	 * <li>MM		Monat		    2-stellig (01-12)
	 * <li>dd		Tag 				2-stellig (01-31)
	 * <li>HH		Stunde			2-stellig (00-23)
	 * <li>mm		Minute	 	 	2-stellig (00-59)
	 * <li>ss		Sekunde	 		2-stellig (00-59)
	 * <li>SSSSSS	Mikrosekunden 	6-stellig (000000-999999)
	 * </ul><p>
	 * Erfordert, dass das im Input-Timestamp enthaltene Jahr groesser gleich 0
	 * und maximal 4-stellig ist. Ansonsten wird eine IllegalArgumentException
	 * geworfen.
	 * <p>
	 * Aufrufbeispiel:<pre>
	 *		Timestamp javaTs = Timestamp.valueOf("2000-08-11 08:30:00.123456789");
	 *		String db2Timestamp = TimeAndDateHelper.toDB2Timestamp(javaTs);
	 * </pre>
	 * @return String
	 * 		entsprechende Stringdarstellung eines DB2-Timestamps (s.o.)
	 * @param aTimestamp  java.sql.Timestamp
	 * 		der zu konvertierende Timestamp
	 * @exception IllegalArgumentException
	 *		Wenn der uebergebene Timestamp null ist oder nicht den "Mindestanforderungen"
	 *		(Jahr groesser oder gleich 0, maximal 4-stellig) genuegt.
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String toDB2Timestamp(Timestamp aTimestamp) {
		return toDB2TimestampStringBuffer(aTimestamp).toString();
	}

	/**
	 * Liefert eine Stringdarstellung des Timestamps im DB-Format
	 * (genaue Beschreibung siehe toDB2Timestamp)
	 * @return String
	 * 		entsprechende Stringdarstellung eines DB2-Timestamps (s.o.)
	 * @param aTimestamp  java.sql.Timestamp
	 * 		der zu konvertierende Timestamp
	 * @exception IllegalArgumentException
	 *		Wenn der uebergebene Timestamp null ist oder nicht den "Mindestanforderungen"
	 *		(Jahr groesser oder gleich 0, maximal 4-stellig) genuegt.
	 * <p>
	 * @author Guyet, Sven
	 */
	private static StringBuffer toDB2TimestampStringBuffer(Timestamp aTimestamp) {
		if (aTimestamp == null) {
			throw new IllegalArgumentException("Illegal argument for conversion to DB2 timestamp: null");
		}

		// Jetzt wirds ekelig. Um an die Einzelbestandteile heranzukommen,
		// muss ich, wenn ich keine deprecated Methoden nehmen will, erstmal
		// ein Calendar Objekt aus dem Timestamp bauen. Dieses gibt mir dann
		// seine Einzelkomponenten. Nur bei den Sekundenbruchteilen muss man
		// sich wieder an Timestamp wenden. Der gibt Nanos zurueck. Wir wollen
		// aber genau 6 Stellen mit ggf. fuehrenden Nullen haben. Also muss
		// ein wenig Zahlenarithmetik betrieben werden. Das Calendar-Objekt
		// wird statisch vorgehalten und wiederverwendet. Damit muss die
		// Berechnung aber synchronized laufen!
		if (staticCal == null) {
			makeStaticCalendar();
		}
		
		int wert = 0;
		StringBuffer buf = new StringBuffer(26);
		
		synchronized (staticCal) {
			staticCal.clear();
			staticCal.setTime(aTimestamp);
		
			// JAHRE
			wert = staticCal.get(Calendar.YEAR);
			if ((wert > 9999) || (wert < 0)) {
				throw new IllegalArgumentException("Illegal argument for conversion to DB2 timestamp: "+aTimestamp);
			}
		
			if (wert < 10) {
				buf.append("000");
			} else if (wert < 100) {
				buf.append("00");
			} else if (wert < 1000) {
				buf.append('0');
			}
			buf.append(wert);
			buf.append('-');
		
			// MONATE
			wert = staticCal.get(Calendar.MONTH) + 1;
			if (wert < 10) {
				buf.append('0');
			}
			buf.append(wert);
			buf.append('-');
		
			// TAGE
			wert = staticCal.get(Calendar.DAY_OF_MONTH);
			if (wert < 10) {
				buf.append('0');
			}
			buf.append(wert);
			buf.append('-');
		
			// STUNDEN
			wert = staticCal.get(Calendar.HOUR_OF_DAY);
			if (wert < 10) {
				buf.append('0');
			}
			buf.append(wert);
			buf.append('.');
		
			// MINUTEN
			wert = staticCal.get(Calendar.MINUTE);
			if (wert < 10) {
				buf.append('0');
			}
			buf.append(wert);
			buf.append('.');
		
			// SEKUNDEN
			wert = staticCal.get(Calendar.SECOND);
			if (wert < 10) {
				buf.append('0');
			}
			buf.append(wert);
			buf.append('.');
		}
		
		// MIKRO-SEKUNDEN
		wert = aTimestamp.getNanos() / 1000;
		if (wert < 10) {
			buf.append("00000");
		} else if (wert < 100) {
			buf.append("0000");
		} else if (wert < 1000) {
			buf.append("000");
		} else if (wert < 10000) {
			buf.append("00");
		} else if (wert < 100000) {
			buf.append('0');
		}
		buf.append(wert);
		return buf;
	}

	/**
	 * Gebe die Minuten, Sekunden und Millisekunden der uebergebenen Zeit (long-Wert)
	 * als Standardstring im ISO-Format zurueck. Entsprechen die uebergebenen
	 * Millisekunden einer Zeit von groesser gleich einer Stunde, wird
	 * "59:59,999" zurueckgegeben.
	 * <p>
	 * Der Ausgabestring hat das Format:
	 * <p>
	 *		mm:ss,SSS
	 * <p><ul>
	 * <li>mm		Minute	 	 			2-stellig (00-59)
	 * <li>ss		Sekunde	 				2-stellig (00-59)
	 * <li>SSS		Milliosekunden 	3-stellig (000-999)
	 * </ul><p>
	 * Aufrufbeispiel:<pre>
	 *		long time = (30*60+15)*1000+123;
	 *		String str = TimeAndDateHelper.toMinutesAndSecondsString(time);
	 * liefert den String "30:15,123"
	 * </pre>
	 * Hinweis:<br>
	 * Diese Methode wurde hier implementiert, da sie ressourcenschonender
	 * als z.B. Timestamp.toString() oder die zahlreichen, parametergesteuerten
	 * Methoden der diversen Java Formatter-Klassen sind. Das Ergebnis dieser Methode
	 * muss identisch sein zu nachstehendem Code, laeuft aber 6x so schnell :-)<pre>
	 *	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("mm:ss,SSS");
	 *	long elapsedTime = Math.min(3599999, time);
	 *	return formatter.format(new Date(elapsedTime));
	 * </pre>
	 * @return String
	 * 		die entsprechende Stringdarstellung der uebergebenen Zeit (s.o.)
	 * @param time long
	 *		Die Zeit in Millisekunden; @see System#currentTimeMillis()
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String toMinutesAndSecondsString(long time) {
		if (time >= 3600000) {
			return "59:59,999";
		}

		StringBuffer buf = new StringBuffer(9);

		// Um an die Einzelbestandteile zu kommen, nutze ich ein Calendar Objekt.
		// Dieses wird statisch vorgehalten und wiederverwendet. Damit muss die
		// Berechnung aber synchronized laufen!
		if (staticCal == null) {
			makeStaticCalendar();
		}
		
		synchronized (staticCal) {
			staticCal.clear();
			staticCal.setTime(new Date(time));

			int value = staticCal.get(Calendar.MINUTE);
			if (value < 10) {
				buf.append('0');
			}
			buf.append(value);
			buf.append(':');
	
			value = staticCal.get(Calendar.SECOND);
			if (value < 10) {
				buf.append('0');
			}
			buf.append(value);
			buf.append(',');
	
			value = staticCal.get(Calendar.MILLISECOND);
			if (value < 10) {
				buf.append("00");
			} else if (value < 100) {
				buf.append('0');
			}
			buf.append(value);
		}

		return buf.toString();
	}

	/**
	 * Gebe Datum und Uhrzeit als Standardstring im ISO-Format zur
	 * uebergebenen Zeit (long-Wert) zurueck.
	 * <p>
	 * Der Ausgabestring hat das Format:
	 * <p>
	 *		yyyy-MM-dd HH:mm:ss,SSS
	 * <p><ul>
	 * <li>yyyy	Jahr		 		 	  4-stellig
	 * <li>MM		Monat			  	  2-stellig (01-12)
	 * <li>dd		Tag 						2-stellig (01-31)
	 * <li>HH		Stunde					2-stellig (00-23)
	 * <li>mm		Minute	 	 			2-stellig (00-59)
	 * <li>ss		Sekunde	 				2-stellig (00-59)
	 * <li>SSS		Milliosekunden 	3-stellig (000-999)
	 * </ul><p>
	 * Aufrufbeispiel:<pre>
	 * 	long time = System.currentTimeMillis();
	 * 	String str = TimeAndDateHelper.toTimeAndDateString(time);
	 * liefert am 11. August 2001 um Punkt 9 Uhr den String "2001-08-11 09:00:00,000"
	 * </pre>
	 * Hinweis:<br>
	 * Diese Methode wurde hier implementiert, da sie ressourcenschonender
	 * als z.B. Timestamp.toString() oder die zahlreichen, parametergesteuerten
	 * Methoden der diversen Java Formatter-Klassen sind. Das Ergebnis dieser Methode
	 * muss identisch sein zu nachstehendem Code, laeuft aber 40x so schnell :-)<pre>
	 * 	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss,SSS");
	 *	return formatter.format(new Date(time));
	 * </pre>
	 * @return String
	 * 		die entsprechende Stringdarstellung der uebergebenen Zeit (s.o.)
	 * @param time long
	 *		die Zeit in Millisekunden; @see System#currentTimeMillis()
	 * <p>
	 * @author Guyet, Sven
	 */
	public static String toTimeAndDateString(long time) {
		StringBuffer buf = new StringBuffer(23);

		// Um an die Einzelbestandteile zu kommen, nutze ich ein Calendar Objekt.
		// Dieses wird statisch vorgehalten und wiederverwendet. Damit muss die
		// Berechnung aber synchronized laufen!
		if (staticCal == null) {
			makeStaticCalendar();
		}
		
		synchronized (staticCal) {
			staticCal.clear();
			staticCal.setTime(new Date(time));
			//	return (yearString + "-" + monthString + "-" + dayString + " " + 
			//	hourString + ":" + minuteString + ":" + secondString + "."
			//			+ nanosString);

			buf.append(staticCal.get(Calendar.YEAR));
			buf.append('-');

			int value = staticCal.get(Calendar.MONTH) + 1;
			if (value < 10) {
				buf.append('0');
			}
			buf.append(value);
			buf.append('-');
	
			value = staticCal.get(Calendar.DAY_OF_MONTH);
			if (value < 10) {
				buf.append('0');
			}
			buf.append(value);
			buf.append(' ');
	
			value = staticCal.get(Calendar.HOUR_OF_DAY);
			if (value < 10) {
				buf.append('0');
			}
			buf.append(value);
			buf.append(':');
	
			value = staticCal.get(Calendar.MINUTE);
			if (value < 10) {
				buf.append('0');
			}
			buf.append(value);
			buf.append(':');
	
			value = staticCal.get(Calendar.SECOND);
			if (value < 10) {
				buf.append('0');
			}
			buf.append(value);
			buf.append(',');
	
			value = staticCal.get(Calendar.MILLISECOND);
			if (value < 10) {
				buf.append("00");
			} else if (value < 100) {
				buf.append('0');
			}
			buf.append(value);
		}

		return buf.toString();
	}
	
	/**
	 * Gebe zur�ck, ob es sich bei dem �bergebenen Timestamp um den minimalen Timestamp 
	 * (0001-01-01 00:00:00.000000) handelt.
	 * 
	 * @param ts java.sql.Timestamp
	 * 		Der zu pr�fende Timestamp
	 * @return boolean
	 * 		true, falls der �bergebene Timestamp gleich dem Minimaltimestamp ist, sonst false.
	 */
	public static final boolean isMinimumTimestamp(Timestamp ts){
		if (minimumTimestamp == null) {
			minimumTimestamp = new Timestamp(-62135773200000L);
		}
			
		return minimumTimestamp.equals(ts);	
	}
}