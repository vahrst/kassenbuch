package de.vahrst.application.util;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * Diese Klasse stellt Servicemethoden f�r numerische Aufgaben zur Verf�gung.
 * <p>
 * 
 * @author Vahrst, Thomas
 * @version 1.122 (Guyet, Sven - 06.11.2003 18:30)
 */
public abstract class NumericHelper {
	private static char[] radix = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9' };

	/**
	 * Konvertiert einen vorgegebenen BigDecimal-Wert in einen String. Die
	 * Nachkommastellen werden auf den vorgegebenen Wert skaliert, d.h. es wird
	 * entweder kaufmaennisch gerunden, bzw. es werden Nullen angehaengt.
	 * <p>
	 * Aufrufbeispiel:
	 * 
	 * <pre>
	 * 
	 * 		Fuer den BigDecimal-Wert x = 12.345 liefert
	 * 		bigDecToString(x, 0) den String &quot;12&quot;
	 * 		bigDecToString(x, 2) den String &quot;12,35&quot;
	 * 		bigDecToString(x, 3) den String &quot;12,345&quot;
	 * 		bigDecToString(x, 4) den String &quot;12,3450&quot;
	 *  
	 * <p>
	 * 
	 * 		Fuer den BigDecimal-Wert x = 12.543 liefert
	 * 		bigDecToString(x, 0) den String &quot;13&quot;
	 * 		bigDecToString(x, 2) den String &quot;12,54&quot;
	 * 		bigDecToString(x, 3) den String &quot;12,543&quot;
	 * 		bigDecToString(x, 4) den String &quot;12,5430&quot;
	 *  
	 * </pre>
	 * 
	 * @return String Ein String, der den uebergebenen BigDecimal-Wert skaliert
	 *         auf die vorgegebene Anzahl von Nachkommastellen enthaelt. Der
	 *         String wird bei Bedarf hinten mit Nullen aufgefuellt. Hat der
	 *         BigDecimal-Wert bereits mehr Nachkommastellen als gewuenscht, so
	 *         wird kaufmaennisch gerundet.
	 * @param wert
	 *          BigDecimal Der in einen String zu konvertierende Wert.
	 * @param nachkomma
	 *          int Die gewuenschte Anzahl von Nachkommastellen im
	 *          zurueckgelieferten String. Bei Bedarf werden Nullen angefuegt bzw.
	 *          gerundet.
	 *          <p>
	 * @author Guyet, Sven
	 */
	public static String bigDecToString(BigDecimal wert, int nachkomma) {
		return bigDecToString(wert, nachkomma, true);
	}

	/**
	 * Konvertiert einen vorgegebenen BigDecimal-Wert in einen String. Vor- und
	 * Nachkommastellen werden bei Bedarf durch fuehrende bzw. angehaengte Nullen
	 * auf die vorgegebenen Werte ergaenzt.
	 * <p>
	 * Aufrufbeispiele:
	 * 
	 * <pre>
	 * 
	 * 		Fuer den BigDecimal-Wert x = 12.345 liefert
	 * 		bigDecToString(x, 0, 0) den String   &quot;12,345&quot;
	 * 		bigDecToString(x, 3, 2) den String  &quot;012,345&quot;
	 * 		bigDecToString(x, 4, 4) den String &quot;0012,3450&quot;
	 *  
	 * <p>
	 * 
	 * 		Fuer den BigDecimal-Wert x = 12 liefert
	 * 		bigDecToString(x, 0, 0) den String   &quot;12&quot;
	 * 		bigDecToString(x, 3, 2) den String  &quot;012,00&quot;
	 * 		bigDecToString(x, 4, 4) den String &quot;0012,0000&quot;
	 *  
	 * </pre>
	 * 
	 * Sind die im BigDecimal enthaltenen Vor- bzw. Nachkommastellen bereits
	 * groesser gleich den jeweiligen Vorgaben, findet keine Justierung statt. Der
	 * zurueckgegebene String enthaelt nur dann ein Dezimaltrennzeichen, wenn
	 * explizit Nachkommastellen gefordert sind (nachkomma>0) oder der Scale
	 * (Anzahl der Nachkommastellen) der BigDecimal-Zahl groesser als 0 ist.
	 * 
	 * @return String Ein String, der den uebergebenen BigDecimal-Wert skaliert
	 *         auf die vorgegebene Anzahl von Vor- und Nachkommastellen enthaelt.
	 *         Der String wird bei Bedarf vorne und hinten mit Nullen aufgefuellt.
	 *         Hat der BigDecimal-Wert bereits mehr Vor- und/oder Nachkommastellen
	 *         als gewuenscht, bleiben diese erhalten. Der String enthaelt nur
	 *         dann ein Dezimaltrennzeichen, wenn der Scale des BigDecimal-Wertes
	 *         (die Anzahl der Nachkommastellen) groesser 0 ist oder explizit eine
	 *         Mindestzahl von Nachkommastellen vorgegeben wurde.
	 * @param wert
	 *          BigDecimal Der in einen String zu konvertierende Wert.
	 * @param minVorkomma
	 *          int Der gewuenschte Mindestanzahl von Vorkommastellen im
	 *          zurueckgelieferten String. Bei Bedarf werden fuehrende Nullen
	 *          vorangestellt.
	 * @param minNachkomma
	 *          int Die gewuenschte Mindestanzahl von Nachkommastellen im
	 *          zurueckgelieferten String. Bei Bedarf werden Nullen angefuegt.
	 *          <p>
	 * @author Guyet, Sven
	 */
	public static String bigDecToString(BigDecimal wert, int minVorkomma,
			int minNachkomma) {
		String s = wert.toString();
		if (wert.scale() == 0 && minNachkomma < 1) {
			// wert enthaelt keine Nachkommastellen, und es sind auch keine
			// gewuenscht; dann geht's einfach
			return StringHelper.padLengthLeft(s, minVorkomma, '0');
		}

		// Wert enthaelt Nachkommastellen: Zerteilen in die Vor- und
		// Nachkommastrings.
		StringTokenizer st = new StringTokenizer(s, ".");
		StringBuffer buf = new StringBuffer();
		buf.append(StringHelper.padLengthLeft(st.nextToken(), minVorkomma, '0'));
		String nk = st.hasMoreElements() ? st.nextToken() : "";
		nk = StringHelper.padLength(nk, minNachkomma, '0');
		if (nk.length() > 0) {
			buf.append(",").append(nk);
		}
		return buf.toString();
	}

	/**
	 * Konvertiert einen vorgegebenen BigDecimal-Wert in einen String. Die
	 * Nachkommastellen werden auf den vorgegebenen Wert skaliert, d.h. es wird
	 * entweder gerundet oder es werden Nullen angehaengt. Die Art des Rundens
	 * wird ueber das uebergebene Flag gesteuert; ist dies true, wird
	 * kaufmaennisch auf- oder abgerundet, bei false werden ueberzaehlige Stellen
	 * abgeschnitten.
	 * <p>
	 * Aufrufbeispiel:
	 * 
	 * <pre>
	 * 
	 * 		Fuer den BigDecimal-Wert x = 12.345 liefert
	 * 		bigDecToString(x, 0, true) den String &quot;12&quot;
	 * 		bigDecToString(x, 2, true) den String &quot;12,35&quot;
	 * 		bigDecToString(x, 3, true) den String &quot;12,345&quot;
	 * 		bigDecToString(x, 4, true) den String &quot;12,3450&quot;
	 * 		bigDecToString(x, 0, false) den String &quot;12&quot;
	 * 		bigDecToString(x, 2, false) den String &quot;12,34&quot;
	 * 		bigDecToString(x, 3, false) den String &quot;12,345&quot;
	 * 		bigDecToString(x, 4, false) den String &quot;12,3450&quot;
	 *  
	 * <p>
	 * 
	 * 		Fuer den BigDecimal-Wert x = 12.543 liefert
	 * 		bigDecToString(x, 0, true) den String &quot;13&quot;
	 * 		bigDecToString(x, 2, true) den String &quot;12,54&quot;
	 * 		bigDecToString(x, 3, true) den String &quot;12,543&quot;
	 * 		bigDecToString(x, 4, true) den String &quot;12,5430&quot;
	 * 		bigDecToString(x, 0, false) den String &quot;12&quot;
	 * 		bigDecToString(x, 2, false) den String &quot;12,54&quot;
	 * 		bigDecToString(x, 3, false) den String &quot;12,543&quot;
	 * 		bigDecToString(x, 4, false) den String &quot;12,5430&quot;
	 *  
	 * </pre>
	 * 
	 * @return String Ein String, der den uebergebenen BigDecimal-Wert skaliert
	 *         auf die vorgegebene Anzahl von Nachkommastellen enthaelt. Der
	 *         String wird bei Bedarf hinten mit Nullen aufgefuellt. Hat der
	 *         BigDecimal-Wert bereits mehr Nachkommastellen als gewuenscht, so
	 *         wird kaufmaennisch gerundet.
	 * @param wert
	 *          BigDecimal Der in einen String zu konvertierende Wert.
	 * @param nachkomma
	 *          int Die gewuenschte Anzahl von Nachkommastellen im
	 *          zurueckgelieferten String. Bei Bedarf werden Nullen angefuegt bzw.
	 *          gerundet.
	 * @param round
	 *          boolean Falls true, wird kaufmaennisch auf die gewuenschte Anzahl
	 *          von Nachkommastellen gerundet, bei false werden ueberzaehlige
	 *          Stellen abgeschnitten.
	 *          <p>
	 * @author Guyet, Sven
	 */
	public static String bigDecToString(BigDecimal wert, int nachkomma,
			boolean round) {
		int roundMode = round ? BigDecimal.ROUND_HALF_UP : BigDecimal.ROUND_DOWN;
		wert = wert.setScale(nachkomma, roundMode);
		return bigDecToString(wert, 0, 0);
	}

	/**
	 * konvertiert den vorgegebenen int-Wert in ein 4-stelliges Byte-Array
	 * 
	 * @return byte[] die byte Darstellung des Int-Werts
	 * @param int
	 *          der zu konvertierende int-Wert
	 */
	public static byte[] intToByte(int arg) {
		byte[] result = new byte[4];

		result[0] = (byte) (0xff & arg >>> 24);
		result[1] = (byte) (0xff & arg >>> 16);
		result[2] = (byte) (0xff & arg >>> 8);
		result[3] = (byte) (0xff & arg);

		return result;
	}

	/**
	 * Konvertiert einen vorgegebenen int-Wert in einen String vorgegebener
	 * Laenge, indem ggf. fuehrende Nullen hinzugefuegt werden.
	 * <p>
	 * Aufrufbeispiel:
	 * 
	 * <pre>
	 * 
	 * 		intToString(123, 6)
	 * 		liefert den String &quot;000123&quot; zurueck.
	 *  
	 * </pre>
	 * 
	 * Es findet keine Grenzpruefung statt, d.h. der Aufrufer muss sicherstellen,
	 * dass length ausreichend gross ist.
	 * 
	 * @return String Ein String der Laenge length, der den uebergebenen int-Wert
	 *         mit ggf. fuehrenden Nullen enthaelt.
	 * @param arg
	 *          int Der zu konvertierende int-Wert
	 * @param length
	 *          int Die gewuenschte Laenge des Ergebnisstrings
	 *          <p>
	 * @author Guyet, Sven
	 */
	public static String intToString(int arg, int length) {
		return intToString(arg, length, '0');
	}

	/**
	 * Konvertiert einen vorgegebenen int-Wert in einen String vorgegebener
	 * Laenge, indem ggf. das vorgegebene Fuellzeichen am Anfang hinzugefuegt
	 * wird.
	 * <p>
	 * Aufrufbeispiel:
	 * 
	 * <pre>
	 * 
	 * 		intToString(123, 6, 'x')
	 * 		liefert den String &quot;xxx123&quot; zurueck.
	 *  
	 * </pre>
	 * 
	 * Es findet keine Grenzpruefung statt, d.h. der Aufrufer muss sicherstellen,
	 * dass length ausreichend gross ist.
	 * 
	 * @return String Ein String der Laenge length, der den uebergebenen int-Wert
	 *         mit ggf. fuehrenden Nullen enthaelt.
	 * @param arg
	 *          int Der zu konvertierende int-Wert
	 * @param length
	 *          int Die gewuenschte Laenge des Ergebnisstrings
	 * @param filler
	 *          char Das Fuellzeichen, mit dem der Ergebnisstring auf die
	 *          gewuenschte Laenge gebracht wird.
	 *          <p>
	 * @author Guyet, Sven
	 */
	public static String intToString(int arg, int length, char filler) {
		char[] buf = new char[length];
		boolean negative = (arg < 0);
		int charPos = length;

		Arrays.fill(buf, filler);

		if (negative)
			arg = -arg;

		do {
			int digit = arg % 10;
			buf[--charPos] = radix[digit];
			arg = arg / 10;
		} while (arg != 0 && charPos > 0);

		if (negative) {
			buf[--charPos] = '-';
		}

		return new String(buf);
	}

	/**
	 * Liefert die Quersumme der vorgegebenen Zahl.
	 * 
	 * @return int Die Quersumme
	 * @param value
	 *          long Die Zahl, deren Quersumme zurueckgegeben wird.
	 *          <p>
	 * @author Guyet, Sven
	 */
	public static int quersumme(long value) {
		int summe = 0;
		if (value < 0)
			value = -value;

		while (value > 0) {
			long temp = value / 10;
			summe += value - temp * 10;
			value = temp;
		}
		return summe;
	}

	/**
	 * konvertiert den vorgegebenen short-Wert in ein 2-stelliges Byte-Array
	 * 
	 * @return byte[] die byte Darstellung des short-Werts
	 * @param short
	 *          der zu konvertierende short-Wert
	 */
	public static byte[] shortToByte(short arg) {
		byte[] result = new byte[2];

		result[0] = (byte) (0xff & arg >>> 8);
		result[1] = (byte) (0xff & arg);

		return result;
	}

	/**
	 * Converts an long into a 8-byte array
	 * 
	 * @return The position after the long
	 * @param l
	 *          The long
	 * @param b
	 *          The byte array
	 * @param pos
	 *          Position within the byte array to start
	 *          <p>
	 * @author Vahrst, Thomas
	 */
	public static byte[] longToByte(long l) {
		byte[] b = new byte[8];
		int pos = 0;

		b[pos++] = (byte) ((l >> 56) & 0xff);
		b[pos++] = (byte) ((l >> 48) & 0xff);
		b[pos++] = (byte) ((l >> 40) & 0xff);
		b[pos++] = (byte) ((l >> 32) & 0xff);
		b[pos++] = (byte) ((l >> 24) & 0xff);
		b[pos++] = (byte) ((l >> 16) & 0xff);
		b[pos++] = (byte) ((l >> 8) & 0xff);
		b[pos++] = (byte) (l & 0xff);

		return b;
	}

	
	/**
	 * Converts a double into a 8-byte array
	 * 
	 * @return The position after the double
	 * @param d
	 *          The double
	 *          <p>
	 * @author Vahrst, Thomas
	 */
	public static byte[] doubleToByte(double d) {
		long l = Double.doubleToLongBits(d);
		return longToByte(l);
	}

	/**
	 * Converts 8 bytes from a byte array to a double value
	 * 
	 * @return The double value
	 * @param b
	 *          The byte array
	 * @param pos
	 *          Position within the byte array to start
	 *          <p>
	 * @author Vahrst, Thomas
	 */
	public static double byteToDouble(byte b[], int pos) {
		long l = byteToLong(b, pos);

		return Double.longBitsToDouble(l);
	}

	/**
	 * Converts 8 bytes from a byte array to a long value
	 * 
	 * @return The long value
	 * @param b
	 *          The byte array
	 * @param pos
	 *          Position within the byte array to start
	 *          <p>
	 * @author Vahrst, Thomas
	 */
	public static long byteToLong(byte b[], int pos) {
		long l;
		l = byteToUnsignedLong(b[pos++]) << 56;
		l |= byteToUnsignedLong(b[pos++]) << 48;
		l |= byteToUnsignedLong(b[pos++]) << 40;
		l |= byteToUnsignedLong(b[pos++]) << 32;
		l |= byteToUnsignedLong(b[pos++]) << 24;
		l |= byteToUnsignedLong(b[pos++]) << 16;
		l |= byteToUnsignedLong(b[pos++]) << 8;
		l |= byteToUnsignedLong(b[pos]);

		return l;
	}

	/**
	 * Converts a byte into a (unsigned) long (0-255)
	 * 
	 * @return The long
	 * @param b
	 *          The byte
	 *          <p>
	 * @author Vahrst, Thomas
	 */
	public static long byteToUnsignedLong(byte b) {
		return  (( b & 0x07f) + ((b < 0) ? 0x80 : 0));
	}

	/**
	 * Converts a byte into a long (-128 bis 127)
	 * 
	 * @return The long
	 * @param b
	 *          The byte
	 *          <p>
	 * @author Vahrst, Thomas
	 */
	public static long byteToLong(byte b) {
		return  b;
	}

	/**
	 * Gebe den numerischen Wert des durch beginIndex und endIndex definierten
	 * Teilstrings in arg zurueck. Analog zu String.substring() ist beginIndex der
	 * Index des ersten zu beruecksichtigenden Zeichens, endIndex der Index des
	 * letzten Zeichens zuzueglich 1. arg muss im durch beginIndex und endIndex
	 * definierten Bereich ausschliesslich Ziffern enthalten.
	 * <p>
	 * Spezielle Implementierung, da ressourcenschonender als die sehr allgemeine
	 * Methode Integer.parseInt(arg.substring(,)). Spart den Substring und ist
	 * auch sonst schneller, allerdings muessen alle Stellen im Teilbereich auch
	 * wirklich Ziffern enthalten. Somit werden auch keine negativen Zahlen
	 * unterstuetzt :-(
	 * <p>
	 * Aufrufbeispiel:
	 * 
	 * <pre>
	 * 
	 *  	parseInt(&quot;Trullala 12345 blablabla&quot;, 9, 13)
	 *  liefert die Zahl 1234 (Position 9 bis 12) zurueck.
	 *  
	 * </pre>
	 * 
	 * @return int Die zwischen beginIndex (inklusiv) und endIndex (exklusiv) in
	 *         arg enthaltene Zahl.
	 * @param arg
	 *          java.lang.String Der String, aus dem der Rueckgabewert zu
	 *          extrahieren ist.
	 * @param beginIndex
	 *          int Anfangsindex in arg fuer die zu ermittelnde Zahl (inklusive).
	 * @param endIndex
	 *          int Endeindex in arg fuer die zu ermittelnde Zahl (exklusive).
	 * @exception NumberFormatException
	 *              Der Bereich beginIndex;endIndex in arg enthaelt nicht
	 *              ausschliesslich Ziffern.
	 * @exception IndexOutOfBoundsException
	 *              Der Bereich beginIndex;endIndex liegt nicht innerhalb der
	 *              Grenzen von arg.
	 *              <p>
	 * @author Guyet, Sven
	 */
	public static int stringToInt(String arg, int beginIndex, int endIndex)
			throws NumberFormatException, IndexOutOfBoundsException {

		char zeichen;
		int result = 0;
		for (int i = beginIndex; i < endIndex; i++) {
			zeichen = arg.charAt(i);
			if (zeichen >= '0' && zeichen <= '9') {
				result = result * 10 + zeichen - 48;
			} else {
				throw new NumberFormatException();
			}
		}
		return result;
	}
	
	
	/**
	 * Gibt den numerischen int-Wert des vorgegebenen Strings zur�ck. Falls
	 * beim Parsen eine Exception auftritt, wird der vorgegebene defaultValue zur�ckgegeben.
	 * <p>
	 * Diese Methode nutzt den 'regul�ren' parseInt() Service aus der Klasse Integer und
	 * nicht die optimierte Version aus dieser Klasse, weil hier auch negative Integers
	 * ber�cksichtigt werden k�nnen.
	 * 
	 * @return int
	 * 		Das Ergebnis der Konvertierung bzw. defaultValue, falls die Konvertierung zu einer
	 * 		Exception gef�hrt hat.
	 * @param arg String
	 * 		Der in einen int-Wert zu konvertierende String
	 * @param defaultValue int
	 * 		Der zur�ckzugebende Wert, falls bei der Konvertierung eine Exception auftritt.
	 */
	public static int stringToInt(String arg, int defaultValue) {
		try {
			return Integer.parseInt(arg);
		} catch (Exception e) {
			return defaultValue;
		}
	}
}