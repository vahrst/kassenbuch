package de.vahrst.application.util;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
/**
 * Stellt Hilfsroutinen f�r Strings zur Verfuegung.
 * <p>
 * @see java.lang.String
 * <p>
 * @author Guyet, Sven (IBM)
 * @version 2.124 (Guyet, Sven - 04.12.2003 19:19) 
 */
public final class StringHelper {
	/**
	 * Objekt mit der Funktionalitaet, zwei Zeichenketten gemaess
	 * der dt. Rechtschreibung miteinander zu vergleichen.
	 */
	private static final Collator collator = Collator.getInstance(java.util.Locale.GERMAN);

	/**
	 * Defaultposition f�r den Zeilenumbruch in der format-Methode.
	 */
	private static final int DEFAULT_LINE_LENGTH = 90;

	/**
	 * Konstantes, leeres immutable int-Array
	 */
	private static final int[] EMPTY_INT_ARRAY = new int[0];

	/**
	 * Konstantes, leeres immutable String-Array
	 */
	private static final String[] EMPTY_STRING_ARRAY = new String[0];
	/** 
	 * Hilfsvariablen fuer den Soundex Algorithmus; Zuordnung von
	 * Buchstaben zur Wertigkeit
	 */
	private static final String LETTER = "AEIOU���HWYBFPVCGJKQSXZDTLMNR";
	private static final String SOUNDEX = "00000000000111122222222334556";

	/**
	 * Standard-Konstruktor ist private, da diese Klasse nur statische Services
	 * bereitstellt und niemals instanziiert wird.
	 */
	private StringHelper() {
		// Verhindere Instanziierung
	}

	/** 
	 * Der von java.lang.System einmal ermittelte String zur Zeilenumschaltung.
	 */
	public static final String LINE_SEPARATOR = System.getProperty("line.separator", "\n");
	
	/**
	 * Bringe den uebergebenen String auf die vorgegebene Laenge. Laengere Strings
	 * werden entsprechend hinten abgeschnitten, kuerzere String mit Leerzeichen
	 * hinten aufgefuellt. Gebe das Resultat zurueck.
	 * <p>
	 * Die Uebergabe von null als Ursprungsstring wird wie "" behandelt.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.adjustLength("Sven", 6);
	 * liefert den String "Sven  " zurueck.
	 * 	StringHelper.adjustLength("Sven Guyet", 6);
	 * liefert den String "Sven G" zurueck.
	 * 	StringHelper.adjustLength(null, 6);
	 * liefert den String "      " zurueck.
	 * </PRE>
	 * @return String
	 *		der mit Leerzeichen aufgefuellte String
	 * @param input java.lang.String
	 *		der hinten aufzufuellende bzw. abzuschneidende String
	 * @param gesamtlaenge int
	 *		die Gesamtlaenge des zurueckzugebenden Strings
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String adjustLength(String input, int gesamtlaenge) {
		return adjustLength(input, gesamtlaenge, ' ');
	}

	/**
	 * Bringe den uebergebenen String auf die vorgegebene Laenge. Laengere Strings
	 * werden entsprechend hinten abgeschnitten, kuerzere String mit dem vorgegebenen
	 * Zeichen hinten aufgefuellt. Gebe das Resultat zurueck.
	 * <p>
	 * Die Uebergabe von null als Ursprungsstring wird wie "" behandelt.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.adjustLength("Sven", 6, '$');
	 * liefert den String "Sven$$" zurueck.
	 * 	StringHelper.adjustLength("Sven Guyet", 6, '$');
	 * liefert den String "Sven G" zurueck.
	 * 	StringHelper.adjustLength(null, 6, ' ');
	 * liefert den String "      " zurueck.
	 * </PRE>
	 * @return String
	 *		der auf die gewuenschte Laenge gebrachte String
	 * @param input java.lang.String
	 *		der hinten aufzufuellende bzw. abzuschneidende String
	 * @param gesamtlaenge int
	 *		die Gesamtlaenge des zurueckzugebenden Strings
	 * @param filler char
	 *		das Fuellzeichen
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String adjustLength(String input, int gesamtlaenge, char filler) {
		if (input == null) {
			return padLength("", gesamtlaenge, filler, false);
		}
		if (input.length() > gesamtlaenge) {
			return input.substring(0, gesamtlaenge);
		}
		return padLength(input, gesamtlaenge, filler, false);
	}

	/**
	 * Bringe den uebergebenen String auf die vorgegebene Laenge. Laengere Strings
	 * werden entsprechend hinten abgeschnitten, kuerzere String mit Leerzeichen
	 * vorne aufgefuellt. Gebe das Resultat zurueck.
	 * <p>
	 * Die Uebergabe von null als Ursprungsstring wird wie "" behandelt.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.adjustLengthLeft("Sven", 6);
	 * liefert den String "  Sven" zurueck.
	 * 	StringHelper.adjustLengthLeft("Sven Guyet", 6);
	 * liefert den String "Sven G" zurueck.
	 * 	StringHelper.adjustLengthLeft(null, 6);
	 * liefert den String "      " zurueck.
	 * </PRE>
	 * @return String
	 *		der mit Leerzeichen aufgefuellte String
	 * @param input java.lang.String
	 *		der vorne aufzufuellende bzw. hinten abzuschneidende String
	 * @param gesamtlaenge int
	 *		die Gesamtlaenge des zurueckzugebenden Strings
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String adjustLengthLeft(String input, int gesamtlaenge) {
		return adjustLengthLeft(input, gesamtlaenge, ' ');
	}

	/**
	 * Bringe den uebergebenen String auf die vorgegebene Laenge. Laengere Strings
	 * werden entsprechend hinten abgeschnitten, kuerzere String mit dem vorgegebenen
	 * Zeichen vorne aufgefuellt. Gebe das Resultat zurueck.
	 * <p>
	 * Die Uebergabe von null als Ursprungsstring wird wie "" behandelt.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.adjustLengthLeft("Sven", 6, '$');
	 * liefert den String "$$Sven" zurueck.
	 * 	StringHelper.adjustLengthLeft("Sven Guyet", 6, '$');
	 * liefert den String "Sven G" zurueck.
	 * 	StringHelper.adjustLengthLeft(null, 6, ' ');
	 * liefert den String "      " zurueck.
	  * </PRE>
	 * @return String
	 *		der auf die gewuenschte Laenge gebrachte String
	 * @param input java.lang.String
	 *		der vorne aufzufuellende bzw. hinten abzuschneidende String
	 * @param gesamtlaenge int
	 *		die Gesamtlaenge des zurueckzugebenden Strings
	 * @param filler char
	 *		das Fuellzeichen
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String adjustLengthLeft(String input, int gesamtlaenge, char filler) {
		if (input == null) {
			return padLength("", gesamtlaenge, filler, true);
		}
		if (input.length() > gesamtlaenge) {
			return input.substring(0, gesamtlaenge);
		}
		return padLength(input, gesamtlaenge, filler, true);
	}

	/**
	 * Vergleicht die beiden uebergebenen Strings gemae� der deutschen Rechtschreibung.
	 * Liefert das Ergebnis des Vergleichs zurueck.
	 *
	 * @return	int
	 * 			Ergebnis des Vergleichs; negativ wenn str1 kleiner als str2, 0 wenn str1 gleich str2,
	 *			positiv wenn str1 groesser als str2
	 * @param	str1 String
	 * 			Erstes Vergleichsobjekt
	 * @param	str2 String
	 * 			Zweites Vergleichsobjekt
	 * <p>
	 * @author Guyet, Sven
	 */
	public static final int compare(String str1, String str2) {
		if (str1 == null) {
			return (str2 == null) ? 0 : -1;
		}
		if (str1.equals(str2)) {
			return 0;
		}
		if (str2 == null) {
			return 1;
		}

		int i = 0;
		synchronized (collator) {
			i = collator.compare(str1, str2);
		}
		return i;
	}

	/**
	 * Gebe den Soundex Code zum uebergebenen String zurueck. Der Soundex Algorithmus
	 * wurde von Donald E. Knuth entwickelt und dient zur Reduktion eines Wortes auf
	 * signifikante Buchstaben. Aufgrund einer Tabelle werden den Buchstaben eines
	 * Wortes Werte zugeordnet, welche die Signifikanz eines Buchstabens innerhalb
	 * des Wortes ausdruecken. Signifikanzschwache Buchstaben werden entfernt, und
	 * zurueck bleibt ein vierstelliger Code.
	 * <p>
	 * Der Soundex Code wird in 6 Einzelschritten generiert:
	 * <ol>
	 * <li>Umwandlung in Grossbuchstaben</li>
	 * <li>Ersten Buchstaben merken</li>
	 * <li>Alle Buchstaben durch Wertigkeit ersetzen</li>
	 * <li>Ersten Buchstaben mit allen Wiederholungen entfernen</li>
	 * <li>Wiederholende Wertigkeitsziffern auf ein Vorkommen reduzieren</li>
	 * <li>Soundex Code auf 4 Stellen beschneiden bzw. erweitern (durch 0)</li>
	 * </ol>
	 * <p>
	 * Aufrufbeispiele:<br>
	 *   computeSoundex("Meier") liefert denselben Ergebnisstring wie
	 *   computeSoundex("Mayer")
	 *
	 * @return String
	 *	Der Soundexwert des uebergebenen Strings
	 * @param argStr String
	 *	Zeichenkette, deren Soundexwert ermittelt und zurueckgegeben werden soll
	 *
	 * @author Guyet, Sven
	 * @version
	 **/
	public static final String computeSoundex(String argStr) {
		if (argStr == null) {
			return "0000";
		}

		// Schritt 1: Umwandlung in Grossbuchstaben (Wolfenbuettel -> WOLFENBUETTEL)
		// Schritt 2: Erstes Zeichen merken (WOLFENBUETTEL -> W)
		// Schritt 3: Alle Zeichen durch Wertigkeit ersetzen (WOLFENBUETTEL -> 0041051003304)
		// Nicht relevante Zeichen (Ziffern, Sonderzeichen etc.) werden entfernt.
		String str = argStr.trim();
		if (str.length() == 0) {
			return " 000";
		}
		StringBuffer buffer = (new StringBuffer()).append(str.charAt(0));
		str = convertToSoundexDigits(str);

		// Schritt 4: Erstes Zeichen und gleiche folgende entfernen (0041051003304 -> 41051003304)
		char tmpChar = str.charAt(0);
		int i = 1;
		while (i < str.length() && str.charAt(i) == tmpChar) {
			i++;
		}
		str = str.substring(i);

		// Schritt 5: Verbleibende doppelte Zeichen auf ein Vorkommen reduzieren (41051003304 -> 410510304)
		char lastChar = 'x'; // dummy value
		for (i = 0; i < str.length(); i++) {
			tmpChar = str.charAt(i);
			if ((tmpChar != lastChar) && (tmpChar != '0')) {
				buffer.append(tmpChar);
			}
			lastChar = tmpChar;
		}

		// Schritt 6: Normierung auf 4 Zeichen, ggf. durch Anhaengen von "0" (415134 -> 415)
		buffer.append("000");
		return buffer.toString().substring(0, 4);
	}

	/**
	 * pr�ft ob der �bergebene String nur aus Buchstaben besteht.
	 * Leerzeichen sind keine Buchstaben!
	 * Creation date: (13.02.03 10:31:11)
	 * @return boolean
	 * @param aString java.lang.String
	 */
	public static final boolean containsOnlyLetters(String aString) {
		boolean erg = true;
		if (aString == null || aString.length() == 0) {
			return false;
		}
		for (int i = 0; i < aString.length(); i++) {
			if (!Character.isLetter(aString.charAt(i))) {
				erg = false;
				break;
			}
		}
		return erg;
	}

	/**
	 * pr�ft ob der �bergebene String nur auz Ziffern besteht.
	 * Es wird nicht �berpr�ft, ob der String eine g�ltige Zahl
	 * repr�sentiert, sondern ob er nur aus Ziffern besteht.
	 * Creation date: (13.02.03 09:26:39)
	 * @return boolean
	 * @param aString java.lang.String
	 */
	public static final boolean containsOnlyNumbers(String aString) {
		boolean erg = true;
		if (aString == null || aString.length() == 0) {
			return false;
		}
		for (int i = 0; i < aString.length(); i++) {
			if (!Character.isDigit(aString.charAt(i))) {
				erg = false;
				break;
			}
		}
		return erg;
	}

	/**
	 * Liefere einen String zurueck, der die Inhalte des uebergebenen 2-dimensionalen Arrays,
	 * getrennt durch das uebergebene Token, enthaelt. Dabei wird der Index der ersten
	 * Dimension festgehalten, waehrend durch die zweite Dimension iteriert wird.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * Das Array
	 * 	A1 A2
	 * 	B1 B2
	 * 	C1 C2
	 * fuehrt mit dem Token "-" zu folgendem Ergebnis:
	 * 	"A1-A2-B1-B2-C1-C2"
	 * </PRE>
	 * @return String
	 *		der Inhalt des Arrays, getrennt durch das uebergebene Token.
	 * @param arg	java.lang.String[][]
	 *		das Array mit den zu konkatenierenden Werten
	 * @param token java.lang.String
	 *		die zu verwendenden Trennzeichen
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String convertArrayToSeparatedString(String[][] arg, String token) {
		if (arg == null) {
			return "";
		}

		StringBuffer value = new StringBuffer();
		for (int y = 0; y < arg.length; y++) {
			for (int i = 0; i < arg[y].length; i++) {
				// erstes token
				if (y == 0 && i == 0) {
					value.append("");
				} else {
					value.append(token);
				}
				value.append(arg[y][i]);
			}
		}
		return value.toString();
	}

	/**
	 * Liefere einen String zurueck, der die Inhalte des uebergebenen Arrays, getrennt
	 * durch das uebergebene Token, enthaelt.
	 * <p>
	 * @return String
	 *		der Inhalt des Arrays, getrennt durch das uebergebene Token.
	 * @param arg	int[]
	 *		das Array mit den zu konkatenierenden Werten
	 * @param token java.lang.String
	 *		die zu verwendenden Trennzeichen
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String convertArrayToSeparatedString(int[] arg, String token) {
		if (arg == null) {
			return "";
		}

		StringBuffer value = new StringBuffer();
		for (int i = 0; i < arg.length; i++) {
			if (i > 0) {
				value.append(token);
			}
			value.append(arg[i]);
		}
		return value.toString();
	}

	/**
	 * Liefere einen String zurueck, der die Inhalte des uebergebenen Arrays, getrennt
	 * durch das uebergebene Token, enthaelt.
	 * <p>
	 * @return String
	 *		der Inhalt des Arrays, getrennt durch das uebergebene Token.
	 * @param arg	java.lang.String[]
	 *		das Array mit den zu konkatenierenden Werten
	 * @param token java.lang.String
	 *		die zu verwendenden Trennzeichen
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String convertArrayToSeparatedString(String[] arg, String token) {
		if (arg == null) {
			return "";
		}

		StringBuffer value = new StringBuffer();
		for (int i = 0; i < arg.length; i++) {
			if (i > 0) {
				value.append(token);
			}
			value.append(arg[i]);
		}
		return value.toString();
	}

	/**
	 * Gebe f�r einen Leerstring(blank oder null) den uebergebenen
	 * Default-String zur�ck.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	LVMStringHelper.convertEmptyStringToDefault("", "default")
	 * liefert "default" zurueck.
	 * 	LVMStringHelper.convertEmptyStringToDefault("  ", "default")
	 * liefert "default" zurueck.
	 * 	LVMStringHelper.convertEmptyStringToDefault(null, "default")
	 * liefert "default" zurueck.
	 * 	LVMStringHelper.convertEmptyStringToDefault("4711", "default")
	 * liefert "4711" zurueck.
	 * 	LVMStringHelper.convertEmptyStringToDefault("Ein 4711 String", "default")
	 * liefert "Ein 4711 String" zurueck.
	 * </PRE>
	 * @return String
	 *		Der uebergebene Default-String, wenn der zu konvertierende String null war
	 *		oder nur aus Leerzeichen bestand.
	 * 		Den unveraenderten, zu konvertierenden String sonst
	 * @param input java.lang.String
	 * 		Der zu konvertierende String String
	 * @param defaultOutput java.lang.String
	 * 		Der Default-String fuer die Faelle, dass input null ist oder nur aus Leerzeichen besteht.
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String convertEmptyStringToDefault(String input, String defaultOutput) {
		// Falls null oder nur Leerzeichen, den Default-String zurueckgeben.
		// Ansonsten den Originalstring zurueckgeben.
		return ((input == null) || (input.trim().length() == 0)) ? defaultOutput : input;
	}

	/**
	 * Gebe f�r einen Leerstring(blank oder null) den String "0" zur�ck.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	LVMStringHelper.convertEmptyStringToZero("")
	 * liefert "0" zurueck.
	 * 	LVMStringHelper.convertEmptyStringToZero("  ")
	 * liefert "0" zurueck.
	 * 	LVMStringHelper.convertEmptyStringToZero(null)
	 * liefert "0" zurueck.
	 * 	LVMStringHelper.convertEmptyStringToZero("4711")
	 * liefert "4711" zurueck.
	 * 	LVMStringHelper.convertEmptyStringToZero("Ein 4711 String")
	 * liefert "Ein 4711 String" zurueck.
	 * </PRE>
	 * @return String
	 *		"0", wenn der uebergebene String null war oder nur aus Leerzeichen bestand
	 * 		den unveraenderten, uebergebenen String sonst
	 * @param input java.lang.String
	 * 		String
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String convertEmptyStringToZero(String input) {
		return convertEmptyStringToDefault(input, "0");
	}

	/**
	 * Diese Methode liefert einen String zur�ck, der dem uebergebenen String nach
	 * Umsetzen des ersten Zeichens in einen Gross- bzw. Kleinbuchstaben entspricht.
	 * Ist der Ursprungsstring null, wird null zurueckgegeben.
	 * Ist der Ursprungsstring leer, oder beginnt nicht mit einem Buchstaben, oder
	 * das erste Zeichen erfuellt bereits die Umsetzungsbedingung, so wird der 
	 * Ursprungsstring zurueckgegeben.
	 * <p>
	 * @return String
	 *		der Outputstring
	 * @param source String
	 *		der Inputstring
	 * <p>
	 * @see	java.lang.Character#toUpperCase()
	 * @see	java.lang.Character#toLowerCase()
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	private static final String convertFirstChar(String source, boolean toUpperCase) {
		if ((source == null) || (source.length() == 0)) {
			return source;
		}
		if (toUpperCase) {
			if (Character.isUpperCase(source.charAt(0))) {
				// Isch habe gar keine Kleinbuchstabe am Anfang
				return source;
			}
			// Ok, ok, muss tatsaechlich was tun
			char[] newChars = source.toCharArray();
			newChars[0] = Character.toUpperCase(newChars[0]);
			return new String(newChars);
		}
		
		if (Character.isLowerCase(source.charAt(0))) {
			// Isch habe gar keine Grossbuchstabe am Anfang
			return source;
		}
		// Ok, ok, muss tatsaechlich was tun
		char[] newChars = source.toCharArray();
		newChars[0] = Character.toLowerCase(newChars[0]);
		return new String(newChars);
	}

	/**
	 * Konvertiere einen String, getrennt durch das uebergebene Token in ein int-Array 
	 * und liefere dieses zur�ck. 
	 * Wenn der String nicht auswertbar ist, wird ein leeres int-Array zur�ckgegeben.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	convertStringtoIntArray("1,2,2",",");
	 * liefert: int[] {1,2,3};
	 * </PRE>
	 * @return int[]
	 *		das durch die Token getrennte int-Array
	 * @param	java.lang.String
	 *		der zu konvertierende String
	 * @param	java.lang.String
	 *		die zu verwendenden Trennzeichen	
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final int[] convertStringToIntArray(String arg, String token) {
		if (arg == null) {
			return EMPTY_INT_ARRAY;
		}

		StringTokenizer st = new StringTokenizer(arg, token);
		int[] value = new int[st.countTokens()];
		int i = 0;

		while (st.hasMoreElements()) {
			try {
				value[i] = Integer.parseInt(st.nextElement().toString());
				i++;
			} catch (NumberFormatException nfe) {
				return EMPTY_INT_ARRAY;
			}
		}
		return value;
	}

	/**
	 * Konvertiere den �bergebenen String in einen "g�ltigen" DB2-String und gebe
	 * diesen zur�ck. Ist der String leer, null, oder enth�lt der String kein
	 * einziges Hochkomma, so wird der Originalwert zur�ckgegeben.
	 * <p>
	 * Beschreibung:<br>
	 * "G�ltig" bedeutet in diesem Zusammenhang, dass im Ursprungsstring vorhandene 
	 * Hochkommas durch jeweils zwei Hochkommas ersetzt werden. Hintergrund ist, 
	 * dass in SQL Strings durch Hochkommas begrenzt werden. Kommt nun innerhalb 
	 * des Strings ein Hochkomma vor, so w�rde dies z.B. bei Inserts oder Updates 
	 * zu SQL-Fehlern f�hren.
	 * <p>
	 * Verdoppelt man indessen diese Hochkommas, dann ist SQL zufrieden, und letztendlich
	 * landet das einfache Hochkomma in der DB-Tabelle.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	  convertStringToSQLFormat(null)
	 *				liefert null
	 * 	  convertStringToSQLFormat("")
	 *				liefert "" (den Ursprungsstring)
	 * 	  convertStringToSQLFormat("McDonalds") 
	 *				liefert "McDonalds" (den Ursprungsstring)
	 * 	  convertStringToSQLFormat("McDonald's") 
	 *				liefert "McDonald''s"
	 * 	  convertStringToSQLFormat("McDonald''s") 
	 *				liefert "McDonald''''s"
	 * </PRE>
	 * @return String
	 *		der "g�ltige" String, d.h. der Ursprungsstring, wobei jedes Hochkomma durch
	 * 		zwei Hochkommas ersetzt wurde.
	 * @param	input java.lang.String
	 *		der zu konvertierende String
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String convertStringToSQLFormat(String input) {
		if (input == null || input.indexOf("'") < 0) {
			return input;
		}
		return replace(input, "'", "''");
	}

	/**
	 * Diese Methode liefert einen String zur�ck, in dem alle Vorkommen der
	 * vom Soundex-Algorithmus beruecksichtigten Zeichen durch die entsprechende
	 * Soundex-Wertigkeit ersetzt wird. Die Zeichen, die fuer den Soundex-Code
	 * nicht relevant sind (Ziffern, Sonderzeichen etc.) werden eliminiert.
	 * <p>
	 * @see computeSoundex
	 * <p>
	 * @return String
	 *		der Inputstring nach Transformation in die Soundex-Wertigkeiten
	 * @param input String
	 *		Inputstring, dessen Zeichen durch die Soundex-Wertigkeit ersetzt werden sollen
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	private static final String convertToSoundexDigits(String input) {
		if (input == null) {
			return null;
		}

		String source = input.toUpperCase();
		int sourceLen = source.length();
		char[] newArray = new char[sourceLen];
		// char-Array f�r den neuen String. Laenger als der Input kann's nicht werden.

		int pos = 0;
		char tmpChar;
		for (int i = 0; i < sourceLen; i++) {
			tmpChar = source.charAt(i);
			for (int j = 0; j < LETTER.length(); j++) {
				if (tmpChar == LETTER.charAt(j)) {
					newArray[pos++] = SOUNDEX.charAt(j);
					break;
				}
			}
		}
		return new String(newArray, 0, pos);
	}

	/**
	 * Liefert einen String, der den �bergebenen String n-mal enth�lt.
	 * <p>
	 * Aufrufbeispiel:<br>
	 * StringHelper.copy("abc", 3) liefert "abcabcabc"
	 * 
	 * @return java.lang.String
	 * 			Die n-fach Kopie des Inputstrings
	 * @param string java.lang.String
	 * 			Der zu kopierende String
	 * @param number int
	 * 			Anzahl, wie oft der �bergebene String kopiert werden soll.
	 * @exception java.lang.IllegalArgumentException
	 * 			Wenn der �bergeben String gleich null oder die �bergebene
	 *			Anzahl kleiner als 0 ist.
	 * <p>
	 * @author Heerdegen, Andreas
	 */
	public static final String copyString(String string, int number) {
		if (string == null || number < 0) {
			throw new IllegalArgumentException();
		}

		// Erstmal die Trivialf�lle, bei denen nichts zu tun ist, abhandeln.
		if (number == 0) {
			return "";
		}
		if (number == 1) {
			return string;
		}

		char[] source = string.toCharArray();
		int length = source.length;
		char[] destination = new char[number * length];
		for (int i = 0; i < number; i++) {
			System.arraycopy(source, 0, destination, (i * length), length);
		}
		return String.copyValueOf(destination);
	}

	/**
	 * Gibt den �bergebenen String, gek�rzt auf die angegebene Maximall�nge, zur�ck.
	 * Ist der String null oder die L�nge kleiner gleich der vorgegebenen Maximall�nge,
	 * wird der Ursprungsstring zur�ckgegeben. Bei L�nge < 1 wird ein Leerstring
	 * zur�ckgegeben.
	 *
	 * @return	String
	 * 			Der ggf. gek�rzte String
	 * @param	string String
	 * 			Der zu k�rzende String
	 * @param size int
	 * 			Die Maximall�nge, ab der der String abzuschneiden ist.
	 */
	public static final String cut(String string, int size) {
		if (string == null || string.length() <= size) {
			return string;
		}
		return (size < 1) ? "" : string.substring(0, size);
	}

	/**
	 * Gibt den �bergebenen String, gek�rzt auf die angegebene Maximall�nge, zur�ck.
	 * Ist der String null oder die L�nge kleiner gleich der vorgegebenen Maximall�nge,
	 * wird der Ursprungsstring zur�ckgegeben, wobei Zeilenumbr�che durch Leerzeichen
	 * ersetzt werden.
	 *
	 * @return	String
	 * 			Der ggf. gek�rzte String
	 * @param	string String
	 * 			Der zu k�rzende String
	 */
	public static final String cutAndDeleteNewline(String string, int size) {
		if (string == null) {
			return null;
		}
		String result = replace(string, LINE_SEPARATOR, " ");
		return (result.length() < size) ? result : cut(result, size);
	}

	/**
	 * Entferne fuehrende Nullen aus dem uebergebenen String und gebe das Resultat zurueck.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	LVMStringHelper.entferneFuehrendeNullen("000251");
	 * liefert "251" zurueck.
	 * </PRE>
	 * @return String
	 *		der uebergebene String ohne fuehrende Nullen
	 * 		enthaelt der uebergebene String keine fuehrenden Nullen, so wird dieser
	 *		unveraendert zurueckgegeben.
	 * @param input java.lang.String
	 * 		der von fuehrenden Nullen zu befreiende String
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String entferneFuehrendeNullen(String input) {
		if (input == null) {
			return null;
		}
		return entferneFuehrendeNullen(input, input.length());
	}

	/**
	 * Entferne die vorgegebene Anzahl fuehrender Nullen aus dem uebergebenen String
	 * und gebe das Resultat zurueck. Enthaelt der String weniger fuehrende Nullen
	 * als vorgegeben, werden nur diese entfernt.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	LVMStringHelper.entferneFuehrendeNullen("000251", 2);
	 * liefert "0251" zurueck.
	 * </PRE>
	 * @return String
	 *		Der uebergebene String ohne die vorgegebene Anzahl fuehrender Nullen.
	 * 		Enthaelt der uebergebene String weniger fuehrenden Nullen als vorgegeben,
	 * 		werden die vorhandenen entfernt und der verbleibende String zurueckgegeben.
	 * @param input String
	 * 		Der von fuehrenden Nullen zu befreiende String.
	 * @param count int
	 * 		Die maximale Anzahl zu entfernender fuehrender Nullen.
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String entferneFuehrendeNullen(String input, int count) {
		if (input == null) {
			return null;
		}

		int len = input.length();
		int offset = 0;
		for (offset = 0; offset < len && offset < count; offset++) {
			if (input.charAt(offset) != '0') {
				break;
			}
		}

		if (offset == 0) {
			return input;
		}
		return (offset < len) ? input.substring(offset) : "";
	}

	/**
	 * Pr�ft zwei Strings auf Gleichheit.
	 * <p>
	 * Bei der Pr�fung wird das Matchcodezeichen '*' mit ber�cksichtigt.
	 * Das Zeichen ist am Anfang und am Ende des Strings erlaubt.
	 * <p>
	 * Beschreibung: Die Suche mit diesem Matchcodezeichen ist so implementiert, dass
	 * '*' eine beliebige Anzahl von Zeichen ersetzt, also auch ein Endezeichen.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.equalsWithMatchCode("Meier", "*EI*", true);
	 * liefert true, da Gross-/Kleinschreibung ignoriert wird.
	 * </PRE>
	 * @return boolean
	 * 			true, bei Gleichheit; false bei Ungleichheit
	 * @param vergleichString java.lang.String
	 * 			der String, in dem gesucht wird
	 * @param pattern java.lang.String
	 * 			der String, mit dem gesucht wird. Dabei werden fuehrende und
	 *			abschliessende Leerzeichen ignoriert.
	 * @param ignoreCase boolean
	 *			true, falls Gross-/Kleinschreibung egal ist,
	 *			false, wenn Gross-/Kleinschreibung fuer den Vergleich relevant
	 * <p>
	 * @see	java.lang.String#regionMatches(boolean, int, String, int, int)
	 * @see	java.lang.String#trim()
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final boolean equalsWithMatchCode(String vergleichString, String pattern, boolean ignoreCase) {
		// Umschliessende Leerzeichen entfernen
		String suchString = pattern.trim();

		if (suchString.equalsIgnoreCase("*")) {
			// nur Matchcodezeichen als Suchvorgabe; dann immer Uebereinstimmung
			return true;
		}
		
		switch (StringHelper.parseSuchString(suchString)) {
			case 0 :
				// ohne Matchcode
				if (vergleichString.length() == suchString.length()
					&& vergleichString.regionMatches(ignoreCase, 0, suchString, 0, suchString.length())) {
					return true;
				}
				break;
			case 1 :
				// Matchcode am Anfang des Strings
				if (vergleichString.regionMatches(
						ignoreCase, 
						vergleichString.length() - (suchString.length() - 1),
						suchString,
						1,
						suchString.substring(1).length())) {
					return true;
				}
				break;
			case 2 :
				// Matchcode am Ende des Strings
				if (vergleichString.regionMatches(
						ignoreCase, 
						0, 
						suchString.substring(0, suchString.length() - 1), 
						0, 
						suchString.length() - 1)) {
					return true;
				}
				break;
			case 3 :
				// Matchcode am Anfang und Ende des Strings
				for (int j = 0; j <= vergleichString.length() - (suchString.length() - 2); j++) {
					if (vergleichString.regionMatches(ignoreCase, j, suchString, 1, suchString.length() - 2)) {
						return true;
					}
				}
				break;
			default :
				break;
		}
		return false;
	}

	/**
	 * Gebe zur�ck, ob der �bergebene String einem numerischen Nullwert entspricht.
	 * Dies ist dann der Fall, wenn der String einen der Inhalte "0", "0.0", "0.00",
	 * "0,0", "0,00" oder "" hat.
	 *
	 * @return boolean
	 *		True, falls der �bergebene String einem numerischen Nullwert entspricht.
	 * @param value String
	 *		Der zu �berpr�fende String.
	 */
	public static final boolean equalsZero(String value) {
		if (value == null) {
			return false;
		}

		String str = value.trim();
		return str.length() == 0 || "0.00".equals(str) || "0.0".equals(str) || "0,00".equals(str) || "0,0".equals(str) || "0".equals(str);
	}

	/**
	 * Diese Methode liefert einen String zur�ck, der dem uebergebenen String nach
	 * Umsetzen des ersten Zeichens in einen Kleinbuchstaben entspricht.
	 * Ist der Ursprungsstring null, wird null zurueckgegeben.
	 * Ist der Ursprungsstring leer, oder beginnt nicht mit einem Buchstaben, oder
	 * das erste Zeichen ist bereits ein Kleinbuchstabe, so wird der 
	 * Ursprungsstring zurueckgegeben.
	 * <p>
	 * @return String
	 *		der Outputstring
	 * @param source String
	 *		der Inputstring
	 * <p>
	 * @see	java.lang.Character#isLowerCase()
	 * @see	java.lang.Character#toLowerCase()
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String firstCharToLowerCaseAndLeaveTheRestAsIs(String source) {
		return convertFirstChar(source, false);
	}

	/**
	 * Diese Methode liefert einen String zur�ck, der dem uebergebenen String nach
	 * Umsetzen des ersten Zeichens in einen Grossbuchstaben entspricht.
	 * Ist der Ursprungsstring null, wird null zurueckgegeben.
	 * Ist der Ursprungsstring leer, oder beginnt nicht mit einem Buchstaben, oder
	 * das erste Zeichen ist bereits ein Grossbuchstabe, so wird der 
	 * Ursprungsstring zurueckgegeben.
	 * <p>
	 * @return String
	 *		der Outputstring
	 * @param source String
	 *		der Inputstring
	 * <p>
	 * @see	java.lang.Character#isUpperCase()
	 * @see	java.lang.Character#toUpperCase()
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String firstCharToUpperCaseAndLeaveTheRestAsIs(String source) {
		return convertFirstChar(source, true);
	}

	/**
	 * Bricht den �bergebenen String unter Ber�cksichtigung der Default-Maximal-L�nge um.
	 * Der Umbruch erfolgt vorzugsweise bei Leerzeichen. Falls diese innerhalb der gew�nschten
	 * Zeilenl�nge nicht vorhanden sind, wird hart (also mitten im Wort) umbrochen.
	 *
	 * @return	String
	 * 			Der umbrochene String
	 * @param	string String
	 * 			Der umzubrechende String
	 */

	public static final String format(String string) {
		return format(string, DEFAULT_LINE_LENGTH);
	}

	/**
	 * Bricht den �bergebenen String unter Ber�cksichtigung der angebenen Maximal-L�nge um.
	 * Gebe einen String zur�ck, der entsprechend mit Zeilen-Separatoren erg�nzt ist.
	 * Der Umbruch erfolgt vorzugsweise bei Leerzeichen. Falls diese innerhalb der gew�nschten
	 * Zeilenl�nge nicht vorhanden sind, werden auch etwas l�ngere Abschnitte (bis zum 
	 * n�chsten Leerzeichen) akzeptiert.
	 *
	 * @return	String
	 * 			Der umbrochene String
	 * @param	string String
	 * 			Der umzubrechende String
	 * @param linelength int
	 * 			Gew�nschte maximale L�nge der Teilstrings, nach denen jeweils ein Zeilenumbruch
	 * 			eingef�gt werden soll. 
	 */
	public static final String format(String string, int linelength) {
		// Ist �berhaupt was zu tun?
		if (string == null || linelength < 1 || string.length() <= linelength) {
			return string;
		}

		// Zur effizienteren Berechnung baue ich das Ergebnis mit einem StringBuffer zusammen,
		// und greife auf die Stringbestandteile �ber ein char-Array zu
		StringBuffer buf = new StringBuffer(string.length());
		char[] chars = string.toCharArray();
		int start = 0;
		int index = 0;
		// Schleife solange noch ein "zu langes" Restst�ck des Strings bleibt
		while (start < chars.length-linelength) {
			// Schleife �ber den n�chsten Stringabschnitt
			for (index = start+linelength; index > start; index--) {
				if (chars[index] == ' ') {
					break;
				}
			}
			if (index == start) {
				// Im gesamten Bereich war kein Blank. Dann eben auch etwas l�ngeres Teilst�ck zulassen.
				for (index = start+linelength; index < chars.length; index++) {
					if (chars[index] == ' ') {
						break;
					}
				}
				if (index == chars.length) {
					// �berhaupt kein Blank mehr gefunden; dann Ende While-Schleife
					break;
				}
			}

			buf.append(chars, start, index-start);
			start = index + 1;
			if (start < chars.length) {
				// Kommt �berhaupt noch was? Dann Zeilenumbruch
				buf.append(LINE_SEPARATOR);
			}
		}

		// Fertig. Evtl. muss ich noch ein Restst�ck anh�ngen
		if (start < chars.length) {
			buf.append(chars, start, chars.length-start);
		}
		return buf.toString();
	}	

	/**
	 * Diese Methode erwartet als Eingabe einen String, der aus mehreren Teilstrings besteht, die alle
	 * mit der selben Zeichenfolge beginnen. Die Methode erzeugt aus diesem String ein Ergebnis-Array
	 * mit den einzelnen Teilstrings.
	 * <p>
	 * Wenn der Eingabestring nicht direkt mit einem definierten Teilstring beginnt, wird dieser
	 * Teil ignoriert.
	 * <p>
	 * Beispiel:<pre>
	 * Eingabestring: "irgendwas*--StringEins*--StringZwei*--StringDrei trullala"
	 * StartString  : "*--"
	 * Ergebnis     : "*--StringEins" , "*--StringZwei" , "*--StringDrei trullala"
	 * </pre>
	 * Wenn der source-String null ist, wird ein leeres String-Array zur�ckgegeben, wenn der StartString
	 * null ist wird ein String-Array mit einem Element zur�ckgegeben, das den kompletten Sourcestring enth�lt.
	 * 
	 * @return java.lang.String[]
	 * @param source java.lang.String
	 * @param startString java.lang.String
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static String[] getTeilstrings(String source, String startString) {
		// Diese Liste nimmt erstmal die Teilstrings auf. Wird erstmal mit 2 Items initialisiert, um nicht
		// sofort zuviel Speicher zu 'verbraten'.
		if (source == null) {
			return EMPTY_STRING_ARRAY;
		}
		if (startString == null) {
			return new String[] { source };
		}

		List<String> liste = new ArrayList<String>(2);

		int aktstartpos = 0;
		int delimLength = startString.length();

		aktstartpos = source.indexOf(startString, 0);

		while (aktstartpos != -1) {
			int nextStartPos = source.indexOf(startString, aktstartpos + delimLength); // n�chste Position eines Delimiters suchen.
			if (nextStartPos == -1) {
				liste.add(source.substring(aktstartpos)); // bis zum Ende
			} else {
				liste.add(source.substring(aktstartpos, nextStartPos));
			}
			aktstartpos = nextStartPos;
		}

		return CollectionHelper.asStringArray(liste);
	}

	/**
	 * Gebe den Index des ersten numerischen Zeichens im uebergebenen String
	 * zurueck bzw. -1, falls kein numerisches Zeichen enthalten ist.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	LVMStringHelper.indexOfFirstDigit("Hammerstr. 99");
	 * liefert als Ergebnis 11.
	 * </PRE>
	 * @return int
	 *			index der ersten Ziffer im uebergebenen String
	 * 			-1, falls Inputstring keine Ziffern enthaelt
	 * @param input java.lang.String
	 * 			der zu untersuchende String
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final int indexOfFirstDigit(String input) {
		for (int i = 0; i < input.length(); i++) {
			if (Character.isDigit(input.charAt(i))) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Gebe zurueck, ob die beiden uebergebenen Strings den gleichen
	 * Soundex-Code haben; @see computeSoundex
	 * <p>
	 * Aufrufbeispiele:<br>
	 *   isSimilar("Meier", "mayeR") liefert true
	 *
	 * @return boolean
	 *	true, falls die beiden uebergebenen Strings den gleichen Soundex-Code haben.
	 * @param str1 String
	 *	mit str2 zu vergleichender String
	 * @param str2 String
	 *	mit str1 zu vergleichender String
	 *
	 * @author Guyet, Sven
	 * @version
	 **/
	public static final boolean isSimilar(String str1, String str2) {
		return computeSoundex(str1).equals(computeSoundex(str2));
	}

	/**
	 * Gebe zurueck, ob der uebergebene String ungleich null ist und
	 * nicht nur aus Leerzeichen besteht.
	 * 
	 * @return boolean
	 * 		true, falls str ungleich null ist und nicht nur aus Leerzeichen
	 *		besteht, sonst false
	 * @param	str String
	 * 		der zu ueberpruefende String
	 * <p>
	 * @author Tyburski, Stefan
	 */
	public static final boolean isValid(String str) {
		return str != null && str.trim().length() != 0;
	}

	/**
	 * Baue aus einem Array von Strings einen einzigen String zusammen und gebe
	 * diesen zurueck. Die einzelnen Strings des Eingabe-Arrays werden dabei
	 * mit dem uebergebenen Delimiter abgetrennt.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 *	String[] arr = {"a", "dsfdsf", "asas"};
	 * 	StringHelper.join(arr, '&');
	 * liefert "a&dsfdsf&asas" zurueck.
	 * </PRE>
	 * @return String
	 *			Ergebnis, bestehend aus den Strings aus list, separiert durch delim
	 * @param list java.lang.String[]
	 *			Array mit den zu konkatenierenden Strings
	 * @param delim char
	 *			Trennzeichen fuer die konkatenierten Strings
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String join(String[] list, char delim) {
		// Leeres Array? Leerstring zurueckgeben.
		if (list.length == 0) {
			return "";
		}
		// Array mit nur einem String? Diesen zurueckgeben.
		if (list.length == 1) {
			return list[0];
		}

		// Bestimme die Groesse des zurueckzugebenden Strings.
		int size = 0;
		// Zunaechst die einzelnen Stringlaengen addieren.
		for (int i = 0; i < list.length; i++) {
			size += list[i].length();
		}
		// Jetzt noch die Anzahl der erforderlichen Delimiter hinzuzaehlen.
		size += list.length - 1;

		StringBuffer sb = new StringBuffer(size);
		sb.append(list[0]);
		for (int i = 1; i < list.length; i++) {
			sb.append(delim);
			sb.append(list[i]);
		}
		return sb.toString();
	}

	/**
	 * Vergroessere den uebergebenen String auf die vorgegebene Laenge
	 * durch Auffuellen mit Leerzeichen und gebe den resultierenden String
	 * zurueck. Ist der Ursprungsstring bereits laenger, wird dieser
	 * unveraendert zurueckgegeben.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.padLength("Sven", 10);
	 * liefert den String "Sven      " zurueck.
	 * </PRE>
	 * @return String
	 *		der mit Leerzeichen aufgefuellte String
	 * @param input java.lang.String
	 *		der aufzufuellende String
	 * @param gesamtlaenge int
	 *		die Gesamtlaenge des zurueckzugebenden Strings
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String padLength(String input, int gesamtlaenge) {
		return padLength(input, gesamtlaenge, ' ', false);
	}

	/**
	 * Fuelle den uebergebenen String mit dem vorgegebenen Zeichen auf die
	 * vorgegebene Laenge auf, wobei der Ursprungsstring am Anfang und die Fuellzeichen
	 * am Ende stehen. Gebe das Resultat zurueck.
	 * Ist der Ursprungsstring bereits laenger, wird dieser unveraendert zurueckgegeben.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.padLength("Sven", 10, 'X');
	 * liefert den String "SvenXXXXXX" zurueck.
	 * </PRE>
	 * @return String
	 *		der mit dem uebergebenen Fuellzeichen hinten aufgefuellte String
	 * @param input java.lang.String
	 *		der aufzufuellende String
	 * @param gesamtlaenge int
	 *		die Gesamtlaenge des zurueckzugebenden Strings
	 * @param filler char
	 *		das Fuellzeichen
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String padLength(String input, int gesamtlaenge, char filler) {
		return padLength(input, gesamtlaenge, filler, false);
	}

	/**
	 * Fuelle den uebergebenen String mit dem vorgegebenen Zeichen auf die
	 * vorgegebene Laenge auf. Das uebergebene Flag steuert, ob die
	 * Fuellzeichen am Anfang (true) oder am Ende (false) stehen.
	 * Gebe das Resultat zurueck.
	 * Ist der Ursprungsstring bereits laenger, wird dieser unveraendert zurueckgegeben.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.padLength("Sven", 10, 'X', true);
	 * liefert den String "XXXXXXSven" zurueck.
	 * </PRE>
	 * @return String
	 *		der mit dem uebergebenen Fuellzeichen vorne aufgefuellte String
	 * @param input java.lang.String
	 *		der aufzufuellende String
	 * @param gesamtlaenge int
	 *		die Gesamtlaenge des zurueckzugebenden Strings
	 * @param filler char
	 *		das Fuellzeichen
	 * @param starting boolean
	 *		Flag, ob die Fuellzeichen am Anfang (true) oder Ende (false) stehen sollen
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	private static final String padLength(String source, int gesamtlaenge, char filler, boolean starting) {
		String input = (source == null) ? "" : source;
		// Ist der Inputstring bereits lang genug, gebe diesen unveraendert zurueck.
		int inputLength = input.length();
		if (inputLength >= gesamtlaenge) {
			return input;
		}

		// Na gut, dann brauche ich jetzt einen Puffer fuer gesamtlaenge Zeichen.
		char[] buf = new char[gesamtlaenge];

		// Je nach Flag entweder zuerst die Fuellzeichen oder zuerst der Inputstring.
		// In offset merke ich mir, ab welcher Stelle im char-Array nachher noch der
		// Inputstring hinkopiert werden muss.
		int offset = 0;

		if (starting) {
			// Ok, Offset gleich Gesamtlaenge minus Laenge des Inputstrings.
			// Von 0 bis Offset brauche ich Filler, ab Offset den Inputstring.
			offset = gesamtlaenge - inputLength;
			Arrays.fill(buf, 0, offset, filler);
		} else {
			// Ok, Offset kann 0 bleiben, da der Inputstring nach vorne kommt.
			// Hinter den Inputstring bis zum Ende brauche ich Filler.
			Arrays.fill(buf, inputLength, gesamtlaenge, filler);
		}

		// Jetzt also noch den Inputstring an die richtige Stelle kopieren,
		// und dann das Gesamtergebnis als String zurueckgeben.
		input.getChars(0, inputLength, buf, offset);
		return new String(buf);
	}

	/**
	 * Vergroessere den uebergebenen String auf die vorgegebene Laenge
	 * durch Einfuegen von Leerzeichen am Anfang und gebe den resultierenden String
	 * zurueck. Ist der Ursprungsstring bereits laenger, wird dieser
	 * unveraendert zurueckgegeben.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.padLengthLeft("Sven", 10);
	 * liefert den String "      Sven" zurueck.
	 * </PRE>
	 * @return String
	 *		der mit Leerzeichen aufgefuellte String
	 * @param input java.lang.String
	 *		der aufzufuellende String
	 * @param gesamtlaenge int
	 *		die Gesamtlaenge des zurueckzugebenden Strings
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String padLengthLeft(String input, int gesamtlaenge) {
		return padLength(input, gesamtlaenge, ' ', true);
	}

	/**
	 * Fuelle den uebergebenen String mit dem vorgegebenen Zeichen auf die
	 * vorgegebene Laenge auf, wobei der Ursprungsstring am Ende und die Fuellzeichen
	 * am Anfang stehen. Gebe das Resultat zurueck.
	 * Ist der Ursprungsstring bereits laenger, wird dieser unveraendert zurueckgegeben.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.padLength("Sven", 10, 'X');
	 * liefert den String "XXXXXXSven" zurueck.
	 * </PRE>
	 * @return String
	 *		der mit dem uebergebenen Fuellzeichen vorne aufgefuellte String
	 * @param input java.lang.String
	 *		der aufzufuellende String
	 * @param gesamtlaenge int
	 *		die Gesamtlaenge des zurueckzugebenden Strings
	 * @param filler char
	 *		das Fuellzeichen
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String padLengthLeft(String input, int gesamtlaenge, char filler) {
		return padLength(input, gesamtlaenge, filler, true);
	}

	/**
	 * Suche in einem String das Matchcodezeichen '*' und dessen Position.
	 * Gebe zurueck, wo das Matchcodezeichen im Suchstring vorkommt.
	 * <p>
	 * Es werden vier Faelle unterschieden:
	 * <ol>
	 * <li>kein '*' gefunden; keine Matchcodesuche</li>
	 * <li>'*' am Anfang des String</li>
	 * <li>'*' am Ende des Strings</li>
	 * <li>'*' am Anfang und am Ende des Strings</li>
	 * </eol><p>
	 * Aufrufbeispiel:<PRE>
	 *			StringHelper.parseSuchString("*ei*");
	 * liefert das Ergebnis 3.
	 * </PRE>
	 * @return int
	 * 		die Art der Matchcodesuche:<PRE>
	 *		0 => kein Matchcode gefunden
	 *		1 => Matchcode am Anfange des Strings
	 *		2 => Matchcode am Ende des Strings
	 *		3 => Matchcode am Anfang und Ende des Strings</PRE>
	 * @param s java.lang.String
	 *		der zu pruefende String
	 * <p>
	 * @see	java.lang.String#startsWith(String)
	 * @see	java.lang.String#endsWith(String)
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	private static final int parseSuchString(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}
		if (s.charAt(0) == '*') {
			if (s.charAt(s.length()-1) == '*') {
				// Matchcode am Anfang und Ende des Strings
				return 3;
			}
			// Matchcode am Anfang des Strings
			return 1;
		}

		if (s.charAt(s.length()-1) == '*') {
			// Matchcode am Ende des Strings
			return 2;
		}

		return 0; // kein Matchcode
	}

	/**
	 * Diese Methode liefert einen String zur�ck, in dem alle Vorkommen des
	 * vorgebenen Zeichens (toDelete) im Source-String entfernt wurden.
	 * <p>
	 * @return String
	 *		der Inputstring nach Entfernen von toDelete
	 * @param source String
	 *		Inputstring, aus dem alle Vorkommen von toDelete entfernt werden sollen
	 * @param toDelete char
	 *		das aus dem Inputstring zu entfernende Zeichen
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String removeAllOccurences(String source, char toDelete) {
		if (source == null) {
			return null;
		}

		int sourceLen = source.length();
		char[] newArray = new char[sourceLen];
		// char-Array f�r den neuen String. Laenger als der Input kann's nicht werden.

		int pos = 0;
		for (int i = 0; i < sourceLen; i++) {
			if (source.charAt(i) != toDelete) {
				newArray[pos++] = source.charAt(i);
			}
		}
		return new String(newArray, 0, pos);
	}

	/**
	 * Diese Methode liefert einen String zur�ck, in dem alle Vorkommen des
	 * vorgebenen Strings (toDelete) im Source-String entfernt wurden.
	 * <p>
	 * @return String
	 *		der Inputstring nach Entfernen von toDelete
	 * @param source String
	 *		Inputstring, aus dem alle Vorkommen von toDelete entfernt werden sollen
	 * @param deleteString String
	 *		das aus dem Inputstring zu entfernende Zeichen
	 * <p>
	 * @author Schulte, Klaus
	 */
	public static final String removeAllOccurences(String source, String deleteString) {
		if (source == null) {
			return null;
		}

		if (source.length() == 0 || deleteString == null || deleteString.length() == 0) {
			return source;
		}

		StringBuffer result = new StringBuffer(source.length()); // L�nger als der Inputstring kanns nicht werden
		int deleteStringLength = deleteString.length();
		int startPos = 0;
		int offset = source.indexOf(deleteString, startPos);
		while (offset != -1) {
			if (startPos < offset) {
				// Es ist ein St�ckchen festzuhalten
				result.append(source.substring(startPos, offset));
			}
			// Neue Startposition beginnt hinter dem Delete-String
			startPos = offset + deleteStringLength;
			offset = source.indexOf(deleteString, startPos);
		}
		// Evtl. ist noch ein Restst�ck �brig
		if (startPos < source.length()) {
			result.append(source.substring(startPos));
		}
		return result.toString();
	}

	/**
	 * Entfernt aus dem uebergebenen String die letzen n Zeichen und
	 * gebe den resultierenden String zurueck. Ist der urspruengliche String
	 * null, oder ist n kleiner 1, wird der Originalstring zurueckgegeben.
	 * Ist n groesser gleich der Stringlaenge, wird ein Leerstring zurueckgegeben.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.removeTrailingChars("LVM123", 3)
	 * liefert den String "LVM" zurueck.
	 * </PRE>
	 * @return java.lang.String
	 * 			String, der gleich dem uebergebenen String ohne die letzten
	 *			number Zeichen ist. *
	 * @param str java.lang.String
	 * 			der String, von dem die letzen number Zeichen entfernt werden sollen
	 * @param number int
	 * 			die Anzahl der zu entfernenden Zeichen.
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String removeTrailingChars(String str, int number) {
		if ((str == null) || (number < 1)) {
			return str;
		}
		int newLength = str.length() - number;
		return (newLength < 1) ? "" : str.substring(0, newLength);
	}

	/**
	 * Diese Methode liefert einen String zur�ck, in dem alle Zeichenketten 'oldString' durch
	 * 'newString' ersetzt wurden. Ist der Ursprungsstring null, wird null zurueckgegeben.
	 * Ist eine der Zeichenketten null, oder ist die zu ersetzende Zeichenkette leer,
	 * wird eine Kopie des Ursprungsstrings zurueckgegeben.
	 * <p>
	 * @return String
	 *		der Outputstring
	 * @param source String
	 *		der Inputstring
	 * @param oldString String
	 *		die zu ersetzende Zeichenkette
	 * @param newString String
	 *		die Ersetzungszeichenkette
	 * @param ignoreCase boolean
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String replace(String source, String oldString, String newString, boolean ignoreCase) {
		if (source == null) {
			return null;
		}

		if (oldString == null || newString == null || oldString.length() == 0) {
			return source;
		}

		int sourceLength = source.length();
		// wir machen uns noch eine Kopie von source, die wahlweise  
		// in lowercase konvertiert wird:
		String sourceCopy = ignoreCase ? source.toLowerCase() : source;
		String oldStringCopy = ignoreCase ? oldString.toLowerCase() : oldString;
		
		StringBuffer sb = new StringBuffer(sourceLength);

		boolean endOfString = false;
		int oldLength = oldString.length();
		int currentpos = 0;
		while (!endOfString) {
			int pos = sourceCopy.indexOf(oldStringCopy, currentpos);
			if (pos != -1) {
				if (pos != currentpos) {
					sb.append(source.substring(currentpos, pos));
				}
				sb.append(newString);
				currentpos = pos + oldLength;
				if (currentpos >= sourceLength) {
					endOfString = true; // habe fertig
				}
			} else {
				sb.append(source.substring(currentpos));
				endOfString = true; // habe fertig
			}
		}

		return sb.toString();
	}

	/**
	 * Diese Methode liefert einen String zur�ck, in dem alle Zeichenketten 'oldString' durch
	 * 'newString' ersetzt wurden. Ist der Ursprungsstring null, wird null zurueckgegeben.
	 * Ist eine der Zeichenketten null, oder ist die zu ersetzende Zeichenkette leer,
	 * wird eine Kopie des Ursprungsstrings zurueckgegeben.
	 * <p>
	 * @return String
	 *		der Outputstring
	 * @param source String
	 *		der Inputstring
	 * @param oldString String
	 *		die zu ersetzende Zeichenkette
	 * @param newString String
	 *		die Ersetzungszeichenkette
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String replace(String source, String oldString, String newString) {
		return StringHelper.replace(source, oldString, newString, false);
	}
	
	
	/**
	 * Gebe ein Array von Strings zurueck, indem ich den Uebergabestring in Teilstrings
	 * trenne, deren Grenzen durch das uebergebene Trennzeichen bestimmt werden.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.split("a&dsfdsf&asas", '&');
	 * liefert ein Stringarray, bestehend aus den folgenden 3 Strings:
	 * 	"a", "dsfdsf", "asas"
	 * </PRE>
	 * @return String[]
	 *			das Array mit den durch delim bestimmten Teilstrings von input
	 * @param input java.lang.String
	 * 			der aufzuteilende Inputstring
	 * @param delim char
	 *			das Trennzeichen
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String[] split(String input, char delim) {
		return split(input, String.valueOf(delim));
	}

	/**
	 * Gebe ein Array von Strings zurueck, indem ich den Uebergabestring in Teilstrings
	 * trenne, deren Grenzen durch den uebergebenen String bestimmt werden.
	 * <p>
	 * Aufrufbeispiel:<PRE>
	 * 	StringHelper.split("aTRENNERdsfdsfTRENNERasas", "TRENNER");
	 * liefert ein Stringarray, bestehend aus den folgenden 3 Strings:
	 * 	"a", "dsfdsf", "asas"
	 * </PRE>
	 * @return String[]
	 *			das Array mit den durch delim bestimmten Teilstrings von input
	 * @param input String
	 * 			der aufzuteilende Inputstring
	 * @param delim String
	 *			der Trennstring
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String[] split(String input, String delim) {
		if (input == null) {
			return EMPTY_STRING_ARRAY;
		}
		if (delim == null) {
			return new String[]{input};
		}
		
		StringTokenizer tok = new StringTokenizer(input, delim);
		int noOfTokens = tok.countTokens();

		// System.out.println(i + " Delimiters found in " + input + ".");
		String[] retArray = new String[noOfTokens];
		for (int i = 0; i < noOfTokens; i++) {
			retArray[i] = tok.nextToken();
		}

		// for (int i = 0; i < noOfTokens; i++)
		//  System.out.println("split[" + i + "] = <" + retArray[i] + ">");
		return retArray;
	}

	/**
	 * Returns a string representation of the given collection.  The string
	 * representation consists of a list of the collection's elements in the
	 * order they are returned by its iterator.  Adjacent elements are separated 
	 * by the characters <tt>", "</tt> (comma and space).  Elements are converted
	 * to strings as by <tt>String.valueOf(Object)</tt>.
	 * <p>
	 * This implementation creates an empty string buffer and iterates over the 
	 * collection appending the string representation of each element in turn.
	 * After appending each element except the last, the string <tt>", "</tt> is
	 * appended.  Finally a right bracket is appended.  A string is obtained from 
	 * the string buffer, and returned.
	 * 
	 * @return String
	 *		A string representation of the given collection.
	 * @param coll Collection
	 * 		The collection whose string representation shall be returned.
	 * <p>
	 * @author Guyet, Sven (IBM)
	 */
	public static final String toString(Collection<?> coll) {
		StringBuffer buf = new StringBuffer();
		Iterator<?> it = coll.iterator();
		int maxIndex = coll.size() - 1;
		for (int i = 0; i <= maxIndex; i++) {
			buf.append(String.valueOf(it.next()));
			if (i < maxIndex) {
				buf.append(", ");
			}
		}
		return buf.toString();
	}
}