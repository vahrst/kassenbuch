package de.vahrst.application.util;
/**
 * Diese Helperklasse stellt Service-Methoden rund um die Laufzeit
 * Umgebung zur Verf�gung. U.a. gibt's einen Service, der angibt, ob
 * der aktuelle Prozess innerhalb der Entwicklungsumgebung l�uft.
 *
 * @author Vahrst, Thomas
 */
public class RuntimeHelper {
	public static final String LINE_SEPARATOR =
		(System.getProperty("line.separator")==null) ? "\n" : System.getProperty("line.separator");
	// Symbolische Konstanten f�r die Methode 'inIDE'.
	private final static String IN_IDE_ENVIRONMENT_VAR_NAME = "in.ide";
	private final static int NOT_INITIALIZED = 0;
	private final static int IN_IDE = 1;
	private final static int NOT_IN_IDE = 2;
	private static int in_IDE_Status = NOT_INITIALIZED;

/**
 * RuntimeHelper constructor comment.
 */
private RuntimeHelper() {
	super();
}


/**
 * Gebe zur�ck, ob die Programmausf�hrung innerhalb der Entwicklungsumgebung (z.B.
 * VisualAge for Java oder WSAD) erfolgt.
 * <p>
 * [TECHNISCH]<br>
 * Die Pruefung erfolgt anhand einer beim Start des entsprechenden Javaprozesses
 * anzugebenden Environment-Variable ("in.ide"). Ist diese (mit beliebigem Wert) definiert,
 * wird true zur�ckgegeben, anderenfalls false. Da in VisualAge keine Environment-Variablen
 * an z.B. den EJB-Serverprozess weitergegeben werden k�nnen, wird bei Nichtvorhandensein
 * der Variablen vorerst noch �ber 'Class.forName()' zus�tzlich die Existenz der nur innerhalb
 * der VA Entwicklungsumgebung vorhandenen Klasse com.ibm.uvm.tools.DebugSupport gepr�ft.
 * 
 * @return boolean
 *		true, falls die Ausf�hrung innerhalb der Entwicklungsumgebung erfolgt.
 */
public static final boolean inIDE() {
	if (in_IDE_Status == NOT_INITIALIZED) {
		try {
			in_IDE_Status = NOT_IN_IDE;
			if (System.getProperty(IN_IDE_ENVIRONMENT_VAR_NAME) != null) {
				in_IDE_Status = IN_IDE;
			}
		} catch (SecurityException e) {
			// Kein Zugriff auf System.getProperty erlaubt? Dann NOT_IN_IDE unterstellen.
		}
	}
	return in_IDE_Status == IN_IDE;
}

public static final void reset(){
	in_IDE_Status = NOT_INITIALIZED;
}
}