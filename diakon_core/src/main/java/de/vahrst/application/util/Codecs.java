package de.vahrst.application.util;
import java.io.UnsupportedEncodingException;
import java.util.BitSet;
/**
 * Service-Klasse f�r unterschiedliche Codierungen. U.a. kann man �ber
 * diese Klasse base64 de/encodieren.
 * <p>
 * @author Vahrst, Thomas
 * @version
 */
public class Codecs {
	private static byte[]  Base64EncMap;
	private static byte[]  Base64DecMap;
  private static BitSet  EBCDICUnsafeChar;
	
	// Class Initializer
	static {

	// rfc-2045: Base64 Alphabet
	byte[] map = {
	    (byte)'A', (byte)'B', (byte)'C', (byte)'D', (byte)'E', (byte)'F',
	    (byte)'G', (byte)'H', (byte)'I', (byte)'J', (byte)'K', (byte)'L',
	    (byte)'M', (byte)'N', (byte)'O', (byte)'P', (byte)'Q', (byte)'R',
	    (byte)'S', (byte)'T', (byte)'U', (byte)'V', (byte)'W', (byte)'X',
	    (byte)'Y', (byte)'Z',
	    (byte)'a', (byte)'b', (byte)'c', (byte)'d', (byte)'e', (byte)'f',
	    (byte)'g', (byte)'h', (byte)'i', (byte)'j', (byte)'k', (byte)'l',
	    (byte)'m', (byte)'n', (byte)'o', (byte)'p', (byte)'q', (byte)'r',
	    (byte)'s', (byte)'t', (byte)'u', (byte)'v', (byte)'w', (byte)'x',
	    (byte)'y', (byte)'z',
	    (byte)'0', (byte)'1', (byte)'2', (byte)'3', (byte)'4', (byte)'5',
	    (byte)'6', (byte)'7', (byte)'8', (byte)'9', (byte)'+', (byte)'/' };
	Base64EncMap = map;
	Base64DecMap = new byte[128];
	for (int idx=0; idx<Base64EncMap.length; idx++)
	    Base64DecMap[Base64EncMap[idx]] = (byte) idx;

	// EBCDIC unsafe characters to be quoted in quoted-printable
	// See first NOTE in section 6.7 of rfc-2045
	EBCDICUnsafeChar = new BitSet(256);
	EBCDICUnsafeChar.set('!');
	EBCDICUnsafeChar.set('"');
	EBCDICUnsafeChar.set('#');
	EBCDICUnsafeChar.set('$');
	EBCDICUnsafeChar.set('@');
	EBCDICUnsafeChar.set('[');
	EBCDICUnsafeChar.set('\\');
	EBCDICUnsafeChar.set(']');
	EBCDICUnsafeChar.set('^');
	EBCDICUnsafeChar.set('`');
	EBCDICUnsafeChar.set('{');
	EBCDICUnsafeChar.set('|');
	EBCDICUnsafeChar.set('}');
	EBCDICUnsafeChar.set('~');	}

/**
 * Codecs constructor comment.
 */
public Codecs() {
	super();
}


/**
 * This method decodes the given byte[] using the base64-encoding
 * specified in RFC-2045 (Section 6.8).
 *
 * @param  data the base64-encoded data.
 * @return the decoded <var>data</var>.
 */
public final static byte[] base64Decode(byte[] data) {
	if (data == null)
		return null;
	if (data.length == 0)
		return new byte[0];

	int tail = data.length;
	while (data[tail - 1] == '=')
		tail--;

	byte dest[] = new byte[tail - data.length / 4];

	// ascii printable to 0-63 conversion
	for (int idx = 0; idx < data.length; idx++)
		data[idx] = Base64DecMap[data[idx]];

	// 4-byte to 3-byte conversion
	int sidx, didx;
	for (sidx = 0, didx = 0; didx < dest.length - 2; sidx += 4, didx += 3) {
		dest[didx] = (byte) (((data[sidx] << 2) & 255) | ((data[sidx + 1] >>> 4) & 003));
		dest[didx + 1] = (byte) (((data[sidx + 1] << 4) & 255) | ((data[sidx + 2] >>> 2) & 017));
		dest[didx + 2] = (byte) (((data[sidx + 2] << 6) & 255) | (data[sidx + 3] & 077));
	}
	if (didx < dest.length)
		dest[didx] = (byte) (((data[sidx] << 2) & 255) | ((data[sidx + 1] >>> 4) & 003));
	if (++didx < dest.length)
		dest[didx] = (byte) (((data[sidx + 1] << 4) & 255) | ((data[sidx + 2] >>> 2) & 017));

	return dest;
}


/**
 * This method decodes the given string using the base64-encoding
 * specified in RFC-2045 (Section 6.8).
 *
 * @param  str the base64-encoded string.
 * @return the decoded <var>str</var>.
 */
public final static String base64Decode(String str) throws UnsupportedEncodingException{
	if (str == null)
		return null;

	return new String(base64Decode(str.getBytes("8859_1")), "8859_1");
}


    /**
     * This method encodes the given byte[] using the base64-encoding
     * specified in RFC-2045 (Section 6.8).
     *
     * @param  data the data
     * @return the base64-encoded <var>data</var>
     */
    public final static byte[] base64Encode(byte[] data)
    {
	if (data == null)  return  null;

	int sidx, didx;
	byte dest[] = new byte[((data.length+2)/3)*4];


	// 3-byte to 4-byte conversion + 0-63 to ascii printable conversion
	for (sidx=0, didx=0; sidx < data.length-2; sidx += 3)
	{
	    dest[didx++] = Base64EncMap[(data[sidx] >>> 2) & 077];
	    dest[didx++] = Base64EncMap[(data[sidx+1] >>> 4) & 017 |
					(data[sidx] << 4) & 077];
	    dest[didx++] = Base64EncMap[(data[sidx+2] >>> 6) & 003 |
					(data[sidx+1] << 2) & 077];
	    dest[didx++] = Base64EncMap[data[sidx+2] & 077];
	}
	if (sidx < data.length)
	{
	    dest[didx++] = Base64EncMap[(data[sidx] >>> 2) & 077];
	    if (sidx < data.length-1)
	    {
		dest[didx++] = Base64EncMap[(data[sidx+1] >>> 4) & 017 |
					    (data[sidx] << 4) & 077];
		dest[didx++] = Base64EncMap[(data[sidx+1] << 2) & 077];
	    }
	    else
		dest[didx++] = Base64EncMap[(data[sidx] << 4) & 077];
	}

	// add padding
	for ( ; didx < dest.length; didx++)
	    dest[didx] = (byte) '=';

	return dest;
    }


/**
 * This method encodes the given string using the base64-encoding
 * specified in RFC-2045 (Section 6.8). It's used for example in the
 * "Basic" authorization scheme.
 *
 * @param  str the string
 * @return the base64-encoded <var>str</var>
 */
public final static String base64Encode(String str) throws UnsupportedEncodingException{
	if (str == null)
		return null;

		return new String(base64Encode(str.getBytes("8859_1")), "8859_1");
}


	    private final static boolean match(char[] str, int start, char[] arr)
    {
	if (str.length < start + arr.length)  return false;

	for (int idx=1; idx < arr.length; idx++)
	    if (str[start+idx] != arr[idx])  return false;
	return true;
    }


/**
 * decodiert einen mit quotedPrintableUTF8Encode erzeugten String wieder in
 * die Ursprungsform.
 * Creation date: (25.07.03 08:16:31)
 * @return java.lang.String
 * @param str java.lang.String
 */
public static String quotedPrintableUTF8Decode(String str)
	throws UnsupportedEncodingException {
	if (str == null)
		return null;

	char res[] = new char[(int) (str.length() * 1.1)],
		src[] = str.toCharArray(),
		nl[] = System.getProperty("line.separator", "\n").toCharArray();
	int last = 0, didx = 0, slen = str.length();

	for (int sidx = 0; sidx < slen;) {
		char ch = src[sidx++];

		if (ch == '=') {
			if (sidx >= slen - 1)
				throw new UnsupportedEncodingException("Premature end of input detected");

			if (src[sidx] == '\n' || src[sidx] == '\r') { // Rule #5
				sidx++;

				if (src[sidx - 1] == '\r' && src[sidx] == '\n')
					sidx++;
			} else // Rule #1
				{
				char repl;
				int hi = Character.digit(src[sidx], 16),
					lo = Character.digit(src[sidx + 1], 16);

				if ((hi | lo) < 0) {
					throw new UnsupportedEncodingException(new String(src, sidx - 1, 3) + " is an invalid code");
				}
				repl = (char) (hi << 4 | lo);
				sidx += 2;

				res[didx++] = repl;
			}
			last = didx;
		} else
			if (ch == '\n' || ch == '\r') // Rule #4
				{
				if (ch == '\r' && sidx < slen && src[sidx] == '\n')
					sidx++;
				for (int idx = 0; idx < nl.length; idx++)
					res[last++] = nl[idx];
				didx = last;
			} else // Rule #1, #2
				{
				res[didx++] = ch;
				if (ch != ' ' && ch != '\t') // Rule #3
					last = didx;
			}

		if (didx > res.length - nl.length - 2) {
			char[] temp = new char[res.length + 500];
			System.arraycopy(res, 0, temp, 0, res.length);
			res = temp;
		}

	}

	byte[] utf8bytes = new String(res, 0, didx).getBytes("ISO8859_1");
	return new String(utf8bytes, "UTF8");
}


/**
 * erzeugt eine UTF8 Byte-Sequenz aus dem vorgegebenen String, um daraus
 * einen neuen String im quotedPrintable Format zu erzeugen.
 * Creation date: (25.07.03 08:16:31)
 * @return java.lang.String
 * @param str java.lang.String
 */
public static String quotedPrintableUTF8Encode(String str) throws UnsupportedEncodingException {
	if (str == null)  return  null;

	// jetzt erstmal den String in einen normierten String mit 8Bit Char umwandeln:
	String nstr = new String(str.getBytes("UTF8"),"ISO8859_1");
	
	int minsize = (int) (nstr.length() * 1.5);
	if(minsize < 10) minsize = 10;
	
	char map[] =
	    {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'},
	     nl[]  = System.getProperty("line.separator", "\n").toCharArray(),
	     res[] = new char[minsize],
	     src[] = nstr.toCharArray();
	char ch;
	int  cnt  = 0,
	     didx = 1,
	     slen = nstr.length();


	for (int sidx=0; sidx < slen; sidx++)
	{
	    ch = src[sidx];

	    if (ch == nl[0]   &&  match(src, sidx, nl))		// Rule #4
	    {
		if (res[didx-1] == ' ')			// Rule #3
		{
		    res[didx-1] = '=';
		    res[didx++] = '2';
		    res[didx++] = '0';
		}
		else if (res[didx-1] == '\t')		// Rule #3
		{
		    res[didx-1] = '=';
		    res[didx++] = '0';
		    res[didx++] = '9';
		}

		res[didx++] = '\r';
		res[didx++] = '\n';
		sidx += nl.length - 1;
		cnt = didx;
	    }
	    else if (ch > 126  ||  (ch < 32  &&  ch != '\t')  ||  ch == '='  ||
		     EBCDICUnsafeChar.get( ch))
	    {							// Rule #1, #2
		res[didx++] = '=';
		res[didx++] = map[(ch & 0xf0) >>> 4];
		res[didx++] = map[ch & 0x0f];
	    }
	    else						// Rule #1
	    {
		res[didx++] = ch;
	    }

	    if (didx > cnt+70)					// Rule #5
	    {
		res[didx++] = '=';
		res[didx++] = '\r';
		res[didx++] = '\n';
		cnt = didx;
	    }

    if (didx > res.length-5){
			char[] temp = new char[res.length +500];
	    System.arraycopy(res,0,temp,0,res.length);
	    res = temp;
    }
	}

	return String.valueOf(res, 1, didx-1);
}
}