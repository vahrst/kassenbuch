package de.vahrst.application.util;
import java.io.Serializable;

/**
 * repr�sentiert ein Element im Cache. Diese Klasse mu� public sein, weil die
 * Junit-Testklasse diese Klasse auch instanziiert.
 * Creation date: (04.02.02 10:49:03)
 * @author: Vahrst, Thomas
 */
public class DynamicCacheEntry implements Serializable {
	private Object key;
	private Object value;
	
	/** 
	 * Anzahl der Zugriffe auf dieses Element. Wird vom
	 * Cache bei jedem Zugriff (auch setter) erh�ht
	 */
	private int accessCount = 0;

/**
 * CacheElement constructor comment.
 */
public DynamicCacheEntry(Object key, Object value) {
	super();
	this.key = key;
	this.value = value;
}


/**
 * liefert den Wert des Zugriffsz�hlers.
 * Creation date: (04.02.02 11:03:40)
 * @return int
 */
public int getAccessCount() {
	return accessCount;
}


/**
 * liefert den Key
 * Creation date: (04.02.02 11:03:40)
 * @return java.lang.Object
 */
public java.lang.Object getKey() {
	return key;
}


/**
 * liefert den Value
 * Creation date: (04.02.02 11:03:40)
 * @return java.lang.Object
 */
public java.lang.Object getValue() {
	return value;
}


/**
 * erh�ht den Zugriffz�hler um 1
 * Creation date: (04.02.02 11:03:40)
 */
public void incrementAccessCount() {
	accessCount ++;
}


/**
 * setzt den Key
 * Creation date: (04.02.02 11:03:40)
 * @param newKey java.lang.Object
 */
public void setKey(java.lang.Object newKey) {
	key = newKey;
}


/**
 * setzt den Value
 * Creation date: (04.02.02 11:03:40)
 * @param newValue java.lang.Object
 */
public void setValue(java.lang.Object newValue) {
	value = newValue;
}
}