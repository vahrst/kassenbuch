package de.vahrst.application.util;
/**
 * Kapselt einen Eintrag, der von DiffHelper als ge�ndert, gel�scht oder neu
 * identifiziert wurde. 
 * Creation date: (06.12.03 11:59:18)
 * @author: Vahrst, Thomas
 */
public class DiffItem  {
	private static int UNCHANGED = 0;
	private static int NEW = 1;
	private static int DELETED = 2;
	private static int CHANGED = 3;

	private Diffable item = null;
	private int state = UNCHANGED;
	
/**
 * DiffItem constructor comment.
 */
private DiffItem(Diffable item, int state) {
	super();
	this.item = item;
	this.state = state;
}

/**
 * erzeugt eine Instanz mit dem STatus CHANGED
 * Creation date: (06.12.03 12:03:08)
 * @return de.lvm.basis.util.DiffItem
 * @param item java.lang.Object
 */
public static DiffItem createDiffItemChanged(Diffable item) {
	return new DiffItem(item, CHANGED);
}

/**
 * erzeugt eine Instanz mit dem STatus DELETED
 * Creation date: (06.12.03 12:03:08)
 * @return de.lvm.basis.util.DiffItem
 * @param item java.lang.Object
 */
public static DiffItem createDiffItemDeleted(Diffable item) {
	return new DiffItem(item, DELETED);
}

/**
 * erzeugt eine Instanz mit dem STatus NEW
 * Creation date: (06.12.03 12:03:08)
 * @return de.lvm.basis.util.DiffItem
 * @param item java.lang.Object
 */
public static DiffItem createDiffItemNew(Diffable item) {
	return new DiffItem(item, NEW);
}

/**
 * Liefert das in diesem DiffItem verpackte Diffable-Objekt.
 * Creation date: (08.12.03 08:59:04)
 * @return de.lvm.basis.util.Diffable
 */
public Diffable getDiffable() {
	return item;
}

/**
 * gibt an, ob dieser Eintrag ge�ndert ist.
 * Creation date: (06.12.03 12:16:16)
 * @return boolean
 */
public boolean isChanged() {
	return state == CHANGED;
}

/**
 * gibt an, ob dieser Eintrag gel�scht ist.
 * Creation date: (06.12.03 12:16:16)
 * @return boolean
 */
public boolean isDeleted() {
	return state == DELETED;
}

/**
 * gibt an, ob dieser Eintrag neu ist.
 * Creation date: (06.12.03 12:16:16)
 * @return boolean
 */
public boolean isNew() {
	return state == NEW;
}
}