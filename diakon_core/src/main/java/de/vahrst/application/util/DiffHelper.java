package de.vahrst.application.util;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
/**
 * Helper-Klasse f�r den Abgleich zweier Collections. Liefert eine Diff-Liste mit
 * allen ge�nderten, neuen und gel�schten Eintr�gen.
 * Creation date: (06.12.03 11:56:17)
 * @author: Vahrst, Thomas
 */
public class DiffHelper {
/**
 * SyncHelper constructor comment.
 */
public DiffHelper() {
	super();
}

/**
 * vergleicht die beiden Keys miteinander. Liefert -1, wenn keyOld < keyNew, 0 wenn
 * beide Keys identisch sind und +1, wenn keyOld > keyNew
 * Creation date: (08.12.03 07:23:51)
 * @return int
 * @param keyOld java.lang.Comparable
 * @param keyNew java.lang.Comparable
 */
private static int compareKeys(Comparable keyOld, Comparable keyNew) {
	if(keyOld == null){
		return +1;  // in der ListeAlt sind wir am Ende, daher ist KeyOld = max
	}
	
	if(keyNew == null){
		return -1;
	}
	
	int status = 0;	
	int x = keyOld.compareTo(keyNew);
	if(x < 0) status = -1;
	if(x > 0) status = +1;
	return status;
}

/**
 * vergleicht den Inhalt der beiden vorgegebenen Listen. Diese m�ssen Instanzen von
 * Diffable enthalten und vergleichbar sein. Au�erdem m�ssen die Inhalte der beiden Listen nach
 * dem Key der Diffables sortiert sein.<p>
 * Zur�ckgegeben wird ein Array von DiffItems, diese enth�lt die neuen, gel�schten und ge�nderten
 * Werte aus den beiden Listen. Bei neuen und ge�nderten Eintr�gen wird das Diffable aus der
 * ListeNeu verwendet, bei gel�schten Eintr�gen das Diffable aus der ListeAlt.
 * Creation date: (06.12.03 12:18:22)
 * @return de.lvm.basis.util.DiffItem[]
 * @param listOld java.util.List
 * @param listNew java.util.List
 */
public static DiffItem[] diffSorted(List listOld, List listNew) {
	Iterator iterOld = listOld.iterator();
	Iterator iterNew = listNew.iterator();

	Diffable dOld = null;
	Diffable dNew = null;

	ArrayList diffItems = new ArrayList();
	DiffItem item = null;
		
	if(iterOld.hasNext()) dOld = (Diffable) iterOld.next();
	if(iterNew.hasNext()) dNew = (Diffable) iterNew.next();
	
	while(dOld != null || dNew != null){
		// pr�fen, welcher Key der gr��ere/kleinere ist, oder bei
		// beide Keys identisch sind.
		
		Comparable keyOld = (dOld != null)? dOld.getKey() : null;
		Comparable keyNew = (dNew != null)? dNew.getKey() : null;
		int status = compareKeys(keyOld, keyNew);

		switch(status){
			case +1:
				// in der Liste alt sind wir weiter als in der ListeNeu.
				// Das bedeutet: das aktuelle diffableNew ist neu.
				item = DiffItem.createDiffItemNew(dNew);
				diffItems.add(item);
				dNew = null;
				if(iterNew.hasNext()) dNew = (Diffable) iterNew.next();
				break;
			case -1:
				// in der Liste neu sind wir weiter als in der ListeAlt.
				// Das bedeutet: das aktuelle diffableOld wurde gel�scht.
				item = DiffItem.createDiffItemDeleted(dOld);
				diffItems.add(item);
				dOld = null;
				if(iterOld.hasNext()) dOld = (Diffable) iterOld.next();
				break;
			case 0:
				// Key aus dNew und dOld sind identisch. Jetzt vergleichen wir
				// die beiden Diffables.
				if(dNew.compareTo(dOld) != 0){
					// ge�ndert!
					item = DiffItem.createDiffItemChanged(dNew);
					diffItems.add(item);
				}
				dNew = null;
				dOld = null;
				if(iterOld.hasNext()) dOld = (Diffable) iterOld.next();
				if(iterNew.hasNext()) dNew = (Diffable) iterNew.next();
						
			default:
				break;
		}
		
	}	
	return toDiffItemArray(diffItems);	
}

/**
 * vergleicht den Inhalt der beiden vorgegebenen Listen. Diese m�ssen Instanzen von
 * Diffable enthalten und vergleichbar sein. Die Methode sorgt daf�r, dass die Listen
 * vor dem Vergleich sortiert werden, und zwar nach dem Key.<p>
 * Zur�ckgegeben wird ein Array von DiffItems, diese enth�lt die neuen, gel�schten und ge�nderten
 * Werte aus den beiden Listen. Bei neuen und ge�nderten Eintr�gen wird das Diffable aus der
 * ListeNeu verwendet, bei gel�schten Eintr�gen das Diffable aus der ListeAlt.
 * Creation date: (06.12.03 12:18:22)
 * @return de.lvm.basis.util.DiffItem[]
 * @param listOld java.util.List
 * @param listNew java.util.List
 */
public static DiffItem[] diffUnsorted(List listOld, List listNew) {
	Comparator comp = new Comparator(){
		public int compare(Object o1, Object o2){
			Diffable d1 = (Diffable)o1;
			Diffable d2 = (Diffable)o2;
			return d1.getKey().compareTo(d2.getKey());
		}
	};
	
	
	Collections.sort(listOld, comp);
	Collections.sort(listNew, comp);

	return diffSorted(listOld, listNew);
}

/**
 * Macht aus der ArrayList mit DiffItems ein DiffItem[]
 * Creation date: (08.12.03 07:27:30)
 * @return de.lvm.basis.util.DiffItem[]
 * @param list java.util.ArrayList
 */
private static DiffItem[] toDiffItemArray(ArrayList list) {
	DiffItem[] array = new DiffItem[list.size()];
	for(int i=0;i<list.size();i++){
		array[i] = (DiffItem) list.get(i);
	}
	return array;
}
}