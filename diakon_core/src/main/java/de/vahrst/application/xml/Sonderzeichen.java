package de.vahrst.application.xml;

/**
 * h�lt eine statische Tabelle der Sonderzeichen.
 * Creation date: (07.03.01 11:36:42)
 * @author: Vahrst, Thomas
 * @version: <version>
 */
public interface Sonderzeichen {
	public static final String[][] TABELLE =
	{{"\"", "&quot;"},
	 {"&",  "&amp;" },
	 {"<",  "&lt;"  },
	 {">",  "&gt;"  }};
}
