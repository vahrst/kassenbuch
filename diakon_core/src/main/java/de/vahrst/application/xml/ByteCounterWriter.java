package de.vahrst.application.xml;
/**
 * Diese Subklasse von Writer hat lediglich die Aufgabe,alle geschriebenen Bytes zu z�hlen
 * <p>
 * @author Vahrst, Thomas
 * @version 1.1 (Vahrst, Thomas - 18.06.2002 15:15) 
 */
class ByteCounterWriter extends java.io.Writer {
	private int counter = 0;

/**
 * ByteCounterWriter constructor comment.
 */
protected ByteCounterWriter() {
	super();
}


/**
 * ByteCounterWriter constructor comment.
 * @param lock java.lang.Object
 */
protected ByteCounterWriter(Object lock) {
	super(lock);
}


	/**
	 * Close the stream, flushing it first.  Once a stream has been closed,
	 * further write() or flush() invocations will cause an IOException to be
	 * thrown.  Closing a previously-closed stream, however, has no effect.
	 *
	 * @exception  IOException  If an I/O error occurs
	 */
public void close() throws java.io.IOException {}


	/**
	 * Flush the stream.  If the stream has saved any characters from the
	 * various write() methods in a buffer, write them immediately to their
	 * intended destination.  Then, if that destination is another character or
	 * byte stream, flush it.  Thus one flush() invocation will flush all the
	 * buffers in a chain of Writers and OutputStreams.
	 *
	 * @exception  IOException  If an I/O error occurs
	 */
public void flush() throws java.io.IOException {}


/**
 * liefert die L�nge des geschriebenen Streams
 * <p>
 * (18.06.02 10:44:40) Methode angelegt
 * @author Vahrst, Thomas
 * @return int
 */
public int getLength() {
	return counter;
}


	/**
	 * Write a portion of an array of characters.
	 *
	 * @param  cbuf  Array of characters
	 * @param  off   Offset from which to start writing characters
	 * @param  len   Number of characters to write
	 *
	 * @exception  IOException  If an I/O error occurs
	 */
public void write(char[] cbuf, int off, int len) throws java.io.IOException {
	counter += len;
}


		/**
	 * Write a string.
	 *
	 * @param  str  String to be written
	 *
	 */
	public void write(String str) {
	counter += str.length();
	}
}