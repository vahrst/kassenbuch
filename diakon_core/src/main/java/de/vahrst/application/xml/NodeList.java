package de.vahrst.application.xml;

/**
 * Kapselt eine Liste von Node-Objekten.
 * Creation date: (01.03.01 10:19:53)
 * @author: Vahrst, Thomas
 * @version: 1.1.0 
 */
public interface NodeList {
/**
 * Liefert die Anzahl Eintr�ge in dieser NodeList
 * Creation date: (01.03.01 10:21:08)
 * 
 * @return int
 */
int getLength();
/**
 * liefert das i-te Elemten aus dieser Node-List
 * Creation date: (01.03.01 10:21:40)
 * 
 * @return de.lvm.basis.xml.Node
 * @param i int
 */
public Node item(int i);
}
