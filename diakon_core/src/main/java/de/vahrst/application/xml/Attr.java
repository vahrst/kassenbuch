package de.vahrst.application.xml;

import java.io.*;
/**
 * Kapselt ein Attribut eines Elements. Auch dieses Interface erbt von
 * Node, weil in diesem die Funktionalit�t der verketteten Listen implementiert wird.
 * Creation date: (01.03.01 10:04:47)
 * @author: Vahrst, Thomas
 * @version: 1.1.3 
 */
public interface Attr extends Node, Serializable{
/**
 * liefert den Namen dieses Attributs
 * Creation date: (01.03.01 10:36:43)
 * 
 * @return java.lang.String
 */
String getName();
/**
 * liefert den Wert dieses Attributs
 * Creation date: (01.03.01 10:36:52)
 * 
 * @return java.lang.String
 */
String getValue();
/**
 * setzt den Wert f�r dieses Attribut
 * Creation date: (01.03.01 10:37:13)
 * 
 * @param arg java.lang.String
 */
void setValue(String arg);
}
