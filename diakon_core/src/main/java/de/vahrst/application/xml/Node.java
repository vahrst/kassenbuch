package de.vahrst.application.xml;

import java.io.*;
/**
 * Superklasse f�r Elemente und TextElemente. Diese Klasse implementiert die Funktionalit�t
 * f�r verkettete Listen. Au�erdem enth�lt jeder Knoten ein Key-Value-Paar (Name und Value).
 * Creation date: (01.03.01 10:14:00)
 * @author: Vahrst, Thomas
 * @version: 1.1.5 
 */
public interface Node extends Serializable{
	public final int DOCUMENT_NODE = 0;
	public final int ELEMENT_NODE = 1;
	public final int TEXT_NODE = 2;
	public final int ATTRIBUTE_NODE = 3;
/**
 * f�gt den mitgegebenen Child-Knoten in die Liste der Children ein.
 * Creation date: (05.03.01 10:09:35)
 * 
 * @param child de.lvm.basis.xml.Node
 */
void appendChild(Node child);
/**
 * Liefert eine Liste aller Child-Nodes (Elemente und TextNodes)
 * Creation date: (05.03.01 10:48:39)
 * 
 * @return de.lvm.basis.xml.NodeList
 */
NodeList getChildNodes();
/**
 * Liefert den ersten Child-Knoten dieses Knoten.
 * Creation date: (01.03.01 10:28:47)
 * 
 * @return de.lvm.basis.xml.Node
 */
Node getFirstChild();
/**
 * liefert den letzten Child-Knoten dieses Knoten.
 * Creation date: (01.03.01 10:29:19)
 * 
 * @return de.lvm.basis.xml.Node
 */
Node getLastChild();
/**
 * liefert den n�chsten Knoten in aus der eigenen verketteten Liste. (Nachfolger-Knoten)
 * Creation date: (01.03.01 10:29:40)
 * 
 * @return de.lvm.basis.xml.Node
 */
Node getNextSibling();
/**
 * Liefert den Namen dieses Knoten.
 * Creation date: (06.03.01 09:48:26)
 * 
 * @return java.lang.String
 */
String getNodeName();
/**
 * Liefert den Typ dieses Knoten. Die Typen sind als symbolische Konstanten
 * in diesem Interface definiert.
 * Creation date: (01.03.01 10:29:57)
 * 
 * @return int
 */
int getNodeType();
/**
 * Liefert den Inhalt des Wertefelds f�r diesen Knoten.
 * Creation date: (06.03.01 09:48:16)
 * 
 * @return java.lang.String
 */
String getNodeValue();
/**
 * liefert den �bergeordneten 'Parent'-Knoten.
 * Creation date: (01.03.01 10:30:18)
 * 
 * @return de.lvm.basis.xml.Node
 */
Node getParentNode();
/**
 * liefert den vorhergehenden Knoten aus der eigenen verketteten Liste. (Vorg�nger-Knoten)
 * Creation date: (01.03.01 10:30:28)
 * 
 * @return de.lvm.basis.xml.Node
 */
Node getPreviousSibling();
/**
 * gibt an, ob dieser Knoten weitere Child-Knoten besitzt.
 * Creation date: (01.03.01 10:30:44)
 * 
 * @return boolean
 */
boolean hasChildNodes();
}
