package de.vahrst.application.xml;

/**
 * Enumeration von XML-Elementen
 * Creation date: (19.03.01 12:34:28)
 * @author: Vahrst, Thomas
 * @version: <version>
 */
public interface ElementEnumeration {
/**
 * gibt an, ob noch weitere Elemente in der Enumeration vorliegen.
 * Creation date: (19.03.01 12:35:40)
 * 
 * @return boolean
 */
boolean hasMoreElements();
/**
 * liefert das n�chste Element aus der Enumeration oder null, wenn das Ende erreicht ist.
 * Creation date: (19.03.01 12:35:56)
 * 
 * @return de.lvm.basis.xml.Element
 */
Element nextElement();
/**
 * liefert die Anzahl der Elemente in dieser Enumeration
 * Creation date: (12.12.01 09:25:09)
 * @return int
 */
int size();
}
