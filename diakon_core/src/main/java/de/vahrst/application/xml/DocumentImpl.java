package de.vahrst.application.xml;

import java.io.*;
/**
 * repr�sentiert ein komplettes XML-Dokument. Au�erdem dient eine Instanz dieser
 * Klasse als Factory f�r weitere Elemente.
 * Creation date: (05.03.01 10:03:38)
 * @author: Vahrst, Thomas
 * @version: 1.1.3 
 */
public class DocumentImpl extends NodeImpl implements Document, Serializable{
/**
 * DocumentImpl constructor comment.
 */
public DocumentImpl() {
	super();
}
/**
 * �berschreibt die gleichnamige Methode aus der Superklasse. Document darf genau
 * einmal ein Child-Node (vom Type Element) hinzugef�gt werden.
 * Creation date: (05.03.01 10:44:09)
 * 
 * @param child de.lvm.basis.xml.Node
 */
public void appendChild(Node child) {
	if(getFirstChild() != null)
		throw new XMLException("Document hat bereits ein Root-Element");
	
	super.appendChild(child);	
	
}
/**
 * erzeugt ein neues XML-Element Objekt mit dem vorgegebenen tag-Namen.
 * Creation date: (05.03.01 10:03:38)
 * 
 * @return de.lvm.basis.xml.Element
 * @param tagname java.lang.String
 */
public Element createElement(String tagname) {
	return new ElementImpl(tagname);
}
/**
 * erzeugt ein neues XML-Element Objekt mit dem vorgegebenen tag-Namen.
 * Creation date: (05.03.01 10:03:38)
 * 
 * @return de.lvm.basis.xml.Element
 * @param tagname java.lang.String
 */
public Element createElement(String tagname, String text) {
	return new ElementImpl(tagname, text);
}
/**
 * erzeugt ein Text-Element mit dem vorgegebenen Text
 * Creation date: (05.03.01 10:03:38)
 * 
 * @return de.lvm.basis.xml.Text
 * @param text java.lang.String
 */
public Text createTextNode(String text) {
	return new TextImpl(text);
}
/**
 * Liefert das Root-Element f�r dieses XML-Dokument
 * Creation date: (05.03.01 10:03:38)
 * @version 1.1.0
 * @return de.lvm.basis.xml.Element
 */
public Element getDocumentElement() {
	return (Element) getFirstChild();
}
/**
 * Liefert den Typ dieses Knoten. Die Typen sind als symbolische Konstanten
 * in diesem Interface definiert.
 * Creation date: (05.03.01 10:07:06)
 * 
 * @return int
 */
public int getNodeType() {
	return DOCUMENT_NODE;
}
}
