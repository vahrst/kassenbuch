package de.vahrst.application.xml;

import java.io.*;
/**
 * Repr�sentiert ein gesamtes XML-Dokument. Dieses enth�lt in
 * hierarchischer Form weitere Objekte vom Typ 'Element', 'Attr' und 'TextElement'.
 * Creation date: (01.03.01 10:05:14)
 * @author: Vahrst, Thomas
 * @version: 1.1.3 
 */
public interface Document extends Node, Serializable{
/**
 * erzeugt ein neues XML-Element Objekt mit dem vorgegebenen tag-Namen.
 * Creation date: (05.03.01 09:49:12)
 * 
 * @return de.lvm.basis.xml.Element
 * @param tagname java.lang.String
 */
Element createElement(String tagname);
/**
 * erzeugt ein Text-Element mit dem vorgegebenen Text
 * Creation date: (05.03.01 09:49:31)
 * 
 * @return de.lvm.basis.xml.Text
 * @param text java.lang.String
 */
Text createTextNode(String text);
/**
 * Liefert das Root-Element f�r dieses XML-Dokument
 * Creation date: (01.03.01 10:07:49)
 * @version 1.1.0
 * @return de.lvm.basis.xml.Element
 */
Element getDocumentElement();
}
