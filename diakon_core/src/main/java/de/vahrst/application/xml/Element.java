package de.vahrst.application.xml;
import java.io.*;
/**
 * Repr�sentiert ein XML-Element. Dieses kann weitere Elemente und TextElemente sowie eine
 * Liste von Attributen enthalten.
 * Creation date: (01.03.01 10:04:26)
 * @author: Vahrst, Thomas
 * @version: 1.1.11 (Vahrst, Thomas - 15.07.2002 13:33) 
 */
public interface Element extends Node, Serializable {
/**
 * liefert true, wenn eines der Child-Knoten ein Text-Knoten ist.
 * Creation date: (06.03.01 10:19:53)
 * 
 * @return boolean
 */
boolean containsTextNode();


/**
 * liefert den Inhalt des Attributs mit dem vorgegebenen Key, oder einen Leerstring (""), wenn ein
 * solches Attribut nicht existiert. (Entspricht der DOM-Implemtierung des IBM XML-Parsers)
 * Creation date: (05.03.01 14:12:46)
 * 
 * @return java.lang.String
 * @param key java.lang.String
 */
String getAttribute(String key);


/**
 * liefert das Attribut mit dem vorgegebenen Key.
 * Creation date: (01.03.01 10:40:28)
 * 
 * @return de.lvm.basis.xml.Attr
 * @param key java.lang.String
 */
Attr getAttributeNode(String key);


/**
 * liefert eine Liste aller Attribute dieses Elements.
 * Creation date: (05.03.01 14:12:30)
 * 
 * @return de.lvm.basis.xml.NamedNodeMap
 */
NamedNodeMap getAttributes();


/**
 * Liefert eine Liste aller Child-Nodes (Elemente und TextNodes)
 * Creation date: (01.03.01 10:22:30)
 * 
 * @return de.lvm.basis.xml.NodeList
 */
NodeList getChildNodes();


/**
 * Liefert eine ElementEnumeration mit allen Elementen, die den vorgegebenen Tag-Namen besitzen. Wenn
 * tagname == null ist, wird eine Enumeration mit allen Element-Knoten zur�ckgegeben.
 * Creation date: (19.03.01 12:28:22)
 * 
 * @return de.lvm.basis.xml.ElementEnumeration
 * @param tagname java.lang.String
 */
ElementEnumeration getElementsByTagName(String tagname);


/**
 * liefert den ersten Child-Knoten vom Typ 'Element', oder null, falls keiner existiert.
 * <p>
 * (15.07.02 13:29:24) Methode angelegt
 * @author Vahrst, Thomas
 * @return de.lvm.basis.xml.Element
 */
Element getFirstElement();


/**
 * Liefert das (erste) Child.Element mit dem vorgegebenen Namen.
 * Creation date: (19.03.01 14:50:46)
 * 
 * @return de.lvm.basis.xml.Element
 * @param name java.lang.String
 */
Element getFirstElementByName(String name);


/**
 * Liefert den Tag-Namen dieses Elements
 * Creation date: (01.03.01 10:32:47)
 * 
 * @return java.lang.String
 */
String getTagName();


/**
 * liefert den Inhalt des (ersten) Text-Childknotens als String, wenn ein solcher existiert, sonst null.
 * Creation date: (20.03.01 07:11:03)
 * 
 * @return java.lang.String
 */
String getText();


/**
 * liefert den Inhalt des (ersten) Text-Childknotens als String, wenn ein solcher existiert, sonst einen Leerstring.
 * Creation date: (20.03.01 07:11:03)
 * 
 * @return java.lang.String
 */
String getTextNotNull();


/**
 * F�gt diesem Element ein Attribut mit den vorgegebenen Key- und Value-Werten
 * hinzu.
 * Creation date: (01.03.01 10:17:33)
 * 
 * @param key java.lang.String
 * @param value java.lang.String
 */
void setAttribute(String key, String value);
}