package de.vahrst.application.xml;

/**
/**
 * Implementierung einer NodeList. Kapselt eine Liste von Node-Objecten. Diese Klasse ist so implementiert, dass
 * sie keine neuen Objekt-Arrays erzeugt. Vielmehr wird beim Zugriff auf ein bestimmtes Element in der verketteten
 * Liste vor/zur�ckpositioniert. Dadurch wird der Zugriff evtl. langsamer, aber wir sparen Speicherplatz.
 * <p>
 * Vergleichswerte Performance, bei sequentieller Abfrage von 10 Elementen (0 bis 9):<br>
 * 1.000.000 x lesen aller Eintr�ge: 3780 ms.
 * <p>
 * bei entsprechende Implementierung mit einer ArrayList:<br>
 * 1.000.000 x lesen aller Eintr�ge: 9253 ms.
 * <p>
 * Bei sequentiellem Zugriff auf die Elemente ist diese Implementierung sogar schneller, weil bei der Instanziierung der NodeList nicht
 * jedesmal eine ArrayList erzeugt werden mu�.
 * Creation date: (05.03.01 10:52:17)
 * @author: Vahrst, Thomas
 * @version: 1.1.3 
 */
class NodeListImpl implements NodeList {
	/** die L�nger der NodeList = Anzahl Elemente in der Liste */
	protected int length = 0;

	/** das erste Element der verketteten Liste */
	protected Node firstNode = null;

	/** der letzte abgefragte Index */
	private int lastIndex = -1;

	/** der Knoten des letzten abgefragen Index */
	private Node lastIndexNode = null;
	
/**
 * NodeListImpl constructor comment.
 */
public NodeListImpl(Node firstChildNode, int count) {
	super();
	this.firstNode = firstChildNode;
	this.length = count;

	// lastIndex init.
	if(count >0){
		lastIndex = 1;  
		lastIndexNode = firstChildNode;
	}	
}
/**
 * Liefert die Anzahl Eintr�ge in dieser NodeList
 * Creation date: (05.03.01 10:52:17)
 * 
 * @return int
 */
public int getLength() {
	return length;
}
/**
 * liefert das i-te Elemten aus dieser Node-List. liefert null, wenn der Index ung�ltig ist !!
 * Creation date: (05.03.01 10:52:17)
 * 
 * @return de.lvm.basis.xml.Node
 * @param i int
 */
public Node item(int index) {
	if(index>=length || index < 0)
		return null;

	// wenn der gesuchte index n�her an 0 liegt als der letzte gesuchte Index, fangen wir vorne an zu suchen.
	if(index < lastIndex-index){
		lastIndex = 0;
		lastIndexNode = firstNode;
	}	
		
	int richtung = index - lastIndex; // >0 nach vorne, <0 zur�ck
	while(lastIndex != index){
		if(richtung >0){
			lastIndex++;
			lastIndexNode = lastIndexNode.getNextSibling();
		}else{
			lastIndex--;
			lastIndexNode = lastIndexNode.getPreviousSibling();
		}
	}
	return lastIndexNode;
	
}
}
