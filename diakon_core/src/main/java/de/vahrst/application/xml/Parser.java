package de.vahrst.application.xml;
import java.io.*;
import java.util.*;
/**
 * Parst ein im String-Format vorliegendes XML-Dokument und erzeugt ein Objektnetz
 * von XML-Objekten.
 * Creation date: (06.03.01 10:47:01)
 * @author: Vahrst, Thomas
 * @version: 1.1.6 
 */
public class Parser implements Sonderzeichen{
	/** Liste der Whitespace-Zeichen */
	private static char[] whitespaces = {' ', '<', '>', '\"', '=', '/', '\n', '\r', '\t'};
	private static int wsCount = whitespaces.length;
	
	/**
	 * Der zu parsende String mit dem XML-Dokument
	 */
	private String xmlString;

	/** die aktuelle Position im xmlString */
	private int position = 0;
	
	/**
	 * solange wir noch kein Root-Element gefunden haben, m�ssen
	 * wir auch mit einer DTD rechnen...
	 */
	private boolean rootElementFound = false;

	
	
	/**
	 * das neu zu erstellende Document
	 */
	private Document document;


	private static Map<String, String> konvertTable = new HashMap<String, String>();

	// initialisierung der konvertTable
	static{
		for(int i=0; i< TABELLE.length; i++){
			konvertTable.put(TABELLE[i][1], TABELLE[i][0]);
		}
	}
	
/**
 * Parser constructor comment.
 */
private Parser(Reader reader) throws IOException {
	super();
	this.xmlString = readXML(reader);
}


/**
 * Parser constructor comment.
 */
private Parser(String xmlString) {
	super();
	this.xmlString = xmlString;
}


/**
 * Insert the method's description here.
 * Creation date: (04.11.02 14:28:46)
 * @return java.lang.String
 * @param tagname java.lang.String
 */
private String checkComment(String tagname) {
	int endeElementPos = 0;

	// Alle Kommentar Tags konsumieren und nicht weiterverwenden.
	while ("!--".equals(tagname)) {
		endeElementPos = xmlString.indexOf('>', position); // liefert die Ende-Position des Elements.
		if (xmlString.substring(endeElementPos-2, endeElementPos).equals("--")) {
			position = endeElementPos;	
			tagname = extractTagName();
		} else {
			throw new XMLException("Kommentar Element '!--' wird nicht richtig geschlossen.");
		}
	}
	return tagname;
}


/**
 * pr�ft, ob an der aktuellen Position eine DTD beginnt. In diesem Fall wird
 * auf das n�chste Element positioniert.
 * Creation date: (20.03.01 10:33:52)
 * 
 */
private boolean checkDTD(String tagname) {
	
	// wenn das aktuelle 'Tag' = !DOCTYPE ist, haben wir eine DTD!
	if ("!DOCTYPE".equalsIgnoreCase(tagname)) {
		
		int zaehler = 1; // < und > suchen, bis wir wieder auf 0 sind. (wir befinden uns bereits auf einem '<').

		int laenge = xmlString.length();
		position++;
		while (zaehler > 0) {
			if (position >= laenge) {
				position = -1;
				return true;
			}

			if (xmlString.charAt(position) == '<')
				zaehler++;
			if (xmlString.charAt(position) == '>')
				zaehler--;
			position++;
		}
		int aktpos = position;
		position = xmlString.indexOf("<", aktpos); // jetzt nach dem n�chsten Element-Beginn suchen.
		return true;
	}
	return false;
}


/**
 * Der Zeiger zeigt aktuell auf ein Ende-Element. Diese Methode pr�ft, ob dieses
 * Ende-Element zum vorgegebenen Element-Objekt passt. Sonst gibts eine XMLException.
 * Creation date: (07.03.01 11:01:11)
 * 
 * @param element de.lvm.basis.xml.Element
 */
private void checkEndeTag(Element element) {
	String endeTag = extractTagName();
	if(!endeTag.equals(element.getTagName()))
		throw new XMLException("EndeTag </"+endeTag+"> passt nicht zu Element <" + element.getTagName()+">");

	position = xmlString.indexOf(">", position);	
}


/**
 * zeiger steht auf Anfang eines Elements (<). Diese Methode liefert den Tagnamen dieses Elements zur�ck.
 * Zeiger wird auf Zeichen hinter Tagname positioniert.
 * Creation date: (07.03.01 10:57:02)
 * 
 * @return java.lang.String
 */
private String extractTagName() {
	int startTagnamePos = getNextNotWhitespacePos(position+1); // liefert die Position des ersten Nicht-Whitespace Zeichens	
	int endeTagnamePos = getNextWhitespacePos(startTagnamePos);  // liefert die Ende-Position des Tag-Namens

	String tagname = xmlString.substring(startTagnamePos, endeTagnamePos);
	position = endeTagnamePos;	
	return tagname;
}


/**
 * liefert die nach startpos folgende Position eines Nicht Whitespaces.
 * Creation date: (06.03.01 12:56:17)
 * 
 * @return int
 * @param startpos int
 */
private int getNextNotWhitespacePos(int startpos) {
	for(int i=startpos; i<xmlString.length(); i++){
		char c = xmlString.charAt(i);
		if(!isWhiteSpace(c))
			return i;
	}
	
	return -1;
}


/**
 * liefert die nach startpos folgende Position eines Whitespaces.
 * Creation date: (06.03.01 12:56:17)
 * 
 * @return int
 * @param startpos int
 */
private int getNextWhitespacePos(int startpos) {
	for(int i=startpos; i<xmlString.length(); i++){
		char c = xmlString.charAt(i);
		if(isWhiteSpace(c))
			return i;
	}
	
	return -1;
}


/**
 * gibt an, ob das mitgegebene Zeichen ein Whitespace ist.
 * Creation date: (07.03.01 12:38:26)
 * 
 * @return boolean
 * @param c char
 */
private boolean isWhiteSpace(char c) {
	boolean isWS = false;
	for(int j=0; j<wsCount && !isWS; j++){
		if(c == whitespaces[j])
			isWS = true;
	}
	
	return isWS;
}


/**
 * sucht im Source-String nach '&'. Wenn solche Zeichen gefunden werden, werden
 * die entsprechenden K�rzel (z.B. &gt;) in die richtigen Ascii-zeichen konvertiert.
 * Creation date: (07.03.01 11:18:03)
 * 
 * @return java.lang.String
 * @param source java.lang.String
 */
private String konvertText(String source) {
	int pos = source.indexOf('&');
	if(pos == -1)
		return source;

	StringBuffer target = new StringBuffer(source.length());
	int aktpos = 0;
	while(pos != -1){
		target.append(source.substring(aktpos, pos));
		int semikolonPos = source.indexOf(';', pos);
		String toKonvert = source.substring(pos, semikolonPos+1);

		String neuesZeichen = konvertTable.get(toKonvert);
		if(neuesZeichen != null)
			target.append(neuesZeichen);
			
		aktpos = semikolonPos+1;
		pos = source.indexOf('&', aktpos);
	}
	if(aktpos < source.length())
		target.append(source.substring(aktpos));
	
	return target.toString();		
}


/**
 * Wir haben ein '<' gefunden. An der aktuellen Position im String
 * beginnt also ein neues Element-Tag. Wir wissen, dass es sich dabei
 * um ein Start-Tag handelt. Return, wenn das zugeh�rige Ende-Tag verarbeitet wurde. Beim return
 * steht der Positionszeiger auf dem letzten '>' des Ende-Elements.
 * Creation date: (06.03.01 12:39:40)
 * 
 * @param knoten de.lvm.basis.xml.Node
 */
private void newElementTag(Node aktuellerKnoten) {
	String tagname = extractTagName();
	int endeElementPos = 0;

	// Alle Kommentar Tags konsumieren und nicht weiterverwenden.
	tagname = checkComment(tagname);
	
	// solange wir noch kein RootElement gefunden haben, kann auch noch eine DTD kommen...
	if(!rootElementFound){
		// evtl DTD �berlesen...
		if(checkDTD(tagname)){
			if(position == -1) return;
			tagname = extractTagName();
			tagname = checkComment(tagname);  // alle nach Kommentare �berlesen...
		}
	}
	
	Element element = document.createElement(tagname);  
	// position = Zeichen hinter tagname.	
	rootElementFound = true;
	
	endeElementPos = xmlString.indexOf('>', position); // liefert die Ende-Position des Elements.
		
	
	// Attribute parsen.
	parseAttributes(position, endeElementPos, element);
	
	// Position auf das Ende-zeichen '>' setzten
	position = endeElementPos;	
	
	// enth�lt dieses Element direkt das Ende-Kennzeichen '/' ?
	// In diesem Fall gibt es keine weiteren Sub-Elemente.
	if(xmlString.charAt(endeElementPos-1) == '/'){

		// ja: appendChild und return
		aktuellerKnoten.appendChild(element);
		return;
		
	}else{
		boolean endetag = false;

		do{
			position++;
			parseTextNode(element);

			// auf Anfang des n�chsten Elements positionieren
			position = xmlString.indexOf('<', position);
			if(position == -1)
				throw new XMLException("EndeTag f�r Element'" + element.getTagName() + "' fehlt");

			endetag =  (xmlString.charAt(position+1) == '/');
			if(!endetag)
				newElementTag(element);  // Rekursiv neues Element erzeugen.
		}while(!endetag);
		
		checkEndeTag(element);  // pr�fen, ob das Ende-Tag zum aktuellen Element geh�rt.				
		aktuellerKnoten.appendChild(element);
	
	}
}


/**
 * statische Service-Methode zum Parsen des Inhalts eines Reader-Objekts. Erzeugt intern eine
 * Instanz von Parser und ruft parseXMLString() auf.
 * Creation date: (06.03.01 10:56:59)
 * 
 * @return de.lvm.basis.xml.Document
 * @param reader java.io.Reader
 */
public static Document parse(Reader reader) throws IOException{
	
	
	Parser parser = new Parser(reader);
	return parser.parseXMLString();
}


/**
 * statische Service-Methode zum Parsen eines XML-Strings. Erzeugt intern eine
 * Instanz von Parser und ruft parseXMLString() auf.
 * Creation date: (06.03.01 10:56:59)
 * 
 * @return de.lvm.basis.xml.Document
 * @param xmlString java.lang.String
 */
public static Document parse(String xmlString) {
	Parser parser = new Parser(xmlString);
	return parser.parseXMLString();
}


/**
 * sucht ab der aktuellen Position bis zum Ende des Elements nach Attributen
 * und f�gt diese dem mitgegebenen Element hinzu.
 * Creation date: (07.03.01 09:14:02)
 * 
 * @param startpos int Startposition f�r die Suche, 1. Zeichen hinter dem Element-Tagnamen
 * @param endepos int Endeposition f�r die Suche, Position des '>' f�r dieses Element.
 * @param element de.lvm.basis.xml.Element
 */
private void parseAttributes(int startpos, int endepos, Element element) {

	// Ein Attribut innerhalb eines XML-Elements hat die Form: key="value", also z.B:
	// Element mit zwei Attribut-Feldern. <ViewUpdate Active="0" Version="1.0"/>
	
	boolean ende = false;
	do{
		int startAttrPos = getNextNotWhitespacePos(startpos);
		if(startAttrPos >= endepos || startAttrPos == -1){
			ende = true;  // keine (weiteren) Attribute
		}else{
			int endeAttrPos = getNextWhitespacePos(startAttrPos);  // Endeposition des Namens ermitteln
			int startAttrValuePos = xmlString.indexOf('"', endeAttrPos);  // Startposition des Attribut-Wertes = Position des ersten "
			int endeAttrValuePos = xmlString.indexOf('"', startAttrValuePos+1); // Endeposition des Attribut-Wertes = Position des zweiten "

			String attrValue = xmlString.substring(startAttrValuePos+1, endeAttrValuePos);
			attrValue = konvertText(attrValue);
			
			// jetzt ein neues Attribut-Objekt in das Element einf�gen.
			element.setAttribute(xmlString.substring(startAttrPos, endeAttrPos),attrValue);

			startpos = endeAttrValuePos+1;
			
		}
	}while(!ende);
}


/**
 * parst den Prolog des XML-String, erzeugt ein Document-Objekt und
 * positioniert auf das erste Element.
 * Creation date: (06.03.01 11:01:19)
 * 
 */
private void parseProlog() {
	document = new DocumentImpl();
	
	// von der aktuellen Position ab nach dem ersten < suchen.
	int pos = xmlString.indexOf('<', 0);

	if(pos == -1 || xmlString.charAt(pos+1) != '?')
		throw new XMLException("kein g�ltiger Header");

	position = pos;
	pos = xmlString.indexOf('>', position+1);

	if(pos == -1 || xmlString.charAt(pos-1) != '?')
		throw new XMLException("kein g�ltiger Header");

	position = pos;
	pos = xmlString.indexOf('<', position+1);  // jetzt nach erstem Root-Element suchen...

	position = pos;	
}


/**
 * Ermittelt den Text f�r zwischen zwei Elementen und erzeugt daraus einen
 * Textknoten, der dem Element hinzugef�gt wird. Positionszeiger mu� auf dem ersten Zeichen hinter
 * '>' stehen.
 * Creation date: (07.03.01 09:43:14)
 * 
 * @param element de.lvm.basis.xml.Element
 */
private void parseTextNode(Element element) {
	if(element == document)
		return; // Das Document enth�lt nur ein Root-Element, keine weiteren Text-Knoten.
	
	int endepos = xmlString.indexOf('<', position);
	if (endepos == -1)
		endepos = xmlString.length();

	// wenn beide Elemente direkt aneinandergrenzen, gibts kein Text-Knoten.
	if(endepos == position){
 	// Wir sind bereits wieder auf einem Start-Zeichen, damit die folgenden Methoden das n�chste
 	// Element finden, jetzt einen Schritt zur�ck machen.
		position--;
		return;
	}
			
	String text = xmlString.substring(position, endepos);
	element.appendChild(document.createTextNode(konvertText(text)));
}


/**
 * Einstiegspunkt zum Parsen eines XML-Dokuments.
 * Creation date: (06.03.01 10:58:53)
 * 
 * @return de.lvm.basis.xml.Document
 */
private Document parseXMLString() {
	
	// zuerst mal den Prolog parsen und ein Document-Objekt erstellen.
	parseProlog();
	
	if(position != -1)
		newElementTag(document);  // '<' gefunden, an der aktuellen Position beginnt ein neues Element-Tag.
	return document;
}


/**
 * liest den Inhalt des Readers ein und liefert einen entsprechend gef�llten String zur�ck.
 * Creation date: (20.03.01 09:59:21)
 * 
 * @return java.lang.String
 * @param in java.io.Reader
 */
private String readXML(Reader in) throws IOException {
	StringBuffer sb = new StringBuffer(32);

	BufferedReader bufferedIn = new BufferedReader(in);
	String zeile = bufferedIn.readLine();
	while(zeile !=null){
		sb.append(zeile).append("\n");
		zeile = bufferedIn.readLine();
	}
	return sb.toString();
}
}