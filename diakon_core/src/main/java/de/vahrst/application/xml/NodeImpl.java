package de.vahrst.application.xml;

import java.io.*;

/**
 * Abstrakte Oberklasse f�r Text-Knoten und ElementImpl. Diese Klasse implementiert die
 * Funktionalit�t f�r verkettete Listen. Au�erdem enth�lt jeder Knoten ein Wertepaar (name, value), das
 * von den jeweiligen Subklassen genutzt werden kann.
 * 
 * Creation date: (05.03.01 09:51:11)
 * @author: Vahrst, Thomas
 * @version: 1.1.4 
 */
abstract class NodeImpl implements Node, Serializable{
	/** erster Child-Knoten */
	protected NodeImpl firstChild = null;

	/** letzter Child-Knoten */
	protected NodeImpl lastChild = null;

	/** Nachfolger-Knoten. Ist null, wenn dies der letzte Knoten in der Kette ist*/
	protected NodeImpl nextSibbling = null;

	/** Vorg�nger-Knoten. Ist null, wenn dies der erste Knoten in der Kette ist */
	protected NodeImpl prevSibbling = null;

	/** Eltern-Knoten */
	protected NodeImpl parentNode = null;

	/** 
	 * Anzahl Child-Nodes. Dieser Z�hler wird ben�tigt, um bei der Erzeugung einer
	 * NodeList diese mit der passenden Gr��e zu initialisieren.
	 */
	private int childCount = 0;

	/** Name dieses Knoten */
	private String name;

	/** Dieses Feld enth�lt den Inhalt f�r diesen Knoten */
	private String value;
/**
 * NodeImpl constructor comment.
 */
public NodeImpl() {
	super();
}
/**
 * f�gt den mitgegebenen Child-Knoten in die Liste der Children ein.
 * Creation date: (05.03.01 10:08:17)
 * 
 * @param child de.lvm.basis.xml.Node
 */
public void appendChild(Node child) {

	// wir casten Node => NodeImpl, um an die nicht im Interface verf�gbaren Methoden heranzukommen.
	NodeImpl newChild = (NodeImpl) child;	
	
	// zuerst den neuen Childknoten in die Liste der schon vorhandenen Childknoten einf�gen und den Parent setzen.
	newChild.insertIntoQueue(lastChild, null);  // vorg�nger, nachfolger; hier also: hinten anh�ngen.
	newChild.setParent(this);
	
	// Zeiger auf den letzten Knoten in der Child-Kette aktualisieren.
	lastChild = newChild;

	// wenn dies der erste Child-Knoten ist, ist die
	// verkettete Liste bisher leer und wir m�ssen noch die Referenz auf den ersten Knoten der Kette setzen.
	if(firstChild == null)
		firstChild = newChild;
			
	childCount++;
}
/**
 * Liefert eine Liste aller Child-Knoten als NodeList.
 * Creation date: (05.03.01 10:51:33)
 * 
 * @return de.lvm.basis.xml.NodeList
 */
public NodeList getChildNodes() {
	return new NodeListImpl(firstChild, childCount);
}
/**
 * Liefert den ersten Child-Knoten dieses Knoten.
 * Creation date: (05.03.01 09:52:35)
 * 
 * @return de.lvm.basis.xml.Node
 */
public Node getFirstChild() {
	return firstChild;
}
/**
 * liefert den letzten Child-Knoten dieses Knoten.
 * Creation date: (05.03.01 09:52:35)
 * 
 * @return de.lvm.basis.xml.Node
 */
public Node getLastChild() {
	return lastChild;
}
/**
 * liefert den n�chsten Knoten in aus der eigenen verketteten Liste. (Nachfolger-Knoten)
 * Creation date: (05.03.01 09:52:35)
 * 
 * @return de.lvm.basis.xml.Node
 */
public Node getNextSibling() {
	return nextSibbling;
}
/**
 * liefert den Namen dieses Knoten.
 * Creation date: (06.03.01 10:05:01)
 * 
 * @return java.lang.String
 */
public String getNodeName() {
	return name;
}
/**
 * Liefert den Inhalt des Werte-Felds f�r diesen Knoten.
 * Creation date: (06.03.01 10:02:35)
 * 
 * @return java.lang.String
 */
public String getNodeValue() {
	return value;
}
/**
 * liefert den �bergeordneten 'Parent'-Knoten.
 * Creation date: (05.03.01 09:52:35)
 * 
 * @return de.lvm.basis.xml.Node
 */
public Node getParentNode() {
	return parentNode;
}
/**
 * liefert den vorhergehenden Knoten aus der eigenen verketteten Liste. (Vorg�nger-Knoten)
 * Creation date: (05.03.01 09:52:35)
 * 
 * @return de.lvm.basis.xml.Node
 */
public Node getPreviousSibling() {
	return prevSibbling;
}
/**
 * gibt an, ob dieser Knoten weitere Child-Knoten besitzt.
 * Creation date: (05.03.01 09:52:35)
 * 
 * @return boolean
 */
public boolean hasChildNodes() {
	return (firstChild != null);
}
/**
 * f�gt diesen Knoten zwischen den Knoten 'prev' und 'next' ein. Wenn
 * prev = null ist, wird der Knoten vorne eingef�gt, wenn 'next' null ist
 * wird dieser Knoten an die verkn�pfte Liste angeh�ngt.
 * Creation date: (05.03.01 10:28:39)
 * 
 * @param prev de.lvm.basis.xml.NodeImpl
 * @param next de.lvm.basis.xml.NodeImpl
 */
void insertIntoQueue(NodeImpl prev, NodeImpl next) {
	prevSibbling = prev;
	nextSibbling = next;
	
	if(prev != null)
		prev.setNextSibbling(this);

	if(next != null)
		next.setPrevSibbling(this);
	
}
/**
 * setzt den Nachfolger-Knoten
 * Creation date: (05.03.01 10:32:41)
 * 
 * @param next de.lvm.basis.xml.NodeImpl
 */
void setNextSibbling(NodeImpl next) {
	nextSibbling = next;	
}
/**
 * setzt den Namen f�r diesen Knoten.
 * Creation date: (06.03.01 10:05:58)
 * 
 * @param arg java.lang.String
 */
void setNodeName(String arg) {
	name = arg;	
}
/**
 * setzt den Inhalt f�r diesen Knoten
 * Creation date: (06.03.01 10:06:10)
 * 
 * @param arg java.lang.String
 */
void setNodeValue(String arg) {
	value = arg;
}
/**
 * setzt den Parentknoten f�r diesen Node.
 * Creation date: (05.03.01 10:25:13)
 * 
 * @param parent de.lvm.basis.xml.NodeImpl
 */
void setParent(NodeImpl parent) {
	this.parentNode = parent;	
		
}
/**
 * setzt den Vorg�nger-Knoten
 * Creation date: (05.03.01 10:32:41)
 * 
 * @param prev de.lvm.basis.xml.NodeImpl
 */
void setPrevSibbling(NodeImpl prev) {
	prevSibbling = prev;
}
}
