package de.vahrst.application.xml;

/**
 * Repräsentiert einen Textknoten innerhalb eines XML-Elements. Die Textinhalt dieses Elements wird
 * im Value-Feld der Oberklasse gespeichert.
 * Creation date: (05.03.01 14:29:34)
 * @author: Vahrst, Thomas
 * @version: 1.1.2 
 */
class TextImpl extends NodeImpl implements Text {
/**
 * TextImpl constructor comment.
 */
public TextImpl(String data) {
	super();
	setNodeValue(data);
}
/**
 * liefert den Inhalt dieses Text-Elements
 * Creation date: (05.03.01 14:29:34)
 * 
 * @return java.lang.String
 */
public String getData() {
	return getNodeValue();
}
/**
 * Liefert den Typ dieses Knoten. Die Typen sind als symbolische Konstanten
 * in diesem Interface definiert.
 * Creation date: (05.03.01 14:29:34)
 * 
 * @return int
 */
public int getNodeType() {
	return TEXT_NODE;
}
}
