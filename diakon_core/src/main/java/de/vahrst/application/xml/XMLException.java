package de.vahrst.application.xml;

/**
 * Exceptionklasse f�r die XML-Implemtierung dieses Packages. Diese
 * Klasse erbt von java.lang.RuntimeException, damit nicht alle
 * Methoden der XML-Klassen in einen try-catch Block eingesetzt werden
 * m�ssen. (Siehe auch org.w3c.dom, die machen das auch so).
 * Creation date: (05.03.01 10:42:16)
 * @author: Vahrst, Thomas
 * @version: 1.1.0 
 */
public class XMLException extends RuntimeException {
/**
 * XMLException constructor comment.
 */
public XMLException() {
	super();
}
/**
 * XMLException constructor comment.
 * @param s java.lang.String
 */
public XMLException(String s) {
	super(s);
}
}
