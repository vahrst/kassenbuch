package de.vahrst.application.xml;
import java.io.*;
/**
 * Service-Klasse, die ein vorgegebenes XML-Dokument auf einem Ausgabestream oder in einen String
 * ausgibt.
 * Creation date: (05.03.01 14:54:17)
 * @author: Vahrst, Thomas
 * @version: 1.1.4 (Vahrst, Thomas - 18.06.2002 10:52) 
 */
public class XMLWriter {
	/** Arbeitsvariable f�r normalize */
	private StringBuffer work = new StringBuffer(32);

	/** Ausgabe-Writer */
	private Writer out;

/**
 * XMLWriter constructor comment.
 */
public XMLWriter() {
	super();
}


/**
 * Liefert die L�nge des Datenstroms, wenn der vorgegebene Knoten geschrieben w�rde.
 * <p>
 * (18.06.02 10:36:42) Methode angelegt
 * @author Vahrst, Thomas
 * @return int
 * @param node de.lvm.basis.xml.Node
 */
public static int getLength(Node node) {
	XMLWriter xmlWriter = new XMLWriter();

	try{
		ByteCounterWriter out = new ByteCounterWriter();

		xmlWriter.setWriter(out);
		xmlWriter.printNode(node);  // StringBuffer: WorkBuffer f�r normalize
		return out.getLength();
	}catch(Exception e){
	}

	return 0;
}


/** 
 * 'Normalisiert den vorgegebenen String, d.h. aus Sonderzeichen wie & oder >
 * werden &amp; und &gt;.
 */
protected String normalize(String s) {
	work.setLength(0);  // Stringbuffer l�schen

	int len = (s != null) ? s.length() : 0;
	for (int i = 0; i < len; i++) {
		char ch = s.charAt(i);
		switch (ch) {
			case '<' :
					work.append("&lt;");
					break;
			case '>' :
					work.append("&gt;");
					break;
			case '&' :
					work.append("&amp;");
					break;
			case '"' :
					work.append("&quot;");
					break;
					// else, default append char
			default :
					work.append(ch);
		}
	}

	return work.toString();
} // normalize(String):String


/**
 * liefert eine String-Darstellung des XML-Dokuments.
 * Creation date: (05.03.01 14:55:28)
 * 
 * @return java.lang.String
 * @param doc de.lvm.basis.xml.Node
 */
public static String print(Node node) {
	XMLWriter xmlWriter = new XMLWriter();

	try{
		StringWriter out = new StringWriter(64);

		xmlWriter.setWriter(out);
		xmlWriter.printNode(node);  // StringBuffer: WorkBuffer f�r normalize
		return out.toString();
	}catch(Exception e){
	}

	return null;

}


/**
 * gibt das vorgegebene XML-Dokument (doc) auf dem mitgegebenen Writer aus.
 * Creation date: (05.03.01 14:55:28)
 * 
 * @return java.lang.String
 * @param node de.lvm.basis.xml.Node
 */
public static void print(Node node, Writer writer) {
	XMLWriter xmlWriter = new XMLWriter();

	try{
		xmlWriter.setWriter(writer);
		xmlWriter.printNode(node);  
	}catch(Exception e){
	}

}


/**
 * Gibt den Inhalt des mitgegebenen XML-Elements formatiert auf
 * dem Ausgabestream aus.
 * Creation date: (04.10.00 12:57:27)
 * @param elem org.w3c.dom.Element
 */
private void printElement(Element elem) throws IOException {
	out.write("<");
	out.write(elem.getTagName());

	NamedNodeMap attrs = elem.getAttributes();
	for (int i = 0; i < attrs.getLength(); i++) {
		Attr attr = (Attr) attrs.item(i);
		out.write(" ");
		out.write(attr.getName());
		out.write("=\"");
		out.write(normalize(attr.getValue()));
		out.write("\"");
	}

	if (!elem.hasChildNodes()) {
		out.write("/>\n");
	} else {
		// Dieses Element enth�lt noch weitere Knoten. Wenn darunter ein Textknoten ist,
		// darf kein \n hinter dem Element stehen, weil dieses als Bestandteil des Textes
		// interpretiert w�rde.
		if (elem.containsTextNode())
			out.write(">");
		else
			out.write(">\n");
		// hier jetzt alle enthaltenen Nodes ausgeben.
		NodeList children = elem.getChildNodes();
		int len = children.getLength();
		for (int i = 0; i < len; i++) {
			printNode(children.item(i));
		}

		// und jetzt noch das EndeTag schreiben.
		out.write("</");
		out.write(elem.getTagName());
		out.write(">\n");
	}
}


/**
 * Gibt den vorgegebenen Knoten auf dem Writer aus. Die Methode l�uft
 * rekursiv ab.
 */
protected void printNode(Node node) throws IOException {

	// is there anything to do?
	if (node == null) {
		return;
	}
	int type = node.getNodeType();
	switch (type) {
		// print document
		case Node.DOCUMENT_NODE :
			out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			printNode(((Document) node).getDocumentElement());
			break;

		// print element with attributes
		case Node.ELEMENT_NODE :
			printElement((Element) node);
			break;

			// print text
		case Node.TEXT_NODE :
			out.write(normalize(node.getNodeValue()));
			break;

	}
} // print(Node)


/**
 * setzt das Writer-Objekt
 * Creation date: (05.03.01 15:05:38)
 * 
 * @param writer java.io.Writer
 */
protected void setWriter(Writer writer) {
	out = writer;	
	
}
}