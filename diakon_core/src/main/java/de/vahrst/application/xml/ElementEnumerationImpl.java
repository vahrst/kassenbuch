package de.vahrst.application.xml;

/**
 * Enumeration von XML-Elementen. 
 * Creation date: (19.03.01 12:37:35)
 * @author: Vahrst, Thomas
 * @version: <version>
 */
public class ElementEnumerationImpl implements ElementEnumeration {
	/** der erste Knotennt der verketteten Liste */
	private Node firstNode;
	
	/** Referenz auf das n�chste herauszugebende Element */
	private Node nextNode;

	/** Filter, wenn != null, werden nur Elemente ausgeben, die diesen Tagnamen besitzen */
	private String filter;
	
/**
 * ElementEnumerationImpl constructor comment.
 */
public ElementEnumerationImpl(Node firstNode, String filter) {
	super();
	this.firstNode = firstNode;
	this.filter = filter;
	this.nextNode = firstNode;
	findNextElement();  // wenn der erste Knoten kein passendes Element ist, wird auf das erste passende Element positioniert.
}
/**
 * sucht, beginnend beim aktuellen Knoten, nach einem Element-Knoten, bei dem
 * auch noch der vorgegebene Filter-String mit dem Tagnamen �bereinstimmt. Wenn der Filter-String
 * null ist, wird die �berpr�fung des Tagnamens nicht durchgef�hrt, d.h. jeder Knoten, der ein Element ist,
 * ist g�ltig.
 * 
 * Creation date: (19.03.01 12:47:55)
 * 
 */
private void findNextElement() {
	while(nextNode != null){		
		if(nextNode.getNodeType() == Node.ELEMENT_NODE){
			if(filter == null || filter.equals(nextNode.getNodeName()))
				return;  // dieser Knoten passt.
		}
		nextNode = nextNode.getNextSibling();
	}

	return;	
	
}
/**
 * gibt an, ob noch weitere Elemente in der Enumeration vorliegen.
 * Creation date: (19.03.01 12:37:35)
 * 
 * @return boolean
 */
public boolean hasMoreElements() {
	return (nextNode != null);
}
/**
 * liefert das n�chste Element aus der Enumeration oder null, wenn das Ende erreicht ist.
 * Creation date: (19.03.01 12:37:35)
 * 
 * @return de.lvm.basis.xml.Element
 */
public Element nextElement() {
	Element retNode = (Element) nextNode;
	
	nextNode = nextNode.getNextSibling(); // einen Knoten vorpositionieren
	findNextElement();  // auf das n�chste Element positionieren.
	
  return retNode;
}
/**
 * liefert die Anzahl Elemente in dieser Enumeration.
 * Creation date: (12.12.01 09:21:40)
 * @return int
 */
public int size() {
	// erstmal den aktuellen Zustand merken.
	Node save_nextNode = nextNode;

	int zaehler = 0;
	nextNode = firstNode;
	findNextElement();  // wenn der erste Knoten kein passendes Element ist, wird auf das erste passende Element positioniert.

	while(hasMoreElements()){
		nextElement();
		zaehler++;
	}	
	
	nextNode = save_nextNode;
	return zaehler;
}
}
