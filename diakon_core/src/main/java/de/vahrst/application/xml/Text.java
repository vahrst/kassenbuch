package de.vahrst.application.xml;

import java.io.*;
/**
 * Repräsentiert einen Textknoten innerhalb eines XML-Elements.
 * Creation date: (01.03.01 10:12:43)
 * @author: Vahrst, Thomas
 * @version: 1.1.1 
 */
public interface Text extends Node, Serializable{
/**
 * liefert den Inhalt dieses Text-Elements
 * Creation date: (01.03.01 10:49:19)
 * 
 * @return java.lang.String
 */
String getData();
}
