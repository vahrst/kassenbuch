package de.vahrst.application.xml;
import java.io.*;
/**
 * Repr�sentiert ein XML-Element. Dieses kann weitere Elemente und TextElemente sowie eine
 * Liste von Attributen enthalten. Der Tag-Name dieses Elements wird in der name-Variablen
 * aus der Oberklasse gespeichert.
 * Creation date: (05.03.01 11:15:45)
 * @author: Vahrst, Thomas
 * @version: 1.1.11 (Vahrst, Thomas - 15.07.2002 13:33) 
 */
public class ElementImpl extends NodeImpl implements Element, Serializable {


	/** erstes Attribut der verketteten Liste von Attributen */
	private AttributImpl firstAttribut = null;

	/** das letzte Attribut, hier wird angekettet */
	private AttributImpl lastAttribut = null;
	

	/** Anzahl Attribute in der verketteten Liste */
	private int attrCount = 0;

	/** Flag, das angibt, ob einer der ChildKnoten ein TextKnoten ist */
	private boolean containsText = false;

/**
 * ElementImpl constructor comment.
 */
public ElementImpl(String tagname) {
	super();
	setNodeName(tagname);
}


/**
 * ElementImpl constructor comment.
 */
public ElementImpl(String tagname, String text) {
	super();
	setNodeName(tagname);

	if(text != null)
		appendChild(new TextImpl(text));
}


/**
 * F�r Debug-Zwecke. Liefert einen String mit den Werten aller Attribute.
 * Creation date: (05.03.01 14:34:36)
 * 
 * @return java.lang.String
 */
public String _debugAttributeList() {
	StringBuffer sb = new StringBuffer();
	sb.append("Liste der Attribute: \n");
	Attr attr = firstAttribut;
	while(attr != null){
		sb.append(attr.getName()).append(" => ").append(attr.getValue()).append("\n");
		attr = (Attr) attr.getNextSibling();
	}
	return sb.toString();
}


/**
 * F�gt einen Child-Knoten hinzu. Hier wird nur gepr�ft, ob es sich bei dem neuen
 * Child-Knoten um einen Textknoten handelt und anschlie�end das entsprechende Flag
 * gesetzt. Alles weitere �bernimmt die Superklasse.
 * Creation date: (06.03.01 10:21:33)
 * 
 * @param child de.lvm.basis.xml.Node
 */
public void appendChild(Node child) {
	if(child.getNodeType() == TEXT_NODE)
		containsText = true;
		
	super.appendChild(child);	
}


/**
 * liefert true, wenn eines der Child-Knoten ein Text-Knoten ist.
 * Creation date: (06.03.01 10:20:19)
 * 
 * @return boolean
 */
public boolean containsTextNode() {
	return containsText;
}


/**
 * liefert den Inhalt des Attributs mit dem vorgegebenen Key, oder einen Leerstring (""), wenn ein
 * solches Attribut nicht existiert. (Entspricht der DOM-Implemtierung des IBM XML-Parsers)
 * Creation date: (05.03.01 14:13:40)
 * 
 * @return java.lang.String
 * @param key java.lang.String
 */
public java.lang.String getAttribute(java.lang.String key) {
	Attr attr = getAttributeNode(key);
	if(attr != null)
		return attr.getValue();
	else
		return "";
}


/**
 * liefert das Attribut mit dem vorgegebenen Key.
 * Creation date: (05.03.01 11:15:45)
 * 
 * @return de.lvm.basis.xml.Attr
 * @param key java.lang.String
 */
public Attr getAttributeNode(String key) {
		
	int hash = key.hashCode();

	Attr attr = firstAttribut;
	while(attr != null){
		if(attr.hashCode() == hash)
			if(attr.getName().equals(key))
				return attr;
		attr = (Attr) attr.getNextSibling();
	}
	return null;
}


/**
 * liefert eine Liste aller Attribute dieses Elements.
 * Creation date: (05.03.01 14:13:40)
 * 
 * @return de.lvm.basis.xml.NamedNodeMap
 */
public NamedNodeMap getAttributes() {
	return new NamedNodeMapImpl(firstAttribut, attrCount);
}


/**
 * Liefert eine ElementEnumeration mit allen Elementen, die den vorgegebenen Tag-Namen besitzen.Wenn
 * tagname == null ist, wird eine Enumeration mit allen Element-Knoten zur�ckgegeben.
 * Creation date: (19.03.01 12:29:15)
 * 
 * @return de.lvm.basis.xml.ElementEnumeration
 * @param tagname java.lang.String
 */
public ElementEnumeration getElementsByTagName(String tagname) {
	return new ElementEnumerationImpl(firstChild, tagname);
}


/**
 * liefert den ersten Child-Knoten vom Typ 'Element', oder null, falls keiner existiert.
 * <p>
 * (15.07.02 13:29:24) Methode angelegt
 * @author Vahrst, Thomas
 * @return de.lvm.basis.xml.Element
 */
public Element getFirstElement() {
	Node lauf = firstChild;
	while(lauf != null){
		if(lauf.getNodeType() == Node.ELEMENT_NODE )
			return (Element) lauf;

		lauf = lauf.getNextSibling();
	}
	
	return null;
}


/**
 * Liefert das (erste) Child.Element mit dem vorgegebenen Namen.
 * Creation date: (19.03.01 14:51:20)
 * 
 * @return de.lvm.basis.xml.Element
 * @param name java.lang.String
 */
public Element getFirstElementByName(java.lang.String name) {
	Node lauf = firstChild;
	while(lauf != null){
		if(lauf.getNodeType() == Node.ELEMENT_NODE && lauf.getNodeName().equals(name))
			return (Element) lauf;

		lauf = lauf.getNextSibling();
	}
	
	return null;
}


/**
 * Liefert den Typ dieses Knoten. Die Typen sind als symbolische Konstanten
 * in diesem Interface definiert.
 * Creation date: (05.03.01 11:15:45)
 * 
 * @return int
 */
public int getNodeType() {
	return ELEMENT_NODE;
}


/**
 * Liefert den Tag-Namen dieses Elements
 * Creation date: (05.03.01 11:15:45)
 * 
 * @return java.lang.String
 */
public String getTagName() {
	return getNodeName();
}


/**
 * liefert den Inhalt des (ersten) Text-Childknotens als String, wenn ein solcher existiert, sonst null.
 * Creation date: (20.03.01 07:12:41)
 * 
 * @return java.lang.String
 */
public java.lang.String getText() {
	if(!containsTextNode())
		return null;

	Node lauf = firstChild;
	while(lauf != null){
		if(lauf.getNodeType() == Node.TEXT_NODE)
			return lauf.getNodeValue();
		lauf = lauf.getNextSibling();
	}
	return null;
}


/**
 * liefert den Inhalt des (ersten) Text-Childknotens als String, wenn ein solcher existiert, sonst einen Leerstring.
 * Creation date: (20.03.01 07:12:41)
 * 
 * @return java.lang.String
 */
public java.lang.String getTextNotNull() {
	return (getText() == null)? "" : getText();
	
}


/**
 * F�gt diesem Element ein Attribut mit den vorgegebenen Key- und Value-Werten
 * hinzu.
 * Creation date: (05.03.01 11:15:45)
 * 
 * @param key java.lang.String
 * @param value java.lang.String
 */
public void setAttribute(String key, String value) {
	
	// wenn das Attribut bereits existiert, wird nur der Wert ausgetauscht.
	Attr existingAttr = getAttributeNode(key);
	if(existingAttr != null){
		existingAttr.setValue(value);
		return;
	}
	
	// jetzt ein neues Attr.-Objekt erzeugen.
	AttributImpl attr = new AttributImpl(key, value);
	
	// zuerst den neuen Childknoten in die Liste der schon vorhandenen Childknoten einf�gen
	attr.insertIntoQueue(lastAttribut, null);  // vorg�nger, nachfolger; hier also: hinten anh�ngen.
	lastAttribut = attr;
	
	// wenn dies der erste Child-Knoten ist, ist die
	// verkettete Liste bisher leer und wir m�ssen noch die Referenz auf den ersten Knoten der Kette setzen.
	if(firstAttribut == null)
		firstAttribut = attr;
			
	attrCount++;
	
	
}


/**
 * String-Darstellung dieses Objekts.
 * Creation date: (05.03.01 11:22:09)
 * 
 * @return java.lang.String
 */
public String toString() {
	return "Element: <" + getTagName() + ">";
}
}