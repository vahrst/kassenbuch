package de.vahrst.application.xml;

/**
 * Map f�r Attribute.
 * Creation date: (01.03.01 10:55:18)
 * @author: Vahrst, Thomas
 * @version: 1.1.2 
 */
public interface NamedNodeMap extends NodeList{
/**
 * liefert den Knoten mit dem vorgegebenen Namen.
 * Creation date: (05.03.01 10:00:54)
 * 
 * @return de.lvm.basis.xml.Node
 * @param name java.lang.String
 */
Node getNamedItem(String name);
}
