package de.vahrst.application.xml;

/**
 * Liste von Node-Objekten, auf die auch �ber den Namen zugegriffen werden kann. Diese Listen-Klasse ist
 * reserviert f�r Listen von Attributen.
 * Creation date: (05.03.01 14:03:06)
 * @author: Vahrst, Thomas
 * @version: 1.1.1 
 */
class NamedNodeMapImpl extends NodeListImpl implements NamedNodeMap {
/**
 * NamedNodeMapImpl constructor comment.
 * @param firstAttr de.lvm.basis.xml.Attr
 * @param count int
 */
public NamedNodeMapImpl(Attr firstAttr, int count) {
	super(firstAttr, count);
}
/**
 * liefert den Knoten mit dem vorgegebenen Namen.
 * Creation date: (05.03.01 14:03:06)
 * 
 * @return de.lvm.basis.xml.Node
 * @param name java.lang.String
 */
public Node getNamedItem(String name) {
	
	int hash = name.hashCode();

	Attr attr = (Attr) firstNode;
	while(attr != null){
		if(attr.hashCode() == hash)
			if(attr.getName().equals(name))
				return attr;
		attr = (Attr) attr.getNextSibling();
	}

	return null;
}
}
