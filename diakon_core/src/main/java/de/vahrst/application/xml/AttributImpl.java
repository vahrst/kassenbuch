package de.vahrst.application.xml;

import java.io.*;
/**
 * Kapselt ein Attribut eines Elements. Auch dieses Interface erbt von
 * Node, weil in diesem die Funktionalit�t der verketteten Listen implementiert wird.
 * Creation date: (05.03.01 11:47:20)
 * @author: Vahrst, Thomas
 * @version: 1.1.2 
 */
class AttributImpl extends NodeImpl implements Attr, Serializable {

	/** Hashcode dieses Attributs */
	private int hash = 0;
/**
 * AttributImpl constructor comment.
 */
public AttributImpl(String name, String value) {
	super();
	setNodeName(name);
	setNodeValue(value);
	this.hash = name.hashCode();
}
/**
 * liefert den Namen dieses Attributs
 * Creation date: (05.03.01 11:47:20)
 * 
 * @return java.lang.String
 */
public String getName() {
	return getNodeName();
}
/**
 * Liefert den Typ dieses Knoten. Die Typen sind als symbolische Konstanten
 * in diesem Interface definiert.
 * Creation date: (05.03.01 11:47:20)
 * 
 * @return int
 */
public int getNodeType() {
	return ATTRIBUTE_NODE;
}
/**
 * liefert den Wert dieses Attributs
 * Creation date: (05.03.01 11:47:20)
 * 
 * @return java.lang.String
 */
public String getValue() {
	return getNodeValue();
}
/**
 * liefert den Hashcode dieses Attributs. Dieser ist gleich dem Hashcode des Keys (Name) dieses
 * Attributs.
 * Creation date: (05.03.01 13:42:17)
 * 
 * @return int
 */
public int hashCode() {
	return hash;
}
/**
 * setzt den Wert f�r dieses Attribut
 * Creation date: (05.03.01 11:47:20)
 * 
 * @param arg java.lang.String
 */
public void setValue(String arg) {
	setNodeValue(arg);
}
/**
 * Insert the method's description here.
 * Creation date: (05.03.01 13:57:57)
 * 
 * @return java.lang.String
 */
public String toString() {
	return new StringBuffer("Attribute: ").append(getName()).append(" => ").append(getValue()).toString();
}
}
