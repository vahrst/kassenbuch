package de.vahrst.application.prozesse;

/**
 * 
 * @author Thomas
 * @version 
 */
public interface EndTransactionHandler {
	public void commit(AbstractProcess source);
	public void commitAndRestart(AbstractProcess source);
	public void rollback(AbstractProcess source);
}
