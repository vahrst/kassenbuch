package de.vahrst.application.prozesse;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
/**
 * Abstracte Oberklasse f�r alle Processe.
 * @author Thomas
 * @version 
 * @file AbstractProcess.java
 */
public abstract class AbstractProcess {
	protected Logger logger;
	protected Logger log = Logger.getLogger("application");

	protected AbstractProcess parentProcess;

	protected List<AbstractProcess> childProcesses = new ArrayList<AbstractProcess>();

	protected EndTransactionHandler eth;	
	/**
	 * Constructor for AbstractProcess.
	 */
	public AbstractProcess(AbstractProcess parent, EndTransactionHandler eth) {
		super();
		logger = Logger.getLogger(this.getClass());

		this.parentProcess = parent;
		if(parentProcess != null)
			parentProcess.childProcesses.add(this);

		this.eth = eth;
	}

	public abstract AbstractController getController();

	public void reactivate(){
		getController().reactivate();
	}
	
	public void commit(){
		if(eth != null){
			eth.commit(this);
		}
	}
	
	public void commitAndRollback(){
		if(eth != null){
			eth.commitAndRestart(this);
		}
	}
	
	public void rollback(){
		if(eth != null){
			eth.rollback(this);
		}
	}
	
	/**
	 * l�scht diesen Process.
	 */
	public void destroy(){
		logger.debug("destroy");
		getController().destroy();
		
		if(parentProcess != null){
			parentProcess.childProcesses.remove(this);
		}
		this.parentProcess = null;
		
	}	
	
	/**
	 * liefert den Parent-Process
	 */
	AbstractProcess getParent(){
		return parentProcess;
	}	
		
}
