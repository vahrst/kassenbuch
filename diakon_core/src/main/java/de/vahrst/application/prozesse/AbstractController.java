package de.vahrst.application.prozesse;

import java.awt.event.*;
import java.lang.reflect.*;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import org.apache.log4j.Logger;

import de.vahrst.application.global.ApplicationContext;
/**
 * 
 * @author Thomas
 * @version 
 * @file AbstractController.java
 */
public abstract class AbstractController implements ActionListener, FocusListener {
	protected Logger logger;

	private Map<String, GenericAction> actionMap = new HashMap<String, GenericAction>();

	private Map<String, Method> methodMap = new HashMap<String, Method>();
	
	protected Map<String, JComponent> aspektTabelle = new HashMap<String, JComponent>();


	/**
	 * Constructor for AbstractController.
	 */
	public AbstractController() {
		super();
		logger = Logger.getLogger(this.getClass());
	}

	public Method getMethodForActionname(String name)
		throws NoSuchMethodException {
		String actionname =
			name.substring(0, 1).toUpperCase() + name.substring(1);
		String methodname = "handle" + actionname + "Event";

		// haben wir bereits nach der Methode gesucht??
		if(methodMap.containsKey(methodname)){
			Method method = methodMap.get(methodname);
			if(method == null){
				throw new NoSuchMethodException();
			}else{
				return method;
			}		
		}else{
			// wir suchen zum ersten mal nach der Methode:			
			methodMap.put(methodname, null);  // initialisieren...
			
			// wir probieren erst die Methode mit einem Parameter, dann
			// die ohne Parameter:
			try{
				Method method = getClass().getMethod(methodname, new Class[]{Object.class});
				methodMap.put(methodname, method);
				return method;
			}catch(NoSuchMethodException e){}
			
			Method method = getClass().getMethod(methodname, new Class[0]);
			methodMap.put(methodname, method);
			return method;
		}

	}

	protected void createAction(String methodname, String id)
		throws NoSuchMethodException {
		GenericAction ga = new GenericAction(methodname, id, this);
		actionMap.put(id, ga);
	}

	public GenericAction getAction(String id) {
		GenericAction action = actionMap.get(id);
		
		// nicht gefunden? rekursiv in den Parent-Controllern suchen...
		if(action == null){
			AbstractController parentController = getParentController();
			if(parentController != null){
				action = parentController.getAction(id);
			}
		}
		return action;
	}

	/**
	 * initiale Start-Methode des Controllers. Hier sollten
	 * die Panels erzeugt und sichtbar gemacht werden
	 */
	public abstract void start();

	/**
	 * sorgt daf�r, dass die zugeh�rigen Panels wieder 
	 * sichtbar gemacht werden
	 */
	public abstract void reactivate();

	/**
	 * L�scht diesen Controller. Muss in den konkreten
	 * Subklassen reimplementiert werden und daf�r sorgen,
	 * dass alle Views geschlossen werden
	 */
	public abstract void destroy();

	/**
	 * Liefert das Process-Objekt. Muss in den konkreten
	 * Subklassen reimplementiert werden. Wird von der
	 * Default-Implementierung 'handleViewClosed' ben�tigt.
	 */
	public abstract AbstractProcess getProcess();

	public void actionPerformed(ActionEvent ae) {
		logger.debug("ActionEvent: " + ae);

		try {
			Method m = getMethodForActionname(ae.getActionCommand());
			if(m.getParameterTypes().length == 0)
				m.invoke(this, new Object[0]);
			else
				m.invoke(this, new Object[]{ae.getSource()});
				
		} catch (IllegalAccessException iae) {
			logger.error(
				"Methode f�r ActionEvent fehlt: " + ae.getActionCommand(),
				iae);
		} catch (InvocationTargetException ite) {
			logger.error(
				"Fehler bei handler-Methode f�r: " + ae.getActionCommand(),
				ite);
		} catch (NoSuchMethodException nme) {
			logger.error(
				"Methode f�r ActionEvent fehlt: " + ae.getActionCommand(),
				nme);
		}

	}

	/**
	 * Zeigt einen Fehlermeldungsdialog mit der vorgegeabenen
	 * Fehlermeldung an.
	 */
	public void showErrorMessage(String message) {
		JOptionPane.showConfirmDialog(
			ApplicationContext.getMainApplicationWindow(),
			message,
			"Fehler!",
			JOptionPane.DEFAULT_OPTION,
			JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Default-Implementierung f�r viewClosed
	 */
	public void handleViewClosedEvent(Object source) {
		getProcess().rollback();
	}


	/**
	 * liefert den Controller des parent-Prozesses, oder null,
	 * wenn kein Parent-Prozess existiert.
	 */
	public AbstractController getParentController(){
		AbstractProcess proc = getProcess();
		if(proc != null){
			AbstractProcess parentProc = proc.getParent();
			if(parentProc != null){
				return parentProc.getController();
			}
		}
		return null;
			
	}
	/**
	 * @see java.awt.event.FocusListener#focusGained(FocusEvent)
	 */
	public void focusGained(FocusEvent e) {
		String cmpName = e.getComponent().getName();
		if(cmpName == null){
			logger.warn("FocusGained f�r Componente ohne Namen: " + e.getComponent());
		}else{
			try{
				String actionCommand = cmpName + "FocusGained";
				Method m = getMethodForActionname(actionCommand);
				m.invoke(this, new Object[0]);
			}catch(Exception e1){
			}
		}
	}

	/**
	 * @see java.awt.event.FocusListener#focusLost(FocusEvent)
	 */
	public void focusLost(FocusEvent e) {
		if(e.isTemporary()) return;
		
		String cmpName = e.getComponent().getName();
		if(cmpName == null){
			logger.warn("FocusLost f�r Componente ohne Namen: " + e.getComponent());
		}else{
			try{
				String actionCommand = cmpName + "FocusLost";
				Method m = getMethodForActionname(actionCommand);
				m.invoke(this, new Object[0]);
			}catch(Exception e1){
			}
		}
	}

	/**
	 * setzt den Focus auf das Element mit dem vorgegebenen Aspekt
	 */
	protected void focusOnAspekt(String asp){
		JComponent c = aspektTabelle.get(asp);
		if(c != null){
			c.requestFocusInWindow();
		}
	}


}
