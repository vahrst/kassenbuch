package de.vahrst.application.prozesse;

import java.awt.event.ActionEvent;
import java.lang.reflect.*;

import javax.swing.*;

import org.apache.log4j.Logger;

import de.vahrst.application.global.ResourceFactory;

/**
 * Allgemeine Action-Klasse.
 * @author Thomas
 * @version 
 */
public class GenericAction extends AbstractAction {

	private Logger logger = null;

	/** die Id dieser Action */
	private String id = null;
	
	/**
	 * Die Handle-Methode des Controllers, die per Method Introspection
	 * beschafft wird und in der actionPerformed-methode aufgerufen wird.
	 */
	private Method handler = null;
	
	/**
	 * Der zu dieser Action gehörende Controller
	 */
	private AbstractController controller = null;

	/**
	 * Constructor for GenericAction.
	 * @param name
	 */
	public GenericAction(String methodName, String id, AbstractController controller) throws NoSuchMethodException {
		super();
		this.logger = Logger.getLogger(this.getClass());
		this.id = id;
		this.controller = controller;
		handler = controller.getMethodForActionname(methodName);
		ResourceFactory.setActionFields(this);
	}


	public void actionPerformed(ActionEvent e){
		try{
			handler.invoke(controller, new Object[0]);		
		}catch(IllegalAccessException iae){
			logger.fatal("Schwerer Fehler bei Action " + getName(), iae);
			
		}catch(InvocationTargetException ite){
			logger.error("Fehler bei Action " + getName(), ite);
			logger.error("Auslösende Exception", ite.getCause());
		}
	}

	public String getName(){
		return (String) getValue(Action.NAME);
	}

	public String getId(){
		return id;
	}
}
