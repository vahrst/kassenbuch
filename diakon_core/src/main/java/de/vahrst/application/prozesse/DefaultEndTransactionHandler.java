package de.vahrst.application.prozesse;

import org.apache.log4j.Logger;
/**
 * Default-Implementierung des EndTransactionhandler Interfaces.
 * Commit- und Rollback f�hren ein Destroy des Source-Modells aus.
 * @author Thomas
 * @version 
 * @file DefaultEndTransactionHandler.java
 */
public class DefaultEndTransactionHandler implements EndTransactionHandler {
	private Logger logger = Logger.getLogger(DefaultEndTransactionHandler.class);
	
	/**
	 * @see de.diakon.prozesse.EndTransactionHandler#commit(AbstractProcess)
	 */
	public void commit(AbstractProcess source) {
		logger.debug("commit");
		source.destroy();
	}

	/**
	 * @see de.diakon.prozesse.EndTransactionHandler#commitAndRestart(AbstractProcess)
	 */
	public void commitAndRestart(AbstractProcess source) {
		logger.debug("commitAndRestart");
	}

	/**
	 * @see de.diakon.prozesse.EndTransactionHandler#rollback(AbstractProcess)
	 */
	public void rollback(AbstractProcess source) {
		logger.debug("rollback");
		source.destroy();
	}

}
