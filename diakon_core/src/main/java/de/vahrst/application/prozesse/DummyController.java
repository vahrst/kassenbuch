/*
 * Created on 16.01.2004
 *
 */
package de.vahrst.application.prozesse;

/**
 * Dummy-Controller für Prozesse, die keine eigene Präsentationslogik
 * besitzen.
 * @author Thomas
 *
 */
public class DummyController extends AbstractController {
	private AbstractProcess process;

	public DummyController (AbstractProcess process){
		super();
		this.process = process;
	}

	/* (non-Javadoc)
	 * @see de.vahrst.application.prozesse.AbstractController#start()
	 */
	public void start() {

	}

	/* (non-Javadoc)
	 * @see de.vahrst.application.prozesse.AbstractController#reactivate()
	 */
	public void reactivate() {

	}

	/* (non-Javadoc)
	 * @see de.vahrst.application.prozesse.AbstractController#destroy()
	 */
	public void destroy() {

	}

	/* (non-Javadoc)
	 * @see de.vahrst.application.prozesse.AbstractController#getProcess()
	 */
	public AbstractProcess getProcess() {
		return process;
	}

}
