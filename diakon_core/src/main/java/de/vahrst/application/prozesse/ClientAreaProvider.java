package de.vahrst.application.prozesse;

import java.util.HashMap;
import java.util.Map;

import de.vahrst.application.gui.ClientAreaPanel;

/**
 * verwaltet in einer statischen Tabelle alle verf�gbaren
 * ClientAreas f�r diese Anwendung. Diese k�nnen per ID
 * abgefragt werden.
 * @author Thomas
 * @version 
 */
public class ClientAreaProvider {

//	/** Das Haupt-Applikationsfenster */
//	private static JFrame mainApplicationWindow = null;
	
	/** die interne Tabelle mit den ClientAreas */
	private static Map<String, ClientAreaPanel> clientAreaTabelle = new HashMap<String, ClientAreaPanel>();


	public static void registerClientArea(String id, ClientAreaPanel panel){
		clientAreaTabelle.put(id, panel);
	}

	public static ClientAreaPanel getClientArea(String id){
		return clientAreaTabelle.get(id);
	}

}
