/*
 * Created on 27.12.2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package de.vahrst.application.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.border.AbstractBorder;

/**
 * @author Thomas
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ShadowBorder extends AbstractBorder {
	
	private static int DEFAULT_THICKNESS = 2;

	/**
	 * Returns the insets of the border.
	 * @param c the component for which this border insets value applies
	 */
	public Insets getBorderInsets(Component c) {
		return new Insets(1, 1, 1+DEFAULT_THICKNESS, 1+DEFAULT_THICKNESS);
	}

	/** 
	 * Reinitialize the insets parameter with this Border's current Insets. 
	 * @param c the component for which this border insets value applies
	 * @param insets the object to be reinitialized
	 */
	public Insets getBorderInsets(Component c, Insets insets) {
		insets.left = insets.top = 1;
		insets.right = insets.bottom = 1+DEFAULT_THICKNESS;
		return insets;
	}



	/**
	  * Paints the border for the specified component with the specified
	  * position and size.
	  * @param c the component for which this border is being painted
	  * @param g the paint graphics
	  * @param x the x position of the painted border
	  * @param y the y position of the painted border
	  * @param width the width of the painted border
	  * @param height the height of the painted border
	  */
	public void paintBorder(Component c,Graphics g,	int x,	int y,	int width,	int height) {
		Color oldColor = g.getColor();

		g.translate(x, y);

		g.setColor(Color.gray);
		g.drawRect(0, 0, width-1-DEFAULT_THICKNESS, height-1-DEFAULT_THICKNESS);
		
		for(int i=0;i<DEFAULT_THICKNESS;i++){
			int t = DEFAULT_THICKNESS-i;
			g.drawLine(i+1,height-t, width,height-t);
			g.drawLine(width-t,i+1, width-t, height);
		}
		
//		g.drawLine(1,height-2, width,height-2);
//		g.drawLine(2,height-1, width,height-1);
		
//		g.drawLine(width-2,1, width-2, height);
//		g.drawLine(width-1,2, width-1, height);
//			

		g.translate(-x, -y);
		g.setColor(oldColor);
	}

	/**
	  * Returns whether or not the border is opaque.
	  */
	 public boolean isBorderOpaque() { 
		 return false; 
	 }
}
