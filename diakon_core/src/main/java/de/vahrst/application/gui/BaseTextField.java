package de.vahrst.application.gui;
import java.awt.*;
import java.awt.event.*;

import javax.swing.JTextField;
import javax.swing.text.Document;
/**
 * Text-EingabeFeld (Basisklasse).<p>
 * <p>
 * (18.01.02 15:05:44) Gaede: implementiert Interface ValidatableComponent zur Verhinderung von Laschenwechsel bei fehlerhafter Eingabe<br>
 * 07.01.02 Gaede, Anzeige eines Tooltips, falls der Text zu lang ist (nur im Ansichtsmodus).
 * 18.01.01 Gaede, Getter f�r aspectName - muss von erbenden Klassen �berschrieben werden<br>
 *                 UpdateEventEnabled: Property zum Unterdr�cken von UpdateEvents (z.B. in LVMTextFieldDate)
 *
 * 08.09.00 JAVA2: Interface LVMTableCellComponent entfernt
 * 27.05.00 Interface LVMTableCellComponent implementiert (Dr. Gaede, Frank).<br>
 * 23.05.00 Implementiert LVMUpdateableComponent (Dr. Gaede, Frank)
 */
public class BaseTextField extends JTextField implements  FocusListener , KeyListener{



	/** HilfeID f�r JavaHelp */
	private java.lang.String fieldHelpID = new String();


	/** Modell der Komponente */
	private TextFieldDocument document = null;
	

	/** validierter formatierter Inhalt des Textfelds (zu setzender Textinhalt) */
	protected String tmpValue = null;


	protected boolean showToolTip = false ;



/**
 * LVMTextField constructor comment.
 */
public BaseTextField() {
	this( null, null, 0 );
}


/**
 * LVMTextField constructor comment.
 * @param cols int
 */
public BaseTextField(int cols) {
	this(null, null, cols );
}


public BaseTextField(String text) {
	this(null, text, 0);
}


/**
 * LVMTextField constructor comment.
 * @param lab java.lang.String
 * @param cols int
 */
public BaseTextField(String lab, int cols) {
	this( null, lab, cols );
}


public BaseTextField(Document doc, String text, int columns) {
	super(doc, text, columns);
	initialize();	
}




/**
 * erzeugt das Document-Modell f�r das TextFeld
 * @return Document
 */
protected Document createDocument() {
	document = new TextFieldDocument();
	return document;
}


protected String delete(String text, char z) {
	StringBuffer neu = new StringBuffer("");
	for (int i = 0; i < text.length(); i++) {
		if (text.charAt(i) != z) {
			neu.append(text.charAt(i));
		}
	}
	return neu.toString();
}




public void focusGained(java.awt.event.FocusEvent fe) {
	// implementiert Standard-Windows-Verhalten: wenn das Textfeld den Fokus erh�lt ist der gesamte
	// Text selektiert
	selectAll();
	//select(0, getText().length()); // setCaretPosition(0);
}


public void focusLost(java.awt.event.FocusEvent fe) {

	// bei Inhalten die breiter als die Komponente sind, an den Anfang zur�ckscrollen
	setScrollOffset(0);
}




/**
 * Gets the helpID property (java.lang.String) value.
 * @return The helpID property value.
 * @see #setHelpID
 */
public java.lang.String getHelpID() {
	return fieldHelpID;
}


/**
 * Gets the maxLength property (int) value.
 * @return The maxLength property value.
 * @see #setMaxLength
 */
public int getMaxLength() {
	return document.getMaxLength();
}



/**
 * liefert die vordefinierte Gr��e der Komponente.
 * Ist eine �berschriebene Methode der Oberklasse und gibt immer
 * die Standardh�he (19 Pixel) zur�ck, damit Formulare im VCE sauber
 * positioniert werden k�nnen
 **/

public Dimension getPreferredSize() {
	return new Dimension(super.getPreferredSize().width, 19);
}





/**
 * Liefert einen ToolTipText zur�ck wenn der Text zu lang ist.
 * Creation date: (20.12.01 17:06:02)
 * @param: 
 * @return:
 */

public String getToolTipText(MouseEvent event) {

    if (showToolTip)
        return getText();
    else
        return null;

}


/**
 * Initialisierung der Komponente
 **/
protected void initialize() {
    // alte Umrandung merken

    // Modell des TextFeldes erzeugen
    document = (TextFieldDocument)createDocument();    
    setDocument( document );


    // Listener f�r die Validierung
    addFocusListener(this);
    
    // KeyListener f�r Enter und PfeilHoch/Runter
    addKeyListener(this);

}


/**
 * �berpr�fen der Korrektheit der Eingabe. Hier wird immer true zur�ckgegeben -
 * wird von Subklassen �bverschrieben (z.B. LVMTextFieldDate).
 * Creation date: (23.06.00 13:51:51)
 * @param:
 * @return:
 */
public boolean isValidEntry() {
	return document.isValidEntry();
}


/**
 * �berpr�fen der Korrektheit der Eingabe. Hier wird immer true zur�ckgegeben -
 * wird von Subklassen �bverschrieben (z.B. LVMTextFieldDate).
 * Creation date: (23.06.00 13:51:51)
 * @param  autoFormatText	boolean
 * @return boolean
 */
public boolean isValidEntry(boolean autoFormatText) {

	// autoFormatText gibt in den Unterklassen an, ob der
	// formatierte Feldinhalt gesetzt werden soll.

	// default
	return true;
}


/**
 * Jedesmal wenn wir gemalt werden, �berpr�fen wir ob der Text nicht zu lang ist
 * (denn dann zeigen wir einben ToolTip an) .
 * Creation date: (07.01.02 12:00:18)
 * @param: 
 * @return:
 */
protected void paintComponent(Graphics g) {

    super.paintComponent(g);

    // checken, ob der Text  zu lang ist und ein Tooltip  angezeigt werden soll

    int KORREKTUR = 5;

    String strValue = getText();

    Font font = getFont();
    int textWidth = getFontMetrics(font).stringWidth(strValue);
    int cellWidth = getSize().width;

    if ((textWidth + KORREKTUR) < cellWidth )
        setShowToolTip(false);
    else
        setShowToolTip(true);

}




/**
 * �berschriebene Methode der Oberklasse.
 * Wenn ein neues Model gesetzt wird, werden gleichzeitig neue Listener
 * hinzugef�gt
 * @param doc Document
 */
public void setDocument(Document doc) {

	if ( doc instanceof TextFieldDocument) {
		document = (TextFieldDocument)doc;
	}
	
	super.setDocument(doc);
}


/**
 * Sets the helpID property (java.lang.String) value.
 * @param helpID The new value for the property.
 * @see #getHelpID
 */
public void setHelpID(java.lang.String helpID) {
	String oldValue = fieldHelpID;
	fieldHelpID = helpID;
	firePropertyChange("helpID", oldValue, helpID);

	// setze CSH HelpID
	// javax.help.CSH.setHelpIDString(this, getHelpID());
}


/**
 * Sets the maxLength property (int) value.
 * @param maxLength The new value for the property.
 * @see #getMaxLength
 */
public void setMaxLength(int maxLength) {

	document.setMaxLength(maxLength);
}



/**
 * Wenn ein Tooltip angezeigt werden soll, dann muss die Komponente auch beim DefaultToolTipManager registriert werden.
 * Dies geschieht durch den Aufruf von setToolTipText(). Falls kein ToolTip angezeigt werden soll, m�ssen wir uns entsprechend wieder abmelden.
 * Creation date: (07.01.02 12:02:11)
 * @param: 
 * @return:
 */
public void setShowToolTip(boolean newShowToolTip) {

    //  wenn �nderung, dann Tooltip entsprechend (de)aktivieren
    if (showToolTip != newShowToolTip) {
        if (newShowToolTip)
            setToolTipText("");
        else
            setToolTipText(null);
    }

    showToolTip = newShowToolTip;

}



/**
 * setzt den Text der Komponente.
 * Erweitert die Funktionalit�t um das anzeigen der ersten Zeichen bei
 * Textinhalten die sonst nach hinten gescrollt w�rden.
 * <p>
 * @param arg java.lang.String 
 */
public void setText(String arg) {
    super.setText(arg);

    // sorgt daf�r, da� bei langen Texten immer
    // der Angang des Textes in der Komponente sichtbar ist
    setCaretPosition(0);

}


	/**
	 * @see java.awt.event.KeyListener#keyPressed(KeyEvent)
	 */
	public void keyPressed(KeyEvent e) {
	}

	/**
	 * @see java.awt.event.KeyListener#keyReleased(KeyEvent)
	 */
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_DOWN){
			if(e.getModifiers() == 0){
				transferFocus();
			}
		}

		if(e.getKeyCode() == KeyEvent.VK_UP && e.getModifiers() == 0){
			transferFocusBackward();
		}
		
	}

	/**
	 * @see java.awt.event.KeyListener#keyTyped(KeyEvent)
	 */
	public void keyTyped(KeyEvent e) {
	}

}