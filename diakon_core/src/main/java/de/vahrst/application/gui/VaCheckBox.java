package de.vahrst.application.gui;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
/**
 * #@Checkbox.java<p>
 *
 * Eine Checkbox (Kontrollk�stchen) wird benutzt, um zwei klar voneinander zu unterscheidende Zust�nde anzuzeigen. Checkboxen k�nnen in einer
 * Gruppe zusammengefa�t werden und so ein Multiple-Choice Field erzeugen.<br>
 *
 */
public class VaCheckBox extends JCheckBox implements KeyListener{

	/**
	 * inner Class: eigenes Icon f�r CheckBox
	 **/
	 class LVMCheckBoxIcon extends ImageIcon {

		VaCheckBox cb;

		public LVMCheckBoxIcon(VaCheckBox cb) {
			this.cb = cb;
		}

		public int getIconHeight() {
			return 18;
		}

		public int getIconWidth() {
			return 18;
		}

		public synchronized void paintIcon(Component c, Graphics g, int x, int y) {

			g.setColor(cb.isEnabled() ? Color.white : Color.lightGray);

			g.fillRect(x  , y, 18, 17);


			// BORDER malen
			if (cb.hasFocus()) {
				// [PENDING] das malen kann hier noch verbessert werden...
				g.setColor(ColorKonstanten.CHECKBOX_FOCUSCOLOR);
				g.drawRect(x,  y,  17,18);
				g.drawRect(x+1,y+1,15,16);
				g.setColor(cb.getForeground());
			}
			else
			{
				if (cb.isEnabled())
					BasicDrawUtil.drawFlush3DBorder(g,x, y,18,19);
				else
					BasicDrawUtil.drawDisabledBorder(g,x, y,18,18);
			}

			
			// ICON malen
			if (cb.isSelected())	{

				//System.out.println("y: "+y+"\tx: "+x);

			  Graphics2D g2 = (Graphics2D)g;
				// Gl�tten einschalten
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

				int[] xp = new int[] {  x+ 3, x+ 8,  x+17, x+ 8 };
				int[] yp = new int[] {  y+10, y+12,  y+ 2, y+17 };
				
				Shape s = new Polygon( xp, yp , xp.length);
				
				g.setColor( Color.lightGray );
				g2.fill(s);

				g2.setColor( Color.black );
				g2.translate( -1, -1 );				
				g2.fill(s);
					
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_OFF);
				g2.translate( 1, 1 );				

			}
		   
		}
	 }
	// zwischenspeichern der enabled-Variable

/**
 * LVMCheckBox constructor comment.
 */
public VaCheckBox() {
	super();
	initialize();
}


/**
 * LVMCheckBox constructor comment.
 * @param text java.lang.String
 */
public VaCheckBox(String text) {
	super(text);
	initialize();
}


/**
 * LVMCheckBox constructor comment.
 * @param text java.lang.String
 * @param icon javax.swing.Icon
 */
public VaCheckBox(String text, javax.swing.Icon icon) {
	super(text, icon);
	initialize();
}


/**
 * LVMCheckBox constructor comment.
 * @param text java.lang.String
 * @param icon javax.swing.Icon
 * @param selected boolean
 */
public VaCheckBox(String text, javax.swing.Icon icon, boolean selected) {
	super(text, icon, selected);
	initialize();
}


/**
 * LVMCheckBox constructor comment.
 * @param text java.lang.String
 * @param selected boolean
 */
public VaCheckBox(String text, boolean selected) {
	super(text, selected);
	initialize();
}


/**
 * LVMCheckBox constructor comment.
 * @param icon javax.swing.Icon
 */
public VaCheckBox(javax.swing.Icon icon) {
	super(icon);
	initialize();
}


/**
 * LVMCheckBox constructor comment.
 * @param icon javax.swing.Icon
 * @param selected boolean
 */
public VaCheckBox(javax.swing.Icon icon, boolean selected) {
	super(icon, selected);
	initialize();
}






/**
 * liefert die vordefinierte Gr��e der Komponente.
 * Ist eine �berschriebene Methode der Oberklasse und gibt immer
 * die Standardh�he (19 Pixel) zur�ck, damit Formulare im VCE sauber
 * positioniert werden k�nnen
 * @return Dimension
 */
public Dimension getPreferredSize() {
	int width = super.getPreferredSize().width;
	if (this.getText().length() != 0)
		return new Dimension(width, 19);
	return new Dimension (22,19);
}



protected void initialize() {

	// GUI-Einstellungen geh�ren mit in UI?
	setSelectedIcon(new LVMCheckBoxIcon(this));
	setIcon(new LVMCheckBoxIcon(this));

	setDisabledSelectedIcon(new LVMCheckBoxIcon(this));
	setDisabledIcon(new LVMCheckBoxIcon(this));

	setFont(new Font("SansSerif", Font.BOLD, 12));

	setAlignmentX(0.5f);
	setAlignmentY(0.5f);


	// Transparent
	setOpaque(false);

	// Focus als gr�nen Rand malen
	UIManager.put("CheckBox.focus",ColorKonstanten.CHECKBOX_FOCUSCOLOR);

	this.addKeyListener(this);

}

	/**
	 * @see java.awt.event.KeyListener#keyPressed(KeyEvent)
	 */
	public void keyPressed(KeyEvent e) {
	}

	/**
	 * @see java.awt.event.KeyListener#keyReleased(KeyEvent)
	 */
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_DOWN){
			if(e.getModifiers() == 0){
				transferFocus();
			}
		}

		if(e.getKeyCode() == KeyEvent.VK_UP && e.getModifiers() == 0){
			transferFocusBackward();
		}
		
	}

	/**
	 * @see java.awt.event.KeyListener#keyTyped(KeyEvent)
	 */
	public void keyTyped(KeyEvent e) {
	}


}