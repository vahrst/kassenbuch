package de.vahrst.application.gui;

import java.awt.event.*;

import javax.swing.*;

/**
 * Erweiterte Fassung eines Buttons. Dieser
 * reagiert auf Enter wie 'Blank' mit einem ActionEvent.
 * @author Thomas
 * @version 
 * @file Button.java
 */
public class VaButton extends JButton implements KeyListener{

	/**
	 * Constructor for Button.
	 */
	public VaButton() {
		super();
		initialize();
	}

	/**
	 * Constructor for Button.
	 * @param icon
	 */
	public VaButton(Icon icon) {
		super(icon);
		initialize();
	}

	/**
	 * Constructor for Button.
	 * @param text
	 */
	public VaButton(String text) {
		super(text);
		initialize();
	}

	/**
	 * Constructor for Button.
	 * @param a
	 */
	public VaButton(Action a) {
		super(a);
		initialize();
	}

	/**
	 * Constructor for Button.
	 * @param text
	 * @param icon
	 */
	public VaButton(String text, Icon icon) {
		super(text, icon);
		initialize();
	}

	private void initialize(){
		this.addKeyListener(this);		
	}	

	/**
	 * @see java.awt.event.KeyListener#keyPressed(KeyEvent)
	 */
	public void keyPressed(KeyEvent e) {
	}

	/**
	 * @see java.awt.event.KeyListener#keyReleased(KeyEvent)
	 */
	public void keyReleased(KeyEvent e) {
		if(e.getModifiers() == 0){
			if(e.getKeyCode() == KeyEvent.VK_ENTER){
				e.consume();
				doClick();
				return;
			}
		
			if(e.getKeyCode() == KeyEvent.VK_RIGHT ||
			   e.getKeyCode() == KeyEvent.VK_DOWN){
				e.consume();
				transferFocus();
				
				return;
			}

			if(e.getKeyCode() == KeyEvent.VK_LEFT ||
			   e.getKeyCode() == KeyEvent.VK_UP){
				e.consume();
				transferFocusBackward();
				return;
			}
		}
	}

	/**
	 * @see java.awt.event.KeyListener#keyTyped(KeyEvent)
	 */
	public void keyTyped(KeyEvent e) {
	}

}
