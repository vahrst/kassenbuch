package de.vahrst.application.gui;



/**
 * 
 * @author Thomas
 * @version 
 * @file YesNoComboBox.java
 */
public class YesNoComboBox extends VaComboBox {

	private static Object[] items = {"Ja", "Nein"};
	
	/**
	 * Constructor for YesNoComboBox.
	 * @param items
	 */
	public YesNoComboBox() {
		super(items);
		setSelectedIndex(0);
	}

	public YesNoComboBox(boolean state){
		super(items);
		setState(state);
	}

	public void setState(boolean state){
		setSelectedIndex(state? 0:1);
	}
	
	public boolean getState(){
		return getSelectedIndex() == 0;
	}
	
}
