package de.vahrst.application.gui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.SwingConstants;
/**
 * Service-Klasse f�r Standard MalRoutinen.<p>
 * @author: Ron Kastner
 */
public class BasicDrawUtil {


/**
 * LVMBasicDrawUtil constructor comment.
 */
public BasicDrawUtil() {
	super();
}
public static void drawDashedRect(
	Graphics g,
	int x,
	int y,
	int width,
	int height) {
	int vx, vy;

	// draw upper and lower horizontal dashes
	for (vx = x; vx < (x + width); vx += 2) {
		g.drawLine(vx, y, vx, y);
		g.drawLine(vx, y + height - 1, vx, y + height - 1);
	}

	// draw left and right vertical dashes
	for (vy = y; vy < (y + height); vy += 2) {
		g.drawLine(x, vy, x, vy);
		g.drawLine(x + width - 1, vy, x + width - 1, vy);
	}
}
/**
 * zeichnet den Rahmen einer ausgeschalteten Komponente
 **/
public static void drawDisabledBorder(Graphics g, int x, int y, int w, int h) {

	Color ControlDarkShadow = new Color(102, 102, 102);

	g.translate(x, y);
	g.setColor(ControlDarkShadow);
	g.drawRect(0, 0, w - 1, h - 1);
	g.translate(-x, -y);
}
/**
 * zeichnet einen Rahmen (identisch mit Rahmen aus dem Metal-L&F)
 */
public static void drawFlush3DBorder(Graphics g, int x, int y, int w, int h) {

	Color ControlDarkShadow = new Color(102, 102, 102);
	Color ControlHighLight = new Color(204, 204, 204);

	g.translate(x, y);
	g.setColor(ControlDarkShadow);
	g.drawRect(0, 0, w - 2, h - 2);
	g.setColor(Color.white);
	g.drawRect(1, 1, w - 2, h - 2);
	g.setColor(ControlHighLight);
	g.drawLine(0, h - 1, 1, h - 2);
	g.drawLine(w - 1, 0, w - 2, 1);
	g.translate(-x, -y);
}
/** Draw a string with the graphics g at location (x,y) just like g.drawString() would.
 *  The first occurence of underlineChar in text will be underlined. The matching is
 *  not case sensitive.
 */
public static void drawString(
	Graphics g,
	String text,
	int underlinedChar,
	int x,
	int y) {

	char b[] = new char[1];
	String s;
	char lc, uc;
	int index = -1, lci, uci;

	if (underlinedChar != '\0') {
		b[0] = (char) underlinedChar;
		s = new String(b).toUpperCase();
		uc = s.charAt(0);

		s = new String(b).toLowerCase();
		lc = s.charAt(0);

		uci = text.indexOf(uc);
		lci = text.indexOf(lc);

		if (uci == -1)
			index = lci;
		else
			if (lci == -1)
				index = uci;
			else
				index = (lci < uci) ? lci : uci;
	}

	g.drawString(text, x, y);
	if (index != -1) {
		FontMetrics fm = g.getFontMetrics();
		Rectangle underlineRect = new Rectangle();
		underlineRect.x = x + fm.stringWidth(text.substring(0, index));
		underlineRect.y = y;
		underlineRect.width = fm.charWidth(text.charAt(index));
		underlineRect.height = 1;
		g.fillRect(
			underlineRect.x,
			underlineRect.y + fm.getDescent() - 1,
			underlineRect.width,
			underlineRect.height);
	}
}
public static void paintArrow(
	Graphics g,
	int x,
	int y,
	int width,
	int height,
	Color c,
	int direction) {

	int xPos[] = new int[3];
	int yPos[] = new int[3];

	switch (direction) {

		case SwingConstants.BOTTOM :
			xPos[0] = x;
			yPos[0] = y;
			xPos[1] = x + width;
			yPos[1] = y;
			xPos[2] = x + width / 2;
			yPos[2] = y + height;
			break;

		case SwingConstants.TOP :
			xPos[0] = x;
			yPos[0] = y + height;
			xPos[1] = x + width;
			yPos[1] = y + height;
			xPos[2] = x + width / 2;
			yPos[2] = y;
			break;

		case SwingConstants.LEFT :
			xPos[0] = x;
			yPos[0] = y + height / 2;
			xPos[1] = x + width;
			yPos[1] = y + height;
			xPos[2] = x + width;
			yPos[2] = y;
			break;

		case SwingConstants.RIGHT :
			xPos[0] = x;
			yPos[0] = y;
			xPos[1] = x;
			yPos[1] = y + height;
			xPos[2] = x + width;
			yPos[2] = y + height / 2;
			break;

		default :
			xPos[0] = x;
			yPos[0] = y + height;
			xPos[1] = x + width;
			yPos[1] = y + height / 2;
			xPos[2] = x + 0;
			yPos[2] = y + height;

	}

	/*	if (direction == SwingConstants.BOTTOM)
		{
		}
		if (direction == SwingConstants.TOP)
		{
			xPos[0] = x;
			yPos[0] = y+height;
			xPos[1] = x + width;
			yPos[1] = y+height;
			xPos[2] = x+width/2;
			yPos[2] = y;
		}
		else // RIGHT
		{

		}*/

	g.setColor(c);
	g.fillPolygon(xPos, yPos, 3);

}
public static void paintButtonPanelBackGround(
	Graphics g,
	int startX,
	int startY,
	int width,
	int height,
	Color backColor,
	Color lineColor) {
//	int patternWidth = 5;
//	int patternHeight = 3;

	g.setColor(backColor);
	g.fillRect(startX, startY, width, height);

	g.setColor(lineColor);

	for (int i = 0; i < height - 1; i++) {
		i += 1;
		g.fillRect(startX, startY + i, width, 1);

	}

	// unteren Schatten malen
	/*
	g.setColor(new Color(51, 65, 52)); // Dunkelste stelle
	g.fillRect(startX, startY + height - 1, width, 1);

	g.setColor(new Color(87, 92, 111)); // 2 Pixel etwas heller
	g.fillRect(startX, startY + height - 3, width, 2);
	g.setColor(new Color(87, 92, 111)); //
	g.fillRect(startX, startY + height - 4, width, 1);
	g.setColor(new Color(90, 110, 111)); // Misch schwarz & Hintergrund
	g.fillRect(startX, startY + height - 5, width, 1);
	*/
	// obere wei�e Linie malen
	//g.setColor(Color.white);
  //g.fillRect(startX, startY, width, 1);
	//g.setColor(new Color(145, 162, 178)); // wei�-blau �bergang
	//g.fillRect(startX, startY + 1, width, 1);

}
/**
 * malt den Focus einer Komponente (als gestricheltes Rechteck)
 */
public static void paintFocus(Graphics g, Rectangle dim) {

	paintFocus( g , dim, Color.black );
}
public static void paintFocus(Graphics g, Rectangle dim, Color c ) {

	g.setColor( c );
	
	BasicDrawUtil.drawDashedRect(
		g,
		dim.x - 1,
		dim.y - 1,
		dim.width + 2,
		dim.height + 2);	
}
/*
 * malt die Hintergrundstreifen (z,b. beim LVM NavigationTree)
 */
public static void paintStripe(
	Graphics g,
	int startX,
	int startY,
	int width,
	int height,
	Color backColor,
	Color lineColor) {
//	int patternWidth = 5;
//	int patternHeight = 3;

	g.setColor(backColor);
	g.fillRect(startX, startY, width, height);

	g.setColor(lineColor);

	for (int i = 0; i < height - 1; i++) {
		i += 2;
		g.fillRect(startX, startY + i, width, 1);

	}
}
}
