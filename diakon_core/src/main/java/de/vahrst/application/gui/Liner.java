package de.vahrst.application.gui;

import java.awt.*;
/**
 * Klasse zum Positionieren von Linien
 */
public class Liner extends javax.swing.JComponent {

	private int fieldLineThickness = 0;

	private boolean fieldPaintVerticalLine = false;
	private boolean fieldPaintHorizontalLine = true;
	private Insets fieldVerticalLineInsets = new Insets(0, 0, 0, 0);
	private Insets fieldHorizontalLineInsets = new Insets(0, 0, 0, 0);

/**
 * LVMLiner constructor comment.
 */
public Liner() {
	super();

	setSize(30, 10);
	setLineThickness(1);
	setForeground(ColorKonstanten.COLOR_LINE);
}
/**
 * Gets the horizontalLineInsets property (java.awt.Insets) value.
 * @return The horizontalLineInsets property value.
 * @see #setHorizontalLineInsets
 */
public Insets getHorizontalLineInsets() {
	return fieldHorizontalLineInsets;
}
/**
 * Gets the lineThickness property (int) value.
 * @return The lineThickness property value.
 * @see #setLineThickness
 */
public int getLineThickness() {
	return fieldLineThickness;
}
/**
 * Gets the paintHorizontalLine property (boolean) value.
 * @return The paintHorizontalLine property value.
 * @see #setPaintHorizontalLine
 */
public boolean getPaintHorizontalLine() {
	return fieldPaintHorizontalLine;
}
/**
 * Gets the paintVerticalLine property (boolean) value.
 * @return The paintVerticalLine property value.
 * @see #setPaintVerticalLine
 */
public boolean getPaintVerticalLine() {
	return fieldPaintVerticalLine;
}
/**
 * Gets the verticalLineInsets property (java.awt.Insets) value.
 * @return The verticalLineInsets property value.
 * @see #setVerticalLineInsets
 */
public Insets getVerticalLineInsets() {
	return fieldVerticalLineInsets;
}
public void paintComponent(Graphics g) {
	super.paintComponent(g);

	if (getPaintHorizontalLine())
		g.fillRect(
			getHorizontalLineInsets().left,
			(getHeight() - getLineThickness()) / 2
				+ getHorizontalLineInsets().top
				- getHorizontalLineInsets().bottom,
			getWidth() - getHorizontalLineInsets().left - getHorizontalLineInsets().right,
			getLineThickness());

	if (getPaintVerticalLine())
		g.fillRect(
			(getWidth() - getLineThickness()) / 2
				+ getVerticalLineInsets().left
				- getVerticalLineInsets().right,
			getVerticalLineInsets().top,
			getLineThickness(),
			getHeight() - getVerticalLineInsets().top - getVerticalLineInsets().bottom);

}
/**
 * Sets the horizontalLineInsets property (java.awt.Insets) value.
 * @param horizontalLineInsets The new value for the property.
 * @see #getHorizontalLineInsets
 */
public void setHorizontalLineInsets(Insets horizontalLineInsets) {
	Insets oldValue = fieldHorizontalLineInsets;
	fieldHorizontalLineInsets = horizontalLineInsets;
	firePropertyChange("horizontalLineInsets", oldValue, horizontalLineInsets);
}
/**
 * Sets the lineThickness property (int) value.
 * @param lineThickness The new value for the property.
 * @see #getLineThickness
 */
public void setLineThickness(int lineThickness) {
	int oldValue = fieldLineThickness;
	fieldLineThickness = lineThickness;
	firePropertyChange(
		"lineThickness",
		new Integer(oldValue),
		new Integer(lineThickness));
}
/**
 * Sets the paintHorizontalLine property (boolean) value.
 * @param paintHorizontalLine The new value for the property.
 * @see #getPaintHorizontalLine
 */
public void setPaintHorizontalLine(boolean paintHorizontalLine) {
	boolean oldValue = fieldPaintHorizontalLine;
	fieldPaintHorizontalLine = paintHorizontalLine;
	firePropertyChange(
		"paintHorizontalLine",
		new Boolean(oldValue),
		new Boolean(paintHorizontalLine));
}
/**
 * Sets the paintVerticalLine property (boolean) value.
 * @param paintVerticalLine The new value for the property.
 * @see #getPaintVerticalLine
 */
public void setPaintVerticalLine(boolean paintVerticalLine) {
	boolean oldValue = fieldPaintVerticalLine;
	fieldPaintVerticalLine = paintVerticalLine;
	firePropertyChange(
		"paintVerticalLine",
		new Boolean(oldValue),
		new Boolean(paintVerticalLine));
}
/**
 * Sets the verticalLineInsets property (java.awt.Insets) value.
 * @param verticalLineInsets The new value for the property.
 * @see #getVerticalLineInsets
 */
public void setVerticalLineInsets(Insets verticalLineInsets) {
	Insets oldValue = fieldVerticalLineInsets;
	fieldVerticalLineInsets = verticalLineInsets;
	firePropertyChange("verticalLineInsets", oldValue, verticalLineInsets);
}
}
