package de.vahrst.application.gui;

import java.awt.Dimension;

import javax.swing.JLabel;

/**
 * 
 * @author Thomas
 * @version 
 */
public class Strut extends JLabel {

	/**
	 * Constructor for Strut.
	 */
	public Strut() {
		super();
		this.setOpaque(false);
		this.setText("");
		this.setMinimumSize(new Dimension(0,0));
		this.setFocusable(false);
	}

	
}
