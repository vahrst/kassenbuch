package de.vahrst.application.gui;

/**
 * Beschreibung der Instanzverantwortlichkeiten...
 * <p>
 * (optional) Beschreibung der Klassenverantwortlichkeiten...
 * <p>
 * (07.03.01 14:00:04) Type angelegt
 * @author H�nteler, Hartmut
 */
public class TextFieldNumericNotEmpty extends TextFieldNumeric {
/**
 * LVMTextFieldNumericNotNull constructor comment.
 */
public TextFieldNumericNotEmpty() {
	super();
}
/**
 * LVMTextFieldNumericNotNull constructor comment.
 * @param cols int
 */
public TextFieldNumericNotEmpty(int cols) {
	super(cols);
}
/**
 * LVMTextFieldNumericNotNull constructor comment.
 * @param text java.lang.String
 */
public TextFieldNumericNotEmpty(String text) {
	super(text);
}
/**
 * LVMTextFieldNumericNotNull constructor comment.
 * @param lab java.lang.String
 * @param cols int
 */
public TextFieldNumericNotEmpty(String lab, int cols) {
	super(lab, cols);
}
/**
 * LVMTextFieldNumericNotNull constructor comment.
 * @param doc javax.swing.text.Document
 * @param text java.lang.String
 * @param columns int
 */
public TextFieldNumericNotEmpty(javax.swing.text.Document doc, String text, int columns) {
	super(doc, text, columns);
}
public void focusLost(java.awt.event.FocusEvent fe) 
{
	if (getText().length()==0)
		setText(americanize(formatText("0")));
	super.focusLost(fe);
}
protected void initialize() 
{
	super.initialize();
	setText(americanize(formatText("0")));
}
public void setText(String arg) 
{
	super.setText(arg);
}
}
