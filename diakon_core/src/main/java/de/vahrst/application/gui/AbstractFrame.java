package de.vahrst.application.gui;


import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

/**
 * Oberklasse f�r alle Frames in dieser Anwendung
 * @author Thomas
 * @version 
 * @file AbstractFrame.java
 */
public class AbstractFrame extends JFrame {
	List<ActionListener> actionListeners = new ArrayList<ActionListener>(1);

	/**
	 * Constructor for AbstractFrame.
	 * @throws HeadlessException
	 */
	public AbstractFrame(ActionListener controller) throws HeadlessException {
		super();
		initializeFrame(controller);
	}


	/**
	 * Constructor for AbstractFrame.
	 * @param title
	 * @throws HeadlessException
	 */
	public AbstractFrame(String title, ActionListener controller) throws HeadlessException {
		super(title);
		initializeFrame(controller);
	}


	public void addActionListener(ActionListener l){
		actionListeners.add(l);
	}
	
	public void fireActionEvent(ActionEvent ae){
		for(int i=0; i<actionListeners.size();i++)
			actionListeners.get(i).actionPerformed(ae);
	}
	

	private void initializeFrame(ActionListener controller){
		this.addActionListener(controller);
		
	
		this.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				if(getDefaultCloseOperation() != DO_NOTHING_ON_CLOSE){
					fireActionEvent(new ActionEvent(AbstractFrame.this, ActionEvent.ACTION_PERFORMED, "viewClosed"));
				}
			}
		});
	}	
	
	public void centerToScreen(){
		Insets ins = Toolkit.getDefaultToolkit().getScreenInsets(this.getGraphicsConfiguration());
		Dimension ownerSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension mySize = getSize();
		
		int x = (ownerSize.width - mySize.width - ins.left - ins.right )/2;
		int y = (ownerSize.height - mySize.height - ins.top - ins.bottom)/2;
		
		this.setLocation(x+ins.left,y+ins.top);
		
	}

}
