/*
 * Created on 25.10.2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package de.vahrst.application.gui;


/**
 * Textfeld, das eine numerische Eingabe erkennt und als
 * int-Wert zur�ckgeben kann. Speziell f�r das Eingabefeld
 * der Kontonummer entworfen.
 * @author Thomas
 */
public class KombiTextField extends BaseTextField {

	/**
	 * 
	 */
	public KombiTextField() {
		super();
	}

	/**
	 * @param cols
	 */
	public KombiTextField(int cols) {
		super(cols);
	}

	/**
	 * @param text
	 */
	public KombiTextField(String text) {
		super(text);
	}


	/**
	 * gibt an, ob der Inhalt des TextFields numerisch ist. Bei einem 
	 * leeren Textfield wird false zur�ckgegeben.
	 * @return boolean
	 */
	public boolean isNumeric(){
		try{
			Integer.parseInt(this.getText());
			return true;
		}catch(NumberFormatException e){
			return false;
		}
	}
	
	/**
	 * liefert den Inhalt dieses KombiTextFields als int-Wert. Wenn der 
	 * Inhalt nicht numerisch ist, wird eine NumberFormatException geworfen.
	 * @return int
	 * @throws NumberFormatException
	 */
	public int getIntegerValue()throws NumberFormatException{
		return Integer.parseInt(this.getText());
	}
	
	/**
	 * setzt den vorgegebenen Wert als Inputwert f�r dieses Textfeld
	 * @param arg
	 */
	public void setIntegerValue(int arg){
		this.setText(Integer.toString(arg));
	}
}
