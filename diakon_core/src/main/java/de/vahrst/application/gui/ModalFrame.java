package de.vahrst.application.gui;

import java.awt.*;

import javax.swing.JFrame;

/**
 * 
 * @author Thomas
 * @version 
 * @file ModalFrame.java
 */
public class ModalFrame extends JFrame {

	/**
	 * Constructor for ModalFrame.
	 * @throws HeadlessException
	 */
	public ModalFrame() throws HeadlessException {
		super();
	}

	/**
	 * Constructor for ModalFrame.
	 * @param gc
	 */
	public ModalFrame(GraphicsConfiguration gc) {
		super(gc);
	}

	/**
	 * Constructor for ModalFrame.
	 * @param title
	 * @throws HeadlessException
	 */
	public ModalFrame(String title) throws HeadlessException {
		super(title);
	}

	/**
	 * Constructor for ModalFrame.
	 * @param title
	 * @param gc
	 */
	public ModalFrame(String title, GraphicsConfiguration gc) {
		super(title, gc);
	}

}
