package de.vahrst.application.gui;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDialog;

import de.vahrst.application.prozesse.AbstractController;

/**
 * 
 * @author Thomas
 * @version 
 * @file KontenAnlageDialog.java
 */
public class AbstractDialog extends JDialog {
	private List<ActionListener> actionListeners = new ArrayList<ActionListener>(2);

	/**
	 * Constructor for KontenAnlageDialog.
	 * @param owner
	 * @param modal
	 * @throws HeadlessException
	 */
	public AbstractDialog(Frame owner, boolean modal, AbstractController controller)
		throws HeadlessException {
		super(owner, modal);
		initializeDialog(controller);
	}


	/**
	 * Constructor for KontenAnlageDialog.
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws HeadlessException
	 */
	public AbstractDialog(Frame owner, String title, boolean modal, AbstractController controller)
		throws HeadlessException {
		super(owner, title, modal);
		initializeDialog(controller);
	}

	/**
	 * Constructor for KontenAnlageDialog.
	 * @param owner
	 * @param modal
	 * @throws HeadlessException
	 */
	public AbstractDialog(Dialog owner, boolean modal, AbstractController controller)
		throws HeadlessException {
		super(owner, modal);
		initializeDialog(controller);
	}


	public void addActionListener(ActionListener l){
		actionListeners.add(l);
	}
	
	public void fireActionEvent(ActionEvent ae){
		for(int i=0; i<actionListeners.size();i++)
			actionListeners.get(i).actionPerformed(ae);
	}
	
	/** 
	 * feuert ActionEvent 'ViewClosed'
	 */
	public void closeView(){
		fireActionEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "viewClosed"));
	}

	private void initializeDialog(ActionListener controller){
		this.addActionListener(controller);
		
	
		this.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				if(getDefaultCloseOperation() != DO_NOTHING_ON_CLOSE){
					fireActionEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "viewClosed"));
				}
			}
		});
		
	}	
	
	protected void center(){
		Point ownerLoc = getOwner().getLocationOnScreen();
		Dimension ownerSize = getOwner().getSize();
		Dimension mySize = getSize();
		
		int x = (ownerSize.width - mySize.width)/2 + ownerLoc.x;
		int y = (ownerSize.height - mySize.height)/2 + ownerLoc.y;
		
		this.setLocation(x,y);
		
	}
}
