package de.vahrst.application.gui;

import java.math.BigDecimal;

import de.vahrst.application.types.Waehrungsbetrag;

/**
 * @author m500194
 */
public class TextFieldWaehrung extends TextFieldNumericNotEmpty {
	private BigDecimal faktor = new BigDecimal(100);

	public TextFieldWaehrung(Waehrungsbetrag betrag){
		this();
		setBetrag(betrag);
	}
	
	public TextFieldWaehrung(){
		super(10);
		
	}
	
	
	
	/**
	 * Returns the betrag.
	 * @return Waehrungsbetrag
	 */
	public Waehrungsbetrag getBetrag() {
		int cent  = 0;
		try{
		
		BigDecimal bd = new BigDecimal(getTextAsDouble().doubleValue());
		bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		bd = bd.multiply(faktor);
		
		cent = bd.intValue();
		
		return new Waehrungsbetrag(cent);
			
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
		return new Waehrungsbetrag(cent);
	}

	/**
	 * Sets the betrag.
	 * @param betrag The betrag to set
	 */
	public void setBetrag(Waehrungsbetrag betrag) {
		this.setFormattedText(betrag.format());
	}

}
