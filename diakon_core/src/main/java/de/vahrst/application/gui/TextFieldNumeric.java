package de.vahrst.application.gui;
/**
 * TextFeld f�r Numerische Eingaben
 * Creation date: (09.02.00 11:34:57)
 * <p>
 * 18.01.01 Gaede, Keine UpdateEvents ausl�sen, wenn nur der Focus wechselt<br>
 *
 * 14.08.00 Kastner: 'getTextAsFloat' eingef�gt.<br>
 * 27.05.00 Interface LVMTableCellComponent implementiert - neue Methode isValidEntry() (Dr. Gaede, Frank).<br>
 * @author: H�nteler, Hartmut
 */
import java.util.Locale;

import javax.swing.SwingConstants;
import javax.swing.text.Document;

public class TextFieldNumeric extends BaseTextField {

	/** Modell des numerischen TextFeldes */
	private TextFieldNumericDocument document;

	char decimalSep;
	char groupingSep;
	char minus;

	int vorkomma = 0, nachkomma = 0;

	// zum Ausgabe formatieren
	// Instanz holen
	// VORSICHT: nicht mit null initialisieren !!!
	java.text.DecimalFormat form;

	// String invalidText = "";

	/**
	 * LVMTextFieldNumeric constructor comment.
	 */
	public TextFieldNumeric() {
		super();
		// wird von this.initialize() aufegerufen
		//	initialize();
	}

	/**
	 * LVMTextFieldNumeric constructor comment.
	 * @param cols int
	 */
	public TextFieldNumeric(int cols) {
		super(cols);
	}

	/**
	 * LVMTextFieldNumeric constructor comment.
	 * @param text java.lang.String
	 */
	public TextFieldNumeric(String text) {
		super(text);
	}

	/**
	 * LVMTextFieldNumeric constructor comment.
	 * @param lab java.lang.String
	 * @param cols int
	 */
	public TextFieldNumeric(String lab, int cols) {
		super(lab, cols);
	}

	/**
	 * LVMTextFieldNumeric constructor comment.
	 * @param doc javax.swing.text.Document
	 * @param text java.lang.String
	 * @param columns int
	 */
	public TextFieldNumeric(
		javax.swing.text.Document doc,
		String text,
		int columns) {
		super(doc, text, columns);
	}

	// ----------------------------------------------------------
	// Problem: wenn ein String in ein Double o.�. umgewandelt
	// 			werden soll, dann mu� es im amerikanischen Stil
	//			geschen (Komma=Punkt)
	// ----------------------------------------------------------
	// den Vorkommabereich f�rs umwandeln vorbereiten
	// String in einen Double, nur mit amerik. Formatierung
	protected String americanize(String temp) {
		int pos = temp.indexOf(decimalSep);

		if (pos > -1) {
			String vorne = temp.substring(0, pos);
			while (vorne.substring(0, 0).equals("0"))
				vorne.substring(1);
			vorne = delete(vorne, groupingSep);
			vorkomma = vorne.length();
			nachkomma = temp.substring(pos + 1).length();
			return (vorne + "." + temp.substring(pos + 1));
		} else {
			String vorne = temp.substring(0);
			while (vorne.substring(0, 0).equals("0"))
				vorne.substring(1);
			vorne = delete(vorne, groupingSep);
			vorkomma = vorne.length();
			nachkomma = 0;
			return vorne;
		}
	}

	/**
	 * erzeugt das Document-Modell f�r das TextFeldNumeric
	 * @return Document
	 */
	protected Document createDocument() {
		document = new TextFieldNumericDocument();
		return document;
	}

	public void focusGained(java.awt.event.FocusEvent fe) {
		// wenn wir wieder in das Feld kommen, entfernen wir
		// alle Gruppierungszeichen
		if (isEnabled()) {
			// dabei keine UpdateEvents ausl�sen !

			String p = delete(getText(), groupingSep);

			int inputLen = getIntegerPart() + getFractionPart();
			if (getFractionPart() > 0)
				inputLen++;
			int zeroLen = getText().length();
			if (isCompleteWithZeroes() && zeroLen > 0 && zeroLen < inputLen) {
				p = formatText(p);
				p = delete(p, groupingSep);
			}
			// nur entfernen, wenn VorzeichenEingabe nicht erlaubt
			if (!isSigned())
				p = delete(p, '+');

			if (!getText().equals(p))
				setText(p);

			select(0, getText().length()); // setCaretPosition(0);

		}
	}

	/**
	 * �berpr�fen der Eingane - ruft isValidEntry().
	 * Creation date: (27.06.00 14:02:15)
	 */
	public void focusLost(java.awt.event.FocusEvent fe) {
		// �berpr�fen des Eingabewertes
		isValidEntry();
	}

	protected String formatText(String newText) {
		//	if (!getInvalidText().equals(""))
			//setInvalidText("");
		String zruck = "";
		if (newText.length() > 0
			|| (isCompleteWithZeroes() && newText.length() > 0)) {
			try {
				// min. und max. Anzahl Nachkommastellen setzen
				form.setMinimumFractionDigits(getFractionPart());
				form.setMaximumFractionDigits(getFractionPart());
				form.getDecimalFormatSymbols().setDecimalSeparator(decimalSep);
				form.getDecimalFormatSymbols().setGroupingSeparator(
					groupingSep);
				if (isCompleteWithZeroes()) // blankIsZero)
					form.setMinimumIntegerDigits(getIntegerPart());
				else
					form.setMinimumIntegerDigits(1);

				((TextFieldNumericDocument) getDocument()).setIgnoreInsertation(true);
				
				if (newText.length() > 0 || isCompleteWithZeroes()) {
					String puffer;

					if (newText.length() > 0) {
						// nur wenn etwas eingegeben wurde, ...
						puffer = form.format(Double.valueOf(newText));
					} else
						puffer = form.format(0);

					// positives Vorzeichen ausgeben
					if (isSignedOutput() && puffer.charAt(0) != '-')
						puffer = "+" + puffer;

					// Gruppierung wieder entfernen, wenn nicht gew�nscht
					if (!isIntegerGrouping())
						puffer = delete(puffer, groupingSep);

					if (vorkomma > getIntegerPart()
						|| nachkomma > getFractionPart()) {
						// Nachkommastellen werden abgeschnitten !!!
						// fehlerhafte Eingabe, piepen
						//setInvalidText(newText);
						return getText();
						// Ron: Fehlerhafte Angabe reinsetzen um Korrigieren zu k�nnen!
					} else {
						zruck = puffer;
					}
				} else {
					// ... sonst tun wir so, als ob es Null w�re
					zruck = form.format(0);
				}
				document.setIgnoreInsertation(false);
			} catch (NumberFormatException ne) {
//				setInvalidText(newText);
				return getText();
				// Ron: Fehlerhafte Angabe reinsetzen um Korrigieren zu k�nnen!
			}
		}
		return zruck;
	}

	/**
	 * Gets the decimalPart property (int) value.
	 * @return The decimalPart property value.
	 * @see #setDecimalPart
	 */
	public int getFractionPart() {
		return document.getFractionPart();
	}

	/**
	 * Gets the integerGrouping property (boolean) value.
	 * @return The integerGrouping property value.
	 * @see #setIntegerGrouping
	 */
	public boolean getIntegerGrouping() {
		return document.isIntegerGrouping();
	}

	/**
	 * Gets the integerPart property (int) value.
	 * @return The integerPart property value.
	 * @see #setIntegerPart
	 */
	public int getIntegerPart() {
		return document.getIntegerPart();
	}

	/**
	 * Gets the signed property (boolean) value.
	 * @return The signed property value.
	 * @see #setSigned
	 */
	public boolean getSigned() {
		return document.isSigned();
	}

	/**
	 * Gets the signedOutput property (boolean) value.
	 * @return The signedOutput property value.
	 * @see #setSignedOutput
	 */
	public boolean getSignedOutput() {
		return document.isSignedOutput();
	}

	/**
	 * Liefert den Wert als Float-Value zur�ck.<p>
	 * @return float
	 * @exception NumberFormatException
	 */
	public Double getTextAsDouble() throws NumberFormatException {
		String s = americanize(getText());
		return Double.valueOf(s);
	}

	/**
	 * Liefert den Wert als Float-Value zur�ck.<p>
	 * @return float
	 * @exception NumberFormatException
	 */
	public Float getTextAsFloat() throws NumberFormatException {
		String s = americanize(getText());
		return Float.valueOf(s);
	}

	protected void initialize() {
		super.initialize();

		// die lokalisierten Zeichen f�r Komma, Punkt und Minus
		form = (java.text.DecimalFormat) java.text.NumberFormat.getInstance();
		decimalSep = form.getDecimalFormatSymbols().getDecimalSeparator();
		groupingSep = form.getDecimalFormatSymbols().getGroupingSeparator();
		minus = form.getDecimalFormatSymbols().getMinusSign();

		// der groupingSep als Eingabem�glichkeit, kann nur zu mehr Fehlern f�hren
		//	((LVMTextFieldNumericDocument)getDocument()).addAllowed(getDecimalPart()>-1, "" + groupingSep);

		// initialisierung der Default Vor- und Nachkommastellen
		setIntegerPart(4);
		setFractionPart(2);
		setIntegerGrouping(true);

		setHorizontalAlignment(SwingConstants.RIGHT);
	}

	/**
	 * @author: H�nteler, Hartmut
	 * @return boolean
	 */
	public boolean isCompleteWithZeroes() {
		return document.isCompleteWithZeroes();
	}

	/**
	 * Gets the integerGrouping property (boolean) value.
	 * @return The integerGrouping property value.
	 * @see #setIntegerGrouping
	 */
	public boolean isIntegerGrouping() {
		return document.isIntegerGrouping();
	}

	/**
	 * Gets the signed property (boolean) value.
	 * @return The signed property value.
	 * @see #setSigned
	 */
	public boolean isSigned() {
		return document.isSigned();
	}

	/**
	 * Gets the signedOutput property (boolean) value.
	 * @return The signedOutput property value.
	 * @see #setSignedOutput
	 */
	public boolean isSignedOutput() {
		return document.isSignedOutput();
	}

	/**
	 * Testen ob ein g�ltiger Eintrag vorliegt - falls nicht: Fehlerbehandlung...
	 * Creation date: (26.06.00 11:44:08)
	 * @param:
	 * @return:
	 */
	public boolean isValidEntry() {

		int len = getMaxLength();
		String puffer = formatText(americanize(getText()));
		boolean b = document.isIgnoreInsertation();
		document.setIgnoreInsertation(true);
		document.setMaxLength(puffer.length());

		// Text nur setzten wenn er ver�ndert wurde...
		if (!puffer.equals(getText()))
			setText(puffer);

		document.setIgnoreInsertation(b);
		document.setMaxLength(len);

		//
		return !b;

	}

	/**
	 * F�lle ggfs. die Pflichtstellen der Eingabe mit Nullen auf.
	 * 
	 * <p>
	 * (13.08.01 12:58:35) Methode angelegt
	 * @author: H�nteler, Hartmut
	 * @param newCompleteWithZeroes boolean
	 * 			legt fest ob mit Nullen aufgefuellt werden soll
	 */
	public void setCompleteWithZeroes(boolean newCompleteWithZeroes) {

		document.setCompleteWithZeroes(newCompleteWithZeroes);

		setText(americanize(getText()));
		setText(formatText(getText()));
	}

	/**
	 * Setze ein neues Modell f�r Numerisches TextFeld.
	 * @return Document
	 */
	public void setDocument(Document doc) {

		if (doc instanceof TextFieldNumericDocument) {
			document = (TextFieldNumericDocument) doc;
		}

		super.setDocument(doc);

	}

	/**
	 * Setze den Text ohne R�cksicht auf
	 * die gesetzten Modell-Properties.
	 * Der �bergebene Parameter muss entsprechend
	 * den definierten Properties g�ltig sein, ansonsten
	 * wird der Inhalt des Feldes abgel�scht.
	 * <p>
	 * @param	arg	String
	 */
	public void setFormattedText(String arg) {

		int oldMaxLength = getMaxLength();
		boolean oldIgnoreInsertation = document.isIgnoreInsertation();

		document.setIgnoreInsertation(true);
		setMaxLength(arg.length());

		setText(arg);

		document.setIgnoreInsertation(oldIgnoreInsertation);
		setMaxLength(oldMaxLength);
	}

	/**
	 * Sets the decimalPart property (int) value.
	 * @param decimalPart The new value for the property.
	 * @see #getDecimalPart
	 */
	public void setFractionPart(int fractionPart) {

		document.setFractionPart(fractionPart);
		document.addAllowed(getFractionPart() > 0, "" + decimalSep);
	}

	/**
	 * Sets the integerGrouping property (boolean) value.
	 * @param integerGrouping The new value for the property.
	 * @see #getIntegerGrouping
	 */
	public void setIntegerGrouping(boolean integerGrouping) {

		document.setIntegerGrouping(integerGrouping);
		document.addAllowed(integerGrouping, "" + groupingSep);
	}

	/**
	 * Sets the integerPart property (int) value.
	 * @param integerPart The new value for the property.
	 * @see #getIntegerPart
	 */
	public void setIntegerPart(int integerPart) {

		document.setIntegerPart(integerPart);
	}

	public void setLocale(Locale l) {
		super.setLocale(l);
		form = (java.text.DecimalFormat) java.text.NumberFormat.getInstance(l);
		decimalSep = form.getDecimalFormatSymbols().getDecimalSeparator();
		groupingSep = form.getDecimalFormatSymbols().getGroupingSeparator();

		// um die richtigen Zeichen in das Document zu setzen	
		setIntegerPart(getIntegerPart());
		setFractionPart(getFractionPart());
	}

	/**
	 * Sets the signed property (boolean) value.
	 * @param signed The new value for the property.
	 * @see #getSigned
	 */
	public void setSigned(boolean signed) {

		document.setSigned(signed);
		document.addAllowed(isSigned(), "+" + minus);
	}

	/**
	 * Sets the signedOutput property (boolean) value.
	 * @param signedOutput The new value for the property.
	 * @see #getSignedOutput
	 */
	public void setSignedOutput(boolean signedOutput) {
		document.setSignedOutput(signedOutput);
	}



}