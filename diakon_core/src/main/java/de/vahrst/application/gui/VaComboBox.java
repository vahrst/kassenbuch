package de.vahrst.application.gui;

import java.awt.event.KeyEvent;
import java.util.Vector;

import javax.swing.*;

/**
 * 
 * @author Thomas
 * @version 
 * @file BaseComboBox.java
 */
public class VaComboBox extends JComboBox {
	private boolean popupVisibleDuringLastEnterKeyPressed = false;

	/**
	 * Constructor for BaseComboBox.
	 * @param aModel
	 */
	public VaComboBox(ComboBoxModel aModel) {
		super(aModel);
		initialize();
	}

	/**
	 * Constructor for BaseComboBox.
	 * @param items
	 */
	public VaComboBox(Object[] items) {
		super(items);
		initialize();
	}

	/**
	 * Constructor for BaseComboBox.
	 * @param items
	 */
	public VaComboBox(Vector<?> items) {
		super(items);
		initialize();
	}

	/**
	 * Constructor for BaseComboBox.
	 */
	public VaComboBox() {
		super();
		initialize();
	}

	private void initialize() {
	}

	public void processKeyEvent(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getID() == KeyEvent.KEY_PRESSED){
			popupVisibleDuringLastEnterKeyPressed = isPopupVisible();
		}		
		
		if (e.getKeyCode() == KeyEvent.VK_ENTER && !popupVisibleDuringLastEnterKeyPressed) {
			if(e.getID() == KeyEvent.KEY_RELEASED ){
				transferFocus();
				e.consume();
			}
		}

		if (e.getKeyCode() == KeyEvent.VK_UP &&  !isPopupVisible()){
			if(e.getID() == KeyEvent.KEY_RELEASED)
				transferFocusBackward();
			e.consume();
		}
		
		super.processKeyEvent(e);

	}
}
