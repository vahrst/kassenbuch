package de.vahrst.application.gui;

import java.awt.CardLayout;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import org.apache.log4j.Logger;


/**
 * 
 * @author Thomas
 * @version 
 * @file ClientAreaPanel.java
 */
public class ClientAreaPanel extends JPanel {
	static Logger logger = Logger.getLogger(ClientAreaPanel.class);

	private Map<FormPanel, String> formpanelliste = new HashMap<FormPanel, String>();

	// innerClass: containerListener
	class MyContainerListener implements ContainerListener {
		public void componentAdded(ContainerEvent e) {
			logger.debug("componentAdded Container: " + e.getContainer());
			logger.debug("componentAdded Component: " + e.getComponent());
			logger.debug("componentAdded Child    : " + e.getChild());
		}

		public void componentRemoved(ContainerEvent e) {
			logger.debug("componentRemove: " + e.getContainer());
						
		}
	}

	/**
	 * Constructor for ClientAreaPanel.
	 */
	public ClientAreaPanel() {
		super();
		initialize();
	}

	private void initialize() {
		logger.debug("initialize");
		this.setLayout(new CardLayout());
		this.setBackground(ColorKonstanten.COLOR_KASSEHEADERPANEL);
		this.addContainerListener(new MyContainerListener());
	}

	/** 
	 * Removes the specified component from this container.
	 * @param comp the component to be removed
	 * @see #add
	 */
	public void remove(FormPanel fp) {
		formpanelliste.remove(fp);
		super.remove(fp);
		logger.debug("versuche, das n�chste Panel sichtbar zu machen");
		CardLayout cl = (CardLayout) this.getLayout();
		cl.previous(this);		
		this.revalidate();
	}

	public void add(FormPanel fp, String constraints) {
		formpanelliste.put(fp, constraints);
		super.add(fp, constraints);

		CardLayout cl = (CardLayout) this.getLayout();
		cl.show(this, constraints);
		
		this.revalidate();
	}
	
	public void activate(FormPanel fp){
		String constraints = formpanelliste.get(fp);
		if(constraints == null){
			logger.warn("activate f�r Formpanel empfangen, das nicht registriert ist: " + fp);
		}else{
			CardLayout cl = (CardLayout) this.getLayout();
			cl.show(this, constraints);
			this.revalidate();
		}
		
	}
	
	
}
