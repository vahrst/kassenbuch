package de.vahrst.application.gui;

import java.awt.Component;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import de.vahrst.application.types.Waehrungsbetrag;

/**
 * Ein TabelCellRenderer f�r Waehrungsbetrag.
 * @author Thomas
 * @version 
 * @file WaehrungsbetragRenderer.java
 */
public class WaehrungsbetragRenderer extends JLabel implements TableCellRenderer{

	/**
	 * Constructor for WaehrungsbetragRenderer.
	 */
	public WaehrungsbetragRenderer() {
		super();
		this.setOpaque(true);
	}

		public Component getTableCellRendererComponent(
			JTable table,
			Object o,
			boolean isSelected,
			boolean hasFocus,
			int row,
			int column) {
			if (isSelected) {
				setForeground(table.getSelectionForeground());
				setBackground(table.getSelectionBackground());
			} else {
				setForeground(table.getForeground());
				setBackground(table.getBackground());
			}

			setFont(table.getFont());

			this.setHorizontalAlignment(JLabel.RIGHT);
			if (o instanceof Waehrungsbetrag) {
				Waehrungsbetrag b = (Waehrungsbetrag) o;
				this.setText(b.format());
			} else {
				this.setText(o.toString());
			}

			return this;
		}

}
