package de.vahrst.application.gui;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * 
 * @author Thomas
 * @version 
 * @file SollHabenRenderer.java
 */
public class SollHabenRenderer extends DefaultTableCellRenderer {

	/**
	 * Constructor for SollHabenRenderer.
	 */
	public SollHabenRenderer() {
		super();
	}
	public Component getTableCellRendererComponent(
		JTable table,
		Object o,
		boolean isSelected,
		boolean hasFocus,
		int row,
		int column) {
			this.setHorizontalAlignment(SwingConstants.CENTER);
			return super.getTableCellRendererComponent(table,o,isSelected,hasFocus,row,column);
	}

}
