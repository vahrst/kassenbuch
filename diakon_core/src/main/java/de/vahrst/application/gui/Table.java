package de.vahrst.application.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import de.vahrst.application.types.SollHaben;
import de.vahrst.application.types.Waehrungsbetrag;
/**
 * 
 * @author Thomas
 * @version 
 * @file Table.java
 */
public class Table extends JTable {
//	private static Logger logger = Logger.getLogger(Table.class);
	
	private List<ActionListener> actionListeners = new ArrayList<ActionListener>(1);

	private String actionEventName = "GeheZu";

	/**
	 * Constructor for Table.
	 */
	public Table() {
		super();
		initialize();
	}

	/**
	 * Constructor for Table.
	 * @param dm
	 */
	public Table(TableModel dm) {
		super(dm);
		initialize();
	}

	/**
	 * Constructor for Table.
	 * @param dm
	 * @param cm
	 */
	public Table(TableModel dm, TableColumnModel cm) {
		super(dm, cm);
		initialize();
	}

	/**
	 * Constructor for Table.
	 * @param dm
	 * @param cm
	 * @param sm
	 */
	public Table(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
		super(dm, cm, sm);
		initialize();
	}

	/**
	 * Constructor for Table.
	 * @param numRows
	 * @param numColumns
	 */
	public Table(int numRows, int numColumns) {
		super(numRows, numColumns);
		initialize();
	}

	/**
	 * Constructor for Table.
	 * @param rowData
	 * @param columnNames
	 */
	public Table(Vector rowData, Vector columnNames) {
		super(rowData, columnNames);
		initialize();
	}

	/**
	 * Constructor for Table.
	 * @param rowData
	 * @param columnNames
	 */
	public Table(Object[][] rowData, Object[] columnNames) {
		super(rowData, columnNames);
		initialize();
	}

	/**
	 * f�gt einen ActionListener hinzu
	 */
	public void addActionListener(ActionListener l){
		actionListeners.add(l);
	}

	private void initialize(){
		this.setDefaultRenderer(Waehrungsbetrag.class, new WaehrungsbetragRenderer());
		this.setDefaultRenderer(SollHaben.class, new SollHabenRenderer());

		
		this.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if(e.getClickCount() == 2){
					fireActionEvent(new ActionEvent(Table.this, ActionEvent.ACTION_PERFORMED, actionEventName));
				}
			}
			
		});

	}

  protected void processKeyEvent(KeyEvent e) {
  	if(e.getKeyCode() == KeyEvent.VK_ENTER){
			if(e.getID() == KeyEvent.KEY_RELEASED){
				fireActionEvent(new ActionEvent(Table.this, ActionEvent.ACTION_PERFORMED, actionEventName));
			}
			e.consume();
  	}else{
  		super.processKeyEvent(e);
  	}
  	
  }

	protected void fireActionEvent(ActionEvent ae){
		for(int i=0;i<actionListeners.size();i++){
			actionListeners.get(i).actionPerformed(ae);
		}
	}
	
	/**
	 * Setzt den Namen des ActionEvents bei Doppelklick auf die Tabelle
	 */
	public void setSelectionEventName(String name){
		actionEventName = name;
	}
}
