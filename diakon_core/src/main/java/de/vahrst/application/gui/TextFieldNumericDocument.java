package de.vahrst.application.gui;
/**
 * DatenModell f�r alle numerischen Textfelder
 * @author: H�nteler, Hartmut
 */
public class TextFieldNumericDocument extends TextFieldDocument {
	
	static final String numerischeZeichen = "0123456789";
	
	String erlaubteZeichen = null;
	
	boolean richtig;

	private int integerPart = 4;
	private int fractionPart = 2;
	
	private boolean signed = false;	
	private boolean signedOutput = false;
	private boolean integerGrouping = true;
	private boolean ignoreInsertation = false;
	private boolean completeWithZeroes = false;	

/**
 * LVMTextFieldNumericDocument constructor comment.
 */
public TextFieldNumericDocument() {
	super();

	if (erlaubteZeichen == null)
		erlaubteZeichen = new String(numerischeZeichen);
}


// hier wird davon ausgegangen, dass der Aufbau der
// String-Parameters immer identisch ist.

public void addAllowed(boolean add, String wat) {
	if (add) {
		if (erlaubteZeichen.indexOf(wat) == -1) {
			erlaubteZeichen += wat;
		}
	} else {
		int pos = erlaubteZeichen.indexOf(wat);
		if (pos != -1) {
			String t = erlaubteZeichen.substring(0, pos);
			t = t + erlaubteZeichen.substring(pos + 1);
			erlaubteZeichen = t;
		}
	}
}


	public int getFractionPart() { return fractionPart; }


	public int getIntegerPart() { return integerPart; }


public void insertString(
	int offset,
	String str,
	javax.swing.text.AttributeSet attr)
	throws javax.swing.text.BadLocationException {
	richtig = true;

	if (! isIgnoreInsertation() ) {
		for (int i = 0; i < str.length(); i++) {
			if (erlaubteZeichen.indexOf(str.charAt(i)) == -1) {
				richtig = false;
			}
		}
	}
	if (richtig) {
		super.insertString(offset, str, attr);
	}
}


public boolean isCompleteWithZeroes() {	return completeWithZeroes; }


public boolean isIgnoreInsertation() {	return ignoreInsertation; }


	public boolean isIntegerGrouping() { return integerGrouping; }


public boolean isSigned() {	return signed; }


public boolean isSignedOutput() {	return signedOutput; }


public void setCompleteWithZeroes(boolean arg) {	completeWithZeroes = arg; }


	public void setFractionPart(int arg) { 
		fractionPart = arg;
		updateMaxLength();
	}


public void setIgnoreInsertation(boolean b) {
	ignoreInsertation = b;
}


	public void setIntegerGrouping(boolean arg) { 
		integerGrouping = arg;
		updateMaxLength();
	}


	public void setIntegerPart(int arg) { 
		integerPart = arg;
		updateMaxLength();
	}


public void setSigned(boolean signed) {	
	this.signed = signed;
	updateMaxLength();
}


public void setSignedOutput(boolean signedOutput) {	this.signedOutput = signedOutput;}


	/**
	 * Aktualisierung der Eingabel�nge 
	 * des Datenmodells
	 */
	private void updateMaxLength() {
		setMaxLength(
			getFractionPart()
				+ ((getFractionPart() > 0) ? 1 : 0)
				+ getIntegerPart()
				+ ((isSigned()) ? 1 : 0));		
	}
}