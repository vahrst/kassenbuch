package de.vahrst.application.gui;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;




/**
 * 
 * @author Thomas
 * @version 
 */
public class FormPanel extends JPanel {
	/** Konstante f�r die H�he des Buttonpanels */
	final static int BUTTONPANEL_HEIGHT = 24;
	final static int HEADER_HEIGHT = 22;

	// Flags f�r die zu malenden Bereiche
	private boolean paintButtonArea = true;
	private boolean paintHeaderArea = true;

	private String title;


	/**
	 * Constructor for FormPanel.
	 */
	public FormPanel() {
		super();
		this.setBorder(new EmptyBorder(new Insets(HEADER_HEIGHT,0,0,0)));
		this.setBackground(ColorKonstanten.COLOR_FORMPANEL_BG);
	}

	/**
	 * setzt den Title
	 */
	public void setTitle(String title){
		this.title = title;
	}
	
	/**
	 * setzt das Flag, das steuert, ob der Headerbereich gemalt werden 
	 * soll oder nicht
	 */
	public void setPaintHeaderArea(boolean arg){
		paintHeaderArea = arg;
		Insets in = new Insets ((paintHeaderArea? HEADER_HEIGHT:0),0,0,0);
		this.setBorder(new EmptyBorder(in));
		repaint();
	}
		

	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);

		//		paintMargin(g);

		if(paintHeaderArea)
			paintHeaderBereich(g);
	
		paintButtonBereich(g);


	}
	
	/**
	 * malt den 22 Pixel hohen Header-Bereich
	 */
	protected void paintHeaderBereich(Graphics g) {
	
		
		g.setColor(ColorKonstanten.COLOR_FORMPANEL_HEADER_BG);
		g.fillRect(0, 0, getWidth(), HEADER_HEIGHT);
		
		
		if(title != null){
			g.setColor(ColorKonstanten.COLOR_FORMPANEL_HEADER_FG);
			g.setFont(new Font(null, Font.BOLD, 14));
			g.drawString(title, 3, 17);
		}
	}


	/**
	 * malt den unteren 24 Pixel hohen Bereich f�r die Buttons
	 */
	protected void paintButtonBereich(Graphics g) {


		BasicDrawUtil.paintButtonPanelBackGround(
			g,
			0,
			getHeight() - BUTTONPANEL_HEIGHT,
			getWidth(),
			BUTTONPANEL_HEIGHT,
			ColorKonstanten.ButtonPanel_background,
			ColorKonstanten.ButtonPanel_foreground);

		/*	icon =
				ImageIconFactory.getImageIcon("/lvm_applwin_buttonleiste_rand_west.gif");
			img = icon.getImage();
		
			g.drawImage(img, 0, getHeight() - icon.getIconHeight(), this);
		*/
	}

	protected void paintMargin(Graphics g) {

		Image img;
		ImageIcon icon;

		icon =
			ImageIconFactory.getImageIcon(
				"/lvm_applwin_inhaltsbereich_rand_west.gif");
		img = icon.getImage();

		for (int i = 0; i < getHeight() / icon.getIconHeight() + 1; i++)
			g.drawImage(img, 0, i * icon.getIconHeight(), this);

		icon =
			ImageIconFactory.getImageIcon(
				"/lvm_applwin_inhaltsbereich_rand_innen_east.gif");
		img = icon.getImage();

		for (int i = 0; i < getHeight() / icon.getIconHeight() + 1; i++)
			g.drawImage(
				img,
				getWidth() - icon.getIconWidth(),
				i * icon.getIconHeight(),
				this);

		icon =
			ImageIconFactory.getImageIcon(
				"/lvm_applwin_inhaltsbereich_rand_south.gif");
		img = icon.getImage();

		for (int i = 0; i < getWidth() / icon.getIconWidth() + 1; i++)
			g.drawImage(
				img,
				i * icon.getIconWidth(),
				getHeight()
					- icon.getIconHeight()
					- (paintButtonArea ? BUTTONPANEL_HEIGHT : 0),
				this);

		icon =
			ImageIconFactory.getImageIcon(
				"/lvm_applwin_inhaltsbereich_rand_southwest.gif");
		img = icon.getImage();

		g.drawImage(
			img,
			0,
			getHeight()
				- icon.getIconHeight()
				- (paintButtonArea ? BUTTONPANEL_HEIGHT : 0),
			this);

		icon =
			ImageIconFactory.getImageIcon(
				"/lvm_applwin_inhaltsbereich_rand_southeast.gif");
		img = icon.getImage();

		g.drawImage(
			img,
			getWidth() - icon.getIconWidth(),
			getHeight()
				- icon.getIconHeight()
				- (paintButtonArea ? BUTTONPANEL_HEIGHT : 0),
			this);

	}

}
