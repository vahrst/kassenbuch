package de.vahrst.application.gui;

import java.io.*;
import java.util.Hashtable;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;

/**
 * Klasse zum Laden von Bildchen. Vahrst
 * Die �blichen class.getResource()-Methoden funktionieren
 * im Browser-Betrieb nicht richtig. Daher wurde diese Klasse erstellt,
 * die versucht, die Bildchen �ber verschiedene Mechanismen zu laden. Zuerst �ber
 * den herk�mmlichen Weg (d.h. unter Verwendung des passenden Konstruktors von ImageIcon), wenn
 * das nicht funktioniert, �ber 'class.getResourceAsStream()' und wenn das auch nicht klappt, evtl
 * �ber den Applet-Mechanismus zum Laden von Bildern. Falls das passende Bildchen geladen wurde
 * wird dieses intern gecached.
 * <p>
 * 14.09.99 Vahrst <br>
 * Die unterschiedlichen Lade-Mechanismen, die nacheinander probiert werden, implementiert.
 */
public class ImageIconFactory {
	private static Logger logger = Logger.getLogger(ImageIconFactory.class);

	/**
	 * Der Cache f�r die bereits gelesenen Bildchen.
	 */
	static private Hashtable<String, ImageIcon> imageTable = new Hashtable<String, ImageIcon>();

	/**
	 * ImageIconFactory constructor comment.
	 */
	public ImageIconFactory() {
		super();
	}
	/**
	 * versucht, anhand des vorgegebenen Resource-Namen ein
	 * ImageIcon zu laden. Dieses wird - falls der Ladevorgang erfolgreich
	 * war - zus�tzlich in einer hashtable gecached, um beim n�chsten
	 * Zugriff das ImageIcon-Object nicht erneut instanziieren zu m�ssen.
	 *<p>
	 * [TECHNISCH]<br>
	 * zum Laden der Resource wird die Methode class.getResourceAsStream() verwendet.
	 * @return ImageIcon
	 * @param resourceName java.lang.String
	 */
	public static ImageIcon getImageIcon(String resourceName) {

		// ImageIcon aus Hashtable lesen ...
		ImageIcon imageIcon = imageTable.get(resourceName);

		// gesuchte Resource noch nicht gecached, also lesen
		if (imageIcon == null) {
			imageIcon = loadImageFromURL(resourceName);
		}
		// direktes Lesen von der URL hat nicht funktioniert, jetzt �ber 'getResourceAsStream' lesen
		if (imageIcon == null) {
			imageIcon = loadImageFromStream(resourceName);
		}

		if (imageIcon != null)
			imageTable.put(resourceName, imageIcon);
		else {
			// System.out.println("Resource nicht geladen: " + resourceName);
			logger.warn("Resource nicht geladen: " + resourceName);
		}

		return imageIcon;
	}
	/**
	 * versucht, das Bild zum vorgegebenen Namen �ber 'getResourceAsStream' zu lesen
	 * @return javax.swing.ImageIcon
	 * @param resourceName java.lang.String
	 */
	private static ImageIcon loadImageFromStream(String resourceName) {
		ImageIcon imageicon = null;

		try {
			InputStream inputstream =
				ImageIconFactory.class.getResourceAsStream(resourceName);
			if (inputstream != null) {
				BufferedInputStream bufferedinputstream =
					new BufferedInputStream(inputstream);
				byte abyte0[] = new byte[bufferedinputstream.available()];
				for (int i = 0; i < abyte0.length;) {
					int j =
						bufferedinputstream.read(abyte0, i, abyte0.length - i);
					if (j > -1)
						i += j;
					else {
						// System.out.println("ImageRepository::loadImage(): -ve read on image=" + resourceName);
						if (i == 0)
							throw new Exception("totalRead == 0 !!!");
						i = abyte0.length;
					}
				}
				imageicon = new ImageIcon(abyte0);
				try {
					inputstream.close();
				} catch (Exception ex) {
				}
				try {
					bufferedinputstream.close();
				} catch (Exception ex) {
				}
			}
		} catch (Exception ex) {
			// ex.printStackTrace(); //[PENDING]
			logger.error("",ex);
		}
		return imageicon;
	}
	/**
	 * versucht, das Bildchen mit dem vorgegebenen Namen zu laden.
	 * @param s String
	 *
	 */
	private static ImageIcon loadImageFromURL(String s) {
		ImageIcon imageicon = null;
		try {
			java.net.URL url = ImageIconFactory.class.getResource(s);
			if (url != null)
				imageicon = new ImageIcon(url);
		} catch (Exception ex) {
			logger.warn("Exception in loadImageFromURL, Name = "	+ s, ex);
		}
		return imageicon;
	}
}
