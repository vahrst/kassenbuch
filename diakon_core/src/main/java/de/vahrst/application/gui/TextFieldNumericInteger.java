package de.vahrst.application.gui;



/**
 * 
 * @author Thomas
 * @version 
 * @file TextFieldNumericInteger.java
 */
public class TextFieldNumericInteger extends TextFieldNumeric {

	/**
	 * Constructor for TextFieldNumericInteger.
	 */
	public TextFieldNumericInteger() {
		super();
		init();
	}

	/**
	 * Constructor for TextFieldNumericInteger.
	 * @param cols
	 */
	public TextFieldNumericInteger(int cols) {
		super(cols);
		this.setIntegerPart(cols);
		init();
	}

	/**
	 * Constructor for TextFieldNumericInteger.
	 * @param text
	 */
	public TextFieldNumericInteger(String text) {
		super(text);
		init();
	}


	private void init(){
		super.setFractionPart(0);
		setIntegerGrouping(false);
	}
	

	public void setIntegerValue(int value){
		setText(Integer.toString(value));
	}
	
	public int getIntegerValue(){
		try{
			return Integer.parseInt(getText());
		}catch(NumberFormatException e){
			return 0;
		}
	}
}

