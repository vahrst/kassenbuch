package de.vahrst.application.gui;
import javax.swing.text.PlainDocument;
/**
 * Modell f�r ein standard TextField
 * Creation date: (09.02.00 10:52:07)
 * @author: H�nteler, Hartmut
 */
public class TextFieldDocument extends PlainDocument {

	/** legt fest wieviele Zeichen eingegeben werden d�rfen */
	int maxLength = -1;


	/**
	 * Liefert zur�ck, ob die Eingabe korrekt ist.
	 * Liefert in dieser Implementierung immer <tt>true</tt> zur�ck -
	 * wird von Subklassen �bverschrieben (z.B. LVMTextFieldDateDocument).
	 * @return boolean
	 */
	public boolean isValidEntry() {
		return true;
	}

	
/**
 * LVMTextFieldDocument constructor comment.
 */
public TextFieldDocument() {
	super();
}


/**
 * LVMTextFieldDocument constructor comment.
 */
public TextFieldDocument(int len) {
	super();
	maxLength = len;
}


/**
 * LVMTextFieldDocument constructor comment.
 * @param c javax.swing.text.AbstractDocument.Content
 */
protected TextFieldDocument(javax.swing.text.AbstractDocument.Content c) {
	super(c);
}


public int getMaxLength() {
	return maxLength;
}


public void insertString(
	int offset,
	String str,
	javax.swing.text.AttributeSet attr)
	throws javax.swing.text.BadLocationException {
	if (str == null)
		return; // nicht eingegeben

	if ((getLength() + str.length()) <= maxLength || maxLength < 0) {
		super.insertString(offset, str, attr);
	}
}


public void setMaxLength(int arg) {

	this.maxLength = arg;
}
}