package de.vahrst.application.gui;

import java.awt.Color;

/**
 * 
 * @author Thomas
 * @version 
 */
public interface ColorKonstanten {

//	public static final Color COLOR_HEADER = new Color(153, 153, 153);
	public static final Color COLOR_HEADER = new Color(255, 255, 127);

//	public static final Color COLOR_BUTTONPANEL = new Color(153, 153, 153);
	public static final Color COLOR_BUTTONPANEL = new Color(255,255,127);
	
	public static final Color COLOR_KASSEHEADERPANEL = new Color(255,255,204);

	
	public static final Color COLOR_FORMPANEL_BG = new Color(233,233,190);
	public static final Color COLOR_FORMPANEL_HEADER_BG = new Color(128,128,32);
	public static final Color COLOR_FORMPANEL_HEADER_FG = new Color(255,255,204);

	public static final Color ButtonPanel_background		= new Color(255,255,127);	// dunkles Gelb f�r Hintergrund des Buttonpanels (hintergrund)
	public static final Color ButtonPanel_foreground		= new Color(204,204,204);	// helleres Gelb f�r Hintergrund des Buttonpanels (vordergrund)

	public static final Color aktionsbutton_background	= new Color(65,108,155);	// dunkles Blau bei den Aktionsbuttons (hintergrund)
	
	public static final Color COLOR_LINE = Color.BLACK;


	public static final Color CHECKBOX_FOCUSCOLOR = Color.BLUE;

}
