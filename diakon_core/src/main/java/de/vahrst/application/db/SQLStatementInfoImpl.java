package de.vahrst.application.db;
import java.util.ArrayList;
import java.util.List;

import de.vahrst.application.util.StringHelper;
/**
 * Klasse, welches Informationen zu einem SQL-Statement h�lt. Hierzu
 * geh�rt der SQL-Befehl sowie (bei PreparedStatements) eine Liste
 * der Parameter f�r die Hostvariablen.
 * <p>
 * (CVS: $Revision: 1.1 $ $Date: 2006/10/24 16:13:14 $ $Author: thomas $)
 * <p>
 * @author Guyet, Sven
 */
final class SQLStatementInfoImpl implements SQLStatementInfo {
	// Das Parametermarker Zeichen fuer SQL Prepared Statements
	private static final char PARAMETER_MARKER = '?';

	// Vollst�ndiger SQL-String, d.h. Parametermarker sind durch Hostvariablen-Werte ersetzt.
	private String fullSqlString = null;

	// Liste, in der die Parameter-Werte fuer SQL-Hostvariablen festgehalten werden, 
	// um bei Bedarf zu Loggingzwecken das echte SQL-Statement bilden zu koennen.
	private List<Object> parameterValues = null;

	// Liste, in der je Batch-Aufruf die Parameter-Werte fuer SQL-Hostvariablen festgehalten werden, 
	// um bei Bedarf zu Loggingzwecken die echten SQL-Statements bilden zu koennen.
	private List<List<Object>> batchParameterLists = null;

	// SQL-String
	private String sqlString = null;

	// Schalter f�r die Ausgabe der kompletten SQL-Statements bei PerformanceTraces
	private boolean fullPerformanceTrace = false;

	// Schalter f�r die Ausgabe von PerformanceTraces
	private boolean performanceTrace = false;

	// Schalter f�r die Ausgabe von SQLStatement-Traces
	private boolean sqlStatementTrace = false;

	/**
	 * Konstruktor.
	 */
	SQLStatementInfoImpl() {
		super();
	}

	/**
	 * Konstruktor.
	 * 
	 * @param stmt String
	 * 		Der SQL-Befehl
	 */
	SQLStatementInfoImpl(String stmt) {
		super();
		this.setSqlString(stmt);
	}

	/**
	 * Konstruktor.
	 * 
	 * @param stmt String
	 * 		Der SQL-Befehl
	 * @param sqlStatementTrace boolean
	 * 		Schalter, ob SQLStatements getraced werden sollen
	 * @param performanceTrace boolean
	 * 		Schalter, ob PerformanceTraces geschrieben werden sollen
	 * @param fullPerformanceTrace boolean
	 * 		Schalter, ob in PerformanceTraces die kompletten SQL-Statements enthalten sein sollen
	 */
	SQLStatementInfoImpl(String stmt, boolean sqlStatementTrace, boolean performanceTrace, boolean fullPerformanceTrace) {
		super();
		this.setSqlString(stmt);
		this.setSqlStatementTrace(sqlStatementTrace);
		this.setPerformanceTrace(performanceTrace);
		this.setFullPerformanceTrace(fullPerformanceTrace);
	}

	/**
	 * Setze die Listen der gemerkten Parameterinhalte zurueck.
	 */
	public void clearParameterValues() {
		parameterValues = null;
		batchParameterLists = null;
	}

	/**
	 * Gebe mein vollstaendiges SQL-Statement zurueck, d.h. mein SQL-String, in welchem
	 * Parametermarker ("?") durch den entsprechenden Hostvariablen-Wert ersetzt wurden.
	 */
	public String getFullSqlString() {
		// Falls ich keine Parameter habe, nehme ich nur den SQL-String.
		// Nicht ueber Getter gehen, da sonst evtl. unnoetige Initialisierung erfolgt.
		if (parameterValues == null || parameterValues.size() == 0) {
			return getSqlString();
		}

		// Da die Ersetzung der Parametermarker teuer ist, wird der aufgebaute
		// volle SQL-String gecached.
		if (fullSqlString == null) {
			// Ok, jetzt also alle Parametermarker ("?") ersetzen durch die gespeicherten Werte.
			StringBuffer buf = new StringBuffer();
			// Erst mal den String in die Teile vor und nach den ? aufteilen.
			String[] chunks = StringHelper.split(getSqlString(), PARAMETER_MARKER);
			// Erg�nze den Buffer mit dem vollen SQL-String
			appendSqlString(buf, chunks, parameterValues);
			fullSqlString = buf.toString();
		}
		return fullSqlString;
	}

	/**
	 * Erg�nze den �bergebenen Buffer durch mein vollstaendiges SQL-Statement, d.h.
	 * verwende die �bergebenen Teile meines SQL-Strings und f�lle die L�cken
	 * Parametermarker ("?") durch die entsprechenden Werte der �bergebenen
	 * Hostvariablen-Liste.
	 */
	private void appendSqlString(StringBuffer buf, String[] chunks, List<Object> values) {
		// H�nge mal den ersten Teil an den StringBuffer
		buf.append(chunks[0]);
		// Jetzt f�r die verbleibenden n-1 Teile Parameterwert und naechsten Statementteil konkatenieren
		int parameterIndex = 1;
		while (parameterIndex < chunks.length) {
			buf.append(getParameterValue(values, parameterIndex));
			buf.append(chunks[parameterIndex++]);
		}
		// Falls der String mit einem Parametermarker endete, muss dieser letzte Parameterwert noch
		// an den Buffer drangehaengt werden.
		if (getSqlString().charAt(getSqlString().length()-1) == PARAMETER_MARKER) {
			buf.append(getParameterValue(values, parameterIndex));
		}
	}

	/**
	 * Gebe ein Array meiner vollstaendigen SQL-Statements fuer den Batch-Modus zurueck,
	 * d.h. je Batch-Aufruf meinen SQL-String, in welchem Parametermarker ("?") durch die
	 * entsprechenden Hostvariablen-Wert des Batch-Aufrufs ersetzt wurden.
	 */
	public String[] getFullSqlStrings() {
		// Falls ich keine Batch-Parameter habe, nehme ich nur den SQL-String.
		// Nicht ueber Getter gehen, da sonst evtl. unnoetige Initialisierung erfolgt.
		if (batchParameterLists == null || batchParameterLists.size() == 0) {
			return new String[] { getSqlString() };
		}

		String[] fullSqlStrings = new String[batchParameterLists.size()];
		// Ok, jetzt also pro Batch-Aufruf alle Parametermarker ("?") ersetzen durch die gespeicherten Werte.
		StringBuffer buf = new StringBuffer();
		// Erst mal den String in die Teile vor und nach den ? aufteilen.
		String[] chunks = StringHelper.split(getSqlString(), PARAMETER_MARKER);
		for (int i = 0; i < batchParameterLists.size(); i++) {
			// Initialisiere den Buffer
			buf.setLength(0);
			buf.append("BATCH-");
			// Erg�nze den Buffer mit dem vollen SQL-String
			appendSqlString(buf, chunks, batchParameterLists.get(i));
			fullSqlStrings[i] = buf.toString();
		}
		return fullSqlStrings;
	}

	/**
	 * Gebe den Parameterwert der �bergebenen Liste zum �bergebenen Index zur�ck. Der Index entspricht der
	 * laufenden Nummer des Parametermarkers ("?") in meinem SQL-String. Der erste
	 * Parameter hat also den Index 1.
	 */
	private String getParameterValue(List<Object> values, int index) {
		// Nicht ueber Getter gehen, da sonst evtl. unnoetige Initialisierung erfolgt.
		if (values == null || values.size() < index) {
			return "FEHLER: PARAMETER NICHT GESETZT";
		}
		Object value = values.get(index-1);
		if (value==null) {
			// Kein bzw. Null-Wert: Gebe den String "null" zur�ck
			return "null";
		}
		
		if (value instanceof String || value instanceof java.util.Date) {
			// String- oder Date-Werte: Mit Hochkomma umschliessen
			return "'" + value + "'";
		} 
		
		// Alle anderen Objekte: mit toString() in String umwandeln
		return value.toString();
	}

	/**
	 * Gebe die Liste der gemerkten Parameterinhalte zurueck.
	 *
	 * @return ArrayList
	 *		Liste der gemerkten Parameterinhalte.
	 */
	public List<Object> getParameterValues() {
		if (parameterValues == null) {
			parameterValues = new ArrayList<Object>();
		}
		return parameterValues;
	}

	/**
	 * Gebe die Liste der gemerkten Parameterinhalte zurueck.
	 *
	 * @return ArrayList
	 *		Liste der gemerkten Parameterlisten.
	 */
	private List<List<Object>> getBatchParameterLists() {
		if (batchParameterLists == null) {
			batchParameterLists = new ArrayList<List<Object>>();
		}
		return batchParameterLists;
	}

	/**
	 * Gebe meinen SQL-String zurueck.
	 */
	public String getSqlString() {
		if (sqlString == null) {
			sqlString = "";
		}
		return sqlString;
	}

	/**
	 * Gebe zurueck, ob in PerformanceTraces f�r SQL-Zugriffe die kompletten SQL-Statements enthalten sein sollen.
	 *
	 * @return boolean
	 *		true, falls in PerformanceTraces f�r SQL-Zugriffe die kompletten SQL-Statements enthalten sein sollen.
	 */
	public boolean isFullPerformanceTrace() {
		return fullPerformanceTrace;
	}

	/**
	 * Gebe zurueck, ob PerformanceTraces f�r SQL-Zugriffe geschrieben werden sollen.
	 *
	 * @return boolean
	 *		true, falls PerformanceTraces f�r SQL-Zugriffe geschrieben werden sollen.
	 */
	public boolean isPerformanceTrace() {
		return performanceTrace;
	}

	/**
	 * Gebe zurueck, ob SQL-Statements getraced werden sollen.
	 *
	 * @return boolean
	 *		true, falls SQL-Statements bei der Ausf�hrung getraced werden sollen.
	 */
	public boolean isSqlStatementTrace() {
		return sqlStatementTrace;
	}

	/**
	 * Merke mir den uebergebenen Parameterinhalt.
	 *
	 * @param position int
	 *		Position des Parameters im SQL-Befehl. Der erste Parameter hat die Position 1.
	 * @param obj Object
	 *		Inhalt des Parameters.
	 */
	public void saveParameterValue(int position, Object obj) {
		// Falls Position groesser als momentane Array-Size ist, muss
		// das Array entsprechend vergroessert werden.
		for (int i = getParameterValues().size(); i < position; i++) { // $codepro.audit.disable invalidLoopConstruction
			getParameterValues().add(null);
		}
		// Parameter sichern. Erster Index im Array ist 0!
		getParameterValues().set(position-1, obj);
		// Der ggf. gecachte vollst�ndige SQL-String muss zur�ckgesetzt werden.
		fullSqlString = null;
	}

	/**
	 * Merke mir eine Kopie der aktuellen Liste der Parameterinhalte in meiner BatchParameterList.
	 */
	public void saveBatchParameterValues() {
		getBatchParameterLists().add(new ArrayList<Object>(getParameterValues()));
	}

	/**
	 * Setze den Schalter, der steuert, ob in PerformanceTraces f�r SQL-Zugriffe die kompletten SQL-Statements enthalten sein sollen.
	 *
	 * @param fullPerformanceTrace boolean
	 *		Schalter, ob in PerformanceTraces f�r SQL-Zugriffe die kompletten SQL-Statements enthalten sein sollen.
	 */
	public void setFullPerformanceTrace(boolean fullPerformanceTrace) {
		this.fullPerformanceTrace = fullPerformanceTrace;
	}

	/**
	 * Setze den Schalter, der steuert, ob PerformanceTraces f�r SQL-Zugriffe geschrieben werden sollen.
	 *
	 * @param performanceTrace boolean
	 *		Schalter, ob PerformanceTraces f�r SQL-Zugriffe geschrieben werden sollen.
	 */
	public void setPerformanceTrace(boolean performanceTrace) {
		this.performanceTrace = performanceTrace;
	}

	/**
	 * Setze den Schalter, der steuert, ob SQL-Statements getraced werden sollen.
	 *
	 * @param sqlStatementTrace boolean
	 *		Schalter, ob SQL-Statements bei der Ausf�hrung getraced werden sollen.
	 */
	public void setSqlStatementTrace(boolean sqlStatementTrace) {
		this.sqlStatementTrace = sqlStatementTrace;
	}


	/**
	 * Merke mir den SQL-String, um diesen bei spaeteren Traceausgaben
	 * parat zu haben.
	 */
	public void setSqlString(String stmt) {
		sqlString = stmt;
		// Der ggf. gecachte vollst�ndige SQL-String sowie die ParameterListe muessen zur�ckgesetzt werden.
		fullSqlString = null;
		parameterValues = null;
	}

}