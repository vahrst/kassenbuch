package de.vahrst.application.db;

import java.util.List;

/**
 * Interface fuer Objekte, die Informationen zu einem SQL-Statement kapseln k�nnen. Hierzu
 * geh�ren der SQL-Befehl sowie (bei PreparedStatements) eine Liste der Parameter f�r die
 * Hostvariablen.
 * <p>
 * (CVS: $Revision: 1.1 $ $Date: 2006/10/24 16:13:14 $ $Author: thomas $)
 * <p>
 * @author m500367
 */
public interface SQLStatementInfo {

	/**
	 * Setze die Liste der gemerkten Parameterinhalte zurueck.
	 */
	void clearParameterValues();

	/**
	 * Gebe mein vollstaendiges SQL-Statement zurueck, d.h. mein SQL-String, in welchem
	 * Parametermarker ("?") durch den entsprechenden Hostvariablen-Wert ersetzt wurden.
	 *
	 * @return String
	 *		Der vollst�ndige SQL-Befehl (bei PreparedStatements sind die Parametermarker '?' durch die
	 *		echten Hostvariablen-Werte ersetzt.
	 */
	String getFullSqlString();

	/**
	 * Gebe ein Array meiner vollstaendigen SQL-Statements fuer den Batch-Modus zurueck,
	 * d.h. je Batch-Aufruf meinen SQL-String, in welchem Parametermarker ("?") durch die
	 * entsprechenden Hostvariablen-Wert des Batch-Aufrufs ersetzt wurden.
	 *
	 * @return String[]
	 *		Ein Array mit den vollst�ndigen SQL-Befehlen (bei PreparedStatements sind die Parametermarker '?' durch die
	 *		echten Hostvariablen-Werte der einzelnen Batch-Aufrufe ersetzt.
	 */
	String[] getFullSqlStrings();

	/**
	 * Gebe die Liste der gemerkten Parameterinhalte zurueck.
	 *
	 * @return List
	 *		Liste der gemerkten Parameterinhalte.
	 */
	List<Object> getParameterValues();

	/**
	 * Gebe meinen SQL-String zurueck.
	 *
	 * @return String
	 *		Der SQL-Befehl (bei PreparedStatements ggf. mit enthaltenen Parametermarkern, d.h. '?' Zeichen).
	 */
	String getSqlString();

	/**
	 * Gebe zurueck, ob in PerformanceTraces f�r SQL-Zugriffe die kompletten SQL-Statements enthalten sein sollen.
	 *
	 * @return boolean
	 *		true, falls in PerformanceTraces f�r SQL-Zugriffe die kompletten SQL-Statements enthalten sein sollen.
	 */
	boolean isFullPerformanceTrace();

	/**
	 * Gebe zurueck, ob PerformanceTraces f�r SQL-Zugriffe geschrieben werden sollen.
	 *
	 * @return boolean
	 *		true, falls PerformanceTraces f�r SQL-Zugriffe geschrieben werden sollen.
	 */
	boolean isPerformanceTrace();

	/**
	 * Gebe zurueck, ob SQL-Statements getraced werden sollen.
	 *
	 * @return boolean
	 *		true, falls SQL-Statements bei der Ausf�hrung getraced werden sollen.
	 */
	boolean isSqlStatementTrace();

	/**
	 * Merke mir eine Kopie der aktuellen Liste der Parameterinhalte.
	 */
	void saveBatchParameterValues();

	/**
	 * Merke mir den uebergebenen Parameterinhalt.
	 *
	 * @param position int
	 *		Position des Parameters im SQL-Befehl.
	 * @param obj Object
	 *		Inhalt des Parameters.
	 */
	void saveParameterValue(int position, Object obj);

	/**
	 * Setze den Schalter, der steuert, ob in PerformanceTraces f�r SQL-Zugriffe die kompletten SQL-Statements enthalten sein sollen.
	 *
	 * @param fullPerformanceTrace boolean
	 *		Schalter, ob in PerformanceTraces f�r SQL-Zugriffe die kompletten SQL-Statements enthalten sein sollen.
	 */
	void setFullPerformanceTrace(boolean fullPerformanceTrace);

	/**
	 * Setze den Schalter, der steuert, ob PerformanceTraces f�r SQL-Zugriffe geschrieben werden sollen.
	 *
	 * @param performanceTrace boolean
	 *		Schalter, ob PerformanceTraces f�r SQL-Zugriffe geschrieben werden sollen.
	 */
	void setPerformanceTrace(boolean performanceTrace);

	/**
	 * Setze den Schalter, der steuert, ob SQL-Statements getraced werden sollen.
	 *
	 * @param sqlStatementTrace boolean
	 *		Schalter, ob SQL-Statements bei der Ausf�hrung getraced werden sollen.
	 */
	void setSqlStatementTrace(boolean sqlStatementTrace);

	/**
	 * Merke mir den SQL-String, um diesen bei spaeteren Traceausgaben
	 * parat zu haben.
	 *
	 * @param stmt String
	 *		Der SQL-Befehl (bei PreparedStatements ggf. mit enthaltenen Parametermarkern, d.h. '?' Zeichen).
	 */
	void setSqlString(String stmt);
}