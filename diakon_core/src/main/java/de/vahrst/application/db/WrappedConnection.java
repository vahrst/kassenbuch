package de.vahrst.application.db;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;

import de.vahrst.application.logging.TraceKonstanten;



/**
 * Wrapperklasse fuer ein Connection Objekt. Halte intern eine
 * java.sql.Connection implementierende Instanz und leite alle
 * im Connection Interface definierten Methoden, mit folgenden
 * Ausnahmen:
 * <ul>
 * <li>prepareCall(...): WrappedCallableStatement anstelle von CallableStatement,
 * falls PERFORMANCE_STORED_PROC_CHANNEL gesetzt ist</li>
 * <li>prepareStatement(...): WrappedPreparedStatement anstelle von PreparedStatement,
 * falls PERFORMANCE_JDBC_CHANNEL gesetzt ist</li>
 * <li>createStatement(...): WrappedStatement anstelle von Statement,
 * falls PERFORMANCE_JDBC_CHANNEL gesetzt ist</li>
 * </ul><p>
 * Ziel ist, dass alle DB2-Zugriffe (SQL-Calls via JDBC bzw. Stored Procedure
 * Aufrufe) bei Bedarf automatisch Performance-Traces schreiben, ohne dass
 * dafuer in den Anwendungen Vorkehrungen getroffen werden muessen.
 * <p>
 * (CVS: $Revision: 1.1 $ $Date: 2006/10/24 16:13:14 $ $Author: thomas $)
 * <p>
 * @author Guyet, Sven
 */
public class WrappedConnection implements java.sql.Connection, SQLStatementInfo {
	private static Logger sqlStatementLogger = Logger.getLogger(TraceKonstanten.TRACE_SQL_STATEMENTS); 
	// Die von mir gewrappte Connection
	private java.sql.Connection conn;

	// Zwischenspeicherung der Zeit fuer Performance-Traces
	// Aenderung Guyet fuer Build 112-3: Die GetConnection-Zeiten werden nicht mehr getraced, da kein Problem.
	// private long time;

	// Das SQLStatementInfo Objekt kapselt Informationen ueber das zuletzt ueber mich ausgefuehrte
	// SQL-Statement. Das Objekt wird von den ueber mich erzeugten Statement-Instanzen
	// mit Daten versorgt.
	private final SQLStatementInfo sqlStatementInfo = new SQLStatementInfoImpl();

	/**
	 * Konstruktor. Merke mir das Connection Objekt, welches
	 * ich wrappe und an das ich alle Methoden durchreiche.
	 */
	public WrappedConnection(java.sql.Connection conn) {
		this(conn, false, false, false);
	}

	/**
	 * Konstruktor. Merke mir das Connection Objekt, welches
	 * ich wrappe und an das ich alle Methoden durchreiche.
	 * 
	 * @param conn Connection
	 * 		Das Connection-Objekt, welches ich wrappe
	 * @param performanceTrace boolean
	 * 		Flag, welches steuert, ob PerformanceTraces geschrieben werden sollen
	 * @param fullPerformanceTrace boolean
	 * 		Flag, welches steuert, ob PerformanceTrace-Ausgaben das komplette SQL-Statement enthalten sollen
	 */
	public WrappedConnection(java.sql.Connection conn, boolean sqlStatementTrace, boolean performanceTrace, boolean fullPerformanceTrace) {
		super();
		this.conn = conn;
		sqlStatementInfo.setSqlStatementTrace(sqlStatementTrace);
		sqlStatementInfo.setPerformanceTrace(performanceTrace);
		sqlStatementInfo.setFullPerformanceTrace(fullPerformanceTrace);
		if(sqlStatementTrace){
			sqlStatementLogger.info("neue Connection" + conn.toString());
			
		}
	}

	/**
	 * @see java.sql.Connection
	 */
	public void clearWarnings() throws java.sql.SQLException {
		conn.clearWarnings();
	}

	/**
	 * @see java.sql.Connection
	 */
	public void close() throws java.sql.SQLException {
		if (isSqlStatementTrace()) {
			sqlStatementLogger.info("connection.close()");
		}
		conn.close(); // $codepro.audit.disable closeInFinally
		
	}

	/**
	 * @see java.sql.Connection
	 */
	public void commit() throws java.sql.SQLException {
		if (isSqlStatementTrace()) {
			sqlStatementLogger.info("connection.close()");
		}
		conn.commit();
	}

	/**
	 * @see java.sql.Connection
	 * Wrapperobjekt um Statement bauen und zurueckgeben.
	 */
	public java.sql.Statement createStatement() throws java.sql.SQLException {
		return new WrappedStatement(conn.createStatement(), sqlStatementInfo); // $codepro.audit.disable statementCreation
	}

	/**
	 * @see java.sql.Connection
	 * Wrapperobjekt um Statement bauen und zurueckgeben.
	 */
	public java.sql.Statement createStatement(int resultSetType, int resultSetConcurrency) throws java.sql.SQLException {
		return new WrappedStatement(conn.createStatement(resultSetType, resultSetConcurrency), sqlStatementInfo); // $codepro.audit.disable statementCreation
	}

	/**
	 * @see java.sql.Connection
	 */
	public boolean getAutoCommit() throws java.sql.SQLException {
		return conn.getAutoCommit();
	}

	/**
	 * @see java.sql.Connection
	 */
	public String getCatalog() throws java.sql.SQLException {
		return conn.getCatalog();
	}

	/**
	 * @see java.sql.Connection
	 */
	public java.sql.DatabaseMetaData getMetaData() throws java.sql.SQLException {
		return conn.getMetaData();
	}

	/**
	 * @see java.sql.Connection
	 */
	public int getTransactionIsolation() throws java.sql.SQLException {
		return conn.getTransactionIsolation();
	}

	/**
	 * @see java.sql.Connection
	 */
	public Map<String, Class<?>> getTypeMap() throws SQLException {
		return conn.getTypeMap();
	}

	/**
	 * @see java.sql.Connection
	 */
	public java.sql.SQLWarning getWarnings() throws java.sql.SQLException {
		return conn.getWarnings();
	}

	/**
	 * @see java.sql.Connection
	 */
	public boolean isClosed() throws java.sql.SQLException {
		return conn.isClosed();
	}

	/**
	 * @see java.sql.Connection
	 */
	public boolean isReadOnly() throws java.sql.SQLException {
		return conn.isReadOnly();
	}

	/**
	 * @see java.sql.Connection
	 */
	public String nativeSQL(String sql) throws java.sql.SQLException {
		return conn.nativeSQL(sql);
	}

	/**
	 * @see java.sql.Connection
	 * Baue Wrapperobjekt um CallableStatement und gebe dieses zurueck.
	 */
	public java.sql.CallableStatement prepareCall(String sql) throws java.sql.SQLException {
		// SQL-String fuer spaetere Traceausgaben merken, da man an den ueber das CallableStatement
		// nicht mehr dran kommt.
		saveSqlString(sql);
		return new WrappedCallableStatement(conn.prepareCall(sql), sqlStatementInfo);
	}

	/**
	 * @see java.sql.Connection
	 * Baue Wrapperobjekt um CallableStatement und gebe dieses zurueck.
	 */
	public java.sql.CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws java.sql.SQLException {
		// SQL-String fuer spaetere Traceausgaben merken, da man an den ueber das CallableStatement
		// nicht mehr dran kommt.
		saveSqlString(sql);
		return new WrappedCallableStatement(conn.prepareCall(sql, resultSetType, resultSetConcurrency), sqlStatementInfo);
	}

	/**
	 * @see java.sql.Connection
	 * Baue Wrapperobjekt um PreparedStatement und gebe dieses zurueck.
	 */
	public java.sql.PreparedStatement prepareStatement(String sql) throws java.sql.SQLException {
		// SQL-String fuer spaetere Traceausgaben merken, da man an den ueber das CallableStatement
		// nicht mehr dran kommt.
		saveSqlString(sql);
		return new WrappedPreparedStatement(conn.prepareStatement(sql), sqlStatementInfo);
	}

	/**
	 * @see java.sql.Connection
	 * Baue Wrapperobjekt um PreparedStatement und gebe dieses zurueck.
	 */
	public java.sql.PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
		throws java.sql.SQLException {
		// SQL-String fuer spaetere Traceausgaben merken, da man an den ueber das CallableStatement
		// nicht mehr dran kommt.
		saveSqlString(sql);
		return new WrappedPreparedStatement(conn.prepareStatement(sql, resultSetType, resultSetConcurrency), sqlStatementInfo);
	}

	/**
	 * @see java.sql.Connection
	 */
	public void rollback() throws java.sql.SQLException {
		conn.rollback();
	}

	/**
	 * @see java.sql.Connection
	 */
	public void setAutoCommit(boolean autoCommit) throws java.sql.SQLException {
		conn.setAutoCommit(autoCommit);
	}

	/**
	 * @see java.sql.Connection
	 */
	public void setCatalog(String catalog) throws java.sql.SQLException {
		conn.setCatalog(catalog);
	}

	/**
	 * @see java.sql.Connection
	 */
	public void setReadOnly(boolean readOnly) throws java.sql.SQLException {
		conn.setReadOnly(readOnly);
	}

	/**
	 * @see java.sql.Connection
	 */
	public void setTransactionIsolation(int level) throws java.sql.SQLException {
		conn.setTransactionIsolation(level);
	}

	/**
	 * @see java.sql.Connection
	 */
	public void setTypeMap(Map<String, Class<?>> map) throws java.sql.SQLException {
		conn.setTypeMap(map);
	}

	/**
	 * @see SQLStatementInfo
	 */
	public void saveSqlString(String stmt) {
		sqlStatementInfo.setSqlString(stmt);
	}

	/**
	 * @see SQLStatementInfo
	 */
	public void clearParameterValues() {
		sqlStatementInfo.clearParameterValues();
	}

	/**
	 * @see SQLStatementInfo
	 */
	public String getFullSqlString() {
		return sqlStatementInfo.getFullSqlString();
	}

	/**
	 * @see SQLStatementInfo
	 */
	public String[] getFullSqlStrings() {
		return sqlStatementInfo.getFullSqlStrings();
	}

	/**
	 * @see SQLStatementInfo
	 */
	public List<Object> getParameterValues() {
		return sqlStatementInfo.getParameterValues();
	}

	/**
	 * @see SQLStatementInfo
	 */
	public String getSqlString() {
		return sqlStatementInfo.getSqlString();
	}

	/**
	 * @see SQLStatementInfo
	 */
	public boolean isFullPerformanceTrace() {
		return sqlStatementInfo.isFullPerformanceTrace();
	}

	/**
	 * @see SQLStatementInfo
	 */
	public boolean isPerformanceTrace() {
		return sqlStatementInfo.isPerformanceTrace();
	}

	/**
	 * @see SQLStatementInfo
	 */
	public boolean isSqlStatementTrace() {
		return sqlStatementInfo.isSqlStatementTrace();
	}

	/**
	 * @see SQLStatementInfo
	 */
	public void saveBatchParameterValues() {
		sqlStatementInfo.saveBatchParameterValues();
	}

	/**
	 * @see SQLStatementInfo
	 */
	public void saveParameterValue(int position, Object obj) {
		sqlStatementInfo.saveParameterValue(position, obj);		
	}

	/**
	 * @see SQLStatementInfo
	 */
	public void setFullPerformanceTrace(boolean fullPerformanceTrace) {
		sqlStatementInfo.setFullPerformanceTrace(fullPerformanceTrace);
	}

	/**
	 * @see SQLStatementInfo
	 */
	public void setPerformanceTrace(boolean performanceTrace) {
		sqlStatementInfo.setPerformanceTrace(performanceTrace);
	}

	/**
	 * @see SQLStatementInfo
	 */
	public void setSqlStatementTrace(boolean sqlStatementTrace) {
		sqlStatementInfo.setSqlStatementTrace(sqlStatementTrace);
	}

	/**
	 * @see SQLStatementInfo
	 */
	public void setSqlString(String stmt) {
		sqlStatementInfo.setSqlString(stmt);
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#setHoldability(int)
	 */
	public void setHoldability(int holdability) throws SQLException {
		conn.setHoldability(holdability);
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#getHoldability()
	 */
	public int getHoldability() throws SQLException {
		return conn.getHoldability();
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#setSavepoint()
	 */
	public Savepoint setSavepoint() throws SQLException {
		return conn.setSavepoint();
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#setSavepoint(java.lang.String)
	 */
	public Savepoint setSavepoint(String name) throws SQLException {
		return conn.setSavepoint(name);
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#rollback(java.sql.Savepoint)
	 */
	public void rollback(Savepoint savepoint) throws SQLException {
		conn.rollback(savepoint);
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#releaseSavepoint(java.sql.Savepoint)
	 */
	public void releaseSavepoint(Savepoint savepoint) throws SQLException {
		conn.releaseSavepoint(savepoint);
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#createStatement(int, int, int)
	 */
	public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		return conn.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#prepareStatement(java.lang.String, int, int, int)
	 */
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		return conn.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#prepareCall(java.lang.String, int, int, int)
	 */
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		return conn.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#prepareStatement(java.lang.String, int)
	 */
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
		return conn.prepareStatement(sql, autoGeneratedKeys);
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#prepareStatement(java.lang.String, int[])
	 */
	public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
		return conn.prepareStatement(sql, columnIndexes);
	}

	/* (non-Javadoc)
	 * @see java.sql.Connection#prepareStatement(java.lang.String, java.lang.String[])
	 */
	public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
		return conn.prepareStatement(sql, columnNames);
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		return conn.unwrap(iface);
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return conn.isWrapperFor(iface);
	}

	public Clob createClob() throws SQLException {
		return conn.createClob();
	}

	public Blob createBlob() throws SQLException {
		return conn.createBlob();
	}

	public NClob createNClob() throws SQLException {
		return conn.createNClob();
	}

	public SQLXML createSQLXML() throws SQLException {
		return conn.createSQLXML();
	}

	public boolean isValid(int timeout) throws SQLException {
		return conn.isValid(timeout);
	}

	public void setClientInfo(String name, String value)
			throws SQLClientInfoException {
		conn.setClientInfo(name, value);
	}

	public void setClientInfo(Properties properties)
			throws SQLClientInfoException {
		conn.setClientInfo(properties);
	}

	public String getClientInfo(String name) throws SQLException {
		return conn.getClientInfo(name);
	}

	public Properties getClientInfo() throws SQLException {
		return conn.getClientInfo();
	}

	public Array createArrayOf(String typeName, Object[] elements)
			throws SQLException {
		return conn.createArrayOf(typeName, elements);
	}

	public Struct createStruct(String typeName, Object[] attributes)
			throws SQLException {
		return conn.createStruct(typeName, attributes);
	}

	public void setSchema(String schema) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public String getSchema() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public void abort(Executor executor) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public void setNetworkTimeout(Executor executor, int milliseconds)
			throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public int getNetworkTimeout() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}
}