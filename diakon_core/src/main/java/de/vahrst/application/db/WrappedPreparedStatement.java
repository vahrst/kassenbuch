// $codepro.audit.disable floatingPointUse
package de.vahrst.application.db;

import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.sql.Connection;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;

import org.apache.log4j.Logger;

import de.vahrst.application.logging.TraceKonstanten;

/**
 * Wrapperklasse fuer ein PreparedStatement Objekt. Halte intern eine
 * java.sql.PreparedStatement implementierende Instanz und leite alle
 * im PreparedStatement Interface definierten Methoden durch, mit folgenden
 * Ausnahmen:
 * <ul>
 * <li>alle execute...(...) Methoden: vor und nach dem eigentlichen
 * Execute werden entsprechende Performance-Traces geschrieben.</li>
 * <li>In allen set...(...) Methoden werden die uebergebenen Parameter
 * zwischengespeichert. Die Methode getRealSqlString() liefert den echten
 * SQL-Befehl zurueck, d.h. alle Hostvariablen (Parametermarker, "?") werden durch die
 * aktuellen Inhalt ersetzt.
 * </ul>
 * <p>
 * (CVS: $Revision: 1.1 $ $Date: 2006/10/24 16:13:14 $ $Author: thomas $)
 * <p>
 * @author Guyet, Sven
 */
public class WrappedPreparedStatement extends WrappedStatement implements java.sql.PreparedStatement {
	private static Logger sqlStatementLogger = Logger.getLogger(TraceKonstanten.TRACE_SQL_STATEMENTS); 
	
	// Das von mir gewrappte PreparedStatement
	private final java.sql.PreparedStatement pStmt;

	/**
	 * Einziger Konstruktor. Merke mir das PreparedStatement Objekt, welches
	 * ich wrappe und an das ich alle Methoden durchreiche.
	 * 
	 * @param stmt PreparedStatement
	 * 		Das PreparedStatement-Objekt, welches ich wrappe
	 * @param sqlStatementInfo SQLStatementInfo
	 * 		Objekt, welches Informationen �ber mein SQL-Statement entgegen nehmen kann.
	 * @exception SQLException
	 * 		Wird geworfen, falls kein SQLStatementInfo Objekt uebergeben wurde
	 */
	protected WrappedPreparedStatement(java.sql.PreparedStatement stmt, SQLStatementInfo sqlStatementInfo) {
		super(stmt, sqlStatementInfo);
		this.pStmt = stmt;
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void addBatch() throws java.sql.SQLException {
		sqlStatementInfo.saveBatchParameterValues();
		pStmt.addBatch();
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void clearParameters() throws java.sql.SQLException {
		sqlStatementInfo.clearParameterValues();
		pStmt.clearParameters();
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public boolean execute() throws java.sql.SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return pStmt.execute();
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("PREP-SQL");
			return pStmt.execute();
		} finally {
			writePerformanceLogOnReturn("PREP-SQL", startTime);
		}
	}

	/**
	 * @see java.sql.Statement
	 */
	public int[] executeBatch() throws java.sql.SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statements tracen.
		if (isSqlStatementTrace()) {
			traceSqlBatchStatements();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return pStmt.executeBatch();
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("PREP-BATCH");
			return pStmt.executeBatch();
		} finally {
			writePerformanceLogOnReturn("PREP-BATCH", startTime);
		}
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public java.sql.ResultSet executeQuery() throws java.sql.SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return pStmt.executeQuery();
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("PREP-QUERY");
			return pStmt.executeQuery();
		} finally {
			writePerformanceLogOnReturn("PREP-QUERY", startTime);
		}
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public int executeUpdate() throws java.sql.SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return pStmt.executeUpdate();
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("PREP-UPDATE");
			return pStmt.executeUpdate();
		} finally {
			writePerformanceLogOnReturn("PREP-UPDATE", startTime);
		}
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public java.sql.ResultSetMetaData getMetaData() throws java.sql.SQLException {
		return pStmt.getMetaData();
	}

	/**
	 * Gebe den SQL-String des ausgef�hrten bzw. auszuf�hrenden SQL-Statements zur�ck.
	 * In dem String werden m�gliche Parametermarker durch die echten Hostvariablen-Werte ersetzt.
	 */
	protected String getSqlStatement() {
		return sqlStatementInfo.getFullSqlString();
	}

	/**
	 * Gebe zus�tzliche Informationen f�r Performance-Traces zur�ck, n�mlich den SQL-String des
	 * ausgef�hrten bzw. auszuf�hrenden SQL-Statements. Je nach gesetztem fullPerformanceTrace Flag
	 * werden in dem String m�gliche Parametermarker durch die echten Hostvariablen-Werte ersetzt.
	 */
	protected String getPerformanceLogInfo() {
		return isFullPerformanceTrace() ? sqlStatementInfo.getFullSqlString() : sqlStatementInfo.getSqlString();
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setArray(int i, java.sql.Array x) throws java.sql.SQLException {
		pStmt.setArray(i, x);
		sqlStatementInfo.saveParameterValue(i, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setAsciiStream(int parameterIndex, java.io.InputStream x, int length) throws java.sql.SQLException {
		pStmt.setAsciiStream(parameterIndex, x, length);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setBigDecimal(int parameterIndex, java.math.BigDecimal x) throws java.sql.SQLException {
		pStmt.setBigDecimal(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setBinaryStream(int parameterIndex, java.io.InputStream x, int length) throws java.sql.SQLException {
		pStmt.setBinaryStream(parameterIndex, x, length);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setBlob(int i, java.sql.Blob x) throws java.sql.SQLException {
		pStmt.setBlob(i, x);
		sqlStatementInfo.saveParameterValue(i, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setBoolean(int parameterIndex, boolean x) throws java.sql.SQLException {
		pStmt.setBoolean(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, new Boolean(x));
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setByte(int parameterIndex, byte x) throws java.sql.SQLException {
		pStmt.setByte(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, new Byte(x));
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setBytes(int parameterIndex, byte[] x) throws java.sql.SQLException {
		pStmt.setBytes(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setCharacterStream(int parameterIndex, java.io.Reader reader, int length) throws java.sql.SQLException {
		pStmt.setCharacterStream(parameterIndex, reader, length);
		sqlStatementInfo.saveParameterValue(parameterIndex, reader);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setClob(int i, java.sql.Clob x) throws java.sql.SQLException {
		pStmt.setClob(i, x);
		sqlStatementInfo.saveParameterValue(i, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setDate(int parameterIndex, java.sql.Date x) throws java.sql.SQLException {
		pStmt.setDate(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setDate(int parameterIndex, java.sql.Date x, java.util.Calendar cal) throws java.sql.SQLException {
		pStmt.setDate(parameterIndex, x, cal);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setDouble(int parameterIndex, double x) throws java.sql.SQLException {
		pStmt.setDouble(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, new Double(x));
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setFloat(int parameterIndex, float x) throws java.sql.SQLException {
		pStmt.setFloat(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, new Float(x));
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setInt(int parameterIndex, int x) throws java.sql.SQLException {
		pStmt.setInt(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, new Integer(x));
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setLong(int parameterIndex, long x) throws java.sql.SQLException {
		pStmt.setLong(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, new Long(x));
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setNull(int parameterIndex, int sqlType) throws java.sql.SQLException {
		pStmt.setNull(parameterIndex, sqlType);
		sqlStatementInfo.saveParameterValue(parameterIndex, null);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setNull(int paramIndex, int sqlType, String typeName) throws java.sql.SQLException {
		pStmt.setNull(paramIndex, sqlType, typeName);
		sqlStatementInfo.saveParameterValue(paramIndex, null);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setObject(int parameterIndex, Object x) throws java.sql.SQLException {
		pStmt.setObject(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setObject(int parameterIndex, Object x, int targetSqlType) throws java.sql.SQLException {
		pStmt.setObject(parameterIndex, x, targetSqlType);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setObject(int parameterIndex, Object x, int targetSqlType, int scale) throws java.sql.SQLException {
		pStmt.setObject(parameterIndex, x, targetSqlType, scale);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setRef(int i, java.sql.Ref x) throws java.sql.SQLException {
		pStmt.setRef(i, x);
		sqlStatementInfo.saveParameterValue(i, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setShort(int parameterIndex, short x) throws java.sql.SQLException {
		pStmt.setShort(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, new Short(x));
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setString(int parameterIndex, String x) throws java.sql.SQLException {
		pStmt.setString(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setTime(int parameterIndex, java.sql.Time x) throws java.sql.SQLException {
		pStmt.setTime(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setTime(int parameterIndex, java.sql.Time x, java.util.Calendar cal) throws java.sql.SQLException {
		pStmt.setTime(parameterIndex, x, cal);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setTimestamp(int parameterIndex, java.sql.Timestamp x) throws java.sql.SQLException {
		pStmt.setTimestamp(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public void setTimestamp(int parameterIndex, java.sql.Timestamp x, java.util.Calendar cal) throws java.sql.SQLException {
		pStmt.setTimestamp(parameterIndex, x, cal);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * @see java.sql.PreparedStatement
	 * Diese Methode ist in java.sql.PreparedStatement deprecated.
	 * Laut Dokumentation soll statt dessen die Methode setCharacterStream
	 * fuer Streams von Unicode Zeichen verwendet werden.
	 */
	public void setUnicodeStream(int parameterIndex, java.io.InputStream x, int length) {
		throw new UnsupportedOperationException("No more supported; use setCharacterStream instead!");
		// pStmt.setUnicodeStream(parameterIndex, x, length);
		// sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/**
	 * Hilfsroutine fuer den wiederholten Aufruf von LVMLogging.trace bei Batch-Updates.
	 * In den ausgegebenden Strings sind m�gliche Parametermarker durch die echten Hostvariablen-Werte ersetzt.
	 */
	protected void traceSqlBatchStatements() {
		String[] sqlStatements = sqlStatementInfo.getFullSqlStrings();
		for (int i = 0; i < sqlStatements.length; i++) {
			sqlStatementLogger.info(sqlStatements[i]);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.PreparedStatement#setURL(int, java.net.URL)
	 */
	public void setURL(int parameterIndex, URL x) throws SQLException {
		pStmt.setURL(parameterIndex, x);
		sqlStatementInfo.saveParameterValue(parameterIndex, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.PreparedStatement#getParameterMetaData()
	 */
	public ParameterMetaData getParameterMetaData() throws SQLException {
		return pStmt.getParameterMetaData();
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#getMoreResults(int)
	 */
	public boolean getMoreResults(int current) throws SQLException {
		return pStmt.getMoreResults(current);
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#getGeneratedKeys()
	 */
	public ResultSet getGeneratedKeys() throws SQLException {
		return pStmt.getGeneratedKeys();
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#executeUpdate(java.lang.String, int)
	 */
	public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return pStmt.executeUpdate(sql, autoGeneratedKeys);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("PREP-UPDATE");
			return pStmt.executeUpdate(sql, autoGeneratedKeys);
		} finally {
			writePerformanceLogOnReturn("PREP-UPDATE", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#executeUpdate(java.lang.String, int[])
	 */
	public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return pStmt.executeUpdate(sql, columnIndexes);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("PREP-UPDATE");
			return pStmt.executeUpdate(sql, columnIndexes);
		} finally {
			writePerformanceLogOnReturn("PREP-UPDATE", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#executeUpdate(java.lang.String, java.lang.String[])
	 */
	public int executeUpdate(String sql, String[] columnNames) throws SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return pStmt.executeUpdate(sql, columnNames);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("PREP-UPDATE");
			return pStmt.executeUpdate(sql, columnNames);
		} finally {
			writePerformanceLogOnReturn("PREP-UPDATE", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#execute(java.lang.String, int)
	 */
	public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return pStmt.execute(sql, autoGeneratedKeys);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("PREP-SQL");
			return pStmt.execute(sql, autoGeneratedKeys);
		} finally {
			writePerformanceLogOnReturn("PREP-SQL", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#execute(java.lang.String, int[])
	 */
	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return pStmt.execute(sql, columnIndexes);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("PREP-SQL");
			return pStmt.execute(sql, columnIndexes);
		} finally {
			writePerformanceLogOnReturn("PREP-SQL", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#execute(java.lang.String, java.lang.String[])
	 */
	public boolean execute(String sql, String[] columnNames) throws SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return pStmt.execute(sql, columnNames);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("PREP-SQL");
			return pStmt.execute(sql, columnNames);
		} finally {
			writePerformanceLogOnReturn("PREP-SQL", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#getResultSetHoldability()
	 */
	public int getResultSetHoldability() throws SQLException {
		return pStmt.getResultSetHoldability();
	}

	public ResultSet executeQuery(String sql) throws SQLException {
		return pStmt.executeQuery(sql);
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		return pStmt.unwrap(iface);
	}

	public int executeUpdate(String sql) throws SQLException {
		return pStmt.executeUpdate(sql);
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return pStmt.isWrapperFor(iface);
	}

	public void close() throws SQLException {
		pStmt.close();
	}

	public int getMaxFieldSize() throws SQLException {
		return pStmt.getMaxFieldSize();
	}

	public void setMaxFieldSize(int max) throws SQLException {
		pStmt.setMaxFieldSize(max);
	}

	public int getMaxRows() throws SQLException {
		return pStmt.getMaxRows();
	}

	public void setMaxRows(int max) throws SQLException {
		pStmt.setMaxRows(max);
	}

	public void setEscapeProcessing(boolean enable) throws SQLException {
		pStmt.setEscapeProcessing(enable);
	}

	public int getQueryTimeout() throws SQLException {
		return pStmt.getQueryTimeout();
	}

	public void setQueryTimeout(int seconds) throws SQLException {
		pStmt.setQueryTimeout(seconds);
	}

	public void cancel() throws SQLException {
		pStmt.cancel();
	}

	public SQLWarning getWarnings() throws SQLException {
		return pStmt.getWarnings();
	}

	public void clearWarnings() throws SQLException {
		pStmt.clearWarnings();
	}

	public void setCursorName(String name) throws SQLException {
		pStmt.setCursorName(name);
	}

	public boolean execute(String sql) throws SQLException {
		return pStmt.execute(sql);
	}

	public ResultSet getResultSet() throws SQLException {
		return pStmt.getResultSet();
	}

	public int getUpdateCount() throws SQLException {
		return pStmt.getUpdateCount();
	}

	public boolean getMoreResults() throws SQLException {
		return pStmt.getMoreResults();
	}

	public void setFetchDirection(int direction) throws SQLException {
		pStmt.setFetchDirection(direction);
	}

	public int getFetchDirection() throws SQLException {
		return pStmt.getFetchDirection();
	}

	public void setFetchSize(int rows) throws SQLException {
		pStmt.setFetchSize(rows);
	}

	public int getFetchSize() throws SQLException {
		return pStmt.getFetchSize();
	}

	public int getResultSetConcurrency() throws SQLException {
		return pStmt.getResultSetConcurrency();
	}

	public int getResultSetType() throws SQLException {
		return pStmt.getResultSetType();
	}

	public void addBatch(String sql) throws SQLException {
		pStmt.addBatch(sql);
	}

	public void clearBatch() throws SQLException {
		pStmt.clearBatch();
	}

	public Connection getConnection() throws SQLException {
		return pStmt.getConnection();
	}

	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		pStmt.setRowId(parameterIndex, x);
	}

	public void setNString(int parameterIndex, String value) throws SQLException {
		pStmt.setNString(parameterIndex, value);
	}

	public void setNCharacterStream(int parameterIndex, Reader value, long length)
			throws SQLException {
		pStmt.setNCharacterStream(parameterIndex, value, length);
	}

	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		pStmt.setNClob(parameterIndex, value);
	}

	public void setClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		pStmt.setClob(parameterIndex, reader, length);
	}

	public void setBlob(int parameterIndex, InputStream inputStream, long length)
			throws SQLException {
		pStmt.setBlob(parameterIndex, inputStream, length);
	}

	public boolean isClosed() throws SQLException {
		return pStmt.isClosed();
	}

	public void setPoolable(boolean poolable) throws SQLException {
		pStmt.setPoolable(poolable);
	}

	public void setNClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		pStmt.setNClob(parameterIndex, reader, length);
	}

	public boolean isPoolable() throws SQLException {
		return pStmt.isPoolable();
	}

	public void setSQLXML(int parameterIndex, SQLXML xmlObject)
			throws SQLException {
		pStmt.setSQLXML(parameterIndex, xmlObject);
	}

	public void setAsciiStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		pStmt.setAsciiStream(parameterIndex, x, length);
	}

	public void setBinaryStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		pStmt.setBinaryStream(parameterIndex, x, length);
	}

	public void setCharacterStream(int parameterIndex, Reader reader, long length)
			throws SQLException {
		pStmt.setCharacterStream(parameterIndex, reader, length);
	}

	public void setAsciiStream(int parameterIndex, InputStream x)
			throws SQLException {
		pStmt.setAsciiStream(parameterIndex, x);
	}

	public void setBinaryStream(int parameterIndex, InputStream x)
			throws SQLException {
		pStmt.setBinaryStream(parameterIndex, x);
	}

	public void setCharacterStream(int parameterIndex, Reader reader)
			throws SQLException {
		pStmt.setCharacterStream(parameterIndex, reader);
	}

	public void setNCharacterStream(int parameterIndex, Reader value)
			throws SQLException {
		pStmt.setNCharacterStream(parameterIndex, value);
	}

	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		pStmt.setClob(parameterIndex, reader);
	}

	public void setBlob(int parameterIndex, InputStream inputStream)
			throws SQLException {
		pStmt.setBlob(parameterIndex, inputStream);
	}

	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		pStmt.setNClob(parameterIndex, reader);
	}
}