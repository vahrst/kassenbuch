package de.vahrst.application.db;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import de.vahrst.application.logging.LoggingHelper;
import de.vahrst.application.logging.TraceKonstanten;

/**
 * Wrapperklasse fuer ein Statement Objekt. Halte intern eine
 * java.sql.Statement implementierende Instanz und leite alle
 * im Statement Interface definierten Methoden durch, mit folgenden
 * Ausnahmen:
 * <ul>
 * <li>alle execute...(...) Methoden: vor und nach dem eigentlichen
 * Execute werden entsprechende Performance-Traces geschrieben.</li>
 * </ul>
 * <p>
 * (CVS: $Revision: 1.1 $ $Date: 2006/10/24 16:13:14 $ $Author: thomas $)
 * <p>
 * @author Guyet, Sven
 */
class WrappedStatement implements java.sql.Statement {
	private static Logger sqlStatementLogger = Logger.getLogger(TraceKonstanten.TRACE_SQL_STATEMENTS); 
	private static Logger sqlPerformanceLogger = Logger.getLogger(TraceKonstanten.TRACE_SQL_PERFORMANCE); 

	// Das von mir gewrappte Statement
	private final java.sql.Statement stmt;

	// SQL-Statement-Info
	protected SQLStatementInfo sqlStatementInfo = null;

	/**
	 * Einziger Konstruktor. Merke mir das Statement Objekt, welches
	 * ich wrappe und an das ich alle Methoden durchreiche.
	 *
	 * @param stmt Statement
	 * 		Das Statement-Objekt, welches ich wrappe
	 * @param sqlStatementInfo SQLStatementInfo
	 * 		Objekt, welches Informationen �ber mein SQL-Statement entgegen nehmen kann.
	 * @exception SQLException
	 * 		Wird geworfen, falls kein SQLStatementInfo Objekt uebergeben wurde
	 */
	protected WrappedStatement(java.sql.Statement stmt, SQLStatementInfo sqlStatementInfo) {
		super();
		this.stmt = stmt;
		this.sqlStatementInfo = sqlStatementInfo;
		this.sqlStatementInfo.clearParameterValues(); // Vahrst: Batch-Liste zur�cksetzen???
	}

	/**
	 * @see java.sql.Statement
	 */
	public void addBatch(String sql) throws java.sql.SQLException {
		// SQL-String f�r evtl. Errorlogging merken 
		sqlStatementInfo.setSqlString(sql);
		stmt.addBatch(sql);
	}

	/**
	 * @see java.sql.Statement
	 */
	public void cancel() throws java.sql.SQLException {
		stmt.cancel();
	}

	/**
	 * @see java.sql.Statement
	 */
	public void clearBatch() throws java.sql.SQLException {
		stmt.clearBatch();
	}

	/**
	 * @see java.sql.Statement
	 */
	public void clearWarnings() throws java.sql.SQLException {
		stmt.clearWarnings();
	}

	/**
	 * @see java.sql.Statement
	 */
	public void close() throws java.sql.SQLException {
		stmt.close(); // $codepro.audit.disable closeInFinally
	}

	/**
	 * @see java.sql.Statement
	 */
	public boolean execute(String sql) throws java.sql.SQLException {
		// SQL-String f�r evtl. Errorlogging merken 
		sqlStatementInfo.setSqlString(sql);
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return stmt.execute(sql);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("SQL");
			return stmt.execute(sql);
		} finally {
			writePerformanceLogOnReturn("SQL", startTime);
		}
	}

	/**
	 * @see java.sql.Statement
	 */
	public int[] executeBatch() throws java.sql.SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return stmt.executeBatch();
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("BATCH");
			return stmt.executeBatch();
		} finally {
			writePerformanceLogOnReturn("BATCH", startTime);
		}
	}

	/**
	 * @see java.sql.Statement
	 */
	public java.sql.ResultSet executeQuery(String sql) throws java.sql.SQLException {
		// SQL-String f�r evtl. Errorlogging merken 
		sqlStatementInfo.setSqlString(sql);
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return stmt.executeQuery(sql);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("QUERY");
			return stmt.executeQuery(sql);
		} finally {
			writePerformanceLogOnReturn("QUERY", startTime);
		}
	}

	/**
	 * @see java.sql.Statement
	 */
	public int executeUpdate(String sql) throws java.sql.SQLException {
		// SQL-String f�r evtl. Errorlogging merken 
		sqlStatementInfo.setSqlString(sql);
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return stmt.executeUpdate(sql);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("UPDATE");
			return stmt.executeUpdate(sql);
		} finally {
			writePerformanceLogOnReturn("UPDATE", startTime);
		}
	}

	/**
	 * @see java.sql.Statement
	 */
	public java.sql.Connection getConnection() throws java.sql.SQLException {
		return stmt.getConnection();
	}

	/**
	 * @see java.sql.Statement
	 */
	public int getFetchDirection() throws java.sql.SQLException {
		return stmt.getFetchDirection();
	}

	/**
	 * @see java.sql.Statement
	 */
	public int getFetchSize() throws java.sql.SQLException {
		return stmt.getFetchSize();
	}

	/**
	 * @see java.sql.Statement
	 */
	public int getMaxFieldSize() throws java.sql.SQLException {
		return stmt.getMaxFieldSize();
	}

	/**
	 * @see java.sql.Statement
	 */
	public int getMaxRows() throws java.sql.SQLException {
		return stmt.getMaxRows();
	}

	/**
	 * @see java.sql.Statement
	 */
	public boolean getMoreResults() throws java.sql.SQLException {
		return stmt.getMoreResults();
	}

	/**
	 * Hilfsroutine fuer den Aufruf von LVMLogging.writePerformanceLogOnReturn
	 */
	protected String getPerformanceLogReturnInfo() throws SQLException {
		return (getWarnings() == null) ? "Ok" : getWarnings().getSQLState();
	}

	/**
	 * @see java.sql.Statement
	 */
	public int getQueryTimeout() throws java.sql.SQLException {
		return stmt.getQueryTimeout();
	}

	/**
	 * @see java.sql.Statement
	 */
	public java.sql.ResultSet getResultSet() throws java.sql.SQLException {
		return stmt.getResultSet();
	}

	/**
	 * @see java.sql.Statement
	 */
	public int getResultSetConcurrency() throws java.sql.SQLException {
		return stmt.getResultSetConcurrency();
	}

	/**
	 * @see java.sql.Statement
	 */
	public int getResultSetType() throws java.sql.SQLException {
		return stmt.getResultSetType();
	}

	/**
	 * @see java.sql.Statement
	 */
	public int getUpdateCount() throws java.sql.SQLException {
		return stmt.getUpdateCount();
	}

	/**
	 * @see java.sql.Statement
	 */
	public java.sql.SQLWarning getWarnings() throws java.sql.SQLException {
		return stmt.getWarnings();
	}

	/**
	 * Gebe zur�ck, ob PeformanceTraces bei meiner Ausf�hrung geschrieben werden sollen.
	 * 
	 * @return boolean
	 * 		true, falls PerformanceTraces bei meiner Ausf�hrung geschrieben werden sollen.
	 */
	protected boolean isPerformanceTrace() {
		return sqlStatementInfo.isPerformanceTrace();
	}

	/**
	 * Gebe zur�ck, ob in PeformanceTraces das komplette SQL-Statement enthalten sein soll.
	 * 
	 * @return boolean
	 * 		true, falls in PerformanceTraces das komplette SQL-Statement enthalten sein soll.
	 */
	protected boolean isFullPerformanceTrace() {
		return sqlStatementInfo.isFullPerformanceTrace();
	}

	/**
	 * Gebe zur�ck, ob SQL-Statements bei meiner Ausf�hrung getraced werden sollen.
	 * 
	 * @return boolean
	 * 		true, falls SQL-Statements bei meiner Ausf�hrung getraced werden sollen.
	 */
	protected boolean isSqlStatementTrace() {
		return sqlStatementInfo.isSqlStatementTrace();
	}

	/**
	 * @see java.sql.Statement
	 */
	public void setCursorName(String name) throws java.sql.SQLException {
		stmt.setCursorName(name);
	}

	/**
	 * @see java.sql.Statement
	 */
	public void setEscapeProcessing(boolean enable) throws java.sql.SQLException {
		stmt.setEscapeProcessing(enable);
	}

	/**
	 * @see java.sql.Statement
	 */
	public void setFetchDirection(int direction) throws java.sql.SQLException {
		stmt.setFetchDirection(direction);
	}

	/**
	 * @see java.sql.Statement
	 */
	public void setFetchSize(int rows) throws java.sql.SQLException {
		stmt.setFetchSize(rows);
	}

	/**
	 * @see java.sql.Statement
	 */
	public void setMaxFieldSize(int max) throws java.sql.SQLException {
		stmt.setMaxFieldSize(max);
	}

	/**
	 * @see java.sql.Statement
	 */
	public void setMaxRows(int max) throws java.sql.SQLException {
		stmt.setMaxRows(max);
	}

	/**
	 * @see java.sql.Statement
	 */
	public void setQueryTimeout(int seconds) throws java.sql.SQLException {
		stmt.setQueryTimeout(seconds);
	}

	/**
	 * Gebe den SQL-String des ausgef�hrten bzw. auszuf�hrenden SQL-Statements zur�ck.
	 */
	protected String getSqlStatement() {
		return sqlStatementInfo.getSqlString();
	}

	/**
	 * Gebe zus�tzliche Informationen f�r Performance-Traces zur�ck, n�mlich den SQL-String des
	 * ausgef�hrten bzw. auszuf�hrenden SQL-Statements.
	 */
	protected String getPerformanceLogInfo() {
		return getSqlStatement();
	}

	/**
	 * Hilfsroutine fuer den Aufruf von LVMLogging.trace
	 */
	protected void traceSqlStatement() {
		sqlStatementLogger.info(getSqlStatement());
	}

	/**
	 * Hilfsroutine fuer den Aufruf von LVMLogging.writePerformanceLogOnCall
	 */
	protected long writePerformanceLogOnCall(String target) {
		return LoggingHelper.writePerformanceLogOnCall(sqlPerformanceLogger, getPerformanceLogInfo());
	}

	/**
	 * Hilfsroutine fuer den Aufruf von LVMLogging.writePerformanceLogOnReturn
	 */
	protected void writePerformanceLogOnReturn(String target, long startTime) throws SQLException {
		LoggingHelper.writePerformanceLogOnReturn(sqlPerformanceLogger, getPerformanceLogInfo(), startTime);
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#getMoreResults(int)
	 */
	public boolean getMoreResults(int current) throws SQLException {
		return stmt.getMoreResults(current);
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#getGeneratedKeys()
	 */
	public ResultSet getGeneratedKeys() throws SQLException {
		return stmt.getGeneratedKeys();
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#executeUpdate(java.lang.String, int)
	 */
	public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		// SQL-String f�r evtl. Errorlogging merken 
		sqlStatementInfo.setSqlString(sql);
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return stmt.executeUpdate(sql, autoGeneratedKeys);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("UPDATE");
			return stmt.executeUpdate(sql, autoGeneratedKeys);
		} finally {
			writePerformanceLogOnReturn("UPDATE", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#executeUpdate(java.lang.String, int[])
	 */
	public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
		// SQL-String f�r evtl. Errorlogging merken 
		sqlStatementInfo.setSqlString(sql);
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return stmt.executeUpdate(sql, columnIndexes);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("UPDATE");
			return stmt.executeUpdate(sql, columnIndexes);
		} finally {
			writePerformanceLogOnReturn("UPDATE", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#executeUpdate(java.lang.String, java.lang.String[])
	 */
	public int executeUpdate(String sql, String[] columnNames) throws SQLException {
		// SQL-String f�r evtl. Errorlogging merken 
		sqlStatementInfo.setSqlString(sql);
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return stmt.executeUpdate(sql, columnNames);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("UPDATE");
			return stmt.executeUpdate(sql, columnNames);
		} finally {
			writePerformanceLogOnReturn("UPDATE", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#execute(java.lang.String, int)
	 */
	public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
		// SQL-String f�r evtl. Errorlogging merken 
		sqlStatementInfo.setSqlString(sql);
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return stmt.execute(sql, autoGeneratedKeys);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("SQL");
			return stmt.execute(sql, autoGeneratedKeys);
		} finally {
			writePerformanceLogOnReturn("SQL", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#execute(java.lang.String, int[])
	 */
	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		// SQL-String f�r evtl. Errorlogging merken 
		sqlStatementInfo.setSqlString(sql);
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return stmt.execute(sql, columnIndexes);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("SQL");
			return stmt.execute(sql, columnIndexes);
		} finally {
			writePerformanceLogOnReturn("SQL", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#execute(java.lang.String, java.lang.String[])
	 */
	public boolean execute(String sql, String[] columnNames) throws SQLException {
		// SQL-String f�r evtl. Errorlogging merken 
		sqlStatementInfo.setSqlString(sql);
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return stmt.execute(sql, columnNames);
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("SQL");
			return stmt.execute(sql, columnNames);
		} finally {
			writePerformanceLogOnReturn("SQL", startTime);
		}
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#getResultSetHoldability()
	 */
	public int getResultSetHoldability() throws SQLException {
		return stmt.getResultSetHoldability();
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		return stmt.unwrap(iface);
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return stmt.isWrapperFor(iface);
	}

	public boolean isClosed() throws SQLException {
		return stmt.isClosed();
	}

	public void setPoolable(boolean poolable) throws SQLException {
		stmt.setPoolable(poolable);
	}

	public boolean isPoolable() throws SQLException {
		return stmt.isPoolable();
	}

	public void closeOnCompletion() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public boolean isCloseOnCompletion() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
}