// $codepro.audit.disable floatingPointUse
package de.vahrst.application.db;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

/**
 * Wrapperklasse fuer ein CallableStatement Objekt. Halte intern eine
 * java.sql.CallableStatement implementierende Instanz und leite alle
 * im CallableStatement Interface definierten Methoden durch, mit folgenden
 * Ausnahmen:
 * <ul>
 * <li>alle execute...(...) Methoden: vor und nach dem eigentlichen
 * Execute werden entsprechende Performance-Traces geschrieben.</li>
 * </ul>
 * <p>
 * (CVS: $Revision: 1.1 $ $Date: 2006/10/24 16:13:14 $ $Author: thomas $)
 * <p>
 * @author Guyet, Sven
 */
public class WrappedCallableStatement extends WrappedPreparedStatement implements java.sql.CallableStatement {
	// Das von mir gewrappte CallableStatement
	private final java.sql.CallableStatement cStmt;

	/**
	 * Einziger Konstruktor. Merke mir das CallableStatement Objekt, welches
	 * ich wrappe und an das ich alle Methoden durchreiche.
	 *
	 * @param stmt CallableStatement
	 * 		Das CallableStatement-Objekt, welches ich wrappe
	 * @param sqlStatementInfo SQLStatementInfo
	 * 		Objekt, welches Informationen �ber mein SQL-Statement entgegen nehmen kann.
	 * @exception SQLException
	 * 		Wird geworfen, falls kein SQLStatementInfo Objekt uebergeben wurde
	 */
	protected WrappedCallableStatement(java.sql.CallableStatement stmt, SQLStatementInfo sqlStatementInfo) {
		super(stmt, sqlStatementInfo);
		this.cStmt = stmt;
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public boolean execute() throws java.sql.SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return cStmt.execute();
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("CALL-SQL");
			return cStmt.execute();
		} finally {
			writePerformanceLogOnReturn("CALL-SQL", startTime);
		}
	}

	/**
	 * @see java.sql.Statement
	 */
	public int[] executeBatch() throws java.sql.SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return cStmt.executeBatch();
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("CALL-BATCH");
			return cStmt.executeBatch();
		} finally {
			writePerformanceLogOnReturn("CALL-BATCH", startTime);
		}
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public java.sql.ResultSet executeQuery() throws java.sql.SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return cStmt.executeQuery();
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("CALL-QUERY");
			return cStmt.executeQuery();
		} finally {
			writePerformanceLogOnReturn("CALL-QUERY", startTime);
		}
	}

	/**
	 * @see java.sql.PreparedStatement
	 */
	public int executeUpdate() throws java.sql.SQLException {
		// Falls SQL-Statement-Trace-Ausgaben, Statement tracen.
		if (isSqlStatementTrace()) {
			traceSqlStatement();
		}
		// Falls keine Performance-Trace-Ausgaben, einfach Statement ausf�hren.
		// Ansonsten vor und nach dem Call Performance-Traces schreiben
		if (!isPerformanceTrace()) {
			return cStmt.executeUpdate();
		}
		long startTime = 0L;
		try {
			startTime = writePerformanceLogOnCall("CALL-UPDATE");
			return cStmt.executeUpdate();
		} finally {
			writePerformanceLogOnReturn("CALL-UPDATE", startTime);
		}
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public java.sql.Array getArray(int i) throws java.sql.SQLException {
		return cStmt.getArray(i);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public java.math.BigDecimal getBigDecimal(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getBigDecimal(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 * Diese Methode ist in java.sql.CallableStatement deprecated.
	 * Laut Dokumentation soll bei Verwendung eines JDBC 2.0 Drivers (bzw. spaeterer)
	 * statt dessen der Service getBigDecimal() ohne den Scale-Parameter
	 * verwendet werden. Dieser liefert ein BigDecimal mit voller Genauigkeit
	 * zurueck. Sollte innerhalb einer Anwendung Bedarf fuer eine Aenderung
	 * der Nachkommastellenanzahl bestehen, kann die Methode
	 * java.math.BigDecimal.setScale(int scale, int roundingMode)
	 * verwendet werden.
	 */
	public java.math.BigDecimal getBigDecimal(int parameterIndex, int scale) {
		throw new UnsupportedOperationException("No more supported since JDBC 2.0; use getBigDecimal(int) instead!");
		// return cStmt.getBigDecimal(parameterIndex, scale);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public java.sql.Blob getBlob(int i) throws java.sql.SQLException {
		return cStmt.getBlob(i);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public boolean getBoolean(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getBoolean(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public byte getByte(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getByte(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public byte[] getBytes(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getBytes(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public java.sql.Clob getClob(int i) throws java.sql.SQLException {
		return cStmt.getClob(i);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public java.sql.Date getDate(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getDate(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public java.sql.Date getDate(int parameterIndex, java.util.Calendar cal) throws java.sql.SQLException {
		return cStmt.getDate(parameterIndex, cal);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public double getDouble(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getDouble(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public float getFloat(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getFloat(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public int getInt(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getInt(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public long getLong(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getLong(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public Object getObject(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getObject(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public Object getObject(int i, Map<String, Class<?>> map) throws java.sql.SQLException {
		return cStmt.getObject(i, map);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public java.sql.Ref getRef(int i) throws java.sql.SQLException {
		return cStmt.getRef(i);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public short getShort(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getShort(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public String getString(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getString(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public java.sql.Time getTime(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getTime(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public java.sql.Time getTime(int parameterIndex, java.util.Calendar cal) throws java.sql.SQLException {
		return cStmt.getTime(parameterIndex, cal);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public java.sql.Timestamp getTimestamp(int parameterIndex) throws java.sql.SQLException {
		return cStmt.getTimestamp(parameterIndex);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public java.sql.Timestamp getTimestamp(int parameterIndex, java.util.Calendar cal) throws java.sql.SQLException {
		return cStmt.getTimestamp(parameterIndex, cal);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public void registerOutParameter(int parameterIndex, int sqlType) throws java.sql.SQLException {
		cStmt.registerOutParameter(parameterIndex, sqlType);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public void registerOutParameter(int parameterIndex, int sqlType, int scale) throws java.sql.SQLException {
		cStmt.registerOutParameter(parameterIndex, sqlType, scale);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public void registerOutParameter(int paramIndex, int sqlType, String typeName) throws java.sql.SQLException {
		cStmt.registerOutParameter(paramIndex, sqlType, typeName);
	}

	/**
	 * @see java.sql.CallableStatement
	 */
	public boolean wasNull() throws java.sql.SQLException {
		return cStmt.wasNull();
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int)
	 */
	public void registerOutParameter(String parameterName, int sqlType) throws SQLException {
		cStmt.registerOutParameter(parameterName, sqlType);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int, int)
	 */
	public void registerOutParameter(String parameterName, int sqlType, int scale) throws SQLException {
		cStmt.registerOutParameter(parameterName, sqlType, scale);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int, java.lang.String)
	 */
	public void registerOutParameter(String parameterName, int sqlType, String typeName) throws SQLException {
		cStmt.registerOutParameter(parameterName, sqlType, typeName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getURL(int)
	 */
	public URL getURL(int parameterIndex) throws SQLException {
		return getURL(parameterIndex);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setURL(java.lang.String, java.net.URL)
	 */
	public void setURL(String parameterName, URL val) throws SQLException {
		cStmt.setURL(parameterName, val);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setNull(java.lang.String, int)
	 */
	public void setNull(String parameterName, int sqlType) throws SQLException {
		cStmt.setNull(parameterName, sqlType);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setBoolean(java.lang.String, boolean)
	 */
	public void setBoolean(String parameterName, boolean x) throws SQLException {
		cStmt.setBoolean(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setByte(java.lang.String, byte)
	 */
	public void setByte(String parameterName, byte x) throws SQLException {
		cStmt.setByte(parameterName, x);
		
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setShort(java.lang.String, short)
	 */
	public void setShort(String parameterName, short x) throws SQLException {
		cStmt.setShort(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setInt(java.lang.String, int)
	 */
	public void setInt(String parameterName, int x) throws SQLException {
		cStmt.setInt(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setLong(java.lang.String, long)
	 */
	public void setLong(String parameterName, long x) throws SQLException {
		cStmt.setLong(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setFloat(java.lang.String, float)
	 */
	public void setFloat(String parameterName, float x) throws SQLException {
		cStmt.setFloat(parameterName, x);
	}


	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object, int, int)
	 */
	public void setObject(String parameterName, Object x, int targetSqlType, int scale) throws SQLException {
		cStmt.setObject(parameterName, x, targetSqlType, scale);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object, int)
	 */
	public void setObject(String parameterName, Object x, int targetSqlType) throws SQLException {
		cStmt.setObject(parameterName, x, targetSqlType);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object)
	 */
	public void setObject(String parameterName, Object x) throws SQLException {
		cStmt.setObject(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setCharacterStream(java.lang.String, java.io.Reader, int)
	 */
	public void setCharacterStream(String parameterName, Reader reader, int length) throws SQLException {
		cStmt.setCharacterStream(parameterName, reader, length);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setDate(java.lang.String, java.sql.Date, java.util.Calendar)
	 */
	public void setDate(String parameterName, Date x, Calendar cal) throws SQLException {
		cStmt.setDate(parameterName, x, cal);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setTime(java.lang.String, java.sql.Time, java.util.Calendar)
	 */
	public void setTime(String parameterName, Time x, Calendar cal) throws SQLException {
		cStmt.setTime(parameterName, x, cal);
		
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setTimestamp(java.lang.String, java.sql.Timestamp, java.util.Calendar)
	 */
	public void setTimestamp(String parameterName, Timestamp x, Calendar cal) throws SQLException {
		cStmt.setTimestamp(parameterName, x, cal);
		
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setNull(java.lang.String, int, java.lang.String)
	 */
	public void setNull(String parameterName, int sqlType, String typeName) throws SQLException {
		cStmt.setNull(parameterName, sqlType, typeName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getString(java.lang.String)
	 */
	public String getString(String parameterName) throws SQLException {
		return cStmt.getString(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getBoolean(java.lang.String)
	 */
	public boolean getBoolean(String parameterName) throws SQLException {
		return cStmt.getBoolean(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getByte(java.lang.String)
	 */
	public byte getByte(String parameterName) throws SQLException {
		return cStmt.getByte(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getShort(java.lang.String)
	 */
	public short getShort(String parameterName) throws SQLException {
		return cStmt.getShort(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getInt(java.lang.String)
	 */
	public int getInt(String parameterName) throws SQLException {
		return cStmt.getInt(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getLong(java.lang.String)
	 */
	public long getLong(String parameterName) throws SQLException {
		return cStmt.getLong(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getFloat(java.lang.String)
	 */
	public float getFloat(String parameterName) throws SQLException {
		return cStmt.getFloat(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getDouble(java.lang.String)
	 */
	public double getDouble(String parameterName) throws SQLException {
		return cStmt.getDouble(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getBytes(java.lang.String)
	 */
	public byte[] getBytes(String parameterName) throws SQLException {
		return cStmt.getBytes(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getDate(java.lang.String)
	 */
	public Date getDate(String parameterName) throws SQLException {
		return cStmt.getDate(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getTime(java.lang.String)
	 */
	public Time getTime(String parameterName) throws SQLException {
		return cStmt.getTime(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getTimestamp(java.lang.String)
	 */
	public Timestamp getTimestamp(String parameterName) throws SQLException {
		return cStmt.getTimestamp(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getObject(java.lang.String)
	 */
	public Object getObject(String parameterName) throws SQLException {
		return cStmt.getObject(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getBigDecimal(java.lang.String)
	 */
	public BigDecimal getBigDecimal(String parameterName) throws SQLException {
		return cStmt.getBigDecimal(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getObject(java.lang.String, java.util.Map)
	 */
	public Object getObject(String parameterName, Map<String, Class<?>> map) throws SQLException {
		return cStmt.getObject(parameterName, map);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getRef(java.lang.String)
	 */
	public Ref getRef(String parameterName) throws SQLException {
		return cStmt.getRef(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getBlob(java.lang.String)
	 */
	public Blob getBlob(String parameterName) throws SQLException {
		return cStmt.getBlob(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getClob(java.lang.String)
	 */
	public Clob getClob(String parameterName) throws SQLException {
		return cStmt.getClob(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getArray(java.lang.String)
	 */
	public Array getArray(String parameterName) throws SQLException {
		return cStmt.getArray(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getDate(java.lang.String, java.util.Calendar)
	 */
	public Date getDate(String parameterName, Calendar cal) throws SQLException {
		return cStmt.getDate(parameterName, cal);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getTime(java.lang.String, java.util.Calendar)
	 */
	public Time getTime(String parameterName, Calendar cal) throws SQLException {
		return cStmt.getTime(parameterName, cal);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getTimestamp(java.lang.String, java.util.Calendar)
	 */
	public Timestamp getTimestamp(String parameterName, Calendar cal) throws SQLException {
		return cStmt.getTimestamp(parameterName, cal);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#getURL(java.lang.String)
	 */
	public URL getURL(String parameterName) throws SQLException {
		return cStmt.getURL(parameterName);
	}

	/* (non-Javadoc)
	 * @see java.sql.PreparedStatement#setURL(int, java.net.URL)
	 */
	public void setURL(int parameterIndex, URL x) throws SQLException {
		cStmt.setURL(parameterIndex, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.PreparedStatement#getParameterMetaData()
	 */
	public ParameterMetaData getParameterMetaData() throws SQLException {
		return cStmt.getParameterMetaData();
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#getMoreResults(int)
	 */
	public boolean getMoreResults(int current) throws SQLException {
		return cStmt.getMoreResults(current);
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#getGeneratedKeys()
	 */
	public ResultSet getGeneratedKeys() throws SQLException {
		return cStmt.getGeneratedKeys();
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#executeUpdate(java.lang.String, int)
	 */
	public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		return cStmt.executeUpdate(sql, autoGeneratedKeys);
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#executeUpdate(java.lang.String, int[])
	 */
	public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
		return cStmt.executeUpdate(sql,columnIndexes);
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#executeUpdate(java.lang.String, java.lang.String[])
	 */
	public int executeUpdate(String sql, String[] columnNames) throws SQLException {
		return cStmt.executeUpdate(sql, columnNames);
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#execute(java.lang.String, int)
	 */
	public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
		return cStmt.execute(sql, autoGeneratedKeys);
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#execute(java.lang.String, int[])
	 */
	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		return cStmt.execute(sql, columnIndexes);
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#execute(java.lang.String, java.lang.String[])
	 */
	public boolean execute(String sql, String[] columnNames) throws SQLException {
		return cStmt.execute(sql, columnNames);
	}

	/* (non-Javadoc)
	 * @see java.sql.Statement#getResultSetHoldability()
	 */
	public int getResultSetHoldability() throws SQLException {
		return cStmt.getResultSetHoldability();
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setDouble(java.lang.String, double)
	 */
	public void setDouble(String parameterName, double x) throws SQLException {
		cStmt.setDouble(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setBigDecimal(java.lang.String, java.math.BigDecimal)
	 */
	public void setBigDecimal(String parameterName, BigDecimal x) throws SQLException {
		cStmt.setBigDecimal(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setString(java.lang.String, java.lang.String)
	 */
	public void setString(String parameterName, String x) throws SQLException {
		cStmt.setString(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setBytes(java.lang.String, byte[])
	 */
	public void setBytes(String parameterName, byte[] x) throws SQLException {
		cStmt.setBytes(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setDate(java.lang.String, java.sql.Date)
	 */
	public void setDate(String parameterName, Date x) throws SQLException {
		cStmt.setDate(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setTime(java.lang.String, java.sql.Time)
	 */
	public void setTime(String parameterName, Time x) throws SQLException {
		cStmt.setTime(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setTimestamp(java.lang.String, java.sql.Timestamp)
	 */
	public void setTimestamp(String parameterName, Timestamp x) throws SQLException {
		cStmt.setTimestamp(parameterName, x);
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setAsciiStream(java.lang.String, java.io.InputStream, int)
	 */
	public void setAsciiStream(String parameterName, InputStream x, int length) throws SQLException {
		cStmt.setAsciiStream(parameterName, x, length);
		
	}

	/* (non-Javadoc)
	 * @see java.sql.CallableStatement#setBinaryStream(java.lang.String, java.io.InputStream, int)
	 */
	public void setBinaryStream(String parameterName, InputStream x, int length) throws SQLException {
		cStmt.setBinaryStream(parameterName, x, length);
		
	}

	public ResultSet executeQuery(String sql) throws SQLException {
		return cStmt.executeQuery(sql);
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		return cStmt.unwrap(iface);
	}

	public int executeUpdate(String sql) throws SQLException {
		return cStmt.executeUpdate(sql);
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return cStmt.isWrapperFor(iface);
	}

	public void close() throws SQLException {
		cStmt.close();
	}

	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		cStmt.setNull(parameterIndex, sqlType);
	}

	public int getMaxFieldSize() throws SQLException {
		return cStmt.getMaxFieldSize();
	}

	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		cStmt.setBoolean(parameterIndex, x);
	}

	public void setMaxFieldSize(int max) throws SQLException {
		cStmt.setMaxFieldSize(max);
	}

	public void setByte(int parameterIndex, byte x) throws SQLException {
		cStmt.setByte(parameterIndex, x);
	}

	public void setShort(int parameterIndex, short x) throws SQLException {
		cStmt.setShort(parameterIndex, x);
	}

	public int getMaxRows() throws SQLException {
		return cStmt.getMaxRows();
	}

	public void setInt(int parameterIndex, int x) throws SQLException {
		cStmt.setInt(parameterIndex, x);
	}

	public void setMaxRows(int max) throws SQLException {
		cStmt.setMaxRows(max);
	}

	public void setLong(int parameterIndex, long x) throws SQLException {
		cStmt.setLong(parameterIndex, x);
	}

	public void setEscapeProcessing(boolean enable) throws SQLException {
		cStmt.setEscapeProcessing(enable);
	}

	public void setFloat(int parameterIndex, float x) throws SQLException {
		cStmt.setFloat(parameterIndex, x);
	}

	public int getQueryTimeout() throws SQLException {
		return cStmt.getQueryTimeout();
	}

	public void setDouble(int parameterIndex, double x) throws SQLException {
		cStmt.setDouble(parameterIndex, x);
	}

	public void setQueryTimeout(int seconds) throws SQLException {
		cStmt.setQueryTimeout(seconds);
	}

	public void setBigDecimal(int parameterIndex, BigDecimal x)
			throws SQLException {
		cStmt.setBigDecimal(parameterIndex, x);
	}

	public void cancel() throws SQLException {
		cStmt.cancel();
	}

	public void setString(int parameterIndex, String x) throws SQLException {
		cStmt.setString(parameterIndex, x);
	}

	public SQLWarning getWarnings() throws SQLException {
		return cStmt.getWarnings();
	}

	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		cStmt.setBytes(parameterIndex, x);
	}

	public void clearWarnings() throws SQLException {
		cStmt.clearWarnings();
	}

	public void setDate(int parameterIndex, Date x) throws SQLException {
		cStmt.setDate(parameterIndex, x);
	}

	public void setCursorName(String name) throws SQLException {
		cStmt.setCursorName(name);
	}

	public void setTime(int parameterIndex, Time x) throws SQLException {
		cStmt.setTime(parameterIndex, x);
	}

	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
		cStmt.setTimestamp(parameterIndex, x);
	}

	public boolean execute(String sql) throws SQLException {
		return cStmt.execute(sql);
	}

	public void setAsciiStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		cStmt.setAsciiStream(parameterIndex, x, length);
	}

	public ResultSet getResultSet() throws SQLException {
		return cStmt.getResultSet();
	}


	public int getUpdateCount() throws SQLException {
		return cStmt.getUpdateCount();
	}

	public boolean getMoreResults() throws SQLException {
		return cStmt.getMoreResults();
	}

	public void setBinaryStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		cStmt.setBinaryStream(parameterIndex, x, length);
	}

	public void setFetchDirection(int direction) throws SQLException {
		cStmt.setFetchDirection(direction);
	}

	public void clearParameters() throws SQLException {
		cStmt.clearParameters();
	}

	public int getFetchDirection() throws SQLException {
		return cStmt.getFetchDirection();
	}

	public void setObject(int parameterIndex, Object x, int targetSqlType)
			throws SQLException {
		cStmt.setObject(parameterIndex, x, targetSqlType);
	}

	public void setFetchSize(int rows) throws SQLException {
		cStmt.setFetchSize(rows);
	}

	public int getFetchSize() throws SQLException {
		return cStmt.getFetchSize();
	}

	public void setObject(int parameterIndex, Object x) throws SQLException {
		cStmt.setObject(parameterIndex, x);
	}

	public int getResultSetConcurrency() throws SQLException {
		return cStmt.getResultSetConcurrency();
	}

	public int getResultSetType() throws SQLException {
		return cStmt.getResultSetType();
	}

	public void addBatch(String sql) throws SQLException {
		cStmt.addBatch(sql);
	}

	public void clearBatch() throws SQLException {
		cStmt.clearBatch();
	}

	public void addBatch() throws SQLException {
		cStmt.addBatch();
	}

	public void setCharacterStream(int parameterIndex, Reader reader, int length)
			throws SQLException {
		cStmt.setCharacterStream(parameterIndex, reader, length);
	}

	public void setRef(int parameterIndex, Ref x) throws SQLException {
		cStmt.setRef(parameterIndex, x);
	}

	public Connection getConnection() throws SQLException {
		return cStmt.getConnection();
	}

	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		cStmt.setBlob(parameterIndex, x);
	}

	public void setClob(int parameterIndex, Clob x) throws SQLException {
		cStmt.setClob(parameterIndex, x);
	}

	public void setArray(int parameterIndex, Array x) throws SQLException {
		cStmt.setArray(parameterIndex, x);
	}

	public ResultSetMetaData getMetaData() throws SQLException {
		return cStmt.getMetaData();
	}

	public void setDate(int parameterIndex, Date x, Calendar cal)
			throws SQLException {
		cStmt.setDate(parameterIndex, x, cal);
	}

	public void setTime(int parameterIndex, Time x, Calendar cal)
			throws SQLException {
		cStmt.setTime(parameterIndex, x, cal);
	}

	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal)
			throws SQLException {
		cStmt.setTimestamp(parameterIndex, x, cal);
	}

	public void setNull(int parameterIndex, int sqlType, String typeName)
			throws SQLException {
		cStmt.setNull(parameterIndex, sqlType, typeName);
	}

	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		cStmt.setRowId(parameterIndex, x);
	}

	public void setNString(int parameterIndex, String value) throws SQLException {
		cStmt.setNString(parameterIndex, value);
	}

	public void setNCharacterStream(int parameterIndex, Reader value, long length)
			throws SQLException {
		cStmt.setNCharacterStream(parameterIndex, value, length);
	}

	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		cStmt.setNClob(parameterIndex, value);
	}

	public void setClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		cStmt.setClob(parameterIndex, reader, length);
	}

	public void setBlob(int parameterIndex, InputStream inputStream, long length)
			throws SQLException {
		cStmt.setBlob(parameterIndex, inputStream, length);
	}

	public boolean isClosed() throws SQLException {
		return cStmt.isClosed();
	}

	public void setPoolable(boolean poolable) throws SQLException {
		cStmt.setPoolable(poolable);
	}

	public void setNClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		cStmt.setNClob(parameterIndex, reader, length);
	}

	public boolean isPoolable() throws SQLException {
		return cStmt.isPoolable();
	}

	public void setSQLXML(int parameterIndex, SQLXML xmlObject)
			throws SQLException {
		cStmt.setSQLXML(parameterIndex, xmlObject);
	}

	public void setObject(int parameterIndex, Object x, int targetSqlType,
			int scaleOrLength) throws SQLException {
		cStmt.setObject(parameterIndex, x, targetSqlType, scaleOrLength);
	}

	public void setAsciiStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		cStmt.setAsciiStream(parameterIndex, x, length);
	}

	public void setBinaryStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		cStmt.setBinaryStream(parameterIndex, x, length);
	}

	public void setCharacterStream(int parameterIndex, Reader reader, long length)
			throws SQLException {
		cStmt.setCharacterStream(parameterIndex, reader, length);
	}

	public void setAsciiStream(int parameterIndex, InputStream x)
			throws SQLException {
		cStmt.setAsciiStream(parameterIndex, x);
	}

	public void setBinaryStream(int parameterIndex, InputStream x)
			throws SQLException {
		cStmt.setBinaryStream(parameterIndex, x);
	}

	public void setCharacterStream(int parameterIndex, Reader reader)
			throws SQLException {
		cStmt.setCharacterStream(parameterIndex, reader);
	}

	public void setNCharacterStream(int parameterIndex, Reader value)
			throws SQLException {
		cStmt.setNCharacterStream(parameterIndex, value);
	}

	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		cStmt.setClob(parameterIndex, reader);
	}

	public void setBlob(int parameterIndex, InputStream inputStream)
			throws SQLException {
		cStmt.setBlob(parameterIndex, inputStream);
	}

	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		cStmt.setNClob(parameterIndex, reader);
	}

	public RowId getRowId(int parameterIndex) throws SQLException {
		return cStmt.getRowId(parameterIndex);
	}

	public RowId getRowId(String parameterName) throws SQLException {
		return cStmt.getRowId(parameterName);
	}

	public void setRowId(String parameterName, RowId x) throws SQLException {
		cStmt.setRowId(parameterName, x);
	}

	public void setNString(String parameterName, String value)
			throws SQLException {
		cStmt.setNString(parameterName, value);
	}

	public void setNCharacterStream(String parameterName, Reader value,
			long length) throws SQLException {
		cStmt.setNCharacterStream(parameterName, value, length);
	}

	public void setNClob(String parameterName, NClob value) throws SQLException {
		cStmt.setNClob(parameterName, value);
	}

	public void setClob(String parameterName, Reader reader, long length)
			throws SQLException {
		cStmt.setClob(parameterName, reader, length);
	}

	public void setBlob(String parameterName, InputStream inputStream, long length)
			throws SQLException {
		cStmt.setBlob(parameterName, inputStream, length);
	}

	public void setNClob(String parameterName, Reader reader, long length)
			throws SQLException {
		cStmt.setNClob(parameterName, reader, length);
	}

	public NClob getNClob(int parameterIndex) throws SQLException {
		return cStmt.getNClob(parameterIndex);
	}

	public NClob getNClob(String parameterName) throws SQLException {
		return cStmt.getNClob(parameterName);
	}

	public void setSQLXML(String parameterName, SQLXML xmlObject)
			throws SQLException {
		cStmt.setSQLXML(parameterName, xmlObject);
	}

	public SQLXML getSQLXML(int parameterIndex) throws SQLException {
		return cStmt.getSQLXML(parameterIndex);
	}

	public SQLXML getSQLXML(String parameterName) throws SQLException {
		return cStmt.getSQLXML(parameterName);
	}

	public String getNString(int parameterIndex) throws SQLException {
		return cStmt.getNString(parameterIndex);
	}

	public String getNString(String parameterName) throws SQLException {
		return cStmt.getNString(parameterName);
	}

	public Reader getNCharacterStream(int parameterIndex) throws SQLException {
		return cStmt.getNCharacterStream(parameterIndex);
	}

	public Reader getNCharacterStream(String parameterName) throws SQLException {
		return cStmt.getNCharacterStream(parameterName);
	}

	public Reader getCharacterStream(int parameterIndex) throws SQLException {
		return cStmt.getCharacterStream(parameterIndex);
	}

	public Reader getCharacterStream(String parameterName) throws SQLException {
		return cStmt.getCharacterStream(parameterName);
	}

	public void setBlob(String parameterName, Blob x) throws SQLException {
		cStmt.setBlob(parameterName, x);
	}

	public void setClob(String parameterName, Clob x) throws SQLException {
		cStmt.setClob(parameterName, x);
	}

	public void setAsciiStream(String parameterName, InputStream x, long length)
			throws SQLException {
		cStmt.setAsciiStream(parameterName, x, length);
	}

	public void setBinaryStream(String parameterName, InputStream x, long length)
			throws SQLException {
		cStmt.setBinaryStream(parameterName, x, length);
	}

	public void setCharacterStream(String parameterName, Reader reader,
			long length) throws SQLException {
		cStmt.setCharacterStream(parameterName, reader, length);
	}

	public void setAsciiStream(String parameterName, InputStream x)
			throws SQLException {
		cStmt.setAsciiStream(parameterName, x);
	}

	public void setBinaryStream(String parameterName, InputStream x)
			throws SQLException {
		cStmt.setBinaryStream(parameterName, x);
	}

	public void setCharacterStream(String parameterName, Reader reader)
			throws SQLException {
		cStmt.setCharacterStream(parameterName, reader);
	}

	public void setNCharacterStream(String parameterName, Reader value)
			throws SQLException {
		cStmt.setNCharacterStream(parameterName, value);
	}

	public void setClob(String parameterName, Reader reader) throws SQLException {
		cStmt.setClob(parameterName, reader);
	}

	public void setBlob(String parameterName, InputStream inputStream)
			throws SQLException {
		cStmt.setBlob(parameterName, inputStream);
	}

	public void setNClob(String parameterName, Reader reader) throws SQLException {
		cStmt.setNClob(parameterName, reader);
	}

	public void closeOnCompletion() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public boolean isCloseOnCompletion() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	public <T> T getObject(int parameterIndex, Class<T> type) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T getObject(String parameterName, Class<T> type)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}