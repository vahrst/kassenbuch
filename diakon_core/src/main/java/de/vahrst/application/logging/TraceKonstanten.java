package de.vahrst.application.logging;

public interface TraceKonstanten {
	public static final String TRACE_SQL_STATEMENTS = "TRACE.SQL_STATEMENTS";
	public static final String TRACE_SQL_PERFORMANCE = "TRACE.SQL_PERFORMANCE";
	
}
