package de.vahrst.application.logging;

import org.apache.log4j.Logger;

public class LoggingHelper {

	public static long writePerformanceLogOnCall(Logger logger , String logInfo){
		long current = System.currentTimeMillis();
		
		logger.info("CALL   " + logInfo);
		
		return current;
	}
	public static void writePerformanceLogOnReturn(Logger logger, String logInfo, long startTime){
		long current = System.currentTimeMillis();
		long dauer = current - startTime;
		logger.info("RETURN " + dauer + " ms - " + logInfo);
	}
}
