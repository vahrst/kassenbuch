package de.vahrst.application.global;
import java.awt.event.KeyEvent;
import java.lang.reflect.Field;
import java.util.*;

import javax.swing.KeyStroke;

import org.apache.log4j.Logger;

import de.vahrst.application.prozesse.GenericAction;

/**
 * Dies ist die Factory-Klasse f�r Texte (Labels) und
 * ImageIcons.
 * @author Thomas
 * @version 
 * 
 */
public class ResourceFactory {
	private static Logger logger = Logger.getLogger(ResourceFactory.class);
	
	private static ResourceBundle labelsBundle = ResourceBundle.getBundle("LabelsBundle");
	
	
	/**
	 * Liefert f�r einen vorgegebenen Text-Key den zugeh�rigen
	 * locale-spezifischen Wert. Liefert null, wenn der Key nicht
	 * definiert ist.
	 * @return String
	 * @param key String
	 */
	public static String getLabel(String key){
		if(key == null)
			return null;
			
		try{
			return labelsBundle.getString(key);	
		}catch(MissingResourceException e){
//			logger.warn("unbekannte Resource angefordert: " + key, e);			
			return null;
		}
	}


	public static void setActionFields(GenericAction action){
		String id = action.getId();
		action.putValue(GenericAction.NAME, getLabel(id + "_LABEL"));
		
		String accel = getLabel(id + "_ACCELERATOR");

//		if(id.equals("ACT_KONTEN")){
		if(accel != null){
			logger.debug("Accel-Key f�r " + id + ": " + accel);
			action.putValue(GenericAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke(accel));
		}

		String mnemonic = getLabel(id + "_MNEMONIC");
		if(mnemonic != null){
			Integer mnInt = getKeyEventForString(mnemonic);
			if(mnInt != null){
				action.putValue(GenericAction.MNEMONIC_KEY, mnInt);
			}
		}

	}
	
	
	private static Integer getKeyEventForString(String arg){
		try{
			Field f = KeyEvent.class.getField(arg);
			int value = f.getInt(KeyEvent.class);
			return	new Integer(value);
		}catch(Exception e){
			return null;
		}
	}
}
