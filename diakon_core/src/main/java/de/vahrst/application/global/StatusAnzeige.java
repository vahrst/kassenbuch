package de.vahrst.application.global;

/**
 * Interface f�r Objekte, die eine Statuszeile anzeigen 
 * k�nnen
 * @author Thomas
 * @version 
 */
public interface StatusAnzeige {
	public void setStatusMeldung(String meldung);
}
