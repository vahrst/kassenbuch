package de.vahrst.application.global;

import javax.swing.JFrame;

/**
 * Globaler Kontext f�r die Anwendung. Stellt z.B. Dienste bereit, um
 * das Hauptanwendungsfenster zu beschaffen oder eine Statusmeldung
 * auszugeben.
 * @author Thomas
 * @version 
 * @file ApplicationContext.java
 */
public class ApplicationContext {

	private static JFrame mainApplicationWindow = null;
	private static StatusAnzeige statusAnzeige = null;


	/**
	 * Constructor for ApplicationContext.
	 */
	public ApplicationContext() {
		super();
	}

	public static void setMainApplicationWindow(JFrame arg){
		mainApplicationWindow = arg;
	}
	
	public static JFrame getMainApplicationWindow(){
		return mainApplicationWindow;
	}
	
	public static void setStatusAnzeige(StatusAnzeige sa){
		statusAnzeige = sa;
	}
	
	public static void statusmeldung(String meldung){
		if(statusAnzeige != null)
			statusAnzeige.setStatusMeldung(meldung);
	}
	
	
}
