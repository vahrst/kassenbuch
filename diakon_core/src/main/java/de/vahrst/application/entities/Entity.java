package de.vahrst.application.entities;

import org.apache.log4j.Logger;
/**
 * Abstracte Oberklasse f�r Entities
 * @author Thomas
 * @version 
 * @file Entity.java
 */
public abstract class Entity {
	private static long oid = 0;
	
	protected Logger logger;
	
	
	/** Die Object-Id, ein Timestamp*/
	private long objectId;
	
	
	/**
	 * Constructor for Entity.
	 */
	public Entity() {
		super();
		logger = Logger.getLogger(this.getClass());
		objectId = getNextId();
	}



	/**
	 * liefert die Object-Id dieser Entity
	 */
	public long getObjectId(){
		return objectId;
	}

	/**
	 * setzt die Object-Id. Diese sollte nur von
	 * der Persistenz-Schicht (z.B. in der Home-Klasse)
	 * geschehen
	 */
	public void setObjectId(long id){
		objectId = id;
	}

	private static synchronized long getNextId(){
		// beim ersten mal initialisieren. 
		if(oid == 0){
			oid = System.currentTimeMillis();
		}
		return ++oid;
	}
			
}
