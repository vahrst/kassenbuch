#!/usr/bin/env bash

mvn install:install-file -Dfile=idb.jar -DgroupId=com.lutris.instantdb -DartifactId=idb -Dversion=4.0 -Dpackaging=jar
